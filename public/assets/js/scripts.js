$(function() {

    $(document)
        // disable enter button when active/focused in a form
        // excluding the form for login
        .on('keypress', '.bootbox-body form', function(e) { 
            return e.keyCode != 13;
        })
        // overlay a preloader when submitting the form
        // to prevent extra actions from users
        // while the request is processing
        .on('submit', '.with-preloader', function() {
            loader.show();
        });

    setTimeout(function() {
        $('ul.nav.nav-tabs li').each(function() {
            ulwidth += parseInt($(this).width());
        });
        $('ul.nav.nav-tabs')
            .wrap('<div class="nav-tabs-wrapper"></div>')
            .css({
                'width': ulwidth + 'px'
            });
    }, 500);
    var ulwidth = 10;    

    $('#push-menu-trigger').on('click', function(e) {
        e.preventDefault();
        if ($('body').hasClass('show-nav')) {
            $('.aside-backdrop').remove();
        } else {
            var html = '<div class="aside-backdrop"></div>';
            $(html).insertAfter('aside');
            $('.aside-backdrop').fadeIn(250);
            $('.aside-backdrop').on('click', function() {
                $('body').toggleClass('show-nav');
                $(this).fadeOut();
                setTimeout(function() {
                    $('.aside-backdrop').remove();
                }, 250);
            });
        }
        $('body').toggleClass('show-nav');
    });

    $('#quick-add-trigger').on('click', function() {
        if ($('body').hasClass('show-nav')) {
            $('body').removeClass('show-nav');
            $('.aside-backdrop').remove();
        }
    });

    var target = $('.panel-preloader').attr('data-target');    
    $(target).removeClass('hidden');
    setTimeout(function() {
        $('.panel-preloader').remove();
        $(target).fadeIn();
    }, 100);

    $('#generate-password').on('click', function(e) {
        e.preventDefault();
        var hash = randomString(32);
        $('#password').val(hash);
    });

    autosize($('.autosize'));
    // $(document).on('click', '.bootbox', function(e) { bootbox.hideAll(); });
    // $(document).on('click', '.modal-dialog', function(e) { e.stopPropagation(); });

    $(document).on('click', '.approve-request', function(e) {
        e.preventDefault();
        var url = $(this).attr('href');
        $.post(url, {approve: 1}).done(function(r) {
            r = $.parseJSON(r);
            window.location = r.redirect;
        });
    });

    $(document).on('click', '.disapprove-request', function(e) {
        e.preventDefault();
        var url = $(this).attr('href');
        $.post(url, {approve: 0}).done(function(r) {
            r = $.parseJSON(r);
            window.location = r.redirect;
        });
    });

    $(document).on('click', '.ship-request', function(e) {
        e.preventDefault();
        var url = $(this).attr('href');
        $.post(url, {approve: 2}).done(function(r) {
            r = $.parseJSON(r);
            window.location = r.redirect;
        });
    });

    setTimeout(function() {
        $('.alert-fadeable').css({
            'height' : '0'
        });
        setTimeout(function() {
            $('.alert-fadeable').remove();
        }, 500);
    }, 4000);
    
});
function randomString(length)
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < length; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function promptDelete(message, url)
{
    event.preventDefault();
    if (confirm(message)) {
        window.location = url;
    }
}

var delay = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
    };
})();