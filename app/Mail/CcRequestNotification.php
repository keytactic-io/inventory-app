<?php

namespace App\Mail;

use App\Models\Setting;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Request as RequestProduct;
use Illuminate\Contracts\Queue\ShouldQueue;

class CcRequestNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $requestProduct;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(RequestProduct $requestProduct)
    {
        $this->requestProduct = $requestProduct;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.requests.cc')
            ->subject('New request has been created!');
    }
}
