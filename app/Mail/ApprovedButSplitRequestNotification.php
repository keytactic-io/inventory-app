<?php

namespace App\Mail;

use Carbon\Carbon;
use App\Models\Location;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Request as ProductRequest;
use Illuminate\Contracts\Queue\ShouldQueue;

class ApprovedButSplitRequestNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $productRequest;
    public $fromLocations;
    public $asOfDate;

    public function __construct(ProductRequest $productRequest, $locations)
    {
        $this->productRequest = $productRequest;
        $requestedProduct     = $this->productRequest->requestedProducts()->first();
        $locations            = Location::whereIn('id', $locations)->pluck('name')->toArray();
        $this->fromLocations  = implode(', ', $locations);
        $this->asOfDate       = Carbon::now()->format('F j, Y h:ia');
    }

    public function build()
    {
        return $this->subject('Shipment request ' . $this->productRequest->uid() . ' is now pending shipment')
            ->view('emails.requests.approved-split');
    }
}
