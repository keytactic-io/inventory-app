<?php

namespace App\Mail;

use App\Models\Setting;
use App\Models\PurchaseOrder;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PurchaseOrderReceived extends Mailable
{
    use Queueable, SerializesModels;

    public $purchaseOrder;
    public $attachments = [];

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(PurchaseOrder $purchaseOrder, $filesToAttach)
    {
        $this->purchaseOrder = $purchaseOrder;
        
        $count = 0;
        foreach ($filesToAttach as $fileToAttach) {
            $this->attachments[$count]['file'] = public_path() . '/uploads/attachments/' . $fileToAttach;
            $this->attachments[$count]['options'] = [
                'as' => $fileToAttach, 
                'mime' => 'application/pdf'
            ];
            $count++;
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $filename = $this->purchaseOrder->uid() . '.pdf';
        return $this->view('emails.purchase-orders.received')
            ->subject('Purchase Order received');
            // ->attach($this->filesToAttach, [
            //     'mime' => 'application/pdf',
            // ]);
    }
}
