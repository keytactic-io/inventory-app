<?php

namespace App\Mail;

use App\Models\User;
use App\Models\Setting;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class WelcomeRequester extends Mailable
{
    use Queueable, SerializesModels;

    public $requester;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->requester = $user->requester;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.requesters.welcome')
            ->subject('Welcome to ' . app_setting('application_name') . 
                ' ' . $this->requester->user->first_name);
    }
}
