<?php

namespace App\Mail;

use Carbon\Carbon;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Request as ProductRequest;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewRequestAdminNotification extends Mailable
{

    use Queueable, SerializesModels;

    public $productRequest;
    public $admin;
    public $dateForHuman;

    public function __construct(ProductRequest $productRequest, User $admin)
    {
        $this->productRequest = $productRequest;
        $this->admin = $admin;

        $dateCreated = Carbon::parse($productRequest->created_at)->startOfDay();
        $inhandDate = Carbon::parse($productRequest->expected_inhand_date);

        $this->dateForHuman = $dateCreated->diffForHumans($inhandDate, true);
    }

    public function build()
    {
        return $this->subject($this->productRequest->theRequester->name() . 
            ' has just submitted a new request ' . $this->productRequest->uid())
            ->view('emails.requests.created-admin');
    }

}
