<?php

namespace App\Mail;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Location;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Request as ProductRequest;
use Illuminate\Contracts\Queue\ShouldQueue;

class ApprovedRequestStaffNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $productRequest;
    public $fromLocation;
    public $dateForHuman;
    public $asOfDate;
    public $staff;
    protected $filename;

    public function __construct(ProductRequest $productRequest, Location $location, User $staff)
    {
        $this->productRequest = $productRequest;
        $this->fromLocation   = $location;
        $this->asOfDate       = Carbon::now()->format('F j, Y h:ia');
        $this->staff          = $staff;
        
        $dateCreated          = Carbon::parse($productRequest->created_at)->startOfDay();
        $inhandDate           = Carbon::parse($productRequest->expected_inhand_date);        
        $this->dateForHuman   = $dateCreated->diffForHumans($inhandDate, true);
        $this->filename       = $this->productRequest->uid() . '.pdf';
    }

    public function build()
    {
        return $this->subject($this->productRequest->uid() . ' has been approved for shipment from ' . $this->fromLocation->name)
            ->attach(public_path() . '/temp/' . $this->filename, ['as' => $this->filename, 'mime' => 'application/pdf'])
            ->view('emails.requests.approved-staff');
    }
}
