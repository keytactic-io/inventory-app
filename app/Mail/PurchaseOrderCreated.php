<?php

namespace App\Mail;

use Carbon\Carbon;
use App\Models\Setting;
use App\Models\PurchaseOrder;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PurchaseOrderCreated extends Mailable
{
    use Queueable, SerializesModels;

    public $purchaseOrder;
    public $timestamp;

    private $includeFiles = true;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(PurchaseOrder $purchaseOrder, $includeFiles = true)
    {
        $this->purchaseOrder = $purchaseOrder;
        $this->timestamp = Carbon::parse($purchaseOrder->created_at)->timestamp;

        $this->includeFiles = $includeFiles;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->includeFiles) {
            $filename = $this->purchaseOrder->uid() . '.pdf';
            return $this->view('emails.purchase-orders.created')
                ->subject('Purchase Order from ' . app_setting('application_name'))
                ->attach(public_path() . '/temp/' . $filename, [
                    'as' => $filename,
                    'mime' => 'application/pdf',
                ]);
        } else {
            return $this->view('emails.purchase-orders.created')
                ->subject('Purchase Order from ' . app_setting('application_name'));
        }
    }
}
