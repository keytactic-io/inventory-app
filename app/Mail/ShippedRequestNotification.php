<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Request as ProductRequest;
use Illuminate\Contracts\Queue\ShouldQueue;

class ShippedRequestNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $productRequest;
    public $fromLocation;
    protected $filename;

    public function __construct(ProductRequest $productRequest)
    {
        $this->productRequest = $productRequest;
        $requestedProduct     = $this->productRequest->requestedProducts()->first();
        $this->fromLocation   = $requestedProduct->location->name;
        $this->filename       = $this->productRequest->uid() . '.pdf';
    }

    public function build()
    {
        return $this->subject('Shipment request ' . $this->productRequest->uid() . ' has been shipped!')
            ->attach(public_path() . '/temp/' . $this->filename, ['as' => $this->filename, 'mime' => 'application/pdf'])
            ->view('emails.requests.shipped');
    }

}
