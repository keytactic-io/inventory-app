<?php

namespace App\Mail;

use App\Models\Setting;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Request as RequestProduct;
use Illuminate\Contracts\Queue\ShouldQueue;

class RequestNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $requestProduct;
    public $type;
    public $locationStaff;
    public $location;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(RequestProduct $requestProduct, $type, $locationStaff = null, $location = null)
    {
        $this->requestProduct = $requestProduct;
        $this->type = $type;
        $this->locationStaff = $locationStaff;
        $this->location = $location;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.requests.' . $this->type)
            ->subject('Request Notification from ' . app_setting('application_name'));
    }
}
