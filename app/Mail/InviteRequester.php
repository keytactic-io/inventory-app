<?php

namespace App\Mail;

use App\Models\Setting;
use App\Models\Requester;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InviteRequester extends Mailable
{
    use Queueable, SerializesModels;

    public $requester;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Requester $requester)
    {
        $this->requester = $requester;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.requesters.invite')
            ->subject('Invite from ' . app_setting('application_name'));
    }
}
