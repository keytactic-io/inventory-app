<?php

namespace App\Mail;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Request as ProductRequest;
use Illuminate\Contracts\Queue\ShouldQueue;

class ApprovedRequestNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $productRequest;
    public $fromLocation;
    public $asOfDate;

    public function __construct(ProductRequest $productRequest)
    {
        $this->productRequest = $productRequest;

        $rp = $this->productRequest->requestedProducts()->first();
        $this->fromLocation = $rp->location->name;

        $this->asOfDate = Carbon::now()->format('F j, Y h:ia');
    }

    public function build()
    {
        return $this->subject('Shipment request ' . $this->productRequest->uid() . ' is now pending shipment')
            ->view('emails.requests.approved');
    }

}
