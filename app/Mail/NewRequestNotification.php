<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Request as ProductRequest;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewRequestNotification extends Mailable
{

    use Queueable, SerializesModels;

    public $productRequest;

    public function __construct(ProductRequest $productRequest)
    {
        $this->productRequest = $productRequest;
    }

    public function build()
    {
        return $this->subject('Shipment Request Confirmation ' . $this->productRequest->uid())
            ->view('emails.requests.created');
    }

}
