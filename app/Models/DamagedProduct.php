<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DamagedProduct extends Model
{
    
    protected $fillable = [
    	'product_id',
    	'purchase_order_id',
    	'quantity'
    ];

}
