<?php

namespace App\Models;

use App\Models\BaseModel;

class Category extends BaseModel
{
    
    protected $fillable = [
    	'name',
    	'description',
    	'parent_category'
    ];

    public function childCategories()
    {
    	return $this->hasMany(Category::class, 'parent_category', 'id');
    }

    public function products()
    {
    	return $this->hasMany(ProductCategory::class);
    }

    public function productsCount()
    {
    	return $this->products()->count();
    }

    public function scopeParents($query)
    {
        return $query->where('parent_category', 0);
    }

}
