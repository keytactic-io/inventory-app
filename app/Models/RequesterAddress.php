<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RequesterAddress extends Model
{
    
    protected $fillable = [
        'requester_id',
        'street_1',
        'street_2',
        'city',
        'state',
        'country',
        'postcode',
        'default'
    ];

    public function getAddress()
    {
        return $this->street_1 . ' ' . $this->street_2 . ', ' .
            $this->city . ', ' . $this->state . ', ' . $this->country . ' ' .
            $this->postcode;
    }

    public function requester()
    {
        return $this->belongsTo(Requester::class);
    }

}
