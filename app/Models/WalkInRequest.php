<?php

namespace App\Models;

use App\Models\BaseModel;

class WalkInRequest extends BaseModel
{

    protected $fillable = [
        'location_id',
        'requester_name',
        'notes',
        'is_released'
    ];

    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'walk_in_request_products', 'walk_in_request_id', 'product_id');
    }

    public function requestedProducts()
    {
        return $this->hasMany(WalkInRequestProduct::class, 'walk_in_request_id', 'id');
    }

    public function getUidAttribute()
    {
        return 'WRQ-' . getYear($this->created_at) . '-' .
            sprintf('%05d', $this->id);
    }

    public function scopePending($query, $get = false)
    {
        return $get ? $query->where('is_released', 0)->get() : 
            $query->where('is_released', 0);
    }

    public function scopeReleased($query, $get = false)
    {
        return $get ? $query->where('is_released', 1)->get() : 
            $query->where('is_released', 1);
    }

}
