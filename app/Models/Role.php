<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    
    protected $fillable = [
    	'name', 
    	'description'
    ];

    public function user()
    {
    	return $this->hasMany(User::class);
    }

    public function permissions()
    {
    	return $this->hasMany(RolePermission::class);
    }

    public function allPermissions()
    {
        return $this->belongsToMany(Permission::class, 'role_permissions', 
            'role_id', 'permission_id');
    }

}
