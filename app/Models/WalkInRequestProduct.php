<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WalkInRequestProduct extends Model
{

	protected $fillable = [
		'walk_in_request_id',
		'product_id',
		'quantity',
		'allowed_quantity'
	];

	public function product()
	{
		return $this->belongsTo(Product::class)->withTrashed();
	}

}
