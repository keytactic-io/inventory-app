<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrderFile extends BaseModel
{
    
    protected $fillable = [
    	'purchase_order_id',
    	'filename'
    ];

    public function getUrl()
    {
    	return asset('uploads/attachments/' . $this->filename);
    }

}
