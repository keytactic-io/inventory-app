<?php

namespace App\Models;

use App\Models\BaseModel;

class ApiKey extends BaseModel
{

	protected $fillable = [
		'api_key'
	];

}
