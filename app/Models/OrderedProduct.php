<?php

namespace App\Models;

use App\Models\BaseModel;

class OrderedProduct extends BaseModel
{
    
    protected $fillable = [
        'purchase_order_id',
        'product_id',
        'quantity',
        'price'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class)->withTrashed();
    }

    public function productsLocations()
    {
        return $this->hasMany(OrderedProductsLocation::class);
    }

}
