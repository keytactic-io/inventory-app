<?php

namespace App\Models;

use Carbon\Carbon;
use App\Models\Category;
use App\Models\BaseModel;

class Vendor extends BaseModel
{
    
    protected $fillable = [
        'name',
        'street_1',
        'street_2',
        'city',
        'state',
        'country',
        'postcode',
        'email',
        'mobile_number',
        'landline_number',
        'fax_number',
        'contact_person',
        'photo'
    ];

    public function purchaseOrders()
    {
    	return $this->hasMany(PurchaseOrder::class);
    }

    public function getAddress()
    {
        return $this->street_1 . ' ' . $this->street_2 . ', ' .
            $this->city . ', ' . $this->state . ', ' . $this->country . ' ' .
            $this->postcode;
    }

    public function getPhoto()
    {
        return asset('uploads/vendors/' . $this->photo);
    }

    public function getReceivedPos($year)
    {
        $year = $year == null ? Carbon::now()->year : $year;

        $startDate = Carbon::parse('first day of January ' . $year);
        $endDate = Carbon::parse('last day of December ' . $year)->endOfDay();

        return $this->purchaseOrders()
            ->where('received', 1)
            ->whereBetween('received_at', [$startDate, $endDate])
            ->get();
    }

    public function getTotalSpent($year = null)
    {
        $receivedPos = $this->getReceivedPos($year);

        $totalSpent = 0;

        foreach ($receivedPos as $receivedPo) {
            foreach ($receivedPo->orderedProducts as $orderedProduct) {
                foreach ($orderedProduct->productsLocations as $productsLocation) {
                    $totalSpent += $productsLocation->received_quantity * $orderedProduct->price;
                }
            }
        }

        return $totalSpent;
    }

    public function getProductQuantity($year = null)
    {
        $receivedPos = $receivedPos = $this->getReceivedPos($year);

        $totalQuantity = 0;

        foreach ($receivedPos as $receivedPo) {
            foreach ($receivedPo->orderedProducts as $orderedProduct) {
                foreach ($orderedProduct->productsLocations as $productsLocation) {
                    $totalQuantity += $productsLocation->received_quantity;
                }
            }
        }

        return $totalQuantity;
    }

    public function getProductCategories($year = null)
    {
        $receivedPos = $receivedPos = $this->getReceivedPos($year);

        $categoriesId = [];

        foreach ($receivedPos as $receivedPo) {
            foreach ($receivedPo->orderedProducts as $orderedProduct) {
                foreach ($orderedProduct->product->categories as $category) {
                    $categoriesId[] = $category->category_id;
                }
            }
        }

        $uniqueCatIds = array_unique($categoriesId);

        return Category::whereIn('id', $uniqueCatIds)->get();
    }

}
