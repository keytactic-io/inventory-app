<?php

namespace App\Models;

use App\Models\BaseModel;

class Market extends BaseModel
{
    
    protected $fillable = [
        'name'
    ];

    public function requesters()
    {
        return $this->hasMany(Requester::class);
    }

    public function requestersCount()
    {
        return $this->requesters()->count();
    }

}
