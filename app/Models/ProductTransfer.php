<?php

namespace App\Models;

use App\Models\BaseModel;

class ProductTransfer extends BaseModel
{
    
    protected $fillable = [
    	'from',
    	'to',
    	'product_id',
    	'quantity'
    ];

    public function product()
    {
    	return $this->belongsTo(Product::class)->withTrashed();
    }

    public function locationFrom()
    {
    	return $this->belongsTo(Location::class, 'from', 'id');
    }

    public function locationTo()
    {
    	return $this->belongsTo(Location::class, 'to', 'id');
    }

}
