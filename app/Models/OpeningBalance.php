<?php

namespace App\Models;

use App\Models\BaseModel;

class OpeningBalance extends BaseModel
{
    
    protected $fillable = [
        'product_id',
        'quantity',
        'location_id',
        'created_by'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class)->withTrashed();
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function location()
    {
        return $this->belongsTo(Location::class);
    }

}
