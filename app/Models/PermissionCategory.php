<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PermissionCategory extends Model
{
    
    protected $fillable = [
    	'key',
    	'name'
    ];

    public function permissions()
    {
    	return $this->hasMany(Permission::class);
    }

}
