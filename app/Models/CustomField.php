<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomField extends Model
{
    
    protected $fillable = [
        'model',
        'label',
        'slug',
        'type'
    ];

    public function values()
    {
        return $this->hasMany(CustomFieldValue::class);
    }

    public function scopeProducts($query)
    {
        return $query->where('model', 'pr');
    }

    public function scopeRequests($query)
    {
        return $query->where('model', 'rq');
    }

    public function scopePurchaseOrders($query)
    {
        return $query->where('model', 'po');
    }

}
