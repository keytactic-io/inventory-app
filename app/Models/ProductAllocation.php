<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductAllocation extends Model
{
    
    protected $fillable = [
    	'requester_id',
    	'product_id',
    	'allocation'
    ];

    public function product()
    {
    	return $this->belongsTo(Product::class)->withTrashed();
    }

}
