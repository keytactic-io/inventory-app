<?php

namespace App\Models;

use Carbon\Carbon;
use App\Models\BaseModel;

class Requester extends BaseModel
{
    
    protected $fillable = [
        'user_id',
        'market_id',
        'contact_number',
        'registered',
        'invite_key'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function name()
    {
        return $this->user->name();
    }

    public function dp()
    {
        return $this->user->dp();
    }

    public function addresses()
    {
        return $this->hasMany(RequesterAddress::class);
    }

    public function market()
    {
        return $this->belongsTo(Market::class);
    }

    public function productAllocations()
    {
        return $this->hasMany(ProductAllocation::class);
    }

    public function defaultAddress()
    {
        $defaultAddress = $this->addresses()->where('default', 1)->first();
        if ($defaultAddress) {
            return $defaultAddress->getAddress();            
        }

        return '';
    }

    public function getRequests($year)
    {
        $year = $year == null ? Carbon::now()->year : $year;

        $startDate = Carbon::parse('first day of January ' . $year);
        $endDate = Carbon::parse('last day of December ' . $year)->endOfDay();

        return $this->user->requests()
            ->where('status', 2)
            ->where('status', '<>', 4)
            ->whereBetween('processed_at', [$startDate, $endDate])
            ->get();
    }

    public function getRequestAmount($year = null)
    {
        $year = $year == null ? date('Y') : $year;
        $requests = $this->getRequests($year);

        $amount = 0;
        if (count($requests) > 0) {
            foreach ($requests as $request) {
                foreach ($request->requestedProducts as $rq) {
                    $amount += $rq->price * $rq->allowed_quantity;
                }
            }
        }

        return $amount;
    }
    
    public function getRequestedProductsCount($year = null)
    {
        $year = $year == null ? date('Y') : $year;
        $requests = $this->getRequests($year);

        $count = 0;
        if (count($requests) > 0) {
            foreach ($requests as $request) {
                foreach ($request->requestedProducts as $rq) {
                    $count += $rq->allowed_quantity;
                }
            }
        }
        return $count;
    }

    public function getRequestedProductsCategories($year = null)
    {
        $year = $year == null ? date('Y') : $year;
        $requests = $this->getRequests($year);

        $categoriesId = [];
        if (count($requests) > 0) {
            foreach ($requests as $request) {
                foreach ($request->requestedProducts as $rq) {
                    if ($rq->product) {
                        foreach ($rq->product->categories as $category) {
                            $categoriesId[] = $category->category_id;
                        }
                    }                    
                }
            }
        }

        $uniqueCatIds = array_unique($categoriesId);

        return Category::query()
            ->whereIn('id', $uniqueCatIds)
            ->get();
    }

}
