<?php

namespace App\Models;

use App\Models\BaseModel;

class PurchaseOrder extends BaseModel
{
    
    protected $fillable = [
    	'vendor_id',        
    	'processed_by',
        'received',
        'received_by',
        'received_at',
        'ship_to_location',
        'delivery_date',
        'shipping_method',
        'shipping_terms',
        'notes',
        'with_notification'
    ];

    public function vendor()
    {
    	return $this->belongsTo(Vendor::class);
    }

    public function processedBy()
    {
    	return $this->belongsTo(User::class, 'processed_by', 'id');
    }

    public function orderedProducts()
    {
    	return $this->hasMany(OrderedProduct::class);
    }

    public function productCount()
    {
    	return $this->orderedProducts()->count();
    }

    public function shipTo()
    {
        return $this->belongsTo(Location::class, 'ship_to_location', 'id');
    }

    public function attachments()
    {
        return $this->hasMany(PurchaseOrderFile::class);
    }

    public function uid()
    {
        return 'PO-' . getYear($this->created_at) . '-' . 
            sprintf('%05d', $this->id);
    }

    public function customFields()
    {
        return CustomFieldValue::query()
            ->join('custom_fields', 'custom_field_values.custom_field_id', '=', 'custom_fields.id')
            ->where('custom_fields.model', 'po')
            ->where('custom_field_values.reference_id', $this->id)
            ->select('custom_field_values.*')
            ->get();
    }

    public function customFieldValue($customFieldId)
    {
        $customFieldsValue = CustomFieldValue::query()
            ->join('custom_fields', 'custom_field_values.custom_field_id', '=', 'custom_fields.id')
            ->where('custom_fields.model', 'po')
            ->where('custom_fields.id', $customFieldId)
            ->where('custom_field_values.reference_id', $this->id)
            ->select('custom_field_values.*')
            ->first();

        if ($customFieldsValue) {
            return $customFieldsValue->value;
        }
        return null;
    }

}
