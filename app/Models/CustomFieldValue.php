<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomFieldValue extends Model
{
    
    protected $fillable = [
    	'custom_field_id',
    	'reference_id',
    	'value'
    ];

    public function cf()
    {
    	return $this->belongsTo(CustomField::class, 'custom_field_id', 'id');
    }

}
