<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductPriceLog extends Model
{
    
    protected $fillable = [
        'product_id',
        'price'
    ];

}
