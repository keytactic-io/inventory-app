<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductRemainingStock extends Model
{

    protected  $table = 'archive_product_remaining_stocks';

    protected $fillable = [
        'product_id', 'opening_balances', 'orders', 'requests', 'walkin_requests',
        'status'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

}
