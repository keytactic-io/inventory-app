<?php

namespace App\Models;

use App\Models\BaseModel;

class RequestedProduct extends BaseModel
{
    
    protected $fillable = [
        'request_id',
        'product_id',
        'quantity',
        'allowed_quantity',
        'price',
        'get_from'
    ];

    public function request()
    {
        return $this->belongsTo(Request::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class)->withTrashed();
    }

    public function location()
    {
        return $this->belongsTo(Location::class, 'get_from', 'id');
    }

}
