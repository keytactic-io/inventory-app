<?php

namespace App\Models;

use Session;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    protected $fillable = [
        'first_name',
        'last_name',
        'role_id',
        'display_photo',
        'email',
        'password',
        'is_active',
        'is_requester'
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $dates = [
        'deleted_at'
    ];

    /**
     * -------------------------------------------------------------------------
     * Relationship Methods
     * -------------------------------------------------------------------------
     */

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function requests()
    {
        return $this->hasMany(Request::class, 'requester', 'id');
    }

    public function purchaseOrders()
    {
        return $this->hasMany(PurchaseOrder::class, 'processed_by', 'id');
    }

    public function requester()
    {
        return $this->hasOne(Requester::class);
    }

    public function locations()
    {
        return $this->hasMany(LocationStaff::class);
    }

    /**
     * -------------------------------------------------------------------------
     * Scope Methods
     * -------------------------------------------------------------------------
     */

    public function scopeNotRequester($query)
    {
        return $query->where('role_id', '<>', 0)->where('is_requester', 0);
    }

    public function scopeLocationStaff($query)
    {
        return $query->where('is_requester', 0)->where('role_id', 4);
    }

    /**
     * -------------------------------------------------------------------------
     * Unsorted Methods
     * -------------------------------------------------------------------------
     */

    public function managedLocations()
    {
        $locations = $this->locations;

        if (count($locations) > 0) {
            return $locations;
        }

        return null;
    }

    public function managesLocation($locationId)
    {
        if ($this->is_requester) {
            return false;
        }

        if ($locationId != 0) {
            $managedLocations = $this->managedLocations();
            if ($managedLocations != null) {
                foreach ($managedLocations as $managedLocation) {
                    if ($locationId == $managedLocation->location_id) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public function name()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function dp()
    {
        $dp = $this->display_photo;
        if ($dp != 'no-dp.png') {
            return asset('uploads/photos') . '/' . $dp;
        }
        return asset('assets/images') . '/' . $dp;
    }

    public function allowedTo($key)
    {
        $permissions = [];
        if (Session::has('currentUserPermissions')) {
            $permissions = Session::get('currentUserPermissions');
        }

        return in_array($key, $permissions) ? true : false;
    }

    public function requestsCount()
    {
        return $this->requests->count();
    }
}
