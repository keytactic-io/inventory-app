<?php

namespace App\Models;

use App\Models\BaseModel;

class Request extends BaseModel
{

    protected $fillable = [
        'requester', 'market_id', 'expected_inhand_date', 'status', 'attention', 
        'contact_number', 'company', 'street_1', 'street_2', 'city', 'state', 
        'country', 'postcode', 'justification', 'note', 'approved_by', 
        'approved_at', 'parent_id', 'manual_input', 'courier', 'rate', 
        'tracking_number', 'label_url', 'cc_emails', 'with_notification', 
        'service_level_name', 'duration_terms'
    ];

    public function theRequester()
    {
        return $this->belongsTo(User::class, 'requester', 'id');
    }

    public function market()
    {
        return $this->belongsTo(Market::class);
    }

    public function requestedProducts()
    {
        return $this->hasMany(RequestedProduct::class);
    }

    public function productsCount()
    {
        return $this->requestedProducts()->count();
    }

    public function processedBy()
    {
        return $this->belongsTo(User::class, 'processed_by', 'id');
    }

    public function parentRq()
    {
        return $this->belongsTo(Request::class, 'parent_id', 'id');
    }

    public function childRq()
    {
        return $this->hasMany(Request::class, 'parent_id', 'id');
    }

    public function uid()
    {
        return 'RQ-' . getYear($this->created_at) . '-' .
            sprintf('%05d', $this->id);
    }

    public function getAddress()
    {
        return $this->street_1 . ' ' . $this->street_2 . ', ' .
            $this->city . ', ' . $this->state . ', ' . $this->country . ' ' .
            $this->postcode;
    }

    public function getAddressStreet()
    {
        return $this->street_1 . ', ' . $this->street_2;
    }

    public function getAddressCityStateZip()
    {
        return $this->city . ', ' . $this->state . ' ' . $this->postcode;
    }

    public function getAddressCountry()
    {
        return $this->country;
    }


    public function customFields()
    {
        return CustomFieldValue::query()
            ->join('custom_fields', 'custom_field_values.custom_field_id', '=', 'custom_fields.id')
            ->where('custom_fields.model', 'rq')
            ->where('custom_field_values.reference_id', $this->id)
            ->select('custom_field_values.*')
            ->get();
    }

    public function customFieldValue($customFieldId)
    {
        $customFieldsValue = CustomFieldValue::query()
            ->join('custom_fields', 'custom_field_values.custom_field_id', '=', 'custom_fields.id')
            ->where('custom_fields.model', 'rq')
            ->where('custom_fields.id', $customFieldId)
            ->where('custom_field_values.reference_id', $this->id)
            ->select('custom_field_values.*')
            ->first();

        if ($customFieldsValue) {
            return $customFieldsValue->value;
        }
        return null;
    }

    public function locationToGet()
    {
        if (
            ($this->status == 1 OR $this->status == 2) AND
            count($this->requestedProducts) > 0
        ) {
            $rQ = $this->requestedProducts()->first();
            return $rQ->get_from;
        }

        return 0;
    }

    public function scopePendingApproval($query, $get = false)
    {
        return $get ? $query->whereStatus(0)->get() : $query->whereStatus(0);
    }

    public function scopePendingShipment($query, $get = false)
    {
        return $get ? $query->whereStatus(1)->get() : $query->whereStatus(1);
    }

    public function scopeShipped($query, $get = false)
    {
        return $get ? $query->whereStatus(2)->get() : $query->whereStatus(2);
    }

    public function scopeRejected($query, $get = false)
    {
        return $get ? $query->whereStatus(3)->get() : $query->whereStatus(3);
    }

    public function scopeSplit($query, $get = false)
    {
        return $get ? $query->whereStatus(4)->get() : $query->whereStatus(4);
    }

}
