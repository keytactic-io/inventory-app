<?php

namespace App\Models;

use DB;
use App\Models\BaseModel;

class Location extends BaseModel
{
    
    protected $fillable = [
        'name',
        'staff_incharge',
        'contact_number',
        'street_1',
        'street_2',
        'city',
        'state',
        'country',
        'postcode',
    ];

    public function getAddress()
    {
        return $this->street_1 . ' ' . $this->street_2 . ', ' .
            $this->city . ', ' . $this->state . ', ' . $this->country . ' ' .
            $this->postcode;
    }

    public function staffIncharge()
    {
        return $this->belongsTo(User::class, 'staff_incharge', 'id');
    }

    public function orderedProducts()
    {
        return $this->hasMany(OrderedProduct::class);
    }

    public function requestedProducts()
    {
        return $this->hasMany(RequestedProduct::class, 'get_from', 'id');
    }

    public function walkinRequests()
    {
        return $this->hasMany(WalkInRequest::class);
    }

    public function productsLocations()
    {
        return $this->hasMany(OrderedProductsLocation::class);
    }

    public function openedBalance()
    {
        return $this->hasMany(OpeningBalance::class);
    }

    public function transferedFrom()
    {
        return $this->hasMany(ProductTransfer::class, 'from', 'id');
    }

    public function transferedTo()
    {
        return $this->hasMany(ProductTransfer::class, 'to', 'id');
    }

    public function staff()
    {
        return $this->hasMany(LocationStaff::class);
    }

    public function productsCount()
    {
        $ps = collect(DB::select(
            DB::raw("
                SELECT opening_balances.product_id as id FROM opening_balances
                    JOIN products ON opening_balances.product_id = products.id
                    WHERE location_id = '$this->id'
                    AND products.deleted_at IS NULL
                UNION
                SELECT ordered_products.product_id as id FROM ordered_products
                    JOIN products ON ordered_products.product_id = products.id
                    RIGHT JOIN ordered_products_locations ON ordered_products_locations.ordered_product_id = ordered_products.id
                    WHERE ordered_products_locations.location_id = '$this->id'
                    AND products.deleted_at IS NULL
                UNION
                SELECT product_transfers.product_id as id FROM product_transfers
                    JOIN products ON product_transfers.product_id = products.id
                    WHERE product_transfers.to = '$this->id'
                    AND products.deleted_at IS NULL
                GROUP BY product_id
            ")
        ));
        return count($ps);
    }

    public function stocksRemaining($productId)
    {
        $stocksOrdered = $this->productsLocations()
            ->join('ordered_products', 'ordered_products_locations.ordered_product_id', '=', 'ordered_products.id')
            ->where('ordered_products.product_id', $productId)
            ->select('ordered_products_locations.*')
            ->sum('received_quantity');

        $toHere = $this->transferedTo()
            ->where('product_id', $productId)
            ->sum('quantity');

        $openingBalance = $this->openedBalance()
            ->where('product_id', $productId)
            ->sum('quantity');

        $stocksRequested = $this->requestedProducts()
            ->where('product_id', $productId)
            ->sum('allowed_quantity');

        $fromHere = $this->transferedFrom()
            ->where('product_id', $productId)
            ->sum('quantity');

        $wrqTotal = 0;
        foreach ($this->walkinRequests as $wrq) {
            $wrqTotal += $wrq->requestedProducts()
                ->where('product_id', $productId)
                ->whereNotNull('allowed_quantity')
                ->sum('allowed_quantity');
        }

        return ($stocksOrdered + $openingBalance + $toHere) - ($stocksRequested + $fromHere + $wrqTotal);
    }

}
