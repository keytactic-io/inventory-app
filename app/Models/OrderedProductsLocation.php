<?php

namespace App\Models;

use App\Models\BaseModel;

class OrderedProductsLocation extends BaseModel
{
    
    protected $fillable = [
        'ordered_product_id',
        'received_quantity',
        'location_id'
    ];

    public function location()
    {
    	return $this->belongsTo(Location::class);
    }

}
