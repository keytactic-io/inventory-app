<?php

namespace App\Models;

use DB;
use App\Models\Location;
use App\Models\BaseModel;
use App\Models\CustomFieldValue;

class Product extends BaseModel
{
    
    protected $fillable = [
        'photo',
        'name',
        'description',
        'reorder_point',
        'reorder_quantity',
        'default_vendor'
    ];

    public function prices()
    {
        return $this->hasMany(ProductPriceLog::class);
    }

    public function openingBalances()
    {
        return $this->hasMany(OpeningBalance::class);
    }

    public function orders()
    {
        return $this->hasMany(OrderedProduct::class);
    }

    public function walkinRequestProducts()
    {
        return $this->hasMany(WalkInRequestProduct::class);
    }

    public function categories()
    {
        return $this->hasMany(ProductCategory::class);
    }

    public function requests()
    {
        return $this->hasMany(RequestedProduct::class);
    }

    public function defaultVendor()
    {
        return $this->belongsTo(Vendor::class, 'default_vendor', 'id');
    }

    public function archiveRemainingStock()
    {
        return $this->hasOne(ProductRemainingStock::class);
    }

    public function photo()
    {
        $photo = $this->photo;
        if ($photo === 'no-photo.png') {
            return asset('assets/images/' . $photo);
        }
        else {
            return asset('uploads/products/' . $photo);
        }
    }

    public function latestPrice()
    {
        $latestPrice = $this->prices()->orderBy('id', 'desc')->first();
        if ($latestPrice) {
            return $latestPrice->price;
        }
        return 0;
    }

    public function transferedProducts()
    {
        return $this->hasMany(ProductTransfer::class);
    }

    public function stocksRemaining()
    {
        $archiveRemainingStock = ProductRemainingStock::query()
            ->where('product_id', $this->id)
            ->first();

        if ($archiveRemainingStock) {
            return ($archiveRemainingStock->opening_balances + $archiveRemainingStock->orders) -
                ($archiveRemainingStock->requests + $archiveRemainingStock->walkin_requests);
        }

        return 0;
    }

    public function stocksInLocation($locationId)
    {
        $stocksIn = $this->orders()
            ->leftJoin('ordered_products_locations', 'ordered_products_locations.ordered_product_id', '=', 'ordered_products.id')
            ->where('ordered_products_locations.location_id', $locationId)
            ->sum('ordered_products_locations.received_quantity');

        $toHere = $this->transferedProducts()
            ->where('to', $locationId)
            ->sum('quantity');

        $fromHere = $this->transferedProducts()
            ->where('from', $locationId)
            ->sum('quantity');

        $openingBalance = $this->openingBalances()
            ->where('location_id', $locationId)
            ->sum('quantity');

        $stocksOut = $this->requests()
            ->join('requests', 'requested_products.request_id', '=', 'requests.id')
            ->where('requested_products.get_from', $locationId)
            ->whereRaw("(requests.status > 0 AND requests.status < 3)")
            ->sum('requested_products.allowed_quantity');

        $stocksOut += $this->walkinRequestProducts()
            ->sum('allowed_quantity');

        return ($stocksIn + $openingBalance + $toHere) - ($stocksOut + $fromHere);
    }

    public function isOutOfStock()
    {
        return $this->stocksRemaining() == 0 ? true : false;
    }

    public function isLowStock()
    {
        return ($this->stocksRemaining() > 0 AND $this->stocksRemaining() < $this->reorder_point) ? true : false;
    }

    public function productAllocations()
    {
        return $this->hasMany(ProductAllocation::class);
    }
    
    public function hasAllocation($requesterId)
    {
        $productAllocation = $this->productAllocations()
            ->where('requester_id', $requesterId)
            ->first();
        if ($productAllocation) {
            return $productAllocation->allocation;
        }

        return 0;
    }

    public function customFields()
    {
        return CustomFieldValue::query()
            ->join('custom_fields', 'custom_field_values.custom_field_id', '=', 'custom_fields.id')
            ->where('custom_fields.model', 'pr')
            ->where('custom_field_values.reference_id', $this->id)
            ->select('custom_field_values.*')
            ->get();
    }

    public function customFieldValue($customFieldId)
    {
        $customFieldsValue = CustomFieldValue::query()
            ->join('custom_fields', 'custom_field_values.custom_field_id', '=', 'custom_fields.id')
            ->where('custom_fields.model', 'pr')
            ->where('custom_fields.id', $customFieldId)
            ->where('custom_field_values.reference_id', $this->id)
            ->select('custom_field_values.*')
            ->first();

        if ($customFieldsValue) {
            return $customFieldsValue->value;
        }
        return null;
    }

    public static function mostRequestedProducts()
    {
        return Product::query()
            ->selectRaw("
                products.*, 
                count(*) as request_count, 
                requested_products.allowed_quantity as rq_aq, 
                walk_in_request_products.allowed_quantity as wrq_aq
            ")
            ->leftJoin('requested_products', 'products.id', '=', 'requested_products.product_id')
            ->leftJoin('walk_in_request_products', 'products.id', '=', 'walk_in_request_products.product_id')
            ->whereNotNull('requested_products.allowed_quantity')
            ->orWhereNotNull('walk_in_request_products.allowed_quantity')
            ->groupBy('products.id')
            ->orderBy('request_count', 'desc')
            ->take(10)
            ->get();
    }

    public static function lowStockProducts()
    {
        return Product::query()
            ->selectRaw("
                products.*, 
                (archive_product_remaining_stocks.opening_balances + archive_product_remaining_stocks.orders) - 
                (archive_product_remaining_stocks.requests + archive_product_remaining_stocks.walkin_requests) as remaining_stocks
            ")
            ->leftJoin('archive_product_remaining_stocks', 'products.id', '=', 'archive_product_remaining_stocks.product_id')
            ->whereBetween('archive_product_remaining_stocks.status', [2, 3])
            ->orderBy('remaining_stocks')
            ->take(10)
            ->get();
    }

    public static function inLocation($locationId)
    {
        $ps = collect(DB::select(
            DB::raw("
                SELECT opening_balances.product_id as id FROM opening_balances 
                    WHERE opening_balances.location_id = '$locationId'
                UNION
                SELECT ordered_products.product_id as id FROM ordered_products 
                    RIGHT JOIN ordered_products_locations 
                    ON ordered_products_locations.ordered_product_id = ordered_products.id
                    WHERE ordered_products_locations.location_id = '$locationId'
                UNION
                SELECT product_transfers.product_id as id FROM product_transfers
                    WHERE product_transfers.to = '$locationId'
                GROUP BY product_id
            ")
        ));

        $ids = [];
        foreach ($ps as $product) {
            array_push($ids, $product->id);
        }

        return self::whereIn('id', $ids)
            ->orderBy('name')
            ->get();;
    }

    // in-stock products
    public function scopeInStock($query)
    {
        return $query->leftJoin('archive_product_remaining_stocks', 'products.id', 'archive_product_remaining_stocks.product_id')
            ->where('status', 1)
            ->orWhere('status', 2);
    }

    // low stock products
    public function scopeLowStock($query)
    {
        return $query->leftJoin('archive_product_remaining_stocks', 'products.id', 'archive_product_remaining_stocks.product_id')
            ->where('status', 2);
    }

    // out of stock products
    public function scopeOutOfStock($query)
    {
        return $query->leftJoin('archive_product_remaining_stocks', 'products.id', 'archive_product_remaining_stocks.product_id')
            ->where('status', 3);
    }

}
