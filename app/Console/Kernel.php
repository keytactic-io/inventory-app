<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    
    protected $commands = [
        \App\Console\Commands\ArchiveProducts::class,
    ];

    protected function schedule(Schedule $schedule)
    {
        // archive products every hour
        $schedule->command('archive:products')
            ->hourly();
    }

    protected function commands()
    {
        require base_path('routes/console.php');
    }

}
