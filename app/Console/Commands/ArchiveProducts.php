<?php

namespace App\Console\Commands;

use App\Models\Product;
use Illuminate\Console\Command;
use App\Jobs\ArchiveRemainingStocks;

class ArchiveProducts extends Command
{

    protected $signature = 'archive:products';
    protected $description = 'Archive products';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $products = Product::all();
        $this->info('Archiving ' . count($products) . ' products...');
        dispatch(new ArchiveRemainingStocks($products));

        $this->info('Done archiving!');
    }

}
