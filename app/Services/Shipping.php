<?php

namespace App\Services;

use Shippo;
use Shippo_Shipment;
use Shippo_Transaction;

class Shipping
{

    public $addressFrom;    
    public $addressTo;
    public $parcel;
    
    private static $instance = null;

    public function __construct($addressFrom = null, $addressTo = null, $parcel = null)
    {
        Shippo::setApiKey(env('SHIPPO_PRIVATE'));
        $this->addressFrom = $addressFrom;
        $this->addressTo = $addressTo;
        $this->parcel = $parcel;
    }

    public static function getRates($addressFrom, $addressTo, $parcel)
    {
        if (self::$instance == null) {
            self::$instance = new Shipping($addressFrom, $addressTo, $parcel);
        }

        $rates = Shippo_Shipment::create([
            // 'object_purpose'=> 'PURCHASE', // commented-out to match with the 2017 shippo docs
            'address_from'=> self::$instance->addressFrom,
            'address_to'=> self::$instance->addressTo,
            'parcels'=> self::$instance->parcel,
            'async'=> false
        ]);

        return $rates->rates;
    }

    public static function createTransaction($objectId)
    {
        if (self::$instance == null) {
            self::$instance = new Shipping();
        }

        $transaction = Shippo_Transaction::create([
            'rate' => $objectId, 
            'label_file_type' => 'PDF', 
            'async' => false
        ]);

        // echo '<pre>';
        // var_dump($transaction);
        // echo '</pre>';
        // die;

        return $transaction;
    }

    public static function trackShipment($trackingNumber, $courier)
    {
        if (self::$instance == null) {
            self::$instance = new Shipping();
        }

        $url = 'https://api.goshippo.com/tracks';
        $courier = strtolower($courier);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: ShippoToken ' . env('SHIPPO_PRIVATE')));
        curl_setopt($ch, CURLOPT_URL, $url . '/' . $courier . '/' . $trackingNumber);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close ($ch);

        return json_decode($response);
    }

}