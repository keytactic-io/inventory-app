<?php

namespace App\Services;

class WalkInRequest
{

    private static $instance = null;

    public function __construct($addressFrom = null, $addressTo = null, $parcel = null)
    {
        Shippo::setApiKey(env('SHIPPO_PRIVATE'));
    }

    public static function trackShipment()
    {
        if (self::$instance == null) {
            self::$instance = new WalkInRequest();
        }

        $url = 'https://api.goshippo.com/tracks';
        $courier = strtolower($courier);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: ShippoToken ' . env('SHIPPO_PRIVATE')));
        curl_setopt($ch, CURLOPT_URL, $url . '/' . $courier . '/' . $trackingNumber);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close ($ch);

        return json_decode($response);
    }

}