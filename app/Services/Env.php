<?php

namespace App\Services;

use Brotzka\DotenvEditor\DotenvEditor;
use Brotzka\DotenvEditor\Exceptions\DotEnvException;

class Env
{
    private $env;
    private static $instance = null;

    public function __construct()
    {
        $this->env = new DotenvEditor;
    }

    public static function update($array)
    {
        if (self::$instance == null) {
            self::$instance = new Env;
        }

        foreach ($array as $key => $value) {
            if (!self::$instance->env->keyExists($key)) {
                self::$instance->env->addData([
                    $key => $value
                ]);
            } else {
                self::$instance->env->changeEnv([$key => $value]);
            }
        }
    }


}