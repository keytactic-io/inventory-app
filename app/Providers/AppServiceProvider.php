<?php

namespace App\Providers;

use Auth;
use Cache;
use Request;
use Session;
use Artisan;
use App\Models\User;
use App\Models\Setting;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (!Schema::hasTable('settings')) {
            if (env('APP_KEY') == '') {
                Artisan::call('key:generate');
            }
            Artisan::call('migrate');
        }

        if (env('APP_ENV') === 'local') {
            Cache::flush();
        }
        
        view()->composer('*', function ($view) {

            if (Auth::check()) {
                if (!Session::has('currentUserPermissions')) {
                    $permissions = [];
                    if (!Auth::user()->is_requester) {
                        $permissions = Auth::user()->role->allPermissions()
                            ->pluck('key')
                            ->toArray();
                    }
                    Session::put('currentUserPermissions', $permissions);
                }
                $view->with('currentUser', Auth::user());
            }

        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}
