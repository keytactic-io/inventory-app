<?php

namespace App\Http\Middleware;

use Closure;
use Response;
use App\Models\ApiKey;

class VerifyWalkInRequest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /**
         * Check if request header has mln-key
         * If none, it will response with an error message
         */
        if (!isset($request->header()['api-key'])) {

            return Response::json([
                'error' => 'API Key required.'
            ], 401);

        }

        /**
         * If request header has mln-key, it will query a user that matches 
         * the mln-key. If a user is found, request will continue,
         * else it will response with an error message
         */
        $apiKey = ApiKey::where('api_key', $request->header()['api-key'])->first();

        return $user ? $next($request) : Response::json([
            'error' => 'Authentication failed.'
        ], 403);
    }
}
