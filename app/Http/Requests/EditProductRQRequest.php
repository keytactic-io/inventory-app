<?php

namespace App\Http\Requests;

use Auth;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Factory as ValidationFactory;

class EditProductRQRequest extends FormRequest
{

    public function authorize()
    {
        return Auth::check();
    }

    private function requesterRequired()
    {
        return $this->has('requester_id') ? 'required' : '';
    }

    public function rules()
    {
        return [
            'requester_id'         => $this->requesterRequired(),
            'market_id'            => 'required|integer',
            'expected_inhand_date' => 'required|date',
            'street_1'             => 'required',
            'city'                 => 'required',
            'state'                => 'required',
            'country'              => 'required',
            'postcode'             => 'required',
            'justification'        => 'required',
            'products'             => 'required',
        ];
    }

    public function messages()
    {
        return [
            'products.required' => 'Please select at least one product.'
        ];
    }
    
}
