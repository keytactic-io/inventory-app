<?php

namespace App\Http\Requests;

use Auth;
use Request;
use Illuminate\Foundation\Http\FormRequest;

class SaveUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $password = 'required';
        if (array_key_exists('e_password', Request::all())) {
            $password = '';
        }

        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'role_id' => 'required|integer',
            'email' => 'required',
            'password' => $password
        ];
    }
}
