<?php

namespace App\Http\Requests;

use Auth;
use Request;
use App\Models\Product;
use Illuminate\Foundation\Http\FormRequest;

class SaveProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {

        $unique = '|unique:products';
        if (array_key_exists('product_id', Request::all())) {
            $productName = Request::get('name');
            $productId = Request::get('product_id');
            $product = Product::findOrFail($productId);
            if ($product->name == $productName) {
                $unique = '';
            }
        }

        return [
            'name' => 'required' . $unique,
            'opening_balance' => 'numeric',
            'reorder_point' => 'required|numeric',
            'reorder_quantity' => 'required|numeric',
            'vendor_id' => 'required|integer',
            'location_id' => 'required_with:opening_balance|integer',
            'price' => 'required_with:opening_balance|numeric',
        ];
    }
}
