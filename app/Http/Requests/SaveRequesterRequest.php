<?php

namespace App\Http\Requests;

use Auth;
use Request;
use App\Models\Requester;
use Illuminate\Foundation\Http\FormRequest;

class SaveRequesterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = '|unique:users';
        $password = 'required';
        if (array_key_exists('requester_id', Request::all())) {
            $requesterEmail = Request::get('email');
            $requesterId = Request::get('requester_id');
            $requester = Requester::findOrFail($requesterId);
            if ($requester->user->email == $requesterEmail) {
                $unique = '';
                $passowrd = '';
            }
        }

        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'addresses.1.street_1' => 'required',
            'addresses.1.city' => 'required',
            'addresses.1.state' => 'required',
            'addresses.1.country' => 'required',
            'addresses.1.postcode' => 'required',
            'email' => 'required' . $unique,
            'contact_number' => 'required',
            'password' => $password
        ];
    }
}
