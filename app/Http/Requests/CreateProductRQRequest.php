<?php

namespace App\Http\Requests;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

class CreateProductRQRequest extends FormRequest
{

    public function authorize()
    {
        return Auth::check();
    }

    private function addressRequest()
    {
        return $this->has('link_to_requester') ? 'required' : '';
    }

    private function addressField()
    {
        return $this->has('link_to_requester') ? '' : 'required';
    }

    public function rules()
    {
        return [
            'market_id'            => 'required|integer',
            'expected_inhand_date' => 'required|date',
            'address'              => $this->addressField(),
            'street_1'             => $this->addressRequest(),
            'city'                 => $this->addressRequest(),
            'state'                => $this->addressRequest(),
            'country'              => $this->addressRequest(),
            'postcode'             => $this->addressRequest(),
            'justification'        => 'required',
            'products'             => 'required',
        ];
    }

    public function messages()
    {
        return [
            'products.required' => 'Please select at least one product.'
        ];
    }

}
