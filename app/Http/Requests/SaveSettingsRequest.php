<?php

namespace App\Http\Requests;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

class SaveSettingsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->allowedTo('edit_settings');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'application_name' => 'required',
            'logo' => 'mimes:jpeg,jpg,png|max:1000',
            'allow_update_po' => 'required',
            'show_zero_locations' => 'required',
            'items_per_page' => 'required|integer'
        ];
    }
}
