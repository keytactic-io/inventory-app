<?php

namespace App\Http\Requests;

use Auth;
use Request;
use App\Models\Vendor;
use Illuminate\Foundation\Http\FormRequest;

class SavevendorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = '|unique:vendors';
        if (array_key_exists('vendor_id', Request::all())) {
            $vendorName = Request::get('name');
            $vendorId = Request::get('vendor_id');
            $vendor = Vendor::findOrFail($vendorId);
            if ($vendor->name == $vendorName) {
                $unique = '';
            }
        }

        return [
            'photo' => 'mimes:jpeg,jpg,png',
            'name' => 'required' . $unique,
            'street_1' => 'required',
            'city' => 'required',
            'state' => 'required',
            'country' => 'required',
            'postcode' => 'required',
            'contact_person' => 'required',
            'mobile_number' => 'required'
        ];
    }
}
