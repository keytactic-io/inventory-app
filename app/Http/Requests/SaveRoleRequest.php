<?php

namespace App\Http\Requests;

use Auth;
use Request;
use App\Models\Role;
use Illuminate\Foundation\Http\FormRequest;

class SaveRoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = '|unique:roles';
        if (array_key_exists('role_id', Request::all())) {
            $roleName = Request::get('name');
            $roleId = Request::get('role_id');
            $role = Role::findOrFail($roleId);
            if ($role->name == $roleName) {
                $unique = '';
            }
        }

        return [
            'name' => 'required' . $unique
        ];
    }
}
