<?php

namespace App\Http\Requests;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

class SaveRequesterSignupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'confirm_password' => 'required|same:password',
            'addresses.1.street_1' => 'required',
            'addresses.1.city' => 'required',
            'addresses.1.state' => 'required',
            'addresses.1.country' => 'required',
            'addresses.1.postcode' => 'required',
            'contact_number' => 'required'
        ];
    }
}
