<?php

namespace App\Http\Requests;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

class SetupAppSettingsRequest extends FormRequest
{

    public function authorize()
    {
        return Auth::guest();
    }

    public function rules()
    {
        $this['items_per_page'] = 20;
        
        return [
            'application_name' => 'required',
            'address'          => 'required',
            'email'            => 'required|email',
            'contact_number'   => 'required',
            'logo'             => 'mimes:jpeg,jpg,png|max:1000',
            'first_name'       => 'required',
            'last_name'        => 'required',
            'admin_email'      => 'required|email',
            'password'         => 'required|confirmed',
        ];
    }

}
