<?php

namespace App\Http\Controllers;

use Auth;
use Mail;
use Input;
use Session;
use Artisan;
use Validator;
use Carbon\Carbon;
use App\Models\User;
use App\Http\Requests;
use App\Models\Market;
use App\Models\Product;
use App\Models\Setting;
use App\Models\Location;
use App\Models\Requester;
use App\Services\Shipping;
use App\Models\CustomField;
use Illuminate\Http\Request;
use App\Models\WalkInRequest;
use App\Models\CustomFieldValue;
use App\Models\RequesterAddress;
use App\Models\RequestedProduct;
use App\Mail\RequestNotification;
use App\Mail\NewRequestNotification;
use App\Jobs\ArchiveRemainingStocks;
use App\Mail\ShippedRequestNotification;
use App\Mail\NewRequestAdminNotification;
use App\Mail\ApprovedRequestNotification;
use App\Models\Request as RequestProduct;
use App\Http\Requests\EditProductRQRequest;
use App\Http\Requests\CreateProductRQRequest;
use App\Mail\ApprovedRequestStaffNotification;
use App\Mail\ApprovedButSplitRequestNotification;

class RequestController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getRequests()
    {
        $currentUser = Auth::user();
        abort_if(!$currentUser->allowedTo('view_requests') AND !$currentUser->is_requester, 403);

        $rqAll             = RequestProduct::all();
        $rqPendingApproval = RequestProduct::pendingApproval(true);
        $rqPendingShipment = RequestProduct::pendingShipment(true);
        $rqShipped         = RequestProduct::shipped(true);
        $rqRejected        = RequestProduct::rejected(true);
        $rqSplit           = RequestProduct::split(true);

        // if requester
        if ($currentUser->is_requester) {
            $rqAll = RequestProduct::query()
                ->where('requester', $currentUser->id)
                ->get();
            $rqPendingApproval = RequestProduct::pendingApproval()
                ->where('requester', $currentUser->id)
                ->get();
            $rqPendingShipment = RequestProduct::pendingShipment()
                ->where('requester', $currentUser->id)
                ->get();
            $rqShipped = RequestProduct::shipped()
                ->where('requester', $currentUser->id)
                ->get();
            $rqRejected = RequestProduct::rejected()
                ->where('requester', $currentUser->id)
                ->get();            
            $rqSplit = RequestProduct::split()
                ->where('requester', $currentUser->id)
                ->get();
        }

        $rqAll->load('theRequester', 'market');

        return view('pages.requests.list', compact(
            'rqAll', 'rqPendingApproval', 'rqPendingShipment', 'rqShipped', 
            'rqRejected', 'rqSplit'
        ));
    }

    public function viewRequest($requestId)
    {
        $currentUser = Auth::user();
        abort_if(!$currentUser->allowedTo('view_requests') AND !$currentUser->is_requester, 403);

        $requestProduct = RequestProduct::findOrFail($requestId);
        abort_if($currentUser->is_requester AND $requestProduct->requester != $currentUser->id, 404);
        
        $visibility        = $requestProduct->processed_by != null ? 'hidden' : '';
        $requestedProducts = $requestProduct->requestedProducts;

        $locations = Location::query()
            ->orderBy('name')
            ->get();

        $trackingDetails = null;
        if ($requestProduct->status == 2) {
            $trackingDetails = Shipping::trackShipment(
                $requestProduct->tracking_number, $requestProduct->courier
            );
        }

        $rqParent = $requestProduct->parentRq ? 
            '&nbsp; &larr; &nbsp; ' . $requestProduct->parentRq->uid() : '';

        return response()->json([
            'title' => $requestProduct->uid() . $rqParent,
            'view' => view('pages.requests.details', compact(
                'requestProduct', 'requestedProducts', 'locations',
                'trackingDetails'
            ))->render(),
            'visibility' => $visibility
        ]);
    }

    public function createRequest($requesterId = null)
    {
        $currentUser = Auth::user();
        abort_if(!$currentUser->allowedTo('add_requests') AND !$currentUser->is_requester, 403);

        if ($requesterId == null) {
            if ($currentUser->is_requester) {
                return redirect()->route('requests.create', [
                    'requestId' => $currentUser->requester->id
                ]);
            } else {
                return redirect()->route('requests.requester.select');    
            }
        }

        $requester    = Requester::findOrFail($requesterId);
        $customFields = CustomField::requests()->get();
            
        $products = Product::query()
            ->distinct()
            ->selectRaw("
                products.*, product_allocations.allocation,
                (archive_product_remaining_stocks.opening_balances + archive_product_remaining_stocks.orders) - 
                (archive_product_remaining_stocks.requests + archive_product_remaining_stocks.walkin_requests) as remaining_stocks
            ")
            ->leftJoin('product_allocations', 'products.id', '=', 'product_allocations.product_id')
            ->leftJoin('archive_product_remaining_stocks', 'products.id', '=', 'archive_product_remaining_stocks.product_id')
            ->where('product_allocations.allocation', '<>', 0)
            ->orderBy('products.name')
            ->get();

        $markets = Market::query()
            ->orderBy('name')
            ->get();

        return view('pages.requests.create', compact(
            'requester', 'customFields', 'products', 'markets'
        ));
    }

    public function selectRequester()
    {
        $requesters = Requester::all();

        if (count($requesters) == 0 and Product::count() == 0) {
            return redirect()->back()
                ->withErrors(['At least <strong>one requester and one product</strong> are required to create a new request.']);
        }

        $currentUser = Auth::user();
        if ($currentUser->is_requester) {
            return redirect()->route('requests.create', [
                'requestId' => $currentUser->requester->id
            ]);
        }

        return view('pages.requests.select-requester', compact(
            'requesters'
        ));
    }

    public function createRequestSave(CreateProductRQRequest $request, $requesterId)
    {
        $currentUser = Auth::user();
        abort_if(!$currentUser->allowedTo('add_requests') AND !$currentUser->is_requester, 403);

        if ($requesterId == null) {
            return response()->back()->withInput()
                ->withErrors(['Requester not found.']);
        }

        try {
            $request['expected_inhand_date'] = Carbon::parse($request->expected_inhand_date);
        } catch (\Exception $e) {
            return redirect()->back()
                ->withInput()
                ->withErrors(['Invalid date format for expected in-hand date.']);
        }

        $requester = Requester::findOrFail($requesterId);
        $request['requester_id'] = $requester->id;
        
        if ($request->has('address')) {
            $address = RequesterAddress::findOrFail($request->address);
        } else {
            $address = new RequesterAddress($request->only([
                'street_1', 'street_2', 'city', 'state', 'country', 'postcode',
                'requester_id'
            ]));
            if ($request->link_to_requester) {
                $address->save();
            }
        }

        $attention     = $request->has('attention') ? $request->attention : $requester->user->name();
        $contactNumber = $request->has('contact_number') ? $request->contact_number : $requester->contact_number;
        $company       = $request->has('company') ? $request->company : app_setting('application_name');

        // create request
        $requestProduct = RequestProduct::create([
            'requester'            => $requester->user->id,
            'market_id'            => $request->market_id,
            'expected_inhand_date' => $request->expected_inhand_date,
            'status'               => 0,
            'attention'            => $attention,
            'contact_number'       => $contactNumber,
            'company'              => $company,
            'street_1'             => $address->street_1,
            'street_2'             => $address->street_2,
            'city'                 => $address->city,
            'state'                => $address->state,
            'country'              => $address->country,
            'postcode'             => $address->postcode,
            'justification'        => $request->justification,
            'note'                 => $request->notes,
        ]);

        // link products
        $products     = $request->products;
        $productsData = [];
        $productIds   = [];
        foreach ($products as $id => $quantity) {
            $product = Product::findOrFail($id);
            array_push($productsData, [
                'request_id' => $requestProduct->id,
                'product_id' => $product->id,
                'quantity'   => $quantity,
                'price'      => $product->latestPrice(),
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ]);
            $productIds[] = $product->id;
        }
        RequestedProduct::insert($productsData);

        // save custom fields
        if (count($request->custom_fields) > 0) {
            $customFieldsData = [];
            foreach ($request->custom_fields as $id => $value) {
                if ($value != '') {
                    array_push($customFieldsData, [
                        'custom_field_id' => $id,
                        'reference_id'    => $requestProduct->id,
                        'value'           => $value
                    ]);
                }
            }
            CustomFieldValue::insert($customFieldsData);
        }

        $ccEmails = [];
        if ($request->cc_emails != '') {
            $ccEmailsParts             = explode(',', $request->cc_emails);
            foreach ($ccEmailsParts as $ccEmail) {
                if (filter_var($ccEmail, FILTER_VALIDATE_EMAIL)) {
                    $ccEmails[] = $ccEmail;
                }
            }
            $requestProduct->cc_emails = implode(',', $ccEmails);
            $requestProduct->save();
        }
        if (count($ccEmails) > 0) {
            Mail::to($requester->user->email)
                ->cc($ccEmails)
                ->queue(new NewRequestNotification($requestProduct));
        } else {
            Mail::to($requester->user->email)
                ->queue(new NewRequestNotification($requestProduct));
        }

        $admins = User::query()
            ->where('role_id', 2)
            ->get();
        if (count($admins) > 0) {
            foreach ($admins as $admin) {
                Mail::to($admin->email)
                    ->queue(new NewRequestAdminNotification($requestProduct, $admin));
            }
        }

        dispatch(new ArchiveRemainingStocks(
            Product::whereIn('id', $productIds)->get()
        ));

        return redirect()->route('requests.list')
            ->withStatus($requestProduct->uid() . ' successfully created.');
    }

    public function editRequest($requestId, $requesterId)
    {
        $currentUser = Auth::user();
        abort_if(!$currentUser->allowedTo('edit_requests') AND !$currentUser->is_requester, 403);

        $requester         = Requester::findOrFail($requesterId);
        $markets           = Market::query()->orderBy('name')->get();
        $customFields      = CustomField::requests()->get();
        $requestProduct    = RequestProduct::findOrFail($requestId);
        $requestedProducts = $requestProduct->requestedProducts;

        $products = Product::query()
            ->distinct()
            ->selectRaw("
                products.*, product_allocations.allocation,
                (archive_product_remaining_stocks.opening_balances + archive_product_remaining_stocks.orders) - 
                (archive_product_remaining_stocks.requests + archive_product_remaining_stocks.walkin_requests) as remaining_stocks
            ")
            ->leftJoin('product_allocations', 'products.id', '=', 'product_allocations.product_id')
            ->leftJoin('archive_product_remaining_stocks', 'products.id', '=', 'archive_product_remaining_stocks.product_id')
            ->where('product_allocations.allocation', '<>', 0)
            ->orderBy('products.name')
            ->get();

        return view('pages.requests.edit', compact(
            'requester', 'requestProduct', 'markets', 'products', 
            'requestedProducts', 'customFields'
        ));

    }

    public function editRequestSave(EditProductRQRequest $request, $requestId, $requesterId)
    {
        $currentUser = Auth::user();
        abort_if(!$currentUser->allowedTo('edit_requests') AND !$currentUser->is_requester, 403);

        try {
            $request['expected_inhand_date'] = Carbon::parse($request->expected_inhand_date);
        } catch (\Exception $e) {
            return redirect()->back()
                ->withInput()
                ->withErrors(['Invalid date format for expected in-hand date.']);
        }

        $requestProduct = RequestProduct::findOrFail($requestId);
        $requester = Requester::findOrFail($requesterId);
        $request['requester_id'] = $requester->id;

        $attention     = $request->has('attention') ? $request->attention : $requester->user->name();
        $contactNumber = $request->has('contact_number') ? $request->contact_number : $requester->contact_number;
        $company       = $request->has('company') ? $request->company : app_setting('application_name');

        // create request
        $requestProduct->update([
            'market_id'            => $request->market_id,
            'expected_inhand_date' => $request->expected_inhand_date,
            'attention'            => $attention,
            'contact_number'       => $contactNumber,
            'company'              => $company,
            'street_1'             => $request->street_1,
            'street_2'             => $request->street_2,
            'city'                 => $request->city,
            'state'                => $request->state,
            'country'              => $request->country,
            'postcode'             => $request->postcode,
            'justification'        => $request->justification,
            'note'                 => $request->notes,
            'cc_emails'            => $request->cc_emails,
            'cc_emails'            => $request->cc_emails,
            'with_notification'    => $request->has('send_notification') ? 1 : 0
        ]);

        // link products
        $requestProduct->requestedProducts()->delete();
        $products     = $request->products;
        $productsData = [];
        $productIds   = [];
        foreach ($products as $id => $quantity) {
            $product = Product::findOrFail($id);
            array_push($productsData, [
                'request_id' => $requestProduct->id,
                'product_id' => $product->id,
                'quantity'   => $quantity,
                'price'      => $product->latestPrice(),
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ]);
            $productIds[] = $product->id;
        }
        RequestedProduct::insert($productsData);

        // delete custom fields values
        foreach ($requestProduct->customFields() as $customField) {
            $customField->delete();
        }

        // save custom fields values
        if (count($request->custom_fields) > 0) {
            $customFieldsData = [];
            foreach ($request->custom_fields as $id => $value) {
                if ($value != '') {
                    array_push($customFieldsData, [
                        'custom_field_id' => $id,
                        'reference_id'    => $requestProduct->id,
                        'value'           => $value
                    ]);
                }
            }
            CustomFieldValue::insert($customFieldsData);
        }

        dispatch(new ArchiveRemainingStocks(
            Product::whereIn('id', $productIds)->get()
        ));

        return redirect()->route('requests.list')
            ->withStatus($requestProduct->uid() . ' successfully updated.');
    }

    public function deleteRequest($requestId)
    {
        if (!Auth::user()->allowedTo('delete_requests')) {
            abort(403);
        }

        $request = RequestProduct::findOrFail($requestId);
        $request->delete();

        return redirect()->route('requests.list')
            ->withStatus('Successfully delete a request.');
    }

    public function processRequest(Request $request, $requestId)
    {
        $currentUser = Auth::user();
        abort_if(!$currentUser->allowedTo('process_requests'), 403);

        // find request
        $requestProduct = RequestProduct::findOrFail($requestId);

        // check remaining stock if sufficient
        $products          = $request->products;
        $errors            = [];
        $locationsToNotify = [];
        foreach ($products as $requestedProductId => $keys) {
            $requestedProduct = RequestedProduct::findOrFail($requestedProductId);
            $location         = Location::findOrFail($keys['location']);
            if ($requestedProduct->product->stocksInLocation($location->id) < $keys['allowed']) {
                array_push($errors, 'Insufficient stock of ' . $requestedProduct->product->name . ' in ' . $location->name);
            }
        }
        if (count($errors) > 0) {
            return response()->json([
                'errors' => $errors
            ]);
        }

        // get locations count
        $locations = [];
        foreach ($request->products as $productInfo) {
            $location    = Location::findOrFail($productInfo['location']);
            $locations[] = $productInfo['location'];
        }

        $locations         = array_unique($locations);
        $locationStaffData = [];
        $productIds        = [];

        // clone RQ
        if (count($locations) > 1):

            foreach ($locations as $location) {
                $newRq = RequestProduct::create([
                    'requester'            => $requestProduct->requester,
                    'market_id'            => $requestProduct->market_id,
                    'expected_inhand_date' => Carbon::parse($requestProduct->expected_inhand_date),
                    'status'               => 1, // update RQ status to 1
                    'attention'            => $requestProduct->attention,
                    'contact_number'       => $requestProduct->contact_number,
                    'company'              => $requestProduct->company,
                    'address'              => $requestProduct->address,
                    'street_1'             => $requestProduct->street_1,
                    'street_2'             => $requestProduct->street_2,
                    'city'                 => $requestProduct->city,
                    'state'                => $requestProduct->state,
                    'country'              => $requestProduct->country,
                    'postcode'             => $requestProduct->postcode,
                    'justification'        => $requestProduct->justification,
                    'note'                 => $requestProduct->note,
                    'processed_by'         => $currentUser->id,
                    'processed_at'         => Carbon::now(),
                    'parent_id'            => $requestProduct->id
                ]);

                $theLocation = Location::findOrFail($location);

                // link products per location
                $requestedProductsData = [];
                foreach ($request->products as $requestedProductId => $productInfo) {
                    $requestedProduct = RequestedProduct::findOrFail($requestedProductId);
                    if ($productInfo['location'] == $theLocation->id) {
                        array_push($requestedProductsData, [
                            'request_id'       => $newRq->id,
                            'product_id'       => $requestedProduct->product_id,
                            'quantity'         => $requestedProduct->quantity,
                            'allowed_quantity' => $productInfo['allowed'],
                            'price'            => $requestedProduct->product->latestPrice(),
                            'get_from'         => $theLocation->id
                        ]);
                        $productIds[] = $requestedProduct->product_id;
                    }
                }
                RequestedProduct::insert($requestedProductsData);

                // fetch location staff email
                if (count($theLocation->staff) > 0) {
                    foreach ($theLocation->staff as $staff) {
                        $locationStaffData[] = [
                            'location_id' => $theLocation->id,
                            'email' => $staff->user->email
                        ];
                    }
                }
            }

            // update request status - split
            $requestProduct->status = 4; // split

        else:

            // link products per location
            foreach ($request->products as $requestedProductId => $productInfo) {
                $requestedProduct = RequestedProduct::findOrFail($requestedProductId);
                $location         = Location::findOrFail($productInfo['location']);
                $requestedProduct->update([
                    'allowed_quantity' => $productInfo['allowed'],
                    'price'            => $requestedProduct->product->latestPrice(),
                    'get_from'         => $location->id
                ]);

                // fetch location staff email
                if (count($location->staff) > 0) {
                    foreach ($location->staff as $staff) {
                        $locationStaffData[] = [
                            'location_id' => $location->id,
                            'email' => $staff->user->email
                        ];
                    }
                }
            }

            // update request status - pending for shipment
            $requestProduct->status = 1;

        endif;

        $requestProduct->processed_by = $currentUser->id;
        $requestProduct->processed_at = Carbon::now();
        $requestProduct->save();

        if (count($locations) > 1) {
            // split
            if ($requestProduct->cc_emails != '' AND $requestProduct->cc_emails != null) {
                Mail::to($requestProduct->theRequester->email)
                    ->cc(explode(',', $requestProduct->cc_emails))
                    ->send(new ApprovedButSplitRequestNotification($requestProduct, $locations));
            } else {
                Mail::to($requestProduct->theRequester->email)
                    ->send(new ApprovedButSplitRequestNotification($requestProduct, $locations));
            }
        } else {
            // usual notification
            if ($requestProduct->cc_emails != '' AND $requestProduct->cc_emails != null) {
                Mail::to($requestProduct->theRequester->email)
                    ->cc(explode(',', $requestProduct->cc_emails))
                    ->send(new ApprovedRequestNotification($requestProduct));
            } else {
                Mail::to($requestProduct->theRequester->email)
                    ->send(new ApprovedRequestNotification($requestProduct));
            }
        }

        if (count($locationStaffData) > 0) {

            // create pdf
            $filename = $requestProduct->uid();
            $html = view('pages.requests.pdf', compact(
                'requestProduct', 'filename'
            ))->render();
            toPdf($html, $filename, true);
            // end create pdf

            foreach ($locationStaffData as $locationStaffDatum) {
                $locationStaff = User::where('email', $locationStaffDatum['email'])->first();
                $location      = Location::find($locationStaffDatum['location_id'])->first();
                if ($locationStaff AND $location) {
                    Mail::to($locationStaff->email)
                        ->send(new ApprovedRequestStaffNotification($requestProduct, $location, $locationStaff));
                }
            }

            // unlink pdf
            unlink(public_path() . '/temp/' . $filename . '.pdf');
        }

        dispatch(new ArchiveRemainingStocks(
            Product::whereIn('id', $productIds)->get()
        ));

        return response()->json([
            'redirect' => route('requests.list')
        ]);
    }

    public function quickApprove($requestId)
    {
        
    }

    public function disapproveRequest($requestId)
    {
        $currentUser = Auth::user();
        abort_if(!$currentUser->allowedTo('process_requests'), 403);
        
        // find request
        $requestProduct               = RequestProduct::findOrFail($requestId);
        $requestProduct->processed_by = $currentUser->id;
        $requestProduct->processed_at = Carbon::now();
        $requestProduct->status       = 3; // disapproved
        $requestProduct->save();

        // send email
        Mail::to($requestProduct->theRequester->email)
            ->send(new RequestNotification($requestProduct, 'disapproved'));

        $productIds = $requestProduct->requestedProducts()
            ->pluck('product_id')->toArray();
        dispatch(new ArchiveRemainingStocks(
            Product::whereIn('id', $productIds)->get()
        ));

        return response()->json([
            'redirect' => route('requests.list')
        ]);
    }

    public function pdfView($requestId)
    {
        abort_if(!Auth::user()->allowedTo('view_requests') AND Auth::user()->id != $requestProduct->requester, 403);

        $requestProduct = RequestProduct::findOrFail($requestId);
        $timestamp      = Carbon::parse($requestProduct->created_at)->timestamp;
        $filename       = $requestProduct->uid();

        $html = view('pages.requests.pdf', compact(
            'requestProduct', 'filename'
        ))->render();

        return toPdf($html, $filename, false);
    }

    public function addCustomAddress()
    {
        return view('pages.requests.custom-address');
    }

    public function shipRequest($requestId = null)
    {
        abort_if(!Auth::user()->allowedTo('ship_requests'), 403);

        $requestProduct = $requestId != null ? 
            RequestProduct::findOrFail($requestId) : null;

        return view('pages.shippings.ship-request', compact(
            'requestProduct'
        ));
    }

    public function shippingRates(Request $request)
    {
        abort_if(!Auth::user()->allowedTo('ship_requests'), 403);

        $addressFrom = $request->address['from'];
        $addressTo   = $request->address['to'];
        $parcels     = array_values($request->parcels);

        if ($addressFrom['street2'] == '') {
            unset($addressFrom['street2']);
        }
        if ($addressFrom['company'] == '') {
            unset($addressFrom['company']);
        }
        if ($addressFrom['phone'] == '') {
            unset($addressFrom['phone']);
        }

        if ($addressTo['street2'] == '') {
            unset($addressTo['street2']);
        }
        if ($addressTo['company'] == '') {
            unset($addressTo['company']);
        }
        if ($addressTo['phone'] == '') {
            unset($addressTo['phone']);
        }

        $requestProduct = $request->request_id != '' ?
            RequestProduct::findOrFail($request->request_id) : null;

        $rates = Shipping::getRates($addressFrom, $addressTo, $parcels);

        return view('pages.shippings.rates-list', compact(
            'rates', 'requestProduct'
        ));
    }

    public function createLabel(Request $request, $objectId, $requestId = null)
    {
        abort_if(!Auth::user()->allowedTo('ship_requests'), 403);

        $transaction = Shipping::createTransaction($objectId);

        if ($transaction->status == 'SUCCESS') {

            if ($requestId != null) {
                // save tracking number and label_url
                // then redirect to rq lists
                $requestProduct = RequestProduct::findOrFail($requestId);
                $requestProduct->update([
                    'status'             => 2, // shipped
                    'manual_input'       => 0, // false
                    'courier'            => $request->courier,
                    'rate'               => $request->rate,
                    'tracking_number'    => $transaction->tracking_number,
                    'label_url'          => $transaction->label_url,
                    'service_level_name' => $request->service_level_name,
                    'duration_terms'     => $request->duration_terms,
                ]);

                // email notification
                if ($requestProduct->with_notification) {

                    $emails = $requestProduct->cc_emails != null ? explode(',', $requestProduct->cc_emails) : [];
                    if (count($emails) > 0) {
                        Mail::to($requestProduct->theRequester->email)
                            ->cc($emails)
                            ->send(new RequestNotification($requestProduct, 'shipped'));
                    } else {
                        Mail::to($requestProduct->theRequester->email)
                            ->send(new RequestNotification($requestProduct, 'shipped'));
                    }

                }

                return response()->json([
                    'success'  => true,
                    'redirect' => route('requests.list'),
                    'labelUrl' => $requestProduct->label_url
                ]);
            } else {
                Session::flash('status', 'Tracking number and label are sent to your email. Thank you!');
                return response()->json([
                    'success'  => true,
                    'redirect' => route('requesters.ship'),
                    'labelUrl' => $transaction['label_url']
                ]);
            }
        } else {
            $errorMessageBag = '<ul>';
            if (count($transaction->messages) > 0) {
                foreach ($transaction->messages as $errorMessage) {
                    $errorMessageBag .= '<li>' . $errorMessage->text . '</li>';
                }
            }
            $errorMessageBag .= '</ul>';

            return response()->json([
                'error' => $errorMessageBag
            ]);
        }
    }

    public function shipRequestManual($requestId)
    {
        abort_if(!Auth::user()->allowedTo('ship_requests'), 403);

        $requestProduct = RequestProduct::findOrFail($requestId);
        return response()->json([
            'view' => view('pages.shippings.ship-manual', compact(
                'requestProduct'
            ))->render()
        ]);
    }

    public function shipRequestManualSave(Request $request, $requestId)
    {
        abort_if(!Auth::user()->allowedTo('ship_requests'), 403);
        
        $requestProduct = RequestProduct::findOrFail($requestId);

        $validator = Validator::make($request->all(), [
            'courier'         => 'required',
            'rate'            => 'required',
            'tracking_number' => 'required'
        ]);

        if ($validator->fails()) {
            $errors = $validator->messages();
            return [
                'error' => view('includes.errors', compact(
                    'errors'))->render()
            ];
        }

        $requestProduct->update([
            'status'          => 2, // shipped
            'manual_input'    => 1, // false
            'courier'         => $request->courier,
            'rate'            => $request->rate,
            'tracking_number' => $request->tracking_number
        ]);

        // create pdf
        $filename = $requestProduct->uid();
        $html = view('pages.requests.pdf', compact(
            'requestProduct', 'filename'
        ))->render();
        toPdf($html, $filename, true);
        // end create pdf

        // email notification
        $ccEmails = ($requestProduct->cc_emails != null AND $requestProduct->cc_emails != '') ?
             explode(',', $requestProduct->cc_emails) : [];
        if (count($ccEmails) > 0) {
            Mail::to($requestProduct->theRequester->email)
                ->cc($ccEmails)
                ->send(new ShippedRequestNotification($requestProduct));
        } else {
            Mail::to($requestProduct->theRequester->email)
                ->send(new ShippedRequestNotification($requestProduct));
        }

        // unlink pdf
        unlink(public_path() . '/temp/' . $filename . '.pdf');

        return response()->json([
            'success'  => true,
            'redirect' => route('requests.list')
        ]);
    }

    public function allWrqs()
    {
        $allWrq      = WalkInRequest::all();
        $pendingWrq  = WalkInRequest::pending(true);
        $releasedWrq = WalkInRequest::released(true);

        return view('pages.wrq.all', compact(
            'allWrq', 'pendingWrq', 'releasedWrq'
        ));
    }

    public function processWrq($wrqId)
    {
        $wrq = WalkInRequest::findOrFail($wrqId);

        return response()->json([
            'title' => 'Process Walk-in Request',
            'view' => view('pages.wrq.process', compact(
                'wrq'
            ))->render(),
            'released' => $wrq->is_released ? true : false
        ]);
    }

    public function processWrqSave(Request $request, $wrqId)
    {
        $wrq = WalkInRequest::findOrFail($wrqId);

        $validator = Validator::make($request->all(), [
            'products.*.allowed_quantity' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            $errors = $validator->messages();
            return response()->json([
                'error' => view('includes.errors', compact(
                    'errors'))->render()
            ]);
        }

        // check if quantity is avaible
        $errors = [];
        foreach ($request->products as $requestedProductId => $product) {
            $p              = Product::findOrFail($product['product_id']);
            $remainingStock = $wrq->location->stocksRemaining($p->id);

            if ($remainingStock < $product['allowed_quantity']) {
                array_push($errors, 'Insufficient stock of ' . $p->name);
            }
        }
        if (count($errors) > 0) {
            $errorStr = '<div class="alert alert-danger"><ul><li>';
            $errorStr .= implode('</li><li>', $errors);
            $errorStr .= '</li></ul></div>';
            return response()->json([
                'error' => $errorStr
            ]);
        }

        $productIds = [];
        // save if no error
        foreach ($request->products as $requestedProductId => $product) {
            $requestedProduct = $wrq->requestedProducts()
                ->where('id', $requestedProductId)
                ->first();

            $requestedProduct->allowed_quantity = $product['allowed_quantity'];
            $requestedProduct->save();
            $productIds[] = $requestedProductId;
        }

        $wrq->is_released = 1;
        $wrq->save();

        dispatch(new ArchiveRemainingStocks(
            Product::whereIn('id', $productIds)->get()
        ));

        return response()->json([
            'done'     => true,
            'redirect' => route('wrq.all')
        ]);;
    }

}
