<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\ApiKey;
use App\Http\Requests;
use Illuminate\Http\Request;

class ApiKeyController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function allApiKeys()
    {
        if (
            Auth::user()->is_requester AND
            Auth::user()->role->name != 'Super Admin' AND
            Auth::user()->role->name != 'Admin'
        ) {
            abort(404);
        }

        $apiKeys = ApiKey::all();

        return view('pages.api_keys.all', compact(
            'apiKeys'
        ));
    }

    public function generateApiKey(Request $request)
    {
        if (
            Auth::user()->is_requester AND
            Auth::user()->role->name != 'Super Admin' AND
            Auth::user()->role->name != 'Admin'
        ) {
            abort(404);
        }

        ApiKey::create([
            'api_key' => str_random(64)
        ]);

        return redirect()->back()
            ->withStatus('New API Key successfully generated.');
    }

}
