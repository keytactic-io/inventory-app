<?php

namespace App\Http\Controllers;

use Auth;
use Cache;
use App\Services\Env;
use App\Http\Requests;
use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Requests\SaveSettingsRequest;

class SettingController extends Controller
{

    public function __construct()
    {
    	$this->middleware('auth');
    }

    public function settingsHome()
    {
        if (!Auth::user()->allowedTo('view_settings')) {
            abort(403);
        }

    	return view('pages.settings.home');
    }

    public function settingsSave(SaveSettingsRequest $request)
    {
        if (!Auth::user()->allowedTo('edit_settings')) {
            abort(403);
        }

        foreach ($request->except(['_token, shippo_private']) as $key => $value) {
            $setting = Setting::where('key', $key)->first();
            if ($setting) {
                if ($key == 'logo') {
                    if ($request->hasFile('logo')) {
                        $destinationPath = 'uploads';
                        $extension = $request->logo->getClientOriginalExtension();
                        $filename = 'app-logo.' . $extension;
                        $request->logo->move($destinationPath, $filename);
                        $setting->update(['value' => $filename]);
                    }
                } else {
                    $setting->update(['value' => $value]);
                }   
            } else {
                Setting::create([
                    'key' => $key,
                    'name' => title_case(str_replace('_', ' ', $key)),
                    'value' => $value
                ]);
            }            
        }

        // update shippo key if changed
        if ($request->shippo_private != env('SHIPPO_PRIVATE')) {
            Env::update(['SHIPPO_PRIVATE' => $request->shippo_private]);
        }

        Cache::put('app_settings', flattenSettings(Setting::all()), 
            config('cache.minutes'));

        return back()
            ->withStatus('Settings successfully save.');
    }

    public function removeLogo()
    {
        if (!Auth::user()->allowedTo('edit_settings')) {
            abort(403);
        }

        $setting = Setting::where('key', 'logo')->first();
        unlink('uploads/' . $setting->value);
        $setting->value = '';
        $setting->save();

        return back()
            ->withStatus('Logo successfully removed.');
    }

    public function envValues()
    {
        abort(404);
        // if (!Auth::user()->allowedTo('edit_settings')) {
        //     abort(403);
        // }

        $env = new DotenvEditor;
        $envValues = $env->getContent();

        return view('pages.settings.env', compact(
            'envValues'
        ));
    }

    public function envValuesSave(Request $request)
    {
        abort(404);
        // if (!Auth::user()->allowedTo('edit_settings')) {
        //     abort(403);
        // }
        
        $envValues = $request->except(['_token']);

        $env = new DotenvEditor;

        $newValues = [];
        foreach ($envValues as $key => $value) {
            $newValues[strtoupper($key)] = $value;            
        }

        $env->changeEnv($newValues);
        return redirect()->route('settings.env')
            ->withStatus('Env file has been updated.');
    }

}
