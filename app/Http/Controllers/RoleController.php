<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\Role;
use App\Http\Requests;
use App\Models\Permission;
use Illuminate\Http\Request;
use App\Models\RolePermission;
use App\Models\PermissionCategory;
use App\Http\Requests\SaveRoleRequest;

class RoleController extends Controller
{
    
    private $request;

    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;

        view()->share('roles', Role::all());
        view()->share('permissions', Permission::all());
        view()->share('permissionCategories', PermissionCategory::all());
    }

    public function getRoles()
    {
        if (!Auth::user()->allowedTo('view_roles')) {
            abort(403);
        }

        return view('pages.roles_permissions.list');
    }

    public function addRole()
    {
        if (!Auth::user()->allowedTo('add_roles')) {
            abort(403);
        }

        return view('pages.roles_permissions.add');
    }

    public function addRoleSave(SaveRoleRequest $request)
    {
        if (!Auth::user()->allowedTo('add_roles')) {
            abort(403);
        }

        $role = Role::create($request->except(['_token']));

        $permissions = $this->request->permissions;
        foreach ($permissions as $categoryId => $permissionIds) {
            foreach ($permissionIds as $permissionId => $bool) {
                RolePermission::create([
                    'role_id' => $role->id,
                    'permission_id' => $permissionId,
                ]);
            }            
        }

        return redirect()->route('roles.list')
            ->withStat($role->name . ' successfully added');
    }

    public function editRole($roleId)
    {
        $role = Role::findOrFail($roleId);

        if (!Auth::user()->allowedTo('edit_roles')) {
            abort(403);
        }
        
        $permissions = $role->permissions;
        $thePermissions = [];
        $i = 0;
        foreach ($permissions as $permission) {
            $thePermissions[$i] = $permission->permission_id;
            $i++;
        }
        
        return view('pages.roles_permissions.edit', compact(
            'role', 'thePermissions'
        ));
    }

    public function editRoleSave(SaveRoleRequest $request, $roleId)
    {
        $role = Role::findOrFail($roleId);

        if (!Auth::user()->allowedTo('edit_roles')) {
            abort(403);
        }
        
        $role->update($request->except('_token'));
        
        $role->permissions()->delete();

        $permissions = $this->request->permissions;
        foreach ($permissions as $categoryId => $permissionIds) {
            foreach ($permissionIds as $permissionId => $bool) {
                RolePermission::create([
                    'role_id' => $role->id,
                    'permission_id' => $permissionId,
                ]);
            }            
        }

        return redirect()->route('roles.list')
            ->withStatus('Permissions of ' . $role->name . ' successfully updated.');
    }

    public function deleteRole($roleId)
    {
        $role = Role::findOrFail($roleId);
        
        if (!Auth::user()->allowedTo('delete_roles')) {
            abort(403);
        }
        
        $name = $role->name;
        $role->delete();

        return redirect()->route('roles.list')
            ->withStatus($name . ' successfully deleted');
    }

}
