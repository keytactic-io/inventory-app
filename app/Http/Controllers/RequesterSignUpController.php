<?php

namespace App\Http\Controllers;

use Mail;
use Auth;
use Input;
use App\Models\User;
use App\Http\Requests;
use App\Models\Requester;
use Illuminate\Http\Request;
use App\Mail\WelcomeRequester;
use App\Models\RequesterAddress;
use App\Http\Requests\SaveRequesterSignupRequest;

class RequesterSignUpController extends Controller
{
    
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function signupRequester()
    {
        if (Auth::check()) {
            return redirect(url('/'));
        }

        if (Input::has('key')) {
            $key = Input::get('key');
            $requester = Requester::query()
                ->where('invite_key', $key)
                ->where('registered', 0)
                ->first();
            if ($requester) {

                return view('pages.requesters.signup', compact(
                    'requester'
                ));

            } else {
                abort(404);    
            }
        } else {
            abort(404);
        }
    }

    public function signupRequesterSave(SaveRequesterSignupRequest $request)
    {
        // update user
        $user = User::where('email', $request->email)->first();
        
        if (!$user) {
            abort(404);
        }

        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->password = bcrypt($request->password);
        $user->is_active = 1;
        $user->save();

        // update requester
        $data['requester'] = [
            'contact_number' => $request->contact_number,
            'registered' => 1
        ];
        $user->requester->update($data['requester']);

        $addresses = $request->addresses;
        foreach ($addresses as $key => $address) {
            RequesterAddress::create([
                'requester_id' => $user->requester->id,
                'street_1' => $address['street_1'],
                'street_2' => $address['street_2'],
                'city' => $address['city'],
                'state' => $address['state'],
                'country' => $address['country'],
                'postcode' => $address['postcode'],
                'default' => $request->default == $key ? 1 : 0
            ]);
        }

        Mail::to($user->email)
            ->send(new WelcomeRequester($user));

        Auth::login($user);

        return redirect(url('/'))
            ->withStatus('Welcome!');
    }

    public function addAddressFields()
    {
        $count = $this->request->c;
        $view = $this->request->front ? 'new-address-field-su' : 'new-address-field';
        return [
            'view' => view('pages.requesters.address.' . $view, compact(
                'count'
            ))->render()
        ];
    }

}
