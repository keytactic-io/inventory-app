<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\User;
use App\Models\Role;
use App\Models\Market;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\RequesterAddress;
use App\Http\Requests\SaveUserRequest;
use Illuminate\Auth\Events\Registered;

class UserController extends Controller
{
    
    private $request;

    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;

        view()->share('roles', Role::all());
    }

    public function getUsers()
    {
        if (!Auth::user()->allowedTo('view_users')) {
            abort(403);
        }

        $users = User::all();
        $users->load('requester');

        $roles = Role::all();

        return view('pages.users.list', compact(
            'users', 'roles'
        ));
    }

    public function getUserDetails($userId)
    {
        if (!Auth::user()->allowedTo('view_users')) {
            abort(403);
        }

        $user = User::findOrFail($userId);

        return json_encode([
            'view' => view('pages.users.preview', compact(
                'user'
            ))->render()
        ]);
    }

    public function addUser()
    {
        if (!Auth::user()->allowedTo('edit_users')) {
            abort(403);
        }

        return view('pages.users.add');
    }

    public function addUserSave(SaveUserRequest $request)
    {
        if (!Auth::user()->allowedTo('add_users')) {
            abort(403);
        }

        $request->merge(['password' => bcrypt($request->password)]);
        event(new Registered(
            $user = User::create($request->except(['_token']))
        ));

        return redirect()->route('users.list')
            ->withStatus($user->name() . ' successfully added.');
    }

    public function editUser($userId)
    {
        $user = User::findOrFail($userId);

        if (!Auth::user()->allowedTo('edit_users')) {
            abort(403);
        }

        return view('pages.users.edit', compact(
            'user'
        ));
    }

    public function editUserSave(SaveUserRequest $request, $userId)
    {
        $user = User::findOrFail($userId);

        if (!Auth::user()->allowedTo('edit_users')) {
            abort(403);
        }

        if (!isset($request->is_active)) {
            $request->merge(['is_active' => 0]);
        }

        if ($request->e_password != '') {
            $request->merge(['password' => bcrypt($request->e_password)]);
        }

        $user->update($request->except(['e_password', '_token']));

        return redirect()->route('users.list')
            ->withStatus($user->name() . ' successfully updated.');
    }

    // public function deleteUser($userId)
    // {
    //     $user = User::findOrFail($userId);

    //     if (!Auth::user()->allowedTo('edit_users')) {
    //         abort(403);
    //     }

    //     $name = $user->name();
    //     $user->delete();

    //     return redirect()->route('users.list')
    //         ->withStatus($name . ' successfully deleted');
    // }

    public function profile($userId)
    {
        $user = User::findOrFail($userId);
        $requests = $user->requests;
        $purchaseOrders = $user->purchaseOrders;
        $addresses = null;
        if ($user->is_requester) {
            $addresses = $user->requester->addresses;
        }

        return view('pages.users.profile', compact(
            'user', 'requests', 'purchaseOrders', 'addresses'
        ));
    }

    public function editProfile($userId)
    {
        if (
            !Auth::user()->allowedTo('edit_users') AND
            !Auth::user()->id == $userId
        ) {
            abort(403);
        }

        $user = User::findOrFail($userId);
        $markets = Market::orderBy('name')->get();

        return view('pages.users.profile-edit', compact(
            'user', 'markets'
        ));
    }

public function editProfileSave(SaveUserRequest $request, $userId)
    {
        $user = User::findOrFail($userId);

        if (
            !Auth::user()->allowedTo('edit_users') AND
            !Auth::user()->id == $userId
        ) {
            abort(403);
        }

        if (!isset($request->is_active)) {
            $request->merge(['is_active' => 0]);
        }

        if ($request->e_password != '') {
            $request->merge(['password' => bcrypt($request->e_password)]);
        }

        if ($user->is_requester AND !Auth::user()->is_requester) {
            $this->validate($request, [                
                'market_id' => 'required|integer'
            ]);
            $user->requester->market_id = $request->market_id;
            $user->requester->save();
        }

        $user->update($request->except(['e_password', '_token']));
        if (Auth::user()->is_requester) {
            $user->is_active = 1;
            $user->save();
        }

        if ($this->request->hasFile('photo')) {

            $destinationPath = 'uploads/photos';

            // delete old photo
            if ($user->display_photo != 'no-dp.png') {
                unlink($destinationPath . '/' . $user->display_photo);
            }
            
            $extension = $request->file('photo')->getClientOriginalExtension();
            $fileName = $user->id . '_' . time() . '.' . $extension;
            $request->file('photo')->move($destinationPath, $fileName);

            $user->display_photo = $fileName;
            $user->save();

        }

        return redirect()->route('users.profile', ['userId' => $user->id])
            ->withStatus($user->name() . ' profile successfully updated.');
    }

    public function deleteUser($userId)
    {
        $user = User::findOrFail($userId);

        if (!Auth::user()->allowedTo('delete_users')) {
            abort(403);
        }

        if ($user->is_requester) {
            $user->requester()->delete();
        }

        $statusMessage = $user->first_name . ' has been deleted as a user';
        if ($user->is_requester) {
            $statusMessage = $user->first_name . ' has been deleted as a user and rqeuester.';
        }
        
        $user->delete();

        return back()->withStatus($statusMessage);
    }

    public function changeTheme()
    {
        $user = Auth::user();
        $user->theme = $this->request->theme;
        $user->save();

        return [
            'success' => true,
            'newTheme' => asset('assets/css/themes/' . $user->theme . '.css'),
            'hex' => getThemePrimaryColor()[$user->theme]
        ];
    }

}
