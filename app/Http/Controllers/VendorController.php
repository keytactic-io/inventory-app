<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
use App\Models\Vendor;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\SaveVendorRequest;

class VendorController extends Controller
{
    
    protected $request;

    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }

    public function getVendors()
    {
        if (!Auth::user()->allowedTo('view_vendors')) {
            abort(403);
        }

        $vendors = Vendor::orderBy('name')->get();

        return view('pages.vendors.list', compact(
            'vendors'
        ));
    }

    public function viewVendor($vendorId)
    {
        if (!Auth::user()->allowedTo('view_vendors')) {
            abort(403);
        }

        $vendor = Vendor::findOrFail($vendorId);
        $purchaseOrders = $vendor->purchaseOrders;

        return view('pages.vendors.details', compact(
            'vendor', 'purchaseOrders'
        ));
    }

    public function addVendor()
    {
        if (!Auth::user()->allowedTo('add_vendors')) {
            abort(403);
        }

        return view('pages.vendors.add');
    }

    public function addVendorSave(SaveVendorRequest $request)
    {
        if (!Auth::user()->allowedTo('add_vendors')) {
            abort(403);
        }

        $vendor = Vendor::create($request->except(['_token', 'photo']));

        if ($request->hasFile('photo')) {

            $destinationPath = 'uploads/vendors';
            $extension = $request->file('photo')->getClientOriginalExtension();
            $fileName = str_slug($vendor->name) . '.' . $extension;
            $request->file('photo')->move($destinationPath, $fileName);

            $vendor->photo = $fileName;
            $vendor->save();

        }

        return redirect()->route('vendors.list')
            ->withStatus($vendor->name . ' successfully added.');
    }

    public function editVendor($vendorId)
    {
        $vendor = Vendor::findOrFail($vendorId);

        if (!Auth::user()->allowedTo('edit_vendors')) {
            abort(403);
        }

        return view('pages.vendors.edit', compact(
            'vendor'
        ));
    }

    public function editVendorSave(SaveVendorRequest $request, $vendorId)
    {
        $vendor = Vendor::findOrFail($vendorId);

        if (!Auth::user()->allowedTo('edit_vendors')) {
            abort(403);
        }

        $vendor->update($request->except(['_token', 'supplier_id']));

        return redirect()->route('vendors.details', ['vendorId' => $vendor->id])
            ->withStatus($vendor->name . ' successfully updated.');
    }

    public function deleteVendor($vendorId)
    {
        $vendor = Vendor::findOrFail($vendorId);

        if (!Auth::user()->allowedTo('delete_vendors')) {
            abort(403);
        }

        $name = $vendor->name;
        $vendor->delete();

        return redirect()->route('vendors.list')
            ->withStatus($name . ' successfully removed.');
    }

    public function addVendorModal()
    {
        $response = [
            'title' => 'Add Vendor',
            'view' => view('pages.vendors.add-modal')->render()
        ];

        return json_encode($response);
    }

    public function addVendorModalSave()
    {
        $validator = Validator::make($this->request->all(), [
            'name' => 'required|unique:vendors',
            'street_1' => 'required',
            'city' => 'required',
            'state' => 'required',
            'country' => 'required',
            'postcode' => 'required',
            'contact_person' => 'required',
            'mobile_number' => 'required'
        ]);

        if ($validator->fails()) {
            $errors = $validator->messages();
            return [
                'error' => view('includes.errors', compact(
                    'errors'))->render()
            ];
        }

        $vendor = Vendor::create($this->request->except(['_token']));

        return [
            'done' => true,
            'vendor' => [
                'id' => $vendor->id,
                'name' => $vendor->name
            ]
        ];
    }

}
