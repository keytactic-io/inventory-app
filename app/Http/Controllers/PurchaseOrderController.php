<?php

namespace App\Http\Controllers;

use Auth;
use Mail;
use Input;
use Session;
use Validator;
use Carbon\Carbon;
use App\Http\Requests;
use App\Models\Vendor;
use App\Models\Product;
use App\Models\Setting;
use App\Models\Location;
use App\Models\CustomField;
use Illuminate\Http\Request;
use App\Models\MailRecipient;
use App\Models\PurchaseOrder;
use App\Models\OrderedProduct;
use App\Models\ProductPriceLog;
use App\Models\CustomFieldValue;
use App\Models\PurchaseOrderFile;
use App\Mail\PurchaseOrderCreated;
use App\Mail\PurchaseOrderReceived;
use App\Jobs\ArchiveRemainingStocks;
use App\Models\OrderedProductsLocation;

class PurchaseOrderController extends Controller
{

    private $request;
    private $y;

    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;

        $this->y = Carbon::now()->format('y');
    }

    public function getPurchaseOrders()
    {
        if (!Auth::user()->allowedTo('view_purchase_orders')) {
            abort(403);
        }

        $poAll = PurchaseOrder::all();
        $poAll->load('processedBy', 'vendor');

        $poWaiting = PurchaseOrder::query()
            ->whereNull('received')
            ->get();
        $poWaiting->load('processedBy', 'vendor');
        
        $poReceived = PurchaseOrder::query()
            ->where('received', 1)
            ->get();
        $poReceived->load('processedBy', 'vendor');

        return view('pages.purchase-orders.list', compact(
            'poAll', 'poWaiting', 'poReceived'
        ));
    }

    public function previewPurchaseOrder($purchaseOrderId)
    {
        if (!Auth::user()->allowedTo('view_purchase_orders')) {
            abort(403);
        }

        $purchaseOrder = PurchaseOrder::findOrFail($purchaseOrderId);
        $vendor = $purchaseOrder->vendor;
        $orderedProducts = $purchaseOrder->orderedProducts;

        $response = [
            'view' => view('pages.purchase-orders.preview', compact(
                'purchaseOrder', 'vendor', 'orderedProducts'))->render(),
            'title' => $purchaseOrder->uid()
        ];

        return json_encode($response);
    }

    public function addPurchaseOrder()
    {
        if (!Auth::user()->allowedTo('add_purchase_orders')) {
            abort(403);
        }

        $products = Product::query()
            ->orderBy('name')
            ->get();

        $vendors = Vendor::query()
            ->orderBy('name')
            ->get();

        $locations = Location::query()
            ->orderBy('name')
            ->get();

        $customFields = CustomField::purchaseOrders()->get();

        return view('pages.purchase-orders.add', compact(
            'products', 'vendors', 'locations', 'customFields'
        ));
    }

    public function addPurchaseOrderSave()
    {
        if (!Auth::user()->allowedTo('add_purchase_orders')) {
            abort(403);
        }

        $this->validate($this->request, [
            'vendor_id' => 'required|integer',
            'location_id' => 'required|integer',
            'products.*.price' => 'required_with:products.*.quantity',
            'delivery_date' => 'required',
            'shipping_method' => 'required',
            'shipping_terms' => 'required'
        ]);

        $orderedProducts = $this->request->products;
        if (count($orderedProducts) == 0) {
            Session::flash('error', 'Please select at least one product');
            return back()
                ->withInput();
        }

        $purchaseOrder = PurchaseOrder::create([
            'vendor_id' => $this->request->vendor_id,
            'processed_by' => Auth::user()->id,
            'ship_to_location' => $this->request->location_id,
            'delivery_date' => Carbon::parse($this->request->delivery_date),
            'shipping_method' => $this->request->shipping_method,
            'shipping_terms' => $this->request->shipping_terms,
            'notes' => $this->request->notes
        ]);

        $orderedProductsData = [];
        $productIds = [];
        foreach ($orderedProducts as $id => $product) {
            array_push($orderedProductsData, [
                'purchase_order_id' => $purchaseOrder->id,
                'product_id' => $id,
                'quantity' => $product['quantity'],
                'price' => $product['price'],
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ]);
            $productIds[] = $id;
        }
        OrderedProduct::insert($orderedProductsData);

        // save custom fields
        if (count($this->request->custom_fields) > 0) {
            foreach ($this->request->custom_fields as $id => $value) {
                if ($value != '') {
                    CustomFieldValue::create([
                        'custom_field_id' => $id,
                        'reference_id' => $purchaseOrder->id,
                        'value' => $value
                    ]);
                }
            }
        }

        // create pdf
        $version = 'original';
        $timestamp = Carbon::parse($purchaseOrder->created_at)->timestamp;
        $filename = $purchaseOrder->uid();
        $html = view('pages.purchase-orders.pdf', compact(
            'purchaseOrder', 'filename', 'version'
        ))->render();
        toPdf($html, $filename, true);
        // end create pdf

        if (isset($this->request->notify_accounting) AND $this->request->notify_accounting == 1) {
            $purchaseOrder->with_notification = 1;

            $accountingEmails = Setting::where('key', 'accounting_email')->first();
            $emails = [];
            if ($accountingEmails) {
                $emails = explode(',', $accountingEmails->value);
            }
            if (count($emails) > 0) {
                foreach ($emails as $email) {
                    Mail::to($email)
                        ->send(new PurchaseOrderCreated($purchaseOrder, true));
                }                
            }            

        }

        // delete pdf
        unlink(public_path() . '/temp/' . $filename . '.pdf');

        $purchaseOrder->save();

        dispatch(new ArchiveRemainingStocks(
            Product::whereIn('id', $productIds)->get()
        ));

        return redirect()->route('pos.list')
            ->withStatus($purchaseOrder->uid() . ' successfully created.');
    }

    public function editPurchaseOrder($purchaseOrderId)
    {
        if (!Auth::user()->allowedTo('edit_purchase_orders')) {
            abort(403);
        }

        $purchaseOrder = PurchaseOrder::findOrFail($purchaseOrderId);
        if ($purchaseOrder->received) {
            abort(403);
        }

        $settings = Setting::where('key', 'allow_update_po')->first();
        if ($settings AND $settings->value == 0) {
            abort(404);
        }

        $products = Product::query()
            ->orderBy('name')
            ->get();

        $vendors = Vendor::query()
            ->orderBy('name')
            ->get();

        $locations = Location::query()
            ->orderBy('name')
            ->get();

        $customFields = CustomField::purchaseOrders()->get();

        return view('pages.purchase-orders.edit', compact(
            'purchaseOrder', 'products', 'vendors', 'locations', 'customFields'
        ));
    }

    public function editPurchaseOrderSave($purchaseOrderId)
    {
        if (!Auth::user()->allowedTo('edit_purchase_orders')) {
            abort(403);
        }

        $purchaseOrder = PurchaseOrder::findOrFail($purchaseOrderId);
        if ($purchaseOrder->received) {
            abort(403);
        }

        $this->validate($this->request, [
            'vendor_id' => 'required|integer',
            'location_id' => 'required|integer',
            'products.*.price' => 'required_with:products.*.quantity',
            'delivery_date' => 'required',
            'shipping_method' => 'required',
            'shipping_terms' => 'required'
        ]);

        $orderedProducts = $this->request->products;
        if (count($orderedProducts) == 0) {
            Session::flash('error', 'Please select at least one (1) product');
            return back()->withInput();
        }

        $purchaseOrder->update([
            'vendor_id' => $this->request->vendor_id,
            'processed_by' => Auth::user()->id,
            'ship_to_location' => $this->request->location_id,
            'delivery_date' => Carbon::parse($this->request->delivery_date),
            'shipping_method' => $this->request->shipping_method,
            'shipping_terms' => $this->request->shipping_terms,
            'notes' => $this->request->notes
        ]);

        $purchaseOrder->orderedProducts()->delete();
        $orderedProductsData = [];
        $productIds = [];
        foreach ($orderedProducts as $id => $product) {
            array_push($orderedProductsData, [
                'purchase_order_id' => $purchaseOrder->id,
                'product_id' => $id,
                'quantity' => $product['quantity'],
                'price' => $product['price'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
            $productIds[] = $id;
        }
        OrderedProduct::insert($orderedProductsData);

        // delete custom fields values
        foreach ($purchaseOrder->customFields() as $customField) {
            $customField->delete();
        }

        // save custom fields values
        if (count($this->request->custom_fields) > 0) {
            foreach ($this->request->custom_fields as $id => $value) {
                if ($value != '') {
                    CustomFieldValue::create([
                        'custom_field_id' => $id,
                        'reference_id' => $purchaseOrder->id,
                        'value' => $value
                    ]);
                }
            }
        }

        dispatch(new ArchiveRemainingStocks(
            Product::whereIn('id', $productIds)->get()
        ));

        return redirect()->route('pos.list')
            ->withStatus($purchaseOrder->uid() . ' successfully updated.');
    }

    public function deletePurchaseOrder($purchaseOrderId)
    {
        if (!Auth::user()->allowedTo('delete_purchase_orders')) {
            abort(403);
        }

        $purchaseOrder = PurchaseOrder::findOrFail($purchaseOrderId);

        if ($purchaseOrder->received) {
            abort(403);
        }

        $productIds = $purchaseOrder->orderedProducts()
            ->pluck('product_id')->toArray();
        $purchaseOrder->orderedProducts()->delete();
        $purchaseOrder->attachments()->delete();
        $uid = $purchaseOrder->uid();
        $purchaseOrder->delete();

        dispatch(new ArchiveRemainingStocks(
            Product::whereIn('id', $productIds)->get()
        ));

        return redirect()->route('pos.list')
            ->withStatus($uid . ' successfully removed.');
    }

    public function receivePurchaseOrder($purchaseOrderId)
    {
        if (!Auth::user()->allowedTo('receive_purchase_orders')) {
            abort(403);
        }

        $purchaseOrder = PurchaseOrder::findOrFail($purchaseOrderId);
        $orderedProducts = $purchaseOrder->orderedProducts;

        $locations = Location::query()
            ->orderBy('name')
            ->get();

        return view('pages.purchase-orders.receive', compact(
            'purchaseOrder', 'orderedProducts', 'locations'
        ));
    }

    public function receivePurchaseOrderSave($purchaseOrderId)
    {
        if (!Auth::user()->allowedTo('receive_purchase_orders')) {
            abort(403);
        }

        $this->validate($this->request, [
            'products.*.received_quantity' => 'required|numeric',
            'products.*.price' => 'required|numeric',
        ]);

        $purchaseOrder = PurchaseOrder::findOrFail($purchaseOrderId);

        foreach ($this->request->ordered_products as $orderedProductId => $productInfo):

            // record received products
            $orderedProduct = OrderedProduct::findOrFail($orderedProductId);
            OrderedProductsLocation::create([
                'ordered_product_id' => $orderedProduct->id,
                'received_quantity' => $productInfo['received_quantity'],
                'location_id' => $purchaseOrder->ship_to_location
            ]);

            // determine price change
            $lastPrice = ProductPriceLog::where('product_id', $orderedProduct->product_id)
                ->orderBy('id', 'desc')
                ->first();
            if (!$lastPrice OR $lastPrice->price != $productInfo['price']) {
                ProductPriceLog::create([
                    'product_id' => $orderedProduct->product_id,
                    'price' => $productInfo['price']
                ]);
            }

        endforeach; 

        $purchaseOrder->update([
            'received' => 1,
            'received_at' => Carbon::now(),
            'received_by' => Auth::user()->id
        ]);

        // save files
        $filesToAttach = [];
        if ($this->request->hasFile('attachments')) {
            $destinationPath = 'uploads/attachments';
            foreach ($this->request->attachments as $attachment) {
                if ($attachment) {
                    $extension = $attachment->getClientOriginalExtension();
                    $explodeFilename = explode('.', $attachment->getClientOriginalName());
                    $filename = str_slug($explodeFilename[0], '-') . '.' . $extension;
                    $filesToAttach[] = $filename;
                    $attachment->move($destinationPath, $filename);

                    PurchaseOrderFile::create([
                        'purchase_order_id' => $purchaseOrder->id,
                        'filename' => $filename
                    ]);
                }
            }
        }

        // notify accounting if with_notification is 1 or true
        if ($this->request->notify_accounting) {

            $accountingEmails = Setting::where('key', 'accounting_email')->first();
            $emails = [];
            if ($accountingEmails) {
                $emails = explode(',', $accountingEmails->value);
            }

            if (count($emails) > 0) {
                foreach ($emails as $email) {
                    Mail::to($email)
                        ->queue(new PurchaseOrderReceived($purchaseOrder, $filesToAttach));
                }
            }

        }

        $productIds = $purchaseOrder->orderedProducts()
            ->pluck('product_id')->toArray();
        dispatch(new ArchiveRemainingStocks(
            Product::whereIn('id', $productIds)->get()
        ));

        return redirect()->route('pos.list')
            ->withStatus($purchaseOrder->uid() . ' has been received.');
    }

    public function pdfView($purchaseOrderId, $version = 'original')
    {
        if (!Auth::user()->allowedTo('view_purchase_orders')) {
            abort(403);
        }

        if ($version != 'original' AND $version != 'updated') {
            abort(404);
        }

        $purchaseOrder = PurchaseOrder::findOrFail($purchaseOrderId);

        $timestamp = Carbon::parse($purchaseOrder->created_at)->timestamp;
        $filename = $purchaseOrder->uid();
        $html = view('pages.purchase-orders.pdf', compact(
            'purchaseOrder', 'filename', 'version'
        ))->render();

        return toPdf($html, $filename, false);
    }

    public function emailToVendor($purchaseOrderId)
    {
        if (!Auth::user()->allowedTo('add_purchase_orders')) {
            abort(403);
        }

        $email = Input::has('email') ? Input::get('email') : $purchaseOrder->vendor->email;

        $purchaseOrder = PurchaseOrder::findOrFail($purchaseOrderId);
        $recipients = MailRecipient::all();

        return [
            'title' => 'Email Purchase Order',
            'view' => view('pages.purchase-orders.email', compact(
                'purchaseOrder', 'recipients', 'email'
            ))->render()
        ];
    }

    public function emailToVendorSend($purchaseOrderId)
    {
        if (!Auth::user()->allowedTo('add_purchase_orders')) {
            abort(403);
        }

        $validator = Validator::make($this->request->all(), [
            'mail_recipient' => 'required|email'
        ]);

        if ($validator->fails()) {
            $errors = $validator->messages();
            return [
                'error' => view('includes.errors', compact(
                    'errors'))->render()
            ];
        }

        $purchaseOrder = PurchaseOrder::findOrFail($purchaseOrderId);

        $version = 'original';
        $timestamp = Carbon::parse($purchaseOrder->created_at)->timestamp;
        $filename = $purchaseOrder->uid();
        $html = view('pages.purchase-orders.pdf', compact(
            'purchaseOrder', 'filename', 'version'
        ))->render();

        toPdf($html, $filename, true);

        if ($this->request->cc_emails != '') {
            $ccEmails = explode(',', $this->request->cc_emails);
        }

        Mail::to($this->request->mail_recipient)
            ->cc($ccEmails)
            ->queue(new PurchaseOrderCreated($purchaseOrder));

        unlink(public_path() . '/temp/' . $filename . '.pdf');

        Session::flash('status', $purchaseOrder->uid() . ' has been sent to the vendor.');
        return [
            'success' => true,
            'redirect' => route('pos.list')
        ];
    }

    public function createForSpecificProduct($productId)
    {
        $product = Product::findOrFail($productId);
        $vendor = $product->defaultVendor;
        $locations = Location::orderBy('name')->get();

        return view('pages.purchase-orders.add-specific', compact(
            'product', 'vendor', 'locations'
        ));
    }

    public function createForSpecificProductSave($productId)
    {
        $this->validate($this->request, [
            'vendor_id' => 'required|integer',
            'product_id' => 'required|integer',
            'quantity' => 'required|numeric',
            'price' => 'required|numeric',
            'delivery_date' => 'required',
            'shipping_method' => 'required',
            'shipping_terms' => 'required'
        ]);

        if ($this->request->price == 0) {
            Session::flash('error', 'Price must be greater than 0.');
            return back()
                ->withInput();
        }

        $product = Product::findOrFail($this->request->product_id);

        $purchaseOrder = PurchaseOrder::create([
            'vendor_id' => $this->request->vendor_id,
            'processed_by' => Auth::user()->id,
            'ship_to_location' => $this->request->location_id,
            'delivery_date' => Carbon::parse($this->request->delivery_date),
            'shipping_method' => $this->request->shipping_method,
            'shipping_terms' => $this->request->shipping_terms,
            'notes' => $this->request->notes
        ]);

        $orderedProduct = OrderedProduct::create([
           'purchase_order_id' => $purchaseOrder->id,
           'product_id' => $product->id,
           'quantity' => $this->request->quantity,
           'price' => $this->request->price
        ]);

        dispatch(new ArchiveRemainingStocks(
            Product::whereIn('id', [$product->id])->get()
        ));

        return redirect()->route('products.details', ['productId' => $product->id])
            ->withStatus('Purchase orderer (' . $purchaseOrder->uid() . ') for this product successfully created.');
    }

    public function addFile($purchaseOrderId)
    {
        $validator = Validator::make($this->request->all(), [
            'attachment' => 'required'
        ]);

        if ($validator->fails()) {
            $errors = $validator->messages();
            return [
                'error' => view('includes.errors', compact(
                    'errors'))->render()
            ];
        }

        $purchaseOrder = PurchaseOrder::findOrFail($purchaseOrderId);

        if ($this->request->hasFile('attachment')) {
            $destinationPath = 'uploads/attachments';

            $extension = $this->request->attachment->getClientOriginalExtension();
            $explodeFilename = explode('.', $this->request->attachment->getClientOriginalName());
            $filename = str_slug($explodeFilename[0], '-') . '.' . $extension;
            $this->request->attachment->move($destinationPath, $filename);

            $newfile = PurchaseOrderFile::create([
                'purchase_order_id' => $purchaseOrder->id,
                'filename' => $filename
            ]);

        }

        return [
            'success' => true,
            'file' => [
                'id' => $newfile->id,
                'filename' => $newfile->filename,
                'fileUrl' => $newfile->getUrl(),
                'date' => processDate($newfile->created_at),
                'deleteUrl' => route('pos.deletefile') . '/' . $newfile->id
            ]
        ];
    }

    public function deleteFile($fileId)
    {
        $file = PurchaseOrderFile::findOrFail($fileId);
        $id = $file->id;
        $file->delete();

        return [
            'success' => true,
            'id' => $id
        ];
    }

}
