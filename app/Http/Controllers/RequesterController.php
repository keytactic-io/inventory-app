<?php

namespace App\Http\Controllers;

use Auth;
use Mail;
use Input;
use Session;
use Validator;
use App\Models\User;
use App\Http\Requests;
use App\Models\Market;
use App\Models\Product;
use App\Models\Requester;
use Illuminate\Http\Request;
use App\Mail\InviteRequester;
use App\Models\RequesterAddress;
use App\Models\ProductAllocation;
use App\Http\Requests\SaveRequesterRequest;

class RequesterController extends Controller
{
    
    private $request;

    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }

    public function previewRequester($requesterId)
    {
        if (!Auth::user()->allowedTo('view_requesters')) {
            abort(403);
        }

        $requester = Requester::findOrFail($requesterId);

        return json_encode([
            'view' => view('pages.requesters.preview', compact(
                'requester'
            ))->render()
        ]);
    }

    public function getRequesters()
    {
        if (!Auth::user()->allowedTo('view_requesters')) {
            abort(403);
        }

        $requesters = Requester::query()
            ->where('registered', 1)
            ->get();

        return view('pages.requesters.list', compact(
            'requesters'
        ));
    }

    public function addRequester()
    {
        $markets = Market::orderBy('name')->get();

        return view('pages.requesters.add', compact(
            'markets'
        ));
    }

    public function addRequesterSave(SaveRequesterRequest $request)
    {
        if (!Auth::user()->allowedTo('add_requesters')) {
            abort(403);
        }

        $requesterAddresses = $request->addresses;
        if (count($requesterAddresses) < 1) {
            Session::flass('error', 'Please enter at least on address.');
            return back();
        }

        // create user
        $user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'role_id' => 0,
            'is_requester' => 1,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);
        
        // create requester
        $requester = Requester::create([
            'user_id' => $user->id,
            'market_id' => $request->market_id,
            'contact_number' => $request->contact_number
        ]);
        
        foreach ($requesterAddresses as $key => $address) {
            RequesterAddress::create([
                'requester_id' => $requester->id,
                'street_1' => $address['street_1'],
                'street_2' => $address['street_2'],
                'city' => $address['city'],
                'state' => $address['state'],
                'country' => $address['country'],
                'postcode' => $address['postcode'],
                'default' => $request->default == $key ? 1 : 0
            ]);
        }

        $products = Product::all();
        foreach ($products as $product) {
            ProductAllocation::create([
                'requester_id' => $requester->id,
                'product_id' => $product->id,
                'allocation' => -1 // all products = infinite
            ]);
        }

        // return redirect()->route('requesters.list')
        return redirect()->route('requesters.productallocation', ['requesterId' => $requester->id])
            ->withStatus($user->name() . ' successfully added as requester.');
    }

    public function editRequester($requesterId)
    {
        if (!Auth::user()->allowedTo('edit_requesters')) {
            abort(403);
        }

        $requester = Requester::findOrFail($requesterId);

        return view('pages.requesters.edit', compact(
            'requester'
        ));
    }

    public function editRequesterSave(SaveRequesterRequest $request, $requesterId)
    {
        if (!Auth::user()->allowedTo('edit_requesters')) {
            abort(403);
        }

        $requester = Requester::findOrFail($requesterId);
        
        // update account
        $requester->user->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email
        ]);

        if ($request->password != '') {
            $requester->user->password = bcrypt($request->password);
            $requester->user->save();
        }

        // update requester
        $requester->update([
            'address' => $request->address,
            'contact_number' => $request->contact_number
        ]);

        return redirect()->route('requesters.list')
            ->withStatus($requester->name() . ' successfully updated.');
    }

    public function deleteRequester($requesterId)
    {
        if (!Auth::user()->allowedTo('delete_requesters')) {
            abort(403);
        }

        $requester = Requester::findOrFail($requesterId);
        $name = $requester->name();
        $requester->delete();

        return redirect()->route('requesters.list')
            ->withStatus($name . ' successfully removed as requester.');
    }

    public function inviteRequester()
    {
        if (!Auth::user()->allowedTo('add_requesters')) {
            abort(403);
        }

        $markets = Market::orderBy('name')->get();

        return view('pages.requesters.invite', compact(
            'markets'
        ));
    }

    public function inviteRequesterSave()
    {
        if (!Auth::user()->allowedTo('add_requesters')) {
            abort(403);
        }

        $this->validate($this->request, [
            'email' => 'required|email|unique:users',
            'market_id' => 'required|integer'
        ]);

        // check email availability
        $user = User::where('email', $this->request->email)->first();
        if ($user) {
            if ($user->requester AND !$user->requester->registered) {

                Session::flash('error', 'Invite already sent.');
                return redirect($this->request->url())
                    ->withInput();

            } else {

                Session::flash('error', 'Email already in-use.');
                return redirect($this->request->url())
                    ->withInput();

            }
        } else {

            // create user account    
            $user = User::create([
                'role_id' => 0,
                'email' => $this->request->email,
                'password' => str_random(32),
                'is_active' => 0,
                'is_requester' => 1
            ]);

            // create requester
            $requester = Requester::create([
                'user_id' => $user->id,
                'market_id' => $this->request->market_id,
                'registered' => 0,
                'invite_key' => $user->password
            ]);

            // send email
            Mail::to($user->email)
                ->send(new InviteRequester($requester));

        }

        return redirect()->route('requesters.invite')
            ->withStatus('Invite sent.');        
    }

    public function getAddressesAndMarket($requesterId)
    {
        $requester = Requester::findOrFail($requesterId);

        $addresses = [];
        $count = 0;
        foreach ($requester->addresses as $address) {
            $addressData = [
                'id' => $address->id,
                'address' => $address->getAddress(),
                'default' => $address->default
            ];
            $addresses[$count] = $addressData;
            $count++;
        }

        $ap = [];
        $products = null;
        $count = 0;
        $allocatedProducts = $requester->productAllocations;
        if (count($allocatedProducts) > 0) {
            foreach ($allocatedProducts as $allocatedProduct) {
                array_push($ap, $allocatedProduct->product_id);
            }
            $prods = Product::whereIn('id', $ap)->get();
            foreach ($prods as $prod) {
                $products[$count] = [
                    'id' => $prod->id,
                    'name' => $prod->name . ' (' . $prod->stocksRemaining() . ')',
                    'allocation' => $prod->hasAllocation($requester->id),
                    'remaining' => $prod->stocksRemaining()
                ];
                $count++;
            }
        }

        $response = [
            'addresses' => $addresses,
            'market' => $requester->market_id,
            'products' => $products
        ];

        return json_encode($response);
    }

    public function addAddress($requesterId)
    {
        $requester = Requester::findOrFail($requesterId);

        return json_encode([
            'view' => view('pages.requesters.address.add', compact(
                'requester'))->render(),
            'title' => 'Add Address'
        ]);
    }

    public function addAddressSave()
    {
        $validator = Validator::make($this->request->all(), [
            'requester_id' => 'required|integer',
            'street_1' => 'required',
            'city' => 'required',
            'state' => 'required',
            'country' => 'required',
            'postcode' => 'required'
        ]);

        if ($validator->fails()) {
            $errors = $validator->messages();
            return [
                'error' => view('includes.errors', compact(
                    'errors'))->render()
            ];
        }

        $requester = Requester::findOrFail($this->request->requester_id);

        $newAddress = RequesterAddress::create([
            'requester_id' => $requester->id,
            'street_1' => $this->request->street_1,
            'street_2' => $this->request->street_2 != '' ? $this->request->street_2 : null,
            'city' => $this->request->city,
            'state' => $this->request->state,
            'country' => $this->request->country,
            'postcode' => $this->request->postcode
        ]);

        if ($this->request->default == 1) {
            $allAddresses = $newAddress->requester->addresses;
            foreach ($allAddresses as $address) {
                $address->default = 0;
                $address->save();
            }
            $newAddress->default = 1;
            $newAddress->save();
        }        

        return [
            'done' => true,
            'redirect' => route('users.profile') . '/' . $requester->user->id
        ];

    }

    public function editAddress($addressId)
    {
        $address = RequesterAddress::findOrFail($addressId);
        $address->load('requester');

        return json_encode([
            'view' => view('pages.requesters.address.edit', compact(
                'address'))->render(),
            'title' => 'Edit Address'
        ]);
    }

    public function editAddressSave()
    {
        $validator = Validator::make($this->request->all(), [
            'requester_id' => 'required|integer',
            'address_id' => 'required|integer',
            'street_1' => 'required',
            'city' => 'required',
            'state' => 'required',
            'country' => 'required',
            'postcode' => 'required'
        ]);

        if ($validator->fails()) {
            $errors = $validator->messages();
            return [
                'error' => view('includes.errors', compact(
                    'errors'))->render()
            ];
        }

        $requester = Requester::findOrFail($this->request->requester_id);
        $address = RequesterAddress::findOrFail($this->request->address_id);
        $address->update([
            'requester_id' => $requester->id,
            'street_1' => $this->request->street_1,
            'street_2' => $this->request->street_2 != '' ? $this->request->street_2 : null,
            'city' => $this->request->city,
            'state' => $this->request->state,
            'country' => $this->request->country,
            'postcode' => $this->request->postcode
        ]);

        return [
            'done' => true,
            'redirect' => route('users.profile') . '/' . $requester->user->id
        ];

    }

    public function removeAddress($addressId)
    {
        $address = RequesterAddress::findOrFail($addressId);
        $newId = '';
        if ($address->default) {
            $otherAddress = $address->requester->addresses()->where('default', 0)->first();
            if ($otherAddress) {
                $otherAddress->default = 1;
                $otherAddress->save();
                $newId = $otherAddress->id;
            }            
        }
        $address->delete();

        return json_encode([
            'done' => true, 
            'id' => $addressId,
            'new' => $newId
        ]);
    }

    public function setDefaultAddress()
    {
        $newDefaultAddress = RequesterAddress::findOrFail($this->request->address_id);
        $addresses = $newDefaultAddress->requester->addresses;
        foreach ($addresses as $address) {
            $address->default = 0;
            $address->save();
        }
        $newDefaultAddress->default = 1;
        $newDefaultAddress->save();

        return [
            'done' => true,
            'new' => $newDefaultAddress->id
        ];
    }

    public function productAllocation($requesterId)
    {
        if (!Auth::user()->allowedTo('edit_requesters')) {
            if (!Auth::user()->is_requester) {
                abort(403);
            }            
        }

        $requester = Requester::findOrFail($requesterId);
        $products = Product::orderBy('name')->get();

        return view('pages.requesters.product-allocation', compact(
            'requester', 'products'
        ));
    }

    public function productAllocationSave($requesterId)
    {
        if (!Auth::user()->allowedTo('edit_requesters')) {
            abort(403);
        }

        $requester = Requester::findOrFail($requesterId);
        $requester->productAllocations()->delete();

        foreach ($this->request->allocations as $productId => $allocation) {
            if ($allocation != 0) {
                ProductAllocation::create([
                    'requester_id' => $requester->id,
                    'product_id' => $productId,
                    'allocation' => $allocation < -1 ? -1 : $allocation
                ]);
            }
        }

        return redirect()->route('users.profile', ['userId' => $requester->user->id])
            ->withStatus('Successfully allocated products for ' . 
                $requester->user->first_name . '.');
    }

    public function sentInvites()
    {
        if (!Auth::user()->allowedTo('add_requesters')) {
            abort(403);
        }
        
        $requesters = Requester::where('registered', 0)->get();

        return view('pages.requesters.invites.list', compact(
            'requesters'
        ));
    }

}
