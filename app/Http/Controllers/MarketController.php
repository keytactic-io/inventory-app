<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\Market;
use App\Http\Requests;
use App\Models\Requester;
use Illuminate\Http\Request;

class MarketController extends Controller
{
    
    private $request;

    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }

    public function getMarkets()
    {
        if (
            !Auth::user()->allowedTo('view_markets') AND 
            !Auth::user()->is_requester
        ) {
            abort(403);
        }

        $markets = Market::orderBy('name')->get();

        return view('pages.markets.list', compact(
            'markets'
        ));
    }

    public function getMarketDetails($marketId)
    {
        if (
            !Auth::user()->allowedTo('view_markets') AND 
            !Auth::user()->is_requester
        ) {
            abort(403);
        }
        
        $market = Market::findOrFail($marketId);
        return view('pages.markets.details', compact('market'));
    }

    public function addMarket()
    {
        if (!Auth::user()->allowedTo('add_markets')) {
            abort(403);
        }

        $requesters = Requester::all();

        return view('pages.markets.add', compact(
            'requesters'
        ));
    }

    public function addMarketSave()
    {
        if (!Auth::user()->allowedTo('add_markets')) {
            abort(403);
        }

        $this->validate($this->request, [
            'name' => 'required'
        ]);

        $market = Market::create($this->request->except(['_token', 'requesters']));

        if (count($this->request->requesters) > 0) {
            $requesters = $this->request->requesters;
            foreach ($requesters as $requester) {
                $requester = Requester::findOrFail($requester);
                $requester->market_id = $market->id;
                $requester->save();
            }
        }

        return redirect()->route('markets.list')
            ->withStatus($market->name . ' successfully added.');
    }

    public function editMarket($marketId)
    {
        if (!Auth::user()->allowedTo('edit_markets')) {
            abort(403);
        }

        $market = Market::findOrFail($marketId);

        return view('pages.markets.edit', compact(
            'market'
        ));
    }

    public function editMarketSave($marketId)
    {
        if (!Auth::user()->allowedTo('edit_markets')) {
            abort(403);
        }

        $market = Market::findOrFail($marketId);
        $market->update($this->request->except(['_token']));

        return redirect()->route('markets.list')
            ->withStatus($market->name . ' successfully udpated.');
    }

    public function deleteMarket($marketId)
    {
        if (!Auth::user()->allowedTo('delete_markets')) {
            abort(403);
        }

        $market = Market::findOrFail($marketId);
        $name = $market->name;
        $market->delete();

        return redirect()->route('markets.list')
            ->withStatus($name . ' successfully removed.');
    }

}
