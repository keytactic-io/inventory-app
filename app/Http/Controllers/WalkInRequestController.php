<?php

namespace App\Http\Controllers;

use DB;
use Input;
use Session;
use App\Http\Requests;
use App\Models\Product;
use App\Models\Location;
use Illuminate\Http\Request;
use App\Models\WalkInRequest;
use App\Models\WalkInRequestProduct;

class WalkInRequestController extends Controller
{

    public function __construct()
    {

    }

    public function walkInRequest($locationId)
    {
        $location = Location::findOrFail($locationId);
        $ps = collect(DB::select(
            DB::raw("
                SELECT opening_balances.product_id as id FROM opening_balances 
                    WHERE opening_balances.location_id = '$location->id'
                UNION
                SELECT ordered_products.product_id as id FROM ordered_products 
                    RIGHT JOIN ordered_products_locations 
                    ON ordered_products_locations.ordered_product_id = ordered_products.id
                    WHERE ordered_products_locations.location_id = '$location->id'
                UNION
                SELECT product_transfers.product_id as id FROM product_transfers
                    WHERE product_transfers.to = '$location->id'
                GROUP BY product_id
            ")
        ));
        $ids = [];
        foreach ($ps as $product) {
            array_push($ids, $product->id);
        }

        $products = Product::whereIn('id', $ids)
            ->orderBy('name')
            ->get();

        return view('pages.wrq.walkin', compact(
            'location', 'products'
        ));
    }

    public function walkInRequestSave($locationId, Request $request)
    {
        $location = Location::findOrFail($locationId);

        $this->validate($request, [
            'full_name' => 'required',
            'products.*.quantity' => 'required_if:products.*.checked,1|numeric'
        ]);

        $walkInRequest = WalkInRequest::create([
            'location_id' => $location->id,
            'requester_name' => $request->full_name,
            'notes' => $request->notes == '' ? null : $request->notes,
            // released by default - 2017-0523
            'is_released' => 1,
        ]);

        foreach ($request->products as $productId => $product) {
            if (is_numeric($product['quantity'])) {
                WalkInRequestProduct::create([
                    'walk_in_request_id' => $walkInRequest->id,
                    'product_id' => $productId,
                    'quantity' => $product['quantity'],
                    // allow all by default - 2017-0523
                    'allowed_quantity' => $product['quantity'],
                ]);
            }
        }

        Session::flash('wrq', $walkInRequest);

        return redirect()
            ->route('request.walkin.thanks', ['locationId' => $location->id])
            ->withStatus('Request successfully submitted.');
    }

    public function walkInRequestThanks($locationId)
    {
        if (!Session::has('wrq')) {
            return redirect()->route('request.walkin', ['locationId' => $locationId]);
        }

        $walkInRequest = Session::get('wrq');

        return view('pages.wrq.wrq-thanks', compact(
            'walkInRequest'
        ));
    }

    public function switchLocation()
    {
        $locations = Location::all();

        return view('pages.wrq.switch-location', compact(
            'locations'
        ));
    }

}
