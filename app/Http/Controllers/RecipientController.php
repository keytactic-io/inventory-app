<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\MailRecipient;

class RecipientController extends Controller
{

    private $request;
    
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }

    public function getRecipients()
    {
        $recipients = MailRecipient::all();

        return view('pages.recipients.list', compact(
            'recipients'
        ));
    }

    public function addRecipient()
    {
        return [
            'title' => 'Add Recipient',
            'btnlabel' => 'Submit',
            'view' => view('pages.recipients.add')->render()
        ];
    }

    public function addRecipientSave()
    {
        $validator = Validator::make($this->request->all(), [
            'label' => 'required|unique:mail_recipients',
            'email' => 'required|unique:mail_recipients|email'
        ]);

        if ($validator->fails()) {
            $errors = $validator->messages();
            return [
                'error' => view('includes.errors', compact(
                    'errors'))->render()
            ];
        }

        $recipient = MailRecipient::create($this->request->except(['_token']));

        return [
            'success' => true,
            'redirect' => route('recipients.list')
        ];
    }

    public function editRecipient($recipientId)
    {
        $recipient = MailRecipient::findOrFail($recipientId);

        return [
            'title' => 'Edit Recipient',
            'btnlabel' => 'Submit Changes',
            'view' => view('pages.recipients.edit', compact(
                'recipient'))->render()
        ];
    }

    public function editRecipientSave($recipientId)
    {
        $validator = Validator::make($this->request->all(), [
            'label' => 'required',
            'email' => 'required|email'
        ]);

        if ($validator->fails()) {
            $errors = $validator->messages();
            return [
                'error' => view('includes.errors', compact(
                    'errors'))->render()
            ];
        }

        $recipient = MailRecipient::findOrFail($recipientId);
        $recipient->update($this->request->except(['_token']));

        return [
            'success' => true,
            'redirect' => route('recipients.list')
        ];
    }

    public function removeRecipient($recipientId)
    {
        $recipient = MailRecipient::findOrFail($recipientId);
        $id = $recipient->id;
        $recipient->delete();

        return [
            'success' => true,
            'id' => $id
        ];
    }

}
