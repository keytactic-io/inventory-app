<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\Http\Requests;
use App\Models\Vendor;
use App\Models\Product;
use App\Services\Shipping;
use Illuminate\Http\Request;
use App\Models\PurchaseOrder;
use App\Models\Request as ProductRequest;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (Auth::user()->is_requester) {

            $rqWaitingCount = ProductRequest::query()
                ->where('requester', Auth::user()->id)
                ->where('status', 0)
                ->count();
            $rqForShippingCount = ProductRequest::query()
                ->where('requester', Auth::user()->id)
                ->where('status', 1)
                ->count();
            $rqRejectedCount = ProductRequest::query()
                ->where('requester', Auth::user()->id)
                ->where('status', 3)
                ->count();
            $rqSplittedCount = ProductRequest::query()
                ->where('requester', Auth::user()->id)
                ->where('status', 4)
                ->count();

            return view('dashboard', compact(
                'rqWaitingCount', 'rqForShippingCount', 'rqRejectedCount',
                'rqSplittedCount'
            ));

        } else {

            if (
                Auth::user()->role->name == 'Super Admin' OR 
                Auth::user()->role->name == 'Admin'
            ) {

                $rqCount = ProductRequest::count();
                $rqPendingCount = ProductRequest::where('status', 0)->count();
                $rqShippingCount = ProductRequest::where('status', 1)->count();

                $mostRequestedProducts = Product::mostRequestedProducts();
                $lowStockProducts = Product::lowStockProducts();

                return view('dashboard', compact(
                    'rqCount', 'rqPendingCount', 'rqShippingCount',
                    'mostRequestedProducts', 'lowStockProducts'
                ));

            } elseif (Auth::user()->role->name == 'Shipping Staff') {

                $rqShippingCount = ProductRequest::query()
                    ->rightJoin('requested_products', 'requested_products.request_id', '=', 'requests.id')
                    ->leftJoin('locations', 'requested_products.get_from', '=', 'locations.id')
                    ->leftJoin('location_staffs', 'locations.id', '=', 'location_staffs.location_id')
                    ->where('requests.status', 1)
                    ->where('location_staffs.user_id', Auth::user()->id)
                    ->count();
                $rqShipped = ProductRequest::query()
                    ->rightJoin('requested_products', 'requested_products.request_id', '=', 'requests.id')
                    ->leftJoin('locations', 'requested_products.get_from', '=', 'locations.id')
                    ->leftJoin('location_staffs', 'locations.id', '=', 'location_staffs.location_id')
                    ->where('requests.status', 2)
                    ->where('location_staffs.user_id', Auth::user()->id)
                    ->count();

                return view('dashboard', compact(
                    'rqShippingCount', 'rqShipped'
                ));

            } elseif (Auth::user()->role->name == 'Purchasing Staff') {

                $poCount = PurchaseOrder::count();
                $poUndeliveredCount = PurchaseOrder::whereNull('received')->count();

                return view('dashboard', compact(
                    'poCount', 'poUndeliveredCount'
                ));

            } else {
                return view('dashboard');
            }

        }

    }

}
