<?php

namespace App\Http\Controllers;

use Artisan;
use App\Models\User;
use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Requests\SetupAppSettingsRequest;

class SetupController extends Controller
{

    public function appSettings()
    {
        abort_unless(User::where('role_id', 1)->count() == 0, 404);

        return view('setup.app-settings');
    }

    public function saveAppSettings(SetupAppSettingsRequest $request)
    {
        abort_unless(User::where('role_id', 1)->count() == 0, 404);

        foreach ($request->except(['_token', 'first_name', 'last_name', 'admin_email', 'password', 'password_confirmation']) as $key => $value) {
            $setting = Setting::where('key', $key)->first();
            if ($setting) {
                if ($key == 'logo') {
                    if ($request->hasFile('logo')) {
                        $destinationPath = public_path('uploads');
                        $extension = $request->logo->getClientOriginalExtension();
                        $filename = 'app-logo.' . $extension;
                        $request->logo->move($destinationPath, $filename);
                        $setting->update(['value' => $filename]);
                    }
                } else {
                    $setting->update(['value' => $value]);
                }   
            } else {
                if ($key == 'logo') {
                    if ($request->hasFile('logo')) {
                        $destinationPath = 'uploads';
                        $extension = $request->logo->getClientOriginalExtension();
                        $filename = 'app-logo.' . $extension;
                        $request->logo->move($destinationPath, $filename);
                        Setting::create([
                            'key' => $key,
                            'name' => title_case(str_replace('_', ' ', $key)),
                            'value' => $filename
                        ]);
                    }
                } else {
                    Setting::create([
                        'key' => $key,
                        'name' => title_case(str_replace('_', ' ', $key)),
                        'value' => $value
                    ]);
                }
            }
        }

        // seed roles and permissions
        Artisan::call('db:seed', ['--class' => 'RolesTableSeeder']);
        Artisan::call('db:seed', ['--class' => 'RolePermissionsTableSeeder']);

        $request['role_id'] = 1;
        $request['email'] = $request->admin_email;
        User::create($request->only([
            'first_name', 'last_name', 'email', 'password', 'role_id'
        ]));

        return redirect(url('/login'))
            ->withStatus('Congratulations! You can now start using you app.');
    }

}
