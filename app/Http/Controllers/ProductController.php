<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use Input;
use Image;
use Validator;
use Carbon\Carbon;
use App\Http\Requests;
use App\Models\Vendor;
use App\Models\Product;
use App\Models\Location;
use App\Models\Category;
use App\Models\Requester;
use App\Models\CustomField;
use Illuminate\Http\Request;
use App\Models\OpeningBalance;
use App\Models\OrderedProduct;
use App\Models\ProductPriceLog;
use App\Models\ProductTransfer;
use App\Models\ProductCategory;
use App\Models\CustomFieldValue;
use App\Models\ProductAllocation;
use App\Jobs\ArchiveRemainingStocks;
use App\Http\Requests\SaveProductRequest;
use App\Http\Requests\SaveCategoryRequest;
use App\Http\Requests\SaveOpeningBalanceRequest;

class ProductController extends Controller
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;

        view()->share('parentCategories', Category::where('parent_category', 0)->get());
    }

    public function productsList(Request $request, $filter = 'all', $currentCategory = 0)
    {
        $keyword = ($request->has('s') AND $request->s != '') ? $request->s : null;
        $products = Product::query()
            ->distinct()
            ->selectRaw("
                products.*,
                (archive_product_remaining_stocks.opening_balances + archive_product_remaining_stocks.orders) - 
                (archive_product_remaining_stocks.requests + archive_product_remaining_stocks.walkin_requests) as remaining_stocks
            ")
            ->leftJoin('archive_product_remaining_stocks', 'products.id', '=', 'archive_product_remaining_stocks.product_id');

        $expectJson = str_contains($filter, 'json-');
        $filter = str_replace('json-', '', $filter);

        switch ($filter) {

            case 'all':
                $products = $products->orderBy('name');
                break;

            case 'in-stock':
                $products = $products->inStock()
                    ->orderBy('name');
                break;

            case 'low-stock':
                $products = $products->lowStock()
                    ->orderBy('name');
                break;

            case 'out-of-stock':
                $products = $products->outOfStock()
                    ->orderBy('name');
                break;

        }

        if ($currentCategory > 0) {
            $category = Category::find($currentCategory);
            if ($category)  {
                $products = $products->leftJoin('product_categories', 'products.id', 'product_categories.product_id')
                    ->where('product_categories.category_id', $category->id);
            }
        }

        if ($keyword != null) {
            $products = $products->whereRaw(
                "(products.name LIKE '%$keyword%' OR products.description LIKE '%$keyword%')"
            );
        }

        $products = $products->paginate(app_setting('items_per_page', 25));
        $products->appends(['s' => $request->s]);
        $products->load('categories', 'categories.category');

        if ($expectJson) {
            return response()->json([
                'newList' => view('pages.products.nice-list', 
                    compact('products')
                )->render()
            ]);
        }

        $categories = Category::query()
            ->parents()
            ->orderBy('name')
            ->get();
        $categories->load('childCategories');

        return view('pages.products.list', compact(
            'products', 'filter', 'categories', 'currentCategory', 'keyword'
        ));
    }

    public function getProducts()
    {
        if (!Auth::user()->allowedTo('view_products')) {
            if (!Auth::user()->is_requester) {
                abort(403);
            }            
        }

        $category = null;
        $products = Product::all();
        if (Input::has('cat')) {
            $category = Category::findOrFail(Input::get('cat'));
            $products = Product::query()
                ->leftJoin('product_categories', 'products.id', '=', 'product_categories.product_id')
                ->where('product_categories.category_id', $category->id)
                ->select('products.*')
                ->get();
        }
        $products->load('categories');

        $inStockProductsId = [];
        $lowStockProductsId = [];
        $outStockProductsId = [];

        foreach ($products as $product):
            if ($product->stocksRemaining() > 0):
                array_push($inStockProductsId, $product->id);
            endif;
            if ($product->stocksRemaining() <= $product->reorder_point AND $product->stocksRemaining() > 0):
                array_push($lowStockProductsId, $product->id);
            endif;
            if ($product->stocksRemaining() == 0):
                array_push($outStockProductsId, $product->id);
            endif;
        endforeach;

        $inStockProducts = Product::query()
            ->whereIn('id', $inStockProductsId)
            ->orderby('name')
            ->get();
        $lowStockProducts = Product::query()
            ->whereIn('id', $lowStockProductsId)
            ->orderby('name')
            ->get();
        $outStockProducts = Product::query()
            ->whereIn('id', $outStockProductsId)
            ->orderby('name')
            ->get();

        return view('pages.products.list', compact(
            'products', 'inStockProducts', 'lowStockProducts', 
            'outStockProducts', 'category'
        ));
    }

    public function previewProduct($productId)
    {
        if (!Auth::user()->allowedTo('view_products')) {
            if (!Auth::user()->is_requester) {
                abort(403);
            }            
        }

        $product = Product::findOrFail($productId);

        return [
            'view' => view('pages.products.preview', compact(
                'product'
            ))->render()
        ];
    }

    public function viewProduct($productId)
    {
        if (!Auth::user()->allowedTo('view_products')) {
            if (!Auth::user()->is_requester) {
                abort(403);
            }            
        }

        $product = Product::findOrFail($productId);
        $locations = Location::orderBy('name')->get();
        $priceLogs = $product->prices()->orderBy('created_at', 'desc')->get();

        return view('pages.products.details', compact(
            'product', 'locations', 'priceLogs'
        ));
    }

    public function addProduct()
    {
        abort_if(!Auth::user()->allowedTo('add_products'), 403);

        $locations    = Location::orderBy('name')->get();
        $vendors      = Vendor::orderBy('name')->get();
        $customFields = CustomField::products()->get();
        $requesters   = Requester::all();

        $requesters->load('user');

        return view('pages.products.add', compact(
            'locations', 'vendors', 'customFields', 'requesters'
        ));
    }

    public function addProductSave(SaveProductRequest $request)
    {
        abort_if(!Auth::user()->allowedTo('add_products'), 403);

        $product = Product::create([
            'name'             => $request->name,
            'description'      => $request->description == '' ? null : $request->description,
            'reorder_point'    => $request->reorder_point,
            'reorder_quantity' => $request->reorder_quantity,
            'default_vendor'   => $request->vendor_id
        ]);

        if ($request->hasFile('photo')) {
            $destinationPath = 'uploads/products';
            $extension       = $request->file('photo')->getClientOriginalExtension();
            $filename        = $product->id . '_' . time() . '.' . $extension;

            file_exists($destinationPath) ? : File::makeDirectory($destinationPath, 0777, true);
            Image::make($request->file('photo'))
                ->heighten(400)
                ->orientate()
                ->save($destinationPath . '/' . $filename);
            $product->update(['photo' => $filename]);
        }

        $categories = $request->categories;
        if (count($categories) > 0) {
            foreach ($categories as $parentCategoryId => $childCategories) {
                ProductCategory::create([
                    'product_id'  => $product->id,
                    'category_id' => $parentCategoryId
                ]);
                foreach ($childCategories as $childCategoryId => $boolean) {
                    if ($childCategoryId != 0) {
                        ProductCategory::create([
                            'product_id'  => $product->id,
                            'category_id' => $childCategoryId
                        ]);
                    }
                }
            }
        }

        if ($request->opening_balance > 0) {
            OpeningBalance::create([
                'product_id'  => $product->id,
                'location_id' => $request->location_id,
                'created_by'  => Auth::user()->id,
                'quantity'    => $request->opening_balance,
            ]);

            $lastPrice = ProductPriceLog::where('product_id', $product->id)
                ->orderBy('id', 'desc')
                ->first();

            if (!$lastPrice OR $lastPrice->price != $request->price) {
                ProductPriceLog::create([
                    'product_id' => $product->id,
                    'price'      => $request->price
                ]);
            }
        }

        // save custom fields values
        if (count($request->custom_fields) > 0) {
            $customFieldsData = [];
            foreach ($request->custom_fields as $id => $value) {
                if ($value != '') {
                    array_push($customFieldsData, [
                        'custom_field_id' => $id,
                        'reference_id'    => $product->id,
                        'value'           => $value
                    ]);
                }
            }
            CustomFieldValue::insert($customFieldsData);
        }

        // allocate to all RQR
        $productAllocationData = [];
        foreach ($request->allocations as $requester => $allocation) {
            array_push($productAllocationData, [
                'requester_id' => $requester,
                'product_id'   => $product->id,
                'allocation'   => $allocation
            ]);
        }
        ProductAllocation::insert($productAllocationData);

        dispatch(new ArchiveRemainingStocks(
            Product::whereIn('id', [$product->id])->get()
        ));

        return redirect()->route('products.list', ['filter' => 'all'])
            ->withStatus($product->name . ' successfully added.');
    }

    public function editProduct($productId)
    {
        $product = Product::findOrFail($productId);

        if (!Auth::user()->allowedTo('edit_products')) {
            abort(403);
        }

        $theCategories = [];
        $categories = $product->categories;
        foreach ($categories as $cat) {
            $theCategories[$cat->category_id] = $cat->category_id;
        }

        $vendors = Vendor::query()
            ->orderBy('name')
            ->get();

        $customFields = CustomField::products()->get();

        return view('pages.products.edit', compact(
            'product', 'theCategories', 'vendors', 'customFields'
        ));
    }

    public function editProductSave(SaveProductRequest $request, $productId)
    {
        if (!Auth::user()->allowedTo('edit_products')) {
            abort(403);
        }

        $product = Product::findOrFail($productId);

        $product->update([
            'name' => $request->name,
            'description' => $request->description == '' ? null : $request->description,
            'reorder_point' => $request->reorder_point,
            'reorder_quantity' => $request->reorder_quantity
        ]);

        if ($this->request->hasFile('photo')) {

            $destinationPath = 'uploads/products';
            $extension       = $request->file('photo')->getClientOriginalExtension();
            $filename        = $product->id . '_' . time() . '.' . $extension;

            $oldPhoto = $destinationPath . '/' . $product->photo;
            if (file_exists($oldPhoto)) {
                unlink($oldPhoto);
            }

            file_exists($destinationPath) ? : File::makeDirectory($destinationPath, 0777, true);
            Image::make($request->file('photo'))
                ->heighten(400)
                ->orientate()
                ->save($destinationPath . '/' . $filename);
            $product->update(['photo' => $filename]);

        }

        $product->categories()->delete();
        $categories = $request->categories;
        if (count($categories) > 0) {
            foreach ($categories as $parentCategoryId => $childCategories) {
                ProductCategory::create([
                    'product_id' => $product->id,
                    'category_id' => $parentCategoryId
                ]);
                foreach ($childCategories as $childCategoryId => $boolean) {
                    if ($childCategoryId != 0) {
                        ProductCategory::create([
                            'product_id' => $product->id,
                            'category_id' => $childCategoryId
                        ]);
                    }
                }
            }
        }

        // delete custom fields values
        foreach ($product->customFields() as $customField) {
            $customField->delete();
        }

        // save custom fields values
        if (count($request->custom_fields) > 0) {
            foreach ($request->custom_fields as $id => $value) {
                if ($value != '') {
                    CustomFieldValue::create([
                        'custom_field_id' => $id,
                        'reference_id' => $product->id,
                        'value' => $value
                    ]);
                }
            }
        }

        dispatch(new ArchiveRemainingStocks(
            Product::whereIn('id', [$product->id])->get()
        ));

        return redirect()->route('products.details', ['productId' => $product->id])
            ->withStatus($product->name . ' successfully updated.');

    }

    public function deleteProduct($productId)
    {
        if (!Auth::user()->allowedTo('delete_products')) {
            abort(403);
        }

        $product = Product::findOrFail($productId);
        $name = $product->name;

        if (count($product->requests) > 0) {
            return redirect()->route('products.list', ['filter' => 'all'])
                ->withErrors(['Sorry, you cannot delete this product.']);
        }

        $product->archiveRemainingStock()->delete();
        $product->delete();

        return redirect()->route('products.list', ['filter' => 'all'])
            ->withStatus($name . ' successfully removed.');
    }

    public function removeProductPhoto($productId)
    {
        if (!Auth::user()->allowedTo('edit_products')) {
            abort(403);
        }

        $product = Product::findOrFail($productId);
        if ($product->photo != 'no-photo.png') {
            unlink('uploads/products/' . $product->photo);
        }
        $product->photo = 'no-photo.png';
        $product->save();

        return back()
            ->withStatus('Successfully removed the photo of ' . $product->name);
    }

    public function getCategories()
    {
        if (!Auth::user()->allowedTo('view_product_categories')) {
            if (!Auth::user()->is_requester) {
                abort(403);
            }
        }

        return view('pages.products.categories.list');
    }

    public function addCategory()
    {
        if (!Auth::user()->allowedTo('add_product_categories')) {
            abort(403);
        }

        return view('pages.products.categories.add');
    }

    public function addCategorySave(SaveCategoryRequest $request)
    {
        if (!Auth::user()->allowedTo('add_product_categories')) {
            abort(403);
        }

        $category = Category::create($request->except('_token'));

        return redirect()->route('categories.list')
            ->withStatus($category->name . ' successfully added');
    }

    public function editCategory($categoryId)
    {
        $category = Category::findOrFail($categoryId);

        if (!Auth::user()->allowedTo('edit_product_categories')) {
            abort(403);
        }

        return view('pages.products.categories.edit', compact(
            'category'
        ));
    }

    public function editCategorySave(SaveCategoryRequest $request, $categoryId)
    {
        $category = Category::findOrFail($categoryId);

        if (!Auth::user()->allowedTo('edit_product_categories')) {
            abort(403);
        }

        $category->update($request->except('_token'));

        return redirect()->route('categories.list')
            ->withStatus($category->name . ' successfully updated.');
    }

    public function deleteCategory($categoryId)
    {
        $category = Category::findOrFail($categoryId);

        if (!Auth::user()->allowedTo('delete_product_categories')) {
            abort(403);
        }

        $name = $category->name;
        $category->delete();

        return redirect()->route('categories.list')
            ->withStatus($name . ' successfully removed.');
    }

    public function viewOpeningBalances()
    {
        if (!Auth::user()->allowedTo('view_opening_balances')) {
            if (!Auth::user()->is_requester) {
                abort(403);
            }
        }

        $openingBalances = OpeningBalance::all();

        return view('pages.products.opening-balances.list', compact(
            'openingBalances'
        ));
    }

    public function addOpeningBalance()
    {
        if (!Auth::user()->allowedTo('add_opening_balances')) {
            abort(403);
        }

        $products = Product::query()
            ->orderBy('name')
            ->get();
            
        $locations = Location::query()
            ->orderBy('name')
            ->get();

        return view('pages.products.opening-balances.add', compact(
            'products', 'locations'
        ));
    }

    public function addOpeningBalanceSave(SaveOpeningBalanceRequest $request)
    {
        if (!Auth::user()->allowedTo('add_opening_balances')) {
            abort(403);
        }

        $openingBalanceData = [];
        $productIds = [];
        foreach ($request->products as $productInfo) {
            $openingBalanceData[] = [
                'product_id' => $productInfo['product_id'],
                'location_id' => $productInfo['location_id'],
                'quantity' => $productInfo['quantity'],
                'created_by' => Auth::user()->id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];
            $productIds[] = $productInfo['product_id'];

            $lastPrice = ProductPriceLog::where('product_id', $productInfo['product_id'])
                ->orderBy('id', 'desc')
                ->first();

            if (!$lastPrice OR $lastPrice->price != $productInfo['price']) {
                ProductPriceLog::create([
                    'product_id' => $productInfo['product_id'],
                    'price' => $productInfo['price']
                ]);
            }
        }

        OpeningBalance::insert($openingBalanceData);

        dispatch(new ArchiveRemainingStocks(
            Product::whereIn('id', $productIds)->get()
        ));

        return redirect()->route('products.ob.list')
            ->withStatus('Successfully created an opening balances.');
    }

    public function editOpeningBalance($obId)
    {
        if (!Auth::user()->allowedTo('edit_opening_balances')) {
            abort(403);
        }

        $openingBalance = OpeningBalance::findOrFail($obId);

        $products = Product::query()
            ->orderBy('name')
            ->get();
            
        $locations = Location::query()
            ->orderBy('name')
            ->get();

        return view('pages.products.opening-balances.edit', compact(
            'openingBalance', 'products', 'locations'
        ));
    }

    public function editOpeningBalanceSave($obId)
    {
        if (!Auth::user()->allowedTo('edit_opening_balances')) {
            abort(403);
        }

        $openingBalance = OpeningBalance::findOrFail($obId);
        $product = Product::findOrFail($this->request->product_id);
        $openingBalance->update([
            'product_id' => $product->id,
            'location_id' => $this->request->location_id,
            'created_by' => Auth::user()->id,
            'quantity' => $this->request->quantity,
        ]);

        if ($product->latestPrice() != $this->request->price) {
            ProductPriceLog::create([
                'product_id' => $product->id,
                'price' => $this->request->price
            ]);
        }

        dispatch(new ArchiveRemainingStocks(
            Product::whereIn('id', [$product->id])->get()
        ));

        return redirect()->route('products.ob.list')
            ->withStatus('Successfully udpated an opening balances.');
    }

    public function deleteOpeningBalance($obId)
    {
        if (!Auth::user()->allowedTo('delete_opening_balances')) {
            abort(403);
        }

        $openingBalance = OpeningBalance::findOrFail($obId);
        $product = $openingBalance->product;
        $openingBalance->delete();

        dispatch(new ArchiveRemainingStocks(
            Product::whereIn('id', [$product->id])->get()
        ));

        return redirect()->route('products.ob.list')
            ->withStatus('Successfully deleted an opening balances.');
    }

    public function openingBalanceNewField()
    {
        if (!Auth::user()->allowedTo('add_opening_balances')) {
            abort(403);
        }

        $count = $this->request->c;

        $products = Product::query()
            ->orderBy('name')
            ->get();
            
        $locations = Location::query()
            ->orderBy('name')
            ->get();

        return [
            'view' => view('pages.products.opening-balances.new-entry', compact(
                'count', 'products', 'locations'
            ))->render()
        ];
    }

    public function fetchProductsList($locationId)
    {
        if (!Auth::user()->allowedTo('add_opening_balances')) {
            abort(403);
        }

        $location = Location::findOrFail($locationId);

        $ps = collect(DB::select(
            DB::raw("
                SELECT product_id as id FROM opening_balances 
                    WHERE location_id = '$location->id'
                UNION
                SELECT ordered_products.product_id as id FROM ordered_products 
                    RIGHT JOIN ordered_products_locations 
                    ON ordered_products_locations.ordered_product_id = ordered_products.id
                    WHERE ordered_products_locations.location_id = '$location->id'
                UNION
                SELECT product_transfers.product_id as id FROM product_transfers
                    WHERE product_transfers.to = '$location->id'
                GROUP BY product_id
            ")
        ));

        $ids = [];
        foreach ($ps as $product) {
            array_push($ids, $product->id);
        }

        $prods = Product::whereIn('id', $ids)
            ->orderBy('name')
            ->get();

        $products = [];
        foreach ($prods as $product) {
            $products[$product->id] = [
                'id' => $product->id,
                'name' => $product->name,
                'remaining' => $product->stocksInLocation($location->id)
            ];
        }

        return json_encode(['products' => $products]);
    }

    public function transferProduct()
    {
        if (!Auth::user()->allowedTo('add_transferred_products')) {
            abort(403);
        }

        $locations = Location::orderBy('name')->get();

        return view('pages.products.transfers.transfer', compact(
            'locations'
        ));
    }

    public function transferProductSave()
    {
        if (!Auth::user()->allowedTo('add_transferred_products')) {
            abort(403);
        }

        $this->validate($this->request, [
            'from' => 'required|integer|different:to',
            'to' => 'required|integer',
            'product_id' => 'required|integer',
            'quantity' => 'required|integer'
        ]);

        $locationFrom = Location::findOrFail($this->request->from);
        $locationTo = Location::findOrFail($this->request->to);
        $product = Product::findOrFail($this->request->product_id);

        $productTransfer = ProductTransfer::create($this->request->except(['_token']));

        return redirect()->route('products.transferred')
            ->withStatus($productTransfer->quantity . ' ' . $product->name . 
                ' was transferred from ' . $locationFrom->name . 
                ' to ' . $locationTo->name);
    }

    public function transferredProduct()
    {
        if (!Auth::user()->allowedTo('view_transferred_products')) {
            abort(403);
        }

        $transferredProducts = ProductTransfer::all();

        return view('pages.products.transfers.list', compact(
            'transferredProducts'
        ));
    }

    public function revertTransfer($transferId)
    {
        if (!Auth::user()->allowedTo('edit_transferred_products')) {
            abort(403);
        }

        $transferredProduct = ProductTransfer::findOrFail($transferId);
        $transferredProduct->delete();

        return redirect()->route('products.transferred')
            ->withStatus('Product transfer reverted successfully');
    }

    public function addProductModal()
    {
        if (!Auth::user()->allowedTo('add_products')) {
            abort(403);
        }

        $locations = Location::query()
            ->orderBy('name')
            ->get();

        $vendors = Vendor::query()
            ->orderBy('name')
            ->get();

        return json_encode([
            'title' => 'Add New Product',
            'view' => view('pages.products.add-modal', compact(
                'locations', 'vendors'
                ))->render()
        ]);
    }

    public function addProductModalSave()
    {
        if (!Auth::user()->allowedTo('add_products')) {
            abort(403);
        }
        
        $validator = Validator::make($this->request->all(), [
            'name' => 'required|unique:products',
            'default_vendor' => 'required|integer',
            'opening_balance' => 'numeric',
            'reorder_point' => 'required|numeric',
            'reorder_quantity' => 'required|numeric',
            'location_id' => 'required_with:opening_balance',
            'price' => 'required_with:opening_balance',
        ]);

        if ($validator->fails()) {
            $errors = $validator->messages();
            return [
                'error' => view('includes.errors', compact(
                    'errors'))->render()
            ];
        }

        $request = $this->request;

        $product = Product::create([
            'name' => $request->name,
            'description' => $request->description == '' ? null : $request->description,
            'reorder_point' => $request->reorder_point,
            'reorder_quantity' => $request->reorder_quantity,
            'default_vendor' => $request->default_vendor
        ]);

        if ($this->request->hasFile('photo')) {

            $destinationPath = 'uploads/products';
            $extension = $request->file('photo')->getClientOriginalExtension();
            $fileName = $product->id . '_' . time() . '.' . $extension;
            $request->file('photo')->move($destinationPath, $fileName);

            $product->photo = $fileName;
            $product->save();

        }

        $categories = $request->categories;
        if (count($categories) > 0):
            foreach ($categories as $parentCategoryId => $childCategories) {
                ProductCategory::create([
                    'product_id' => $product->id,
                    'category_id' => $parentCategoryId
                ]);
                foreach ($childCategories as $childCategoryId => $boolean) {
                    if ($childCategoryId != 0) {
                        ProductCategory::create([
                            'product_id' => $product->id,
                            'category_id' => $childCategoryId
                        ]);
                    }
                }
            }
        endif;

        if ($request->opening_balance > 0) {
            OpeningBalance::create([
                'product_id' => $product->id,
                'location_id' => $request->location_id,
                'created_by' => Auth::user()->id,
                'quantity' => $request->opening_balance,
            ]);

            $lastPrice = ProductPriceLog::where('product_id', $product->id)
                ->orderBy('id', 'desc')
                ->first();

            if (!$lastPrice OR $lastPrice->price != $request->price) {
                ProductPriceLog::create([
                    'product_id' =>$product->id,
                    'price' => $request->price
                ]);
            }
        }

        dispatch(new ArchiveRemainingStocks(
            Product::whereIn('id', [$product->id])->get()
        ));

        return [
            'done' => true,
            'product' => [
                'id' => $product->id,
                'name' => $product->name,
                'rq' => $product->reorder_quantity,
                'price' => $product->latestPrice()
            ]
        ];
    }

}
