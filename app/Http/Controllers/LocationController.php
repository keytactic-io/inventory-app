<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\Models\User;
use App\Http\Requests;
use App\Models\Setting;
use App\Models\Product;
use App\Models\Location;
use Illuminate\Http\Request;
use App\Models\LocationStaff;
use App\Http\Requests\SaveLocationRequest;

class LocationController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function getLocations()
    {
        if (
            !Auth::user()->allowedTo('view_locations') AND 
            !Auth::user()->is_requester
        ) {
            abort(403);
        }

        $showLocationWithZero = false;
        $locationSettings = Setting::where('key', 'show_zero_locations')->first();
        if ($locationSettings AND $locationSettings->value == 1) {
            $showLocationWithZero = true;
        }

        $locations = Location::query()
            ->orderBy('name')
            ->get();

        return view('pages.locations.list', compact(
            'locations', 'showLocationWithZero'
        ));
    }

    public function viewLocation($locationId)
    {
        if (
            !Auth::user()->allowedTo('view_locations') AND 
            !Auth::user()->is_requester
        ) {
            abort(403);
        }

        $location = Location::findOrFail($locationId);
        $location->load('staff');

        $products = Product::inLocation($location->id);

        return view('pages.locations.details', compact(
            'location', 'products'
        ));
    }

    public function addLocation()
    {
        if (!Auth::user()->allowedTo('add_locations')) {
            abort(403);
        }

        $users = User::notRequester()->get();

        return view('pages.locations.add', compact(
            'users'
        ));
    }

    public function addLocationSave(SaveLocationRequest $request)
    {
        if (!Auth::user()->allowedTo('add_locations')) {
            abort(403);
        }

        $location = Location::create($request->except(['_token']));
        LocationStaff::create([
            'user_id' => $request->staff_incharge,
            'location_id' => $location->id
        ]);
        
        return redirect()->route('locations.list')
            ->withStatus($location->name . ' successfully added.');
    }

    public function editLocation($locationId)
    {
        if (!Auth::user()->allowedTo('edit_locations')) {
            abort(403);
        }

        $location = Location::findOrFail($locationId);
        $users = User::notRequester()->get();

        return view('pages.locations.edit', compact(
            'location', 'users'
        ));
    }

    public function editLocationSave(SaveLocationRequest $request, $locationId)
    {
        if (!Auth::user()->allowedTo('edit_locations')) {
            abort(403);
        }

        $location = Location::findOrFail($locationId);
        $location->update($request->except(['_token']));

        $staff = LocationStaff::where('user_id', $request->staff_incharge)
            ->where('location_id', $location->id)->first();

        if (!$staff) {
            LocationStaff::create([
                'user_id' => $request->staff_incharge,
                'location_id' => $location->id
            ]);
        }
        
        return redirect()->route('locations.details', ['locationId' => $location->id])
            ->withStatus($location->name . ' successfully updated.');
    }

    public function deleteLocation($locationId)
    {
        if (!Auth::user()->allowedTo('delete_locations')) {
            abort(403);
        }

        $location = Location::findOrFail($locationId);
        $name = $location->name;
        $location->staff()->delete();
        $location->delete();

        return redirect()->route('locations.list')
            ->withStatus($name . ' successfully removed.');
    }

    public function addStaff($locationId)
    {
        if (!Auth::user()->allowedTo('edit_locations')) {
            abort(403);
        }

        $location = Location::findOrFail($locationId);
        $location->load('staff');
        $locationStaff = [];
        foreach ($location->staff as $staff) {
            $locationStaff[] = $staff->user_id;
        }
        $users = User::locationStaff()->get();

        return [
            'title' => 'Add Location Staff',
            'view' => view('pages.locations.staff.add', compact(
                'location', 'users', 'locationStaff'
            ))->render()
        ];
    }

    public function addStaffSave(Request $request)
    {
        if (!Auth::user()->allowedTo('edit_locations')) {
            abort(403);
        }

        $location = Location::findOrFail($request->location_id);
        $staff = LocationStaff::create($request->except(['_token']));

        return [
            'done' => true,
            'redirect' => route('locations.details', [
                'locationId' => $location->id
            ])
        ];

    }

    public function deleteStaff($staffId)
    {
        if (!Auth::user()->allowedTo('edit_locations')) {
            abort(403);
        }

        $staff = LocationStaff::findOrFail($staffId);
        $staffToRemove = $staff->id;
        $staff->delete();

        return [
            'done' => true,
            'toRemove' => $staffToRemove
        ];
    }

}
