<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use Validator;
use App\Http\Requests;
use App\Models\CustomField;
use Illuminate\Http\Request;
use App\Models\CustomFieldValue;

class CustomFieldController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function customFields()
    {
        if (!Auth::user()->allowedTo('view_custom_fields')) {
            abort(403);
        }

        $prFields = CustomField::products()->get();
        $rqFields = CustomField::requests()->get();
        $poFields = CustomField::purchaseOrders()->get();

        return view('pages.custom-fields.list', compact(
            'prFields', 'rqFields', 'poFields'
        ));
    }

    public function addCustomField()
    {
        if (!Auth::user()->allowedTo('add_custom_fields')) {
            abort(403);
        }

        return [
            'title' => 'Add Custom Field',
            'view' => view('pages.custom-fields.add')->render()
        ];
    }

    public function addCustomFieldSave(Request $request)
    {
        if (!Auth::user()->allowedTo('add_custom_fields')) {
            abort(403);
        }

        $validator = Validator::make($request->all(), [
            'model' => 'required',
            'label' => 'required',
            'type' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->messages();
            return [
                'error' => view('includes.errors', compact(
                    'errors'))->render()
            ];
        }

        $slug = $request->slug != '' ? str_slug($request->slug, '-') : str_slug($request->label, '-');

        $customField = CustomField::create([
            'model' => $request->model,
            'label' => $request->label,
            'slug' => $slug,
            'type' => $request->type
        ]);

        Session::flash('status', $customField->label . ' created successfully.');
        return [
            'done' => true,
            'redirect' => route('cf.home')
        ];
    }

    public function editCustomField($customFieldId)
    {
        if (!Auth::user()->allowedTo('edit_custom_fields')) {
            abort(403);
        }

        $cf = CustomField::findOrFail($customFieldId);

        return [
            'title' => 'Edit Custom Field',
            'view' => view('pages.custom-fields.edit', compact(
                'cf'
            ))->render()
        ];
    }

    public function editCustomFieldSave($customFieldId, Request $request)
    {
        if (!Auth::user()->allowedTo('edit_custom_fields')) {
            abort(403);
        }

        $customField = CustomField::findOrFail($customFieldId);

        $validator = Validator::make($request->all(), [
            'model' => 'required',
            'label' => 'required',
            'type' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->messages();
            return [
                'error' => view('includes.errors', compact(
                    'errors'))->render()
            ];
        }

        // process slug
        $slug = $request->slug != '' ? str_slug($request->slug, '-') : str_slug($request->label, '-');

        // removed linked values to prevent errors
        $customField->values()->delete();

        // update custom field
        $customField->update([
            'model' => $request->model,
            'label' => $request->label,
            'slug' => $slug,
            'type' => $request->type
        ]);

        // response
        Session::flash('status', $customField->label . ' udpated successfully.');
        return [
            'done' => true,
            'redirect' => route('cf.home')
        ];

    }

    public function deleteCustomField($customFieldId)
    {
        if (!Auth::user()->allowedTo('delete_custom_fields')) {
            abort(403);
        }

        $customField = CustomField::findOrFail($customFieldId);
        $customField->values()->delete();
        $customField->delete();

        return redirect()->route('cf.home')
            ->withStatus('Custom field successfully removed.');
    }

}
