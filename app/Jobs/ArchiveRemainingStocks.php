<?php

namespace App\Jobs;

use App\Models\Product;
use Illuminate\Bus\Queueable;
use App\Models\ProductRemainingStock;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ArchiveRemainingStocks implements ShouldQueue
{

    use InteractsWithQueue, Queueable, SerializesModels;

    private $products;

    public function __construct($products)
    {
        $this->products = $products;
    }

    public function handle()
    {
        if (count($this->products) > 0):
            foreach ($this->products as $product) {
                $archive = $product->archiveRemainingStock;
                if (!$product->archiveRemainingStock) {
                    $archive = ProductRemainingStock::create([
                        'product_id' => $product->id,
                    ]);
                }

                $data['opening_balances'] = $product->openingBalances()
                    ->sum('quantity');

                $data['orders'] = $product->orders()
                    ->leftJoin('ordered_products_locations', 'ordered_products_locations.ordered_product_id', '=', 'ordered_products.id')
                    ->sum('ordered_products_locations.received_quantity');

                $data['requests'] = $product->requests()
                    ->sum('allowed_quantity');

                $data['walkin_requests'] = $product->walkinRequestProducts()
                    ->sum('allowed_quantity');

                $data['status'] = 1;
                if ($product->isLowStock()) {
                    $data['status'] = 2;
                } elseif ($product->isOutOfStock()) {
                    $data['status'] = 3;
                }

                $archive->update($data);
            }
        endif;
    }

}
