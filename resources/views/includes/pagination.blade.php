<div class="pagination-wrap noselect">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-sm-push-3">
            <div class="pagination-info">
                Page {{ $results->currentPage() }} of {{ $results->lastPage() }}
            </div>
        </div>
        <div class="col-xs-6 col-sm-3 col-sm-pull-6">
            @if (!empty($results->previousPageUrl()))
                <a href="{{ str_replace('json-', '', $results->previousPageUrl()) }}" class="pagination-navi navi-prev btn btn-link">
                    <i class="fa fa-chevron-left"></i> &nbsp; Previous
                </a>
            @endif
        </div>
        <div class="col-xs-6 col-sm-3">
            @if (!empty($results->nextPageUrl()))
                <a href="{{ str_replace('json-', '', $results->nextPageUrl()) }}" class="pagination-navi navi-next btn btn-link">
                    Next &nbsp; <i class="fa fa-chevron-right"></i>
                </a>
            @endif
        </div>
    </div>
</div>