@extends('layouts.app')

@section('page-title', 'Create Request')

@section('head-addon')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css">
<link rel="stylesheet" href="{{ asset('assets/css/bt-select-override.css') }}" />
<link href="{{ asset('assets/css/vendors/tagsinput.css') }}" rel="stylesheet" />
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.errors')

    <form action="{{ route('requests.create', ['requesterId' => $requester->id]) }}" method="post" autocomplete="off" id="create-request-form">
    {!! csrf_field() !!}

    <div class="panel">
        <div class="panel-heading">
            @if ($currentUser->id == $requester->user->id)
                <h4>Create Request</h4>
            @else
                <h4>Create Request for <u><a href="{{ route('requests.requester.select') }}">{{ $requester->user->name() }}</a></u></h4>
            @endif
        </div>
        <div class="panel-body">

            <div id="smartwizard">
                <ul>
                    <li><a href="#step-1">Step 1<br /><small>@if ($currentUser->is_requester) Your Info @else Requester Info @endif</small></a></li>
                    <li><a href="#step-2">Step 2<br /><small>Select Asset</small></a></li>
                    <li><a href="#step-3">Step 3<br /><small>Shipping Address</small></a></li>
                    <li><a href="#step-4">Step 4<br /><small>Additional Details</small></a></li>
                </ul>
                <div style="padding-top: 30px;">

                    <div id="step-1">

                        <div id="form-step-1" role="form" data-toggle="validator">
                            <div class="form-group">
                                <label for="market-id">Department</label>
                                <select name="market_id" id="market-id" class="form-control" required>
                                    <option value=""></option>
                                    @foreach ($markets as $market)
                                        <option value="{{ $market->id }}"
                                            @if (old('market_id', $requester->market_id) == $market->id)  selected @endif
                                        >
                                            {{ $market->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                    </div>

                    <div id="step-2">

                        <div id="form-step-2" role="form" data-toggle="validator">
                            <div id="products-panel-body">

                                <div class="form-group">
                                    <label for="products-selection">Select Asset</label>
                                    <select id="products-selection" class="form-control"
                                        data-live-search="true"
                                    >
                                        <option value=""></option>
                                        @foreach ($products as $product)
                                            @if ($product->remaining_stocks > 0)
                                                <option value="{{ $product->id }}"
                                                    data-allocation="{{ $product->allocation }}" 
                                                    data-remaining="{{ $product->remaining_stocks }}"
                                                >
                                                    {{ $product->name }} ({{ $product->remaining_stocks }})
                                                </option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>

                                <div class="table-responsive">
                                    <table id="products-table" class="table">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Allocation</th>
                                                <th style="width:124px;">Quantity</th>
                                                <th style="width:60px;" class="text-right">&nbsp;</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="no-data">
                                                <td colspan="4" class="text-center active table-strong">Please select at least one (1) product</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div id="step-3">
                        <div id="form-step-2" role="form" data-toggle="validator">
                            <div class="form-group">
                                <label for="address">Address</label>
                                <select name="address" id="address" class="form-control">
                                    @foreach ($requester->addresses as $address)
                                        <option value="{{ $address->id }}"
                                            <?php if ($address->default): ?>checked<?php endif; ?>1
                                        >
                                            {{ $address->getAddress() }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group fg-last">
                                <a href="#" class="btn btn-sm btn-primary add-custom-address"
                                    data-url="{{ route('requesters.aca') }}"
                                >
                                    Add New Address
                                </a>
                                <div class="custom-address"></div>
                            </div>
                            <br />
                            <div class="form-group">
                                <label for="eid">Expected In-hand Date</label>
                                <input type="text" name="expected_inhand_date" id="eid" class="form-control" value="{{ old('expected_inhand_date')}}" required />
                            </div>
                        </div>
                    </div>

                    <div id="step-4">
                        <div class="form-group">
                            <label for="justification">Justification</label>
                            <textarea name="justification" id="justification" rows="2" class="form-control autosize" required>{{ old('justification')}}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="notes">Notes</label>
                            <textarea name="notes" id="notes" rows="2" class="form-control autosize">{{ old('notes')}}</textarea>
                        </div>

                        @if (count($customFields) > 0)
                            <div class="form-group-divider"><span></span></div>
                            @foreach ($customFields as $cf)
                                <div class="form-group @if ($loop->last) fg-last @endif">
                                    <label for="cf-{{ $cf->slug }}">{{ $cf->label }}</label>
                                    @if ($cf->type == 'input')
                                        <input type="text" class="form-control" name="custom_fields[{{ $cf->id }}]" id="cf-{{ $cf->slug }}" />
                                    @elseif ($cf->type == 'textarea')
                                        <textarea class="form-control autosize" name="custom_fields[{{ $cf->id }}]" id="cf-{{ $cf->slug }}" rows="2"></textarea>
                                    @endif
                                </div>
                            @endforeach
                        @endif

                        <div class="form-group-divider"><span></span></div>

                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="send_notification" value="1" checked id="send_noti" />
                                    <span>Send notification(s)?</span>
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="cc-email">CC Notifications (optional)</label>
                            <input type="text" name="cc_email" id="cc-email" class="form-control" length="50" value="{{ old('cc_email')}}" />
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
    </form>

</div>
</div>
</div>
@endsection

@section('footer-addon')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js"></script>
<script src="{{ asset('assets/js/vendors/tagsinput.js') }}"></script>
<script>
$(function() {

        $('#cc-email').tagsInput({
            height: 'auto',
            width: 'auto',
            defaultText: 'add email',
        });

        $('#create-request-form').on('submit', function() {
            loader.show();
        });

        $('#eid').datepicker({
            dateFormat: 'MM dd, yy'
        });

        $('#products-selection')
        .selectpicker({
                style: 'btn-primary',
                size: 6,
                noneSelectedText: 'Please select a product'
        })
        .on('changed.bs.select', function(e) {
            var id         = e.target.value,
                selected   = $(this).find('option[value="' + id + '"]'),
                allocation = selected.data('allocation'),
                remaining  = selected.data('remaining');

            var allowed = parseInt(allocation);
            if (remaining < allocation || allowed == -1) {
                allowed = remaining;
            }
            var allocationDisplay = allocation < 0 ? '&#x221e;' : allocation;

            if (id != '' && id > 0) {
                var name = $(this).find('option[value="' + id + '"]').text(),
                    tbody = $('#products-table').find('tbody'),
                    html = '<tr id="product-tr-' + id + '">' +
                                    '<td class="table-strong">' + name + '</td>' +
                                    '<td>' + allocationDisplay + '</td>' +
                                    '<td><input type="number" name="products[' + id + ']" class="input-in-table" placeholder="0" required min="1" max="' + allowed + '" /></td>' +
                                    '<td class="text-right"><a href="#" class="remove-tr" data-target="#product-tr-' + id + '"><i class="fa fa-close fa-fw"></i></a></td>' +
                                    '</tr>';
                if ($('#product-tr-' + id).length == 0) {
                    if (tbody.find('.no-data').length == 0) {
                        $(html).appendTo(tbody);
                    } else {
                        tbody.html(html);
                    }
                    $('#product-tr-' + id).find('input').focus();
                }
            }

            $(this).val('').selectpicker('render');

        });

        $(document).on('click', '.add-custom-address', function(e) {
            e.preventDefault();
            if ($('.custom-address').html() != '') {
                $('#address').prop('disabled', false);
                $('.custom-address').html('');
            } else {
                $('#address').prop('disabled', true);
                $('.custom-address').html('<div style="padding-top:20px;"><i class="fa fa-circle-o-notch fa-spin fa-fw text-primary"></i></div>');
                $('.custom-address').load($(this).data('url'));
            }
        });

        $(document).on('click', '.remove-tr', function(e) {
            e.preventDefault();
            $($(this).attr('data-target')).remove();
        });

        $('#send_noti').on('change', function() {
            var ccfield = $('#cc-email');
            if ($(this).prop('checked')) {
                ccfield.attr('disabled', false);
            } else {
                ccfield.attr('disabled', true);
            }
        })

        var btnFinish = $('<button></button>')
            .text('Finish')
            .addClass('btn btn-info')
            .on('click', function(){
                if ( !$(this).hasClass('disabled')) {
                    var elmForm = $("#create-request-form");
                    if (elmForm) {
                        elmForm.validator('validate');
                        var elmErr = elmForm.find('.has-error');
                        if (elmErr && elmErr.length > 0) {
                            alert('Oops we still have error in the form');
                            return false;
                        }
                    }
                }
            });
        var btnCancel = $('<button></button>').text('Cancel')
            .addClass('btn btn-danger')
            .on('click', function(){
                $('#smartwizard').smartWizard("reset");
                $('#create-request-form').find("input, textarea").val("");
            });



        // Smart Wizard
        $('#smartwizard').smartWizard({
            selected: 1,
            theme: 'dots',
            transitionEffect:'fade',
            toolbarSettings: {toolbarPosition: 'bottom', toolbarExtraButtons: [btnFinish, btnCancel]},
            anchorSettings: {
                markDoneStep: true, // add done css
                markAllPreviousStepsAsDone: true, // When a step selected by url hash, all previous steps are marked done
                removeDoneStepOnNavigateBack: true, // While navigate back done step after active step will be cleared
                enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
            }
        });

        $("#smartwizard").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) {
            var elmForm = $("#form-step-" + stepNumber);
            // stepDirection === 'forward' :- this condition allows to do the form validation
            // only on forward navigation, that makes easy navigation on backwards still do the validation when going next
            if(stepDirection === 'forward' && elmForm){
                elmForm.validator('validate');
                var elmErr = elmForm.children('.has-error');
                if(elmErr && elmErr.length > 0){
                    // Form validation failed
                    return false;
                }
            }
            return true;
        });

        $("#smartwizard").on("showStep", function(e, anchorObject, stepNumber, stepDirection) {
            // Enable finish button only on last step
            if (stepNumber == 3) {
                $('.btn-finish').removeClass('disabled');
            } else {
                $('.btn-finish').addClass('disabled');
            }
        });


});
</script>
@endsection
