@extends('layouts.app')

@section('page-title', 'App Settings')

@section('content')
<div class="container">
<div class="row">
<div class="col-xs-12">

    <form action="{{ route('setup.app.settings') }}" method="post" autocomplete="off" enctype="multipart/form-data">

        <div class="panel">
            <div class="panel-heading">
                <h4>App Settings</h4>
            </div>
            <div class="panel-body">
                
                <div class="form-horizontal">

                    <div class="form-group">
                        <label for="application-name" class="col-xs-12 col-sm-3 control-label">Application Name</label>
                        <div class="col-xs-12 col-sm-6">
                            <input type="text" name="application_name" id="application-name" class="form-control" value="{{ old('application_name') }}" required />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="application-logo" class="col-xs-12 col-sm-3 control-label">Logo</label>
                        <div class="col-xs-12 col-sm-6">
                            <input type="file" name="logo" id="application-logo" class="form-control" />
                            <span class="text-muted" style="display:block; padding-top: 8px;">
                                <small>* jpeg, jpg, png &mdash; max of 1,000kb</small>
                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="application-address" class="col-xs-12 col-sm-3 control-label">Address</label>
                        <div class="col-xs-12 col-sm-6">
                            <input type="text" name="address" id="application-address" class="form-control" value="{{ old('address') }}" required />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="application-email" class="col-xs-12 col-sm-3 control-label">Email</label>
                        <div class="col-xs-12 col-sm-6">
                            <input type="email" name="email" id="application-email" class="form-control" value="{{ old('email') }}" required />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="application-contact-number" class="col-xs-12 col-sm-3 control-label">Contact Number</label>
                        <div class="col-xs-12 col-sm-6">
                            <input type="text" name="contact_number" id="application-contact-number" class="form-control" value="{{ old('contact_number') }}" required />
                        </div>
                    </div>

                    <div class="panel-separator"><span></span></div>

                    <div class="form-group fg-last">
                        <label for="shippo-api-key" class="col-xs-12 col-sm-3 control-label">Shippo API Key</label>
                        <div class="col-xs-12 col-sm-6">
                            <input type="text" name="shippo_private" id="shippo-api-key" class="form-control" value="{{ old('shippo_private') }}" />
                            <div class="text-muted">
                                <small><em>Not required but shipping module will be disabled</em></small>
                            </div>
                        </div>
                    </div>
                    
                </div>

            </div>
        </div>

        <div class="panel">
            <div class="panel-heading">
                <h4>Administrator</h4>
            </div>
            <div class="panel-body">

                <div class="form-horizontal">

                    <div class="form-group">
                        <label for="a-first-name" class="col-xs-12 col-sm-3 control-label">First Name</label>
                        <div class="col-xs-12 col-sm-6">
                            <input type="text" name="first_name" id="a-first-name" class="form-control" value="{{ old('first_name') }}" required />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="a-last-name" class="col-xs-12 col-sm-3 control-label">Last Name</label>
                        <div class="col-xs-12 col-sm-6">
                            <input type="text" name="last_name" id="a-last-name" class="form-control" value="{{ old('last_name') }}" required />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="a-email" class="col-xs-12 col-sm-3 control-label">Email</label>
                        <div class="col-xs-12 col-sm-6 col-lg-4">
                            <input type="email" name="admin_email" id="a-email" class="form-control" value="{{ old('admin_email') }}" required />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="a-password" class="col-xs-12 col-sm-3 control-label">Password</label>
                        <div class="col-xs-12 col-sm-6 col-lg-4">
                            <input type="password" name="password" id="a-password" class="form-control" value="" required />
                        </div>
                    </div>

                    <div class="form-group fg-last">
                        <label for="a-password-c" class="col-xs-12 col-sm-3 control-label">Confirm Password</label>
                        <div class="col-xs-12 col-sm-6 col-lg-4">
                            <input type="password" name="password_confirmation" id="a-password-c" class="form-control" value="" required />
                        </div>
                    </div>

                </div>
                
            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>

    {!! csrf_field() !!}
    </form>
    
</div>
</div>
</div>
@endsection