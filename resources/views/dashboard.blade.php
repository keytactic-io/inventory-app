@extends('layouts.app')

@section('page-title', 'Dashboard')

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.status')
    @include('includes.errors')

    @if ($currentUser->is_requester)

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="panel">
                    <div class="panel-body dashboard-stat">
                        <h2>{{ number_format($rqWaitingCount, 0, '.', ',') }}</h2>
                        <h4>Waiting For Approval</h4>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="panel">
                    <div class="panel-body dashboard-stat">
                        <h2>{{ number_format($rqForShippingCount, 0, '.', ',') }}</h2>
                        <h4>Pending Shipment</h4>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="panel">
                    <div class="panel-body dashboard-stat">
                        <h2>{{ number_format($rqRejectedCount, 0, '.', ',') }}</h2>
                        <h4>Rejected</h4>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="panel">
                    <div class="panel-body dashboard-stat">
                        <h2>{{ number_format($rqSplittedCount, 0, '.', ',') }}</h2>
                        <h4>Splitted</h4>
                    </div>
                </div>
            </div>
            @if ($canMakeRequest)
                <div class="col-xs-12">
                    <a href="{{ route('requests.create') }}" class="btn btn-primary btn-xlg btn-block">Create a New Request</a>
                </div>
            @endif
        </div>

    @else

        @if ($currentUser->role->name == 'Super Admin' OR $currentUser->role->name == 'Admin')

            <div class="row">
                <div class="col-xs-12 col-md-4">
                    <div class="panel">
                        <div class="panel-body dashboard-stat">
                            <h2>{{ number_format($rqCount, 0, '.', ',') }}</h2>
                            <h4>{{ $rqCount == 1 ? 'Request' : 'Requests' }}</h4>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="panel">
                        <div class="panel-body dashboard-stat">
                            <h2>{{ number_format($rqPendingCount, 0, '.', ',') }}</h2>
                            <h4>Pending Approval</h4>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="panel">
                        <div class="panel-body dashboard-stat">
                            <h2>{{ number_format($rqShippingCount, 0, '.', ',') }}</h2>
                            <h4>Pending Shipment</h4>
                        </div>
                    </div>
                </div>
                @if ($currentUser->allowedTo('add_requests') OR $currentUser->is_requester)
                    <div class="col-xs-12">
                        <div class="breaker-button">
                            <a href="{{ route('requests.create') }}" class="btn btn-primary btn-xlg btn-block">Create a New Request</a>
                        </div>
                    </div>
                @endif
                <div class="col-xs-12 col-md-6">
                    
                    <div class="panel">
                        <div class="panel-heading">
                            <h4>Most Requested Products</h4>
                        </div>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th class="text-right">Times Requested</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($mostRequestedProducts as $product)
                                        <tr>
                                            <td>
                                                <a href="{{ route('products.details') }}/{{ $product->id }}">
                                                    <strong>{{ $product->name}}</strong>
                                                </a>
                                            </td>
                                            <td class="text-right">{{ $product->request_count }}</td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="2" class="text-center active noselect">
                                                <strong>No products yet</strong>
                                            </td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
                <div class="col-xs-12 col-md-6">
                    
                    <div class="panel">
                        <div class="panel-heading">
                            <h4>Low Inventory Products</h4>
                        </div>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th class="text-right">Remaining Stocks</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($lowStockProducts as $lowStockProduct)
                                        <tr>
                                            <td>
                                                <a href="{{ route('products.details') }}/{{ $lowStockProduct->id }}">
                                                    <strong>{{ $lowStockProduct->name }}</strong>
                                                </a>
                                            </td>
                                            <td class="text-right">{{ $lowStockProduct->remaining_stocks }}</td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="2" class="text-center active noselect">
                                                <strong>No products yet</strong>
                                            </td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>

        @elseif ($currentUser->role->name == 'Shipping Staff')

            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="panel">
                        <div class="panel-body dashboard-stat">
                            <h2>{{ number_format($rqShippingCount, 0, '.', ',') }}</h2>
                            <h4>Pending Shipment</h4>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="panel">
                        <div class="panel-body dashboard-stat">
                            <h2>{{ number_format($rqShipped, 0, '.', ',') }}</h2>
                            <h4>Shipped</h4>
                        </div>
                    </div>
                </div>
            </div>

        @elseif ($currentUser->role->name == 'Purchasing Staff')

            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="panel">
                        <div class="panel-body dashboard-stat">
                            <h2>{{ number_format($poCount, 0, '.', ',') }}</h2>
                            <h4>{{ $poCount == 1 ? 'Purchase Order' : 'Purchase Orders' }}</h4>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="panel">
                        <div class="panel-body dashboard-stat">
                            <h2>{{ number_format($poUndeliveredCount, 0, '.', ',') }}</h2>
                            <h4>Waiting for Arrival</h4>
                        </div>
                    </div>
                </div>
                @if ($currentUser->allowedTo('add_purchase_orders'))
                    <div class="col-xs-12">
                        <a href="{{ route('pos.create') }}" class="btn btn-primary btn-xlg btn-block">Create Purchase Order</a>
                    </div>
                @endif
            </div>

        @else

        @endif

    @endif

</div>
</div>
</div>
@endsection
