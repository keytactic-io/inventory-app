@extends('layouts.app')

@section('page-title', 'Welcome!')
@section('body-class', 'login')

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    <div id="auth-panel">

        @if (app_setting('logo') != '' OR app_setting('logo') != null)
            <div class="auth-logo" style="background: url({{ asset('uploads/'. app_setting('logo')) }}) center center no-repeat; background-size: contain;">
                <a href="{{ url('/') }}"></a>
            </div>
        @else
            <h3>{{ app_setting('application_name') }}</h3>
        @endif

        @include('includes.errors')

        <div class="panel">

            <div class="panel-heading">
                <h4>Log In</h4>
            </div>

            <div class="panel-body">
                <form class="auth-form" method="post" action="{{ url('/login') }}" autocomplete="off">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <input type="email" class="form-control" id="email" placeholder="Email" name="email" 
                            value="{{ old('email') }}" required
                        />
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="password" placeholder="Password" name="password" 
                            value="{{ old('password') }}" required
                        />
                    </div>

                    <div class="form-group fg-last clearfix">
                        <div class="checkbox pull-left">
                            <label>
                                <input type="checkbox" name="remember" />
                                <span>Remember me</span>
                            </label>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">
                            Log In
                        </button>
                    </div>

                </form>
            </div>
            <div class="panel-footer">
                <small>
                    <a href="{{ url('/password/reset') }}">Forgot Password</a>
                </small>    
            </div>

        </div>

    </div>
    
</div>
</div>
</div>
@endsection