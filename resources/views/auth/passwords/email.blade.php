@extends('layouts.app')

@section('page-title', 'Welcome!')
@section('body-class', 'login')

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    <div id="auth-panel">

        @if (app_setting('logo') != '')
            <div class="auth-logo" style="background: url({{ asset('uploads/'. app_setting('logo')) }}) center center no-repeat; background-size: contain;">
                <a href="{{ url('/') }}"></a>
            </div>
        @else
            <h3>{{ app_setting('application_name') }}</h3>
        @endif

        @include('includes.errors')
        @include('includes.status')

        <div class="panel">

            <div class="panel-heading">
                <h4>Reset Password</h4>
            </div>

            <div class="panel-body">
                <form method="post" action="{{ url('/password/email') }}" autocomplete="off">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <input type="email" class="form-control" id="email" placeholder="Email" name="email" 
                            value="{{ old('email') }}" required
                        />
                    </div>

                    <div class="form-group fg-last">
                        <button type="submit" class="btn btn-primary">
                            Reset Password
                        </button>
                    </div>

                </form>
            </div>
            <div class="panel-footer">
                <small>
                    <a href="{{ url('/login') }}">Log In</a>
                </small>    
            </div>

        </div>

    </div>
    
</div>
</div>
</div>
@endsection