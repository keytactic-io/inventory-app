<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Request Created</title>
    <style>
        body {
            font-family: 'Arial', 'Helvetica', sans-serif;
            font-weight: 400;
            font-size: 14px;
            line-height: 22px!important;
            color: #666666;
            background-color: #f5f5f5;
        }
        .bg-container {
            background-color: #f5f5f5;
            padding: 40px;
            color: #666666;
        }
        .content {
            width: 100%;
            max-width: 680px;
            background: #ffffff;
            padding: 30px;
            margin: 0 auto;
            border-radius: 2px;
            color: #666666;
        }

        strong {
            font-weight: 500;
        }

        .text-right {
            text-align: right;
        }

        table {
            display: block;
            width: 100%;
            border-collapse: collapse;
        }
        table head,
        table tbody {
            display: block;
            width: 100%!important;
        }
        table.product-table thead tr th,
        table.product-table tbody tr td {
            padding: 12px;
            border-top: 1px solid #ececec;
            width: 100%;
        }
        table.product-table thead tr th {
            padding: 7px 12px;
            text-align: left;
            font-weight: 400;
            font-size: 11px;
            text-transform: uppercase;
            border-top: 0;
            background: #fbfbfb;
        }

        .action-btn {
            display: inline-block;
            background: #3bafda;
            padding: 6px 20px;
            color: #ffffff;
            text-decoration: none;
            border-radius: 2px;
        }
    </style>

</head>
<body>

<div class="bg-container">

    <div class="content" style="font-size: 14px; line-height: 22px;">

        <h2>Hi {{ $requester->user->email }}!</h2>
        <h4>You've been invited to become a requester for our {{ $requester->market->name }} market. Please click the button below to create your account.</h4>

        <br />

        <p><a href="{{ route('requesters.signup') }}?key={{ $requester->invite_key }}" class="action-btn">Create Account</a></p>

        <br />
        
        <p>
            Thank you,<br />
            <b>{{ app_setting('application_name') }}</b>
        </p>

    </div>

</div>

</body>
</html>
