<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Request Created</title>
    <style>
        body {
            font-family: 'Arial', 'Helvetica', sans-serif;
            font-weight: 400;
            font-size: 14px;
            line-height: 22px!important;
            color: #666666;
            background-color: #f5f5f5;
        }
        .bg-container {
            background-color: #f5f5f5;
            padding: 40px;
            color: #666666;
        }
        .content {
            width: 100%;
            max-width: 680px;
            background: #ffffff;
            padding: 30px;
            margin: 0 auto;
            border-radius: 2px;
            color: #666666;
        }

        strong {
            font-weight: 500;
        }

        .text-right {
            text-align: right;
        }

        table {
            display: block;
            width: 100%;
            border-collapse: collapse;
        }
        table head,
        table tbody {
            display: block;
            width: 100%!important;
        }
        table.product-table thead tr th,
        table.product-table tbody tr td {
            padding: 12px;
            border-top: 1px solid #ececec;
            width: 100%;
        }
        table.product-table thead tr th {
            padding: 7px 12px;
            text-align: left;
            font-weight: 400;
            font-size: 11px;
            text-transform: uppercase;
            border-top: 0;
            background: #fbfbfb;
        }

        table.details-tables tbody tr td {
            padding: 4px 0;
            vertical-align: top;
        }
        table.details-tables tbody tr td:first-child {
            padding-right: 15px;
        }
    </style>

</head>
<body>

<div class="bg-container">

    <div class="content" style="font-size: 14px; line-height: 22px!important;">

        <h2>Hey <b>{{ $productRequest->theRequester->name() }}</b>,</h2>

        <p>
            Just wanted to let you know that your request <b>{{ $productRequest->uid() }} has been shipped from {{ $fromLocation }}</b> via {{ $productRequest->courier }} {{ $productRequest->service_level_name }} and the tracking # is <b>@if ($productRequest->label_url != null) <a href="{{ $productRequest->label_url }}">{{ $productRequest->tracking_number }}</a> @else {{ $productRequest->tracking_number }} @endif</b>.
        </p>
        Attached is the final pdf of your request with all information. You may also view the request by logging into the <a href="{{ url('/') }}">{{ app_setting('application_name') }}</a>.

        <br />
        
        <p>
            <a href="{{ route('requests.create') }}"
                style="display: inline-block; padding: 8px 24px; background-color: #3bafda; color: #ffffff; text-decoration: none; border-radius: 4px;"
            >
                <b>Create Another Request</b>
            </a>
        </p>

        <br />

        <p>
            Thank you,<br />
            <b>{{ app_setting('application_name') }}</b>
        </p>

    </div>

</div>
</body>
</html>
