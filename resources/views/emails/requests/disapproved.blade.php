<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Request Created</title>
    <style>
        body {
            font-family: 'Arial', 'Helvetica', sans-serif;
            font-weight: 400;
            font-size: 14px;
            line-height: 22px!important;
            color: #666666;
            background-color: #f5f5f5;
        }
        .bg-container {
            background-color: #f5f5f5;
            padding: 40px;
            color: #666666;
        }
        .content {
            width: 100%;
            max-width: 680px;
            background: #ffffff;
            padding: 30px;
            margin: 0 auto;
            border-radius: 2px;
            color: #666666;
        }

        strong {
            font-weight: 500;
        }

        .text-right {
            text-align: right;
        }

        table {
            display: block;
            width: 100%;
            border-collapse: collapse;
        }
        table head,
        table tbody {
            display: block;
            width: 100%!important;
        }
        table.product-table thead tr th,
        table.product-table tbody tr td {
            padding: 12px;
            border-top: 1px solid #ececec;
            width: 100%;
        }
        table.product-table thead tr th {
            padding: 7px 12px;
            text-align: left;
            font-weight: 400;
            font-size: 11px;
            text-transform: uppercase;
            border-top: 0;
            background: #fbfbfb;
        }
    </style>

</head>
<body>

<div class="bg-container">

    <div class="content" style="font-size: 14px; line-height: 22px;">

        <h2>Hi, <b>{{ $requestProduct->theRequester->first_name }}</b>!</h2>
        <h4>Request #<b>{{ $requestProduct->uid() }}</b> was <span style="color: #d9534f;"><b>not approved</b></span> for shipment. If you have any questions regarding this request,
        please forward this email to the staff in charge. Below is a review of your request:</h4>
        <p style="line-height: 22px!important;">
            <b>Requester:</b> {{ $requestProduct->theRequester->name() }}<br />
            <b>Market:</b> {{ $requestProduct->market->name }}<br />
            <b>Ship-to Address:</b> {{ $requestProduct->getAddress() }}<br />
            <b>Expected In-hand Date:</b> {{ processDate($requestProduct->expected_inhand_date) }}
        </p>

        <br />

        <table class="product-table" cellspacing="0" cellpadding="0" border="0" width="100" style="border: 1px solid #ececec;">
            <thead>
                <tr>
                    <th>Product</th>
                    <th class="text-right">Quantity</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($requestProduct->requestedProducts as $rp)
                    <tr>
                        <td><b>{{ $rp->product->name }}</b></td>
                        <td class="text-right">{{ number_format($rp->quantity, '0', '.', ',') }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <br />
        <p>
            Thank you,<br />
            <b>{{ app_setting('application_name') }}</b>
        </p>
        
    </div>

</div>
</body>
</html>
