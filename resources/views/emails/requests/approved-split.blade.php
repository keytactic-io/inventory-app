<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Request Created</title>
    <style>
        body {
            font-family: 'Arial', 'Helvetica', sans-serif;
            font-weight: 400;
            font-size: 14px;
            line-height: 22px!important;
            color: #666666;
            background-color: #f5f5f5;
        }
        .bg-container {
            background-color: #f5f5f5;
            padding: 40px;
            color: #666666;
        }
        .content {
            width: 100%;
            max-width: 680px;
            background: #ffffff;
            padding: 30px;
            margin: 0 auto;
            border-radius: 2px;
            color: #666666;
        }

        strong {
            font-weight: 500;
        }

        .text-right {
            text-align: right;
        }

        table {
            display: block;
            width: 100%;
            border-collapse: collapse;
        }
        table head,
        table tbody {
            display: block;
            width: 100%!important;
        }
        table.product-table thead tr th,
        table.product-table tbody tr td {
            padding: 12px;
            border-top: 1px solid #ececec;
            width: 100%;
        }
        table.product-table thead tr th {
            padding: 7px 12px;
            text-align: left;
            font-weight: 400;
            font-size: 11px;
            text-transform: uppercase;
            border-top: 0;
            background: #fbfbfb;
        }

        table.details-tables tbody tr td {
            padding: 4px 0;
            vertical-align: top;
        }
        table.details-tables tbody tr td:first-child {
            padding-right: 15px;
        }
    </style>

</head>
<body>

<div class="bg-container">

    <div class="content" style="font-size: 14px; line-height: 22px!important;">

        <h2>Hey <b>{{ $productRequest->theRequester->name() }}</b>,</h2>

        <p>Just wanted to let you know that your request <b>{{ $productRequest->uid() }} has been approved</b> and is now pending shipment as of <b>{{ $asOfDate }}</b>. Please note that the request broken into shipments from <b>{{ $fromLocations }}</b>. All locations have been notified of the shipment request, and the expected in-hand delivery date of {{ processDate($productRequest->expected_inhand_date) }}.</p>
        
        <p>We will let you know as soon as the shipment is shipped, and share the tracking number.</p>

        <p>Here’s an overview of your request:</p>

        <br />

        <table class="details-tables" cellspacing="0" cellpadding="0" border="0" width="100">
            <tbody>
                <tr>
                    <td style="width: 40%;"><b>Request #</b></td>
                    <td style="width: 60%;">{{ $productRequest->uid() }}</td>
                </tr>
                <tr>
                    <td style="width: 40%;"><b>Date Created</b></td>
                    <td style="width: 60%;">{{ processDate($productRequest->created_at, true) }}</td>
                </tr>
                <tr>
                    <td style="width: 40%;"><b>Market</b></td>
                    <td style="width: 60%;">{{ $productRequest->market->name }}</td>
                </tr>
                <tr>
                    <td style="width: 40%;"><b>Ship-to Address</b></td>
                    <td style="width: 60%;">
                        <strong>{{ $productRequest->attention }}</strong><br />
                        {{ $productRequest->company }}<br />
                        {{ $productRequest->getAddressStreet() }}<br />
                        {{ $productRequest->getAddressCityStateZip() }}
                    </td>
                </tr>
                <tr>
                    <td style="width: 40%;"><b>Expected In-hand Date</b></td>
                    <td style="width: 60%;">{{ processDate($productRequest->expected_inhand_date) }}</td>
                </tr>
                <tr>
                    <td style="width: 40%;"><b>Justification</b></td>
                    <td style="width: 60%;">{{ $productRequest->justification }}</td>
                </tr>
                <tr>
                    <td style="width: 40%;"><b>Note</b></td>
                    <td style="width: 60%;">{{ $productRequest->note }}</td>
                </tr>
            </tbody>
        </table>

        <br />

        <table class="product-table" cellspacing="0" cellpadding="0" border="0" width="100" style="border: 1px solid #ececec;">
            <thead>
                <tr>
                    <th>Product</th>
                    <th class="text-right">Qty</th>
                    <th class="text-right">Allowed</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($productRequest->requestedProducts as $rp)
                    <tr>
                        <td><b>{{ $rp->product->name }}</b></td>
                        <td class="text-right">{{ number_format($rp->quantity, '0', '.', ',') }}</td>
                        <td class="text-right">{{ number_format($rp->allowed_quantity, '0', '.', ',') }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <br />
        
        <p>
            <a href="{{ route('requests.list') }}"
                style="display: inline-block; padding: 8px 24px; background-color: #3bafda; color: #ffffff; text-decoration: none; border-radius: 4px;"
            >
                <b>View Your Request</b>
            </a>
        </p>

        <br />

        <p>
            Thank you,<br />
            <b>{{ app_setting('application_name') }}</b>
        </p>

    </div>

</div>
</body>
</html>
