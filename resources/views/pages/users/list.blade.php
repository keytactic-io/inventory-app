@extends('layouts.app')

@section('page-title', 'Users')

@section('head-addon')
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.0/css/select.dataTables.min.css" />
<link rel="stylesheet" href="{{ asset('assets/css/dt-override.css') }}" />
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.status')

</div>
<div class="col-xs-12 col-md-9">

    <div class="panel">

        <div class="panel-heading">
            <h4>Users</h4>
        </div>

        <div class="panel-filter">
            <form class="datalist-filter">
                <div class="datalist-filter__search">
                    <input type="text" placeholder="Search users" class="form-control">
                </div>
            </form>
        </div>

        <div class="panel-preloader" data-target="#users-panel-body">
            <i class="fa fa-circle-o-notch fa-spin fa-fw"></i> &nbsp; Preparing users table
        </div>
        <div id="users-panel-body" class="panel-body hidden" style="display:none;">

            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab">All</a></li>
                <li role="presentation"><a href="#non-rqr" aria-controls="non-rqr" role="tab" data-toggle="tab">Non-Requesters</a></li>
                <li role="presentation"><a href="#rqr" aria-controls="rqr" role="tab" data-toggle="tab">Requesters</a></li>
                <li role="presentation"><a href="#active-users" aria-controls="active-users" role="tab" data-toggle="tab">Active</a></li>
                <li role="presentation"><a href="#inactive-users" aria-controls="inactive-users" role="tab" data-toggle="tab">Inactive</a></li>
            </ul>

            <div class="tab-content">

                <div id="all" role="tabpanel" class="tab-pane active">
                    <div class="table-responsive">
                        <table id="users-all" class="table datatable table-hover">
                            <thead>
                                <tr>
                                    <tr>
                                        <th class="table-photo">DP</th>
                                        <th>Name</th>
                                        <th>Role</th>
                                        <th>Status</th>
                                        <th>ID</th>
                                    </tr>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                    @if ($user->requester AND !$user->requester->registered)
                                        <?php continue; ?>
                                    @endif
                                    @include('pages.users.row', [
                                        'uid' => 'users-all-entry',
                                        'user' => $user
                                    ])
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div id="non-rqr" role="tabpanel" class="tab-pane">
                    <div class="table-responsive">
                        <table id="users-non-rqr" class="table datatable table-hover">
                            <thead>
                                <tr>
                                    <tr>
                                        <th class="table-photo">DP</th>
                                        <th>Name</th>
                                        <th>Role</th>
                                        <th>Status</th>
                                        <th>ID</th>
                                    </tr>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                    @if ($user->is_requester)
                                        <?php continue; ?>
                                    @endif
                                    @include('pages.users.row', [
                                        'uid' => 'users-non-rqr-entry',
                                        'user' => $user
                                    ])
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div id="rqr" role="tabpanel" class="tab-pane">
                    <div class="table-responsive">
                        <table id="users-rqr" class="table datatable table-hover">
                            <thead>
                                <tr>
                                    <tr>
                                        <th class="table-photo">DP</th>
                                        <th>Name</th>
                                        <th>Role</th>
                                        <th>Status</th>
                                        <th>ID</th>
                                    </tr>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                    @if (!$user->is_requester OR ($user->requester AND !$user->requester->registered))
                                        <?php continue; ?>
                                    @endif
                                    @include('pages.users.row', [
                                        'uid' => 'users-rqr-entry',
                                        'user' => $user
                                    ])
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div id="active-users" role="tabpanel" class="tab-pane">
                    <div class="table-responsive">
                        <table id="users-active" class="table datatable table-hover">
                            <thead>
                                <tr>
                                    <tr>
                                        <th class="table-photo">DP</th>
                                        <th>Name</th>
                                        <th>Role</th>
                                        <th>Status</th>
                                        <th>ID</th>
                                    </tr>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                    @if ($user->is_active)
                                        @if ($user->requester AND !$user->requester->registered)
                                            <?php continue; ?>
                                        @endif
                                        <tr id="users-active-entry-{{ $user->id }}" data-user-id="{{ $user->id }}">
                                            <td class="table-photo">
                                                <div class="table-user-dp" style="background: url({{ $user->dp() }}) center center no-repeat; background-size: cover; "></div>
                                            </td>
                                            <td class="table-strong">
                                                {{ $user->name() }}
                                            </td>
                                            <td>
                                                @if ($user->role_id > 0)
                                                    {{ $user->role->name }}
                                                @endif
                                                @if ($user->is_requester)
                                                    Requester
                                                @endif
                                            </td>
                                            <td>
                                                @if ($user->is_active)
                                                    Active
                                                @else
                                                    Inactive
                                                @endif
                                            </td>
                                            <td>{{ $user->id }}</td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div id="inactive-users" role="tabpanel" class="tab-pane">
                    <div class="table-responsive">
                        <table id="users-inactive" class="table datatable table-hover">
                            <thead>
                                <tr>
                                    <tr>
                                        <th class="table-photo">DP</th>
                                        <th>Name</th>
                                        <th>Role</th>
                                        <th>Status</th>
                                        <th>ID</th>
                                    </tr>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                    @if (!$user->is_active)
                                        @if ($user->requester AND !$user->requester->registered)
                                            <?php continue; ?>
                                        @endif
                                        <tr id="users-inactive-entry-{{ $user->id }}" data-user-id="{{ $user->id }}">
                                            <td class="table-photo">
                                                <div class="table-user-dp" style="background: url({{ $user->dp() }}) center center no-repeat; background-size: cover; "></div>
                                            </td>
                                            <td class="table-strong">
                                                {{ $user->name() }}
                                            </td>
                                            <td>
                                                @if ($user->role_id > 0)
                                                    {{ $user->role->name }}
                                                @endif
                                                @if ($user->is_requester)
                                                    Requester
                                                @endif
                                            </td>
                                            <td>
                                                @if ($user->is_active)
                                                    Active
                                                @else
                                                    Inactive
                                                @endif
                                            </td>
                                            <td>{{ $user->id }}</td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

        </div>

        @if ($currentUser->allowedTo('add_users'))
            {{-- <div class="panel-footer">
                <a href="{{ route('users.add') }}" class="btn btn-primary">Add User</a>
            </div> --}}
        @endif

    </div>

</div>
<div class="col-xs-12 col-md-3">

    <div class="panel">
        <div class="panel-body">
            <div id="user-preview" class="preview-container" data-preview-url="{{ route('users.details') }}">
                <div class="qp-photo"><i class="fa fa-image"></i></div>
                <br clear="all" />
                <div class="qp-info">
                    <h3>Select a user</h3>
                </div>
            </div>
        </div>
    </div>

</div>
</div>
</div>
@endsection

@section('footer-addon')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script>
$(function() {

    function tabInfo(table) {
        var id = $(table).closest('.tab-pane').attr('id'),
            tab = $('.nav-tabs a[aria-controls='+id+']'),
            length = $(table).DataTable().page.info().recordsDisplay,
            label = tab.find('span');
        if (label.length) { label.remove(); }
        tab.append(' <span>('+length+')</span>');
    }

    var tables = $('.datatable')
        .on('draw.dt', function () {
            tabInfo(this);
        })
        .on('search.dt', function () {
            tabInfo(this);
        })
        .DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5',
                'print'
            ],
            pageLength: 10,
            ordering: true,
            lengthChange: false,
            pagingType: 'numbers',
            select: {
                style: 'single'
            },
            'order': [ 1, 'asc' ],
            'columnDefs': [
                {
                    'targets': [0],
                    'orderable': false
                },
                {
                    'targets': [4],
                    'visible': false
                }
            ]
        })
        .on( 'select', function ( e, dt, type, indexes ) {
            var data = $(this).DataTable().rows(indexes).data()[0],
                id = data[4],
                previewContainer = $('#user-preview'),
                url = previewContainer.attr('data-preview-url');

            previewContainer.append('<div class="preview-preloader-overlay"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i></div>');
            $.get( url + '/' + id ).done(function(r) {
                r = $.parseJSON(r);
                previewContainer.html(r.view);
            });

        });

    $('#quick-search-input').on('keyup', function () {
        tables.search( this.value ).draw();
    });

});
</script>
@endsection
