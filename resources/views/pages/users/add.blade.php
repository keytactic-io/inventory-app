@extends('layouts.app')

@section('page-title', 'Add User')

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">
    @include('includes.errors')
</div>
<div class="col-xs-12">

    <form action="{{ route('users.add') }}" method="post" autocomplete="off">
    {!! csrf_field() !!}
    <div class="panel">
        <div class="panel-heading">
            <h4>Add User</h4>
        </div>
        
        <div class="panel-body">
            
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label for="first-name">First Name</label>
                        <input type="text" class="form-control" id="first-name" name="first_name" required />
                    </div>  
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label for="last-name">Last Name</label>
                        <input type="text" class="form-control" id="last-name" name="last_name" required />
                    </div>  
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="role-id">Role</label>
                        <select name="role_id" id="role-id" class="form-control" required>
                            <option value=""></option>
                            @foreach ($roles as $role)
                                <option value="{{ $role->id }}">{{ $role->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" class="form-control" id="email" name="email" required />
                    </div>  
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="password">Password</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="password" name="password" required />
                            <span class="input-group-btn">
                                <button class="btn btn-primary" id="generate-password" type="button">Generate</button>
                            </span>
                        </div>
                    </div>  
                </div>
            </div>

        </div>

        <div class="panel-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a href="{{ route('users.list') }}" class="btn btn-default">Cancel</a>
        </div>

    </div>
    </form>
</div>
</div>
</div>
@endsection
