@extends('layouts.app')

@section('page-title', $user->name())

@section('head-addon')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css">
<link rel="stylesheet" href="{{ asset('assets/css/bt-select-override.css') }}" />
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">
    
    @include('includes.status')

</div>
<div class="col-xs-12 @if ($user->is_requester) col-md-6 @endif">

    <div class="panel">
        
        <div class="panel-body">

            <div class="profile-head clearfix">

                <div class="profile-photo"
                    style="background: url({{ $user->dp() }}) center center no-repeat;background-size: cover;" 
                ></div>
                <div class="profile-info">
                    <h1>{{ $user->name() }}</h1>
                    <h3>
                        @if ($user->is_requester)
                            Requester
                        @else
                            {{ $user->role->name }}
                        @endif
                    </h3>
                    @if ($user->is_requester)
                        <p>Market: <a href="#">{{ $user->requester->market->name }}</a></p>
                    @endif
                    @if ($user->is_active)
                        <span class="label label-primary">Active</span>
                    @else
                        <span class="label label-warning">Inactive</span>
                    @endif
                </div>
            </div>

            @if ($user->is_requester)
                <form action="{{ route('requesters.address.setdefault') }}" autocomplete="off" id="set-default-form">
                {!! csrf_field() !!}
                <div class="table-responsive" style="margin-top: 24px;">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th style="width: 75px;">Default</th>
                                <th>Address</th>
                                @if ($currentUser->id == $user->id || $currentUser->allowedTo('edit_requesters'))
                                    <th class="text-right">&nbsp;</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($addresses as $address)
                                <tr id="address-tr-{{ $address->id}}">
                                    <td style="width: 75px;">
                                        <div class="radio" style="margin:0;">
                                            @if ($currentUser->allowedTo('edit_requesters'))
                                                <label>
                                                    <input type="radio" name="address_id" id="" class="set_default" value="{{ $address->id }}"
                                                        <?php if ($address->default == 1): ?>checked<?php endif; ?>
                                                        <?php if (!$currentUser->allowedTo('edit_requesters') || $currentUser->id != $user->id): ?>readonly<?php endif; ?>
                                                    />
                                                    <span>&nbsp;</span>
                                                </label>
                                            @else
                                                @if ($address->default == 1)
                                                    <i class="fa fa-check text-primary"></i>
                                                @else
                                                    <i class="fa fa-check text-muted"></i>
                                                @endif
                                            @endif
                                        </div>
                                    </td>
                                    <td>{{ $address->getAddress() }}</td>
                                    @if ($currentUser->id == $user->id || $currentUser->allowedTo('edit_requesters'))
                                        <td class="text-right">
                                            <a href="#" class="edit-address" data-url="{{ route('requesters.address.edit') }}/{{ $address->id }}">
                                                <i class="fa fa-pencil fa-fw"></i>
                                            </a>
                                            <a href="#" class="remove-address" data-url="{{ route('requesters.address.remove') }}/{{ $address->id}}">
                                                <i class="fa fa-trash-o fa-fw"></i>
                                            </a>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                </form>
            @endif

        </div>

        @if ($currentUser->allowedTo('edit_requesters'))
            <div class="panel-footer">
                <div class="btn-group">
                    @if ($currentUser->allowedTo('edit_requesters') || $currentUser->id == $user->id)
                        <a id="edit-btn" href="{{ route('users.profile.edit') }}/{{ $user->id }}" class="btn btn-primary btn-sm">Edit</a>
                    @endif
                    @if ($currentUser->allowedTo('delete_requesters'))
                        <button type="button" class="btn btn-primary btn-sm dropdown-toggle" 
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                        >
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a id="delete-a" href="#">Delete</a></li>
                        </ul>
                    @endif
                </div>
                
                @if ($user->is_requester)
                    <a href="{{ route('requesters.address.add') }}/{{ $user->requester->id }}" class="btn btn-primary btn-sm" id="add-address">
                        Add Address
                    </a>
                    <a href="{{ route('requesters.productallocation', ['requesterId' => $user->requester->id]) }}" class="btn btn-primary btn-sm">
                        Product Allocation
                    </a>
                @endif
            </div>
        @endif

    </div>

</div>
@if ($user->is_requester)
<div class="col-xs-12 col-md-6">

    <div class="panel">

        <div class="panel-heading">
            <h4>Quick Analysis ({{ date('Y') }})</h4>
        </div>

        <div class="panel-body">

            <div class="quick-a-stat row">
                <div class="col-xs-12 col-lg-6">
                    <h2 class="text-primary">
                        ${{ number_format($user->requester->getRequestAmount(), 2, '.', ',') }}
                    </h2>
                    <h5>Requests Amount</h5>
                </div>
                <div class="col-xs-12 col-lg-6">
                    <h2 class="text-primary">
                        {{ number_format($user->requester->getRequestedProductsCount(), 0, '.', ',') }}
                    </h2>
                    <h5>Products Requested</h5>
                </div>
            </div>

            <div class="table-responsive" style="margin-top: 40px;">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Category</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($user->requester->getRequestedProductsCategories() as $cat)
                            <tr>
                                <td>
                                    <a href="{{ route('products.list') }}?cat={{ $cat->id }}">
                                        {{ $cat->name }}
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td class="text-center active">No data found</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>

        </div>

    </div>

</div>
@endif
@if ($user->role_id > 0 AND $user->role_id != 4)
    <div class="col-xs-12">

        <div class="panel">
            <div class="panel-heading">
                <h4>Purchase Orders Made</h4>
            </div>
            <div class="panel-body">
                
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>UID</th>
                                <th>Vendor</th>
                                <th>Products</th>
                                <th>Created At</th>
                                <th class="text-right">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($purchaseOrders as $pO)
                                <tr>
                                    <td class="table-strong nowrap">
                                        <a href="#" class="preview-po" data-url="{{ route('pos.preview') }}/{{ $pO->id }}">
                                            {{ $pO->uid() }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ route('vendors.details') }}/{{ $pO->vendor_id }}" target="_blank">
                                            {{ $pO->vendor->name }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="preview-po" data-url="{{ route('pos.preview') }}/{{ $pO->id }}">
                                            {{ $pO->productCount() }}
                                        </a>
                                    </td>
                                    <td>{{ processDate($pO->created_at, true) }}</td>
                                    <td class="text-right nowrap">
                                        @if ($pO->received)
                                            <span class="label label-success">Received</span>
                                        @else
                                            <span class="label label-warning">Waiting for Arrival</span>
                                        @endif
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5" class="text-center active table-strong">No purchase orders made yet</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        
    </div>
@endif

@if ($user->is_requester)
    <div class="col-xs-12">

        <div class="panel">
            <div class="panel-heading">
                <h4>Requests History</h4>
            </div>
            <div class="panel-body">
                
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>UID</th>
                                <th>Market</th>
                                <th>Ship-to Address</th>
                                <th>Expected In-hand Date</th>
                                <th>Products</th>
                                <th class="text-right">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($requests as $request)
                                <tr>
                                    <td class="table-strong nowrap">
                                        <a href="#" data-url="{{ route('requests.details') }}/{{ $request->id }}" class="view-rq-details" title="View Details">
                                            {{ $request->uid() }}
                                        </a>
                                    </td>
                                    <td class="nowrap">
                                        <a href="{{ route('markets.details') }}/{{ $request->market->id }}" target="_blank">
                                            {{ $request->market->name }}
                                        </a>
                                    </td>
                                    <td>{{ $request->getAddress() }}</td>
                                    <td class="nowrap">{{ processDate($request->expected_inhand_date) }}</td>
                                    <td>
                                        <a href="#" data-url="{{ route('requests.details') }}/{{ $request->id }}" class="view-rq-details" title="View Details">
                                            {{ $request->productsCount() }}
                                        </a>
                                    </td>
                                    <td class="text-right">
                                        @if ($request->status == 0)
                                            <span class="label label-warning">
                                        @elseif ($request->status == 1)
                                            <span class="label label-success">
                                        @elseif ($request->status == 2)
                                            <span class="label label-primary">
                                        @else
                                            <span class="label label-danger">
                                        @endif
                                            {{ requestStatuses()[$request->status] }}
                                        </span>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="7" class="text-center active table-strong">No requests yet</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        
    </div>
@endif
</div>
</div>
@endsection

@section('footer-addon')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js"></script>
<script>
$(function() {

    $('.set_default').each(function() {
        $(this).on('change', function() {
            var form = $('#set-default-form');
            $.post( form.attr('action'), form.serialize() ).done(function(r) {
                console.log('New default address has been set.');
                $('#address-tr-' + r.new).addClass('success');
                setTimeout(function() {
                    $('#address-tr-' + r.new).removeClass('success');
                }, 2000);
                notifyUser(
                    'New default address',
                    'New default address was set!'
                )
            });
        });
    });

    $(document).on('click', '.edit-address', function(e) {
        e.preventDefault();
        loader.show();
        $.get( $(this).attr('data-url') ).done(function(r) {
            r = $.parseJSON(r);
            loader.hide();
            bootbox.dialog({
                title: r.title,
                message: r.view,
                onEscape: function () {
                    bootbox.hideAll();
                },
                buttons: {
                    confirm: {
                        label: 'Submit Changes',
                        className: 'btn-primary',
                        callback: function() {

                            var form = $('#edit-requester-address');
                            $.post( form.attr('action'), form.serialize() ).done(function(r) {
                                if (r.error) {
                                    $('.form-error').html(r.error);
                                }
                                if (r.done) {
                                    window.location = r.redirect;
                                }                                
                            });

                            return false;

                        }
                    },
                    cancel: {
                        label: 'Cancel',
                        className: 'btn-default'
                    }
                }
            });
        });
    });

    $(document).on('click', '.remove-address', function(e) {
        e.preventDefault();
        $.get( $(this).attr('data-url') ).done(function(r) {
            r = $.parseJSON(r);
            if (r.done) {
                $('#address-tr-' + r.id).remove();
                $('#address-tr-' + r.new).find('input').prop('checked', true);
            }
        });
    });

    $(document).on('click', '#add-address', function(e) {
        e.preventDefault();
        loader.show();
        $.get( $(this).attr('href') ).done(function(r) {
            r = $.parseJSON(r);
            loader.hide();
            bootbox.dialog({
                title: r.title,
                message: r.view,
                onEscape: function () {
                    bootbox.hideAll();
                },
                buttons: {
                    confirm: {
                        label: 'Add',
                        className: 'btn-primary',
                        callback: function() {

                            loader.show();
                            var form = $('#add-requester-address');
                            $.post( form.attr('action'), form.serialize() ).done(function(r) {
                                if (r.error) {
                                    $('.form-error').html(r.error);
                                    loader.hide();
                                }
                                if (r.done) {
                                    window.location = r.redirect;                                    
                                }                                
                            });

                            return false;

                        }
                    },
                    cancel: {
                        label: 'Cancel',
                        className: 'btn-default'
                    }
                }
            });
        });
    });

    $(document).on('click', '.preview-po', function(e) {
        e.preventDefault();
        loader.show();
        $.get( $(this).attr('data-url') ).done(function(r) {
            r = $.parseJSON(r);
            bootbox.dialog({
                title: r.title,
                message: r.view,
                size: 'large',
                onEscape: function () {
                    bootbox.hideAll();
                },
                buttons: {
                    cancel: {
                        label: 'Close',
                        className: 'btn-default'
                    }
                }
            });
            loader.hide();
        })
    });

    $(document).on('click', '.view-rq-details', function(e) {
        e.preventDefault();
        loader.show();
        $.get( $(this).data('url') ).done(function(r) {
            r = $.parseJSON(r);
            var ap_dap = r.visibility;
            bootbox.dialog({
                title: r.title,
                message: r.view,
                size: 'large',
                onEscape: function () {
                    bootbox.hideAll();
                },
                buttons: {
                    'Close' : {
                        className: 'btn-default'
                    }
                }
            });
            loader.hide();
        });
    });

});
</script>
@endsection