<tr id="{{ $uid }}-{{ $user->id }}" data-user-id="{{ $user->id }}">
    <td class="table-photo">
        <a id="details-a" href="{{ route('users.profile') }}/{{ $user->id }}"><div class="table-user-dp" style="background: url({{ $user->dp() }}) center center no-repeat; background-size: cover; "></div></a>
    </td>
    <td class="table-strong">
        {{ $user->name() }}
    </td>
    <td>
        @if ($user->role_id > 0)
            {{ $user->role->name }}
        @endif
        @if ($user->is_requester)
            Requester
        @endif
    </td>
    <td>
        @if ($user->is_active)
            Active
        @else
            Inactive
        @endif
    </td>
    <td>{{ $user->id }}</td>
</tr>
