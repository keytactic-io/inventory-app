@extends('layouts.app')

@section('page-title', 'Edit User')

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.errors')

    <form action="{{ route('users.profile.edit') }}/{{ $user->id }}" method="post" autocomplete="off" enctype="multipart/form-data"
        id="edit-profile-form"  class="with-preloader" 
    >
    {!! csrf_field() !!}
    <div class="panel">
        <div class="panel-heading">
            <h4>Edit Profile</h4>
        </div>
        
        <div class="panel-body">

            <div class="form-group">
                <label for="password">Photo</label>
                <input type="file" class="form-control" id="photo" name="photo" />
            </div>
            
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label for="first-name">First Name</label>
                        <input type="text" class="form-control" id="first-name" name="first_name" value="{{ old('first_name', $user->first_name)}}" required />
                    </div>  
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label for="last-name">Last Name</label>
                        <input type="text" class="form-control" id="last-name" name="last_name" value="{{ old('last_name', $user->last_name)}}" required />
                    </div>
                </div>

                @if ($user->is_requester AND !$currentUser->is_requester)
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label for="market">Market</label>
                            <select name="market_id" id="market" class="form-control" required>
                                <option value=""></option>
                                @foreach ($markets as $market)
                                    <option value="{{ $market->id }}"
                                        <?php if ($user->requester->market_id == $market->id): ?>selected<?php endif; ?>
                                    >
                                        {{ $market->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                @endif

                @if ($user->role_id > 0)
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label for="role-id">Role</label>
                        <select name="role_id" id="role-id" class="form-control" required>
                            <option value=""></option>
                            @foreach ($roles as $role)
                                <option value="{{ $role->id }}"
                                    <?php if ($user->role->id == $role->id AND old('role_id') == null): ?>selected<?php endif; ?>
                                    <?php if (old('role_id') != null AND old('role_id') == $role->id): ?>selected<?php endif; ?>
                                >
                                    {{ $role->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                @else
                    <input type="hidden" name="role_id" value="0" />
                @endif

                @if ($user->is_requester)
                <div class="col-xs-12">
                    <div class="panel-separator"><span></span></div>
                </div>

                <div class="col-xs-12">
                    <div class="alert alert-warning" style="margin-bottom: 0;">
                        @if ($user->id == $currentUser->id)
                            If you with to edit your address(es), please proceed to 
                            <a href="{{ route('users.profile') }}/{{ $user->id }}">your profile</a>.
                        @else
                            If you wish to edit the address(es), pleace proceed to 
                            <a href="{{ route('users.profile') }}/{{ $user->id }}">{{ $user->first_name }}'s profile</a>.
                        @endif
                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="panel-separator"><span></span></div>
                </div>
                @endif

                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" class="form-control" id="email" name="email" value="{{ old('email', $user->email)}}" required />
                    </div>  
                </div>
                @if ($user->is_requester)
                    <div class="col-xs-12 col-sm-8">
                @else
                    <div class="col-xs-12 col-sm-6">
                @endif
                    <div class="form-group">
                        <label for="password">Password</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="password" name="e_password" placeholder="Leave blank to remain unchange" />
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="button" id="generate-password">Generate</button>
                            </span>
                        </div>
                    </div>  
                </div>
            </div>

            @if (!$currentUser->is_requester)
                <div class="form-group fg-last">
                    <label>Active</label>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="is_active" id="" value="1" 
                                <?php if ($user->is_active): ?>checked<?php endif ;?>
                            />
                            <span>Check if yes</span>
                        </label>
                    </div>
                </div>
            @endif

        </div>

        <div class="panel-footer">
            <button type="submit" class="btn btn-primary">Submit Changes</button>
            <a href="{{ route('users.list') }}" class="btn btn-default">Cancel</a>
        </div>

    </div>
    </form>

</div>
</div>
</div>
@endsection

@section('footer-addon')
<script>
$(function() {

    $(document).on('click', '#add-address', function(e) {
        e.preventDefault();
        var count = $('.addresses-entry').length;
        $('.addresses').append(addAddress(count + 1));
    });
    $(document).on('click', '.remove-address', function(e) {
        e.preventDefault();
        $($(this).attr('data-target')).remove();
    });

});
function addAddress(count)
{
    var html = '<div id="address-entry-' + count + '" class="addresses-entry row">' +
    '<div class="col-xs-12">' +
    '<div class="form-group"><div class="radio"><label><input type="radio" name="default" id="" value="' + count + '" /><span>Set as default address</span></label></div></div>' +
    '</div>' +
    '<div class="col-xs-12 col-md-6">' +
    '<div class="form-group"><input type="text" name="addresses[' + count + '][street_1]" id="street-1" class="form-control" placeholder="Street 1" /></div>' +
    '</div>' +
    '<div class="col-xs-12 col-md-6">' +
    '<div class="form-group"><input type="text" name="addresses[' + count + '][street_2]" id="street-2" class="form-control" placeholder="Street 2" /></div>' +
    '</div>' +
    '<div class="col-xs-12 col-sm-6 col-md-3">' +
    '<div class="form-group"><input type="text" name="addresses[' + count + '][city]" id="city" class="form-control" placeholder="City" /></div>' +
    '</div>' +
    '<div class="col-xs-12 col-sm-6 col-md-4">' +
    '<div class="form-group"><input type="text" name="addresses[' + count + '][state]" id="state" class="form-control" placeholder="State" /></div>' +
    '</div>' +
    '<div class="col-xs-12 col-sm-8 col-md-3">' +
    '<div class="form-group"><input type="text" name="addresses[' + count + '][country]" id="country" class="form-control" placeholder="Country" /></div>' +
    '</div>' +
    '<div class="col-xs-12 col-sm-4 col-md-2">' +
    '<div class="form-group"><input type="text" name="addresses[' + count + '][postcode]" id="postcode" class="form-control" placeholder="Postcode" /></div>' +
    '</div>' +
    '<div class="col-xs-12">' +
    '<div class="form-group"><a href="#" class="remove-address btn btn-xs btn-danger" data-target="#address-entry-' + count + '">Remove Address</a></div>' +
    '</div>' +
    '<div class="col-xs-12">' +
    '<div class="form-group"><div class="address-separator"><span></span></div></div>' +
    '</div>' +
    '</div>';

    return html;

}
</script>
@endsection
