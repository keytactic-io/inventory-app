@extends('layouts.app')

@section('page-title', 'Edit User')

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.status')

    <form action="{{ route('users.edit') }}/{{ $user->id }}" method="post" autocomplete="off"
        id="edit-user-form" class="with-preloader" 
    >
    {!! csrf_field() !!}
    <div class="panel">
        <div class="panel-heading">
            <h4>Edit User</h4>
        </div>
        
        <div class="panel-body">
            
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label for="first-name">First Name</label>
                        <input type="text" class="form-control" id="first-name" name="first_name" value="{{ old('first_name', $user->first_name)}}" />
                    </div>  
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label for="last-name">Last Name</label>
                        <input type="text" class="form-control" id="last-name" name="last_name" value="{{ old('last_name', $user->last_name)}}" />
                    </div>  
                </div>
                @if ($user->role_id > 1)
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="role-id">Role</label>
                        <select name="role_id" id="role-id" class="form-control">
                            <option value=""></option>
                            @foreach ($roles as $role)
                                <option value="{{ $role->id }}"
                                    <?php if ($user->role->id == $role->id AND old('role_id') == null): ?>selected<?php endif; ?>
                                    <?php if (old('role_id') != null AND old('role_id') == $role->id): ?>selected<?php endif; ?>
                                >
                                    {{ $role->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                @else
                    <input type="hidden" name="role_id" value="0" />
                @endif
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" class="form-control" id="email" name="email" value="{{ old('email', $user->email)}}" />
                    </div>  
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="password">Password</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="password" name="e_password" placeholder="Leave blank to remain unchange" />
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Generate</button>
                            </span>
                        </div>
                    </div>  
                </div>
            </div>

            <div class="form-group">
                <label for="password">Active</label>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="is_active" id="" value="1" 
                            <?php if ($user->is_active): ?>checked<?php endif ;?>
                        />
                        <span>Check if yes</span>
                    </label>
                </div>
            </div>                    

        </div>

        <div class="panel-footer">
            <button type="submit" class="btn btn-primary">Submit Changes</button>
            <a href="{{ route('users.list') }}" class="btn btn-default">Cancel</a>
        </div>

    </div>
    </form>
</div>
</div>
</div>
@endsection
