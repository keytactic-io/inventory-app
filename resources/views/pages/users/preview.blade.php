<div class="qp-photo" style="background: url({{ $user->dp() }}) center center no-repeat; background-size: cover; "></div>
<br clear="all" />
<div class="qp-info">
    @if ($user->is_active)
        <span class="user-status label label-primary">Active</span>
    @else
        <span class="user-status label label-warning">Inactive</span>
    @endif
    <h3><a id="details-a" href="{{ route('users.profile') }}/{{ $user->id }}">{{ $user->name() }}</a></h3>
    <div class="qp-meta">
        <h5>
            @if ($user->role_id > 0)
                {{ $user->role->name }}
            @endif
            @if ($user->is_requester)
                Requester
            @endif
        </h5>
    </div>
</div>
<div class="qp-options clearfix">
	@if ($currentUser->allowedTo('view_users'))
    <a id="details-a" href="{{ route('users.profile') }}/{{ $user->id }}" class="btn btn-primary btn-sm">Profile</a>
	@endif
	@if ($currentUser->allowedTo('edit_users'))
    <a id="edit-btn" href="{{ route('users.profile.edit') }}/{{ $user->id }}" class="btn btn-warning btn-sm">Edit</a>
	@endif
	@if ($currentUser->allowedTo('delete_users'))
    <a id="delete-a" href="{{ route('users.delete') }}/{{ $user->id }}" class="btn btn-danger btn-sm">Delete</a>
	@endif
</div>
