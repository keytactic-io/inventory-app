@extends('layouts.app')

@section('page-title', 'Vendors')

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.status')

    <div id="the-vendors">

        <div class="row">
            <div class="col-xs-12">
                <div class="panel">
                    <div class="panel-body">

                        @if ($currentUser->allowedTo('add_vendors'))
                            <div class="input-group">
                                <input class="search form-control" placeholder="Search vendor" />
                                <span class="input-group-btn">
                                    <a href="{{ route('vendors.add') }}" class="btn btn-primary btn-squared">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </span>
                            </div>
                        @else
                            <input class="search form-control" placeholder="Search vendor" />
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="row list">
            @forelse ($vendors as $vendor)
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                    <div class="panel vendor-entry vendor-entry-{{ $vendor->id }}">
                        <div class="panel-body">
                            <div class="card-photo">
                                @if ($vendor->photo == null)
                                    <div class="card-photo-container no-photo">{{ substr($vendor->name, 0, 1) }}</div>
                                @else
                                    <div class="card-photo-container" style="background:url({{ $vendor->getPhoto() }}) center center no-repeat; background-size: cover;"></div>
                                @endif
                            </div>
                            <div class="card-info text-center">
                                <h4>
                                    <a href="{{ route('vendors.details') }}/{{ $vendor->id }}">
                                        <strong class="vs-name">{{ $vendor->name }}</strong>
                                    </a>
                                </h4>
                                <h5 class="vs-address">{{ $vendor->getAddress() }}</h5>

                                C: <strong class="vs-contact-person">{{ $vendor->contact_person }}</strong>
                                @if ($vendor->mobile_number != null)
                                    <br /><span class="vs-mobile-number">M: {{ $vendor->mobile_number }}</span>
                                @endif
                                @if ($vendor->landline_number != null)
                                    <br /><span class="vs-landline-number">L: {{ $vendor->landline_number }}</span>
                                @endif
                                @if ($vendor->fax_number != null)
                                    <br /><span class="vs-fax-number">F: {{ $vendor->fax_number }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="panel-footer text-center">
                            <span class="hidden vs-email">{{ $vendor->email }}</span>
                            <div class="btn-group" role="group" aria-label="...">
                                <a href="tel:{{ $vendor->mobile_number }}" class="btn btn-default btn-xs"><i class="fa fa-phone-square"></i> &nbsp; Call</a>
                                <a href="mailto:{{ $vendor->email }}" class="btn btn-default btn-xs"><i class="fa fa-envelope"></i> &nbsp; Email</a>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
                <div class="col-xs-12">
                    <div class="alert alert-warning text-center">
                        No vendors yet.
                    </div>
                </div>
            @endforelse
        </div>
        <div class="row">
            <div class="col-xs-12">
                <ul class="pagination"></ul>
            </div>
        </div>

    </div><!-- / the-vendors -->

</div>
</div>
</div>
@endsection

@section('footer-addon')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/list.js/1.3.0/list.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/list.pagination.js/0.1.1/list.pagination.min.js"></script>
<script>
$(function() {

    var userList = new List('the-vendors', {
        valueNames: [
            'vs-name',
            'vs-email',
            'vs-address',
            'vs-fax-number',
            'vs-mobile-number',
            'vs-contact-person',
            'vs-landline-number'
        ],
        page: 12,
        plugins: [ ListPagination({}) ]
    });

});
</script>
@endsection
