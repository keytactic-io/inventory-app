@extends('layouts.app')

@section('page-title', 'Vendors')

@section('head-addon')
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.0/css/select.dataTables.min.css" />
<link rel="stylesheet" href="{{ asset('assets/css/dt-override.css') }}" />
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.status')

    <div class="panel">
        <div class="panel-heading">
            <h4>Vendors</h4>
        </div>

        <div class="panel-filter">
            <input type="text" placeholder="Find vendor" class="form-control" id="quick-search-input" autocomplete="off">
        </div>

        <div class="panel-preloader" data-target="#vendors-panel">
            <i class="fa fa-circle-o-notch fa-spin fa-fw"></i> &nbsp; Preparing vendors table
        </div>
        <div id="vendors-panel" class="panel-body" style="display:none;">

            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab">All</a></li>
                {{-- <li role="presentation"><a href="#received" aria-controls="received" role="tab" data-toggle="tab">Received</a></li> --}}
            </ul>

            <div class="tab-content">

                <!-- all -->
                <div role="tabpanel" class="tab-pane active" id="all">

                    <div class="table-responsive">
                        <table id="vendors-all" class="table datatable table-hover">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Contact Person</th>
                                    <th>Email</th>
                                    <th>Contact Numbers</th>
                                    <th class="text-right">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($vendors as $vendor)
                                    <tr id="vendors-all-entry-{{ $vendor->id }}">
                                        <td>
                                            @if ($currentUser->allowedTo('edit_vendors'))
                                                <a href="{{ route('vendors.details') }}/{{ $vendor->id }}">
                                                    <strong>{{ $vendor->name }}</strong>
                                                </a>
                                            @else
                                                <strong>{{ $vendor->name }}</strong>
                                            @endif
                                            @if ($vendor->address != '' OR $vendor->address != null)
                                                <span style="display:block;" title="Mobile Number"><em>{{ $vendor->address }}</em></span>
                                            @endif
                                        </td>
                                        <td>{{ $vendor->contact_person }}</td>
                                        <td>
                                            <a href="mailto:{{ $vendor->email }}">
                                                {{ $vendor->email }}
                                            </a>
                                        </td>
                                        <td>
                                            @if ($vendor->mobile_number != '' OR $vendor->mobile_number != null)
                                                <span style="display:block;" title="Mobile Number"><i class="fa fa-mobile-phone fa-fw"></i> &nbsp; {{ $vendor->mobile_number }}</span>
                                            @endif
                                            @if ($vendor->tel_number != '' OR $vendor->tel_number != null)
                                                <span style="display:block;" title="Telephone Number"><i class="fa fa-phone fa-fw"></i> &nbsp; {{ $vendor->tel_number }}</span>
                                            @endif
                                            @if ($vendor->fax_number != '' OR $vendor->fax_number != null)
                                                <span style="display:block;" title="Fax Number"><i class="fa fa-fax fa-fw"></i> &nbsp; {{ $vendor->fax_number }}</span>
                                            @endif
                                        </td>
                                        <td class="text-right">
                                            @if ($currentUser->allowedTo('edit_vendors'))
                                                <a href="{{ route('vendors.edit') }}/{{ $vendor->id }}"><i class="fa fa-pencil fa-fw"></i></a>
                                            @endif
                                            @if ($currentUser->allowedTo('delete_vendors'))
                                                <a href="{{ route('vendors.delete') }}/{{ $vendor->id }}"><i class="fa fa-trash-o-fa-fw"></i></a>
                                            @endif
                                        </td>
                                    </tr>
                                @empty
                                    {{-- <tr>
                                        <td colspan="5" class="text-center">No vendors yet.</td>
                                    </tr> --}}
                                @endforelse
                            </tbody>
                        </table>
                    </div>

                </div>
                
            </div>

        </div>
        
        @if ($currentUser->allowedTo('add_vendors'))
            {{-- <div class="panel-footer">
                <a href="{{ route('vendors.add') }}" class="btn btn-primary">Add Vendor</a>
            </div> --}}
        @endif

    </div>
</div>
</div>
</div>
@endsection

@section('footer-addon')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script>
$(function() {

    function tabInfo(table) {
        var id = $(table).closest('.tab-pane').attr('id'),
            tab = $('.nav-tabs a[aria-controls='+id+']'),
            length = $(table).DataTable().page.info().recordsDisplay,
            label = tab.find('span');
        if (label.length) { label.remove(); }
        tab.append(' <span>('+length+')</span>');
    }

    var tables = $('.datatable')
        .on('draw.dt', function () {
            tabInfo(this);
        })
        .on('search.dt', function () {
            tabInfo(this);
        })
        .DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5',
                'print'
            ],
            pageLength: 10,
            ordering: true,
            lengthChange: false,
            pagingType: 'numbers',
            'order': [ 0, 'asc' ],
            'columnDefs': [
                {
                    'targets': [2, 3, 4],
                    'orderable': false
                }
            ]
        });

    $('#quick-search-input').on('keyup', function () {
        tables.search( this.value ).draw();
    });

});
</script>
@endsection
