@extends('layouts.app')

@section('page-title', $vendor->name)

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.status')

</div>
<div class="col-xs-12 col-md-6">    

    <div class="panel">
        
        <div class="panel-body">
            
            <div class="details-content">

                <div class="details-main-info clearfix">
                    @if ($vendor->photo == null)
                        <div class="details-photo details-photo-initial">{{ substr($vendor->name, 0, 1) }}</div>
                    @else
                        <div class="details-photo" style="background:url({{ $vendor->getPhoto() }}) center center no-repeat;background-size:cover;"></div>
                    @endif
                    <h1>{{ $vendor->name }}</h1>
                    <h4 title="Address"><i class="fa fa-map-marker fa-fw"></i> &nbsp; {{ $vendor->getAddress() }}</h4>
                    <h4 title="Contact Person"><i class="fa fa-vcard-o fa-fw"></i> &nbsp; {{ $vendor->contact_person }}</h4>
                </div>

                <div class="form-group-divider"><span></span></div>

                <div class="details-more-info">
                    <h5><i class="fa fa-envelope fa-fw"></i> &nbsp; <a href="mailto:{{ $vendor->email }}">{{ $vendor->email }}</a></h5>
                    <h5><i class="fa fa-mobile fa-fw"></i> &nbsp; {{ $vendor->mobile_number }}</h5>

                    @if ($vendor->landline_number != null OR $vendor->landline_number != '')
                        <h5><i class="fa fa-phone fa-fw"></i> &nbsp; {{ $vendor->landline_number }}</h5>
                    @endif

                    @if ($vendor->fax_number != null OR $vendor->fax_number != '')
                        <h5><i class="fa fa-fax fa-fw"></i> &nbsp; {{ $vendor->fax_number }}</h5>
                    @endif
                </div>

            </div>

        </div>        
        <div class="panel-footer">
            @if ($currentUser->allowedTo('edit_vendors'))
                <a href="{{ route('vendors.edit') }}/{{ $vendor->id }}" 
                    class="btn btn-primary btn-sm"
                >
                    Edit
                </a>
            @endif
            @if ($currentUser->allowedTo('delete_vendors'))
                <a href="{{ route('vendors.delete') }}/{{ $vendor->id }}"
                    class="btn btn-sm btn-danger" 
                >
                    Delete
                </a>
            @endif
        </div>

    </div>

</div>
<div class="col-xs-12 col-md-6">
    
    <div class="panel">

        <div class="panel-heading">
            <h4>Quick Analysis ({{ date('Y') }})</h4>
        </div>

        <div class="panel-body">

            <div class="quick-a-stat row">
                <div class="col-xs-12 col-lg-6">
                    <h2 class="text-primary">
                        ${{ number_format($vendor->getTotalSpent(), 2, '.', ',') }}
                    </h2>
                    <h5>Dollars Spent</h5>
                </div>
                <div class="col-xs-12 col-lg-6">
                    <h2 class="text-primary">
                        {{ number_format($vendor->getProductQuantity(), 0, '.', ',') }}
                    </h2>
                    <h5>Products Purchased</h5>
                </div>
            </div>

            <div class="table-responsive" style="margin-top: 40px;">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Category</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($vendor->getProductCategories() as $cat)
                            <tr>
                                <td>
                                    <a href="{{ route('products.list') }}?cat={{ $cat->id }}">
                                        {{ $cat->name }}
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td class="text-center active">No data found</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>

        </div>

    </div>

</div>
<div class="col-xs-12">
    
    <div class="panel">

        <div class="panel-heading">
            <h4>Purchase Orders History</h4>
        </div>

        <div class="panel-body">

            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>UID</th>
                            <th>Products</th>
                            <th>Created By</th>
                            <th>Created At</th>
                            <th class="text-right">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($purchaseOrders as $purchaseOrder)
                            <tr>
                                <td class="table-strong nowrap">
                                    <a href="#" class="preview-po" data-url="{{ route('pos.preview') }}/{{ $purchaseOrder->id }}">
                                        {{ $purchaseOrder->uid() }}
                                    </a>
                                </td>
                                <td>
                                    <a href="#" class="preview-po" data-url="{{ route('pos.preview') }}/{{ $purchaseOrder->id }}">
                                        {{ $purchaseOrder->productCount() }}
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ route('users.profile') }}/{{ $purchaseOrder->processed_by }}" target="_blank">
                                        {{ $purchaseOrder->processedBy->name() }}
                                    </a>
                                </td>
                                <td>{{ processDate($purchaseOrder->created_at, true) }}</td>
                                <td class="text-right">
                                    @if ($purchaseOrder->received)
                                        <span class="label label-success">Received</span>
                                    @else
                                        <span class="label label-warning">Waiting for Arrival</span>
                                    @endif
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5" class="text-center active table-strong">No purchase orders made yet</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>

    </div>

</div>
</div>
</div>
@endsection

@section('footer-addon')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script>
$(function() {

    $(document).on('click', '.preview-po', function(e) {
        e.preventDefault();
        loader.show();
        $.get( $(this).attr('data-url') ).done(function(r) {
            r = $.parseJSON(r);
            bootbox.dialog({
                title: r.title,
                message: r.view,
                size: 'large',
                onEscape: function () {
                    bootbox.hideAll();
                },
                buttons: {
                    cancel: {
                        label: 'Close',
                        className: 'btn-default'
                    }
                }
            });
            loader.hide();
        })
    });

});
</script>
@endsection
