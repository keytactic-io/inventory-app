@extends('layouts.app')

@section('page-title', 'Edit Vendor')

@section('head-addon')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css">
<link rel="stylesheet" href="{{ asset('assets/css/bt-select-override.css') }}" />
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.errors')

    <form action="{{ route('vendors.edit') }}/{{ $vendor->id }}" method="post" autocomplete="off"
        id="edit-vendor-form" class="with-preloader" 
    >
    {!! csrf_field() !!}
    <input type="hidden" name="vendor_id" value="{{ $vendor->id }}" />
    <div class="panel">

        <div class="panel-heading">
            <h4>Edit Vendor</h4>
        </div>

        <div class="panel-body">

            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $vendor->name) }}" required />
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="form-group fg-last">
                        <label for="street-1">Address</label>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control" id="street-1" name="street_1" placeholder="Street 1"
                                    value="{{ old('street_1', $vendor->street_1) }}" required
                                />
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control" id="street-2" name="street_2" placeholder="Street 2"
                                    value="{{ old('street_2', $vendor->street_2) }}"
                                />
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-5">
                            <div class="form-group">
                                <input type="text" class="form-control" id="city" name="city" placeholder="City"
                                    value="{{ old('city', $vendor->city) }}" required
                                />
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-5">
                            <div class="form-group">
                                <select name="state" id="the-state" class="form-control"
                                    data-live-search="true" required
                                >
                                    <option value=""></option>
                                    @foreach (getStates() as $state)
                                        <option value="{{ $state }}"
                                            @if ($state == $vendor->state) selected @endif
                                        >
                                            {{ $state }}
                                        </option>
                                    @endforeach
                                </select>
                                <input type="hidden" name="country" value="US" />
                            </div>
                        </div>
                        <?php /*
                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control" id="country" name="country" placeholder="Country"
                                    value="{{ old('country', $vendor->country) }}" required
                                />
                            </div>
                        </div>
                        */ ?>
                        <div class="col-xs-12 col-sm-6 col-md-2">
                            <div class="form-group">
                                <input type="text" class="form-control" id="postcode" name="postcode" placeholder="Zip" 
                                    value="{{ old('postcode', $vendor->postcode) }}" required
                                />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="mobile-number">Mobile Number</label>
                        <input type="text" class="form-control" id="mobile-number" name="mobile_number"
                            value="{{ old('mobile_number', $vendor->mobile_number) }}" required
                        />
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="landline-number">Landline Number</label>
                        <input type="text" class="form-control" id="landline-number" name="tel_number"
                            value="{{ old('tel_number', $vendor->tel_number) }}"
                        />
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="fax-number">Fax Number</label>
                        <input type="text" class="form-control" id="fax-number" name="fax_number"
                            value="{{ old('fax_number', $vendor->fax_number) }}"
                        />
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" class="form-control" id="email" name="email"
                            value="{{ old('email', $vendor->email) }}" required
                        />
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <label for="contact-person">Contact Person</label>
                        <input type="text" class="form-control" id="contact-person" name="contact_person"
                            value="{{ old('contact_person', $vendor->contact_person) }}" required
                        />
                    </div>
                </div>
            </div>

        </div>

        <div class="panel-footer">
            <button type="submit" class="btn btn-primary">Submit Changes</button>
            <a href="{{ route('vendors.list') }}" class="btn btn-default">Cancel</a>
        </div>

    </div>
    </form>

</div>
</div>
</div>
@endsection

@section('footer-addon')
<script src="{{ asset('assets/js/vendors/inputmask.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js"></script>
<script>
$(function() {

    $('#landline-number, #mobile-number, #fax-number').mask('999-999-9999');

    $('#the-state').selectpicker({
        style: 'btn-default',
        size: 6
    });

});
</script>
@endsection

