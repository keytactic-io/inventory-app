<div class="form-error"></div>
<form action="{{ route('vendors.add.modal') }}" method="post" autocomplete="off" id="add-vendor-form">
{!! csrf_field() !!}
        
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" />
            </div>  
        </div>
        <div class="col-xs-12">
            <div class="form-group fg-last">
                <label for="street-1">Address</label>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <input type="text" class="form-control" id="street-1" name="street_1" placeholder="Street 1" value="{{ old('street_1') }}" />
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-group">
                        <input type="text" class="form-control" id="street-2" name="street_2" placeholder="Street 2" value="{{ old('street_2') }}" />
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="form-group">
                        <input type="text" class="form-control" id="city" name="city" placeholder="City" value="{{ old('city') }}" />
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="form-group">
                        <input type="text" class="form-control" id="state" name="state" placeholder="State" value="{{ old('state') }}" />
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="form-group">
                        <input type="text" class="form-control" id="country" name="country" placeholder="Country" value="{{ old('country', 'USA') }}" />
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="form-group">
                        <input type="text" class="form-control" id="postcode" name="postcode" placeholder="Postcode" value="{{ old('postcode') }}" />
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label for="mobile-number">Mobile Number</label>
                <input type="text" class="form-control" id="mobile-number" name="mobile_number" value="{{ old('mobile_number') }}" />
            </div>  
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label for="landline-number">Landline Number</label>
                <input type="text" class="form-control" id="landline-number" name="landline_number" value="{{ old('landline_number') }}" />
            </div>  
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label for="fax-number">Fax Number</label>
                <input type="text" class="form-control" id="fax-number" name="fax_number" value="{{ old('fax_number') }}" />
            </div>  
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" id="email" name="email" value="{{ old('email') }}" />
            </div>  
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="form-group">
                <label for="contact-person">Contact Person</label>
                <input type="text" class="form-control" id="contact-person" name="contact_person" value="{{ old('contact_person') }}" />
            </div>  
        </div>

    </div>

</form>

<script src="{{ asset('assets/js/vendors/inputmask.js') }}"></script>
<script>
$(function () {
    $('#landline-number, #mobile-number, #fax-number').mask('999-999-9999');
});
</script>