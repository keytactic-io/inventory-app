<tr id="{{ $uid }}-{{ $request->id }}" 
    data-url="{{ route('requests.details') }}/{{ $request->id }}" 
    class="view-details" 
    data-status="{{ $request->status }}"
>
    <td class="table-strong nowrap">
        {{-- <a data-url="{{ route('requests.details') }}/{{ $request->id }}" class="view-details" 
            title="View Details" data-status="{{ $request->status }}" href="#"
        > --}}
        @if ($request->status == 0)
            <span class="label label-warning">
        @elseif ($request->status == 1)
            <span class="label label-success">
        @elseif ($request->status == 2)
            <span class="label label-info">
        @else
            <span class="label label-danger">
        @endif
            {{ $request->uid() }}
        </span>        
    </td>
    <td class="nowrap">
        <a href="{{ route('users.profile') }}/{{ $request->theRequester->id }}">
            {{ $request->theRequester->name() }}
        </a>
    </td>
    <td>
        <a href="{{ route('markets.details') }}/{{ $request->market->id }}">
            {{ $request->market->name }}
        </a>
    </td>
    <td class="nowrap">{{ processDate2($request->expected_inhand_date) }}</td>
    <td>{{ $request->productsCount() }}</td>
    <td>{{ processDate2($request->created_at, true) }}</td>
    <td class="text-right">
        <a href="{{ route('requests.pdf') }}/{{ $request->id }}" target="_blank">
            <i class="fa fa-file-pdf-o fa-fw"></i>
        </a>
        @if ($request->status == 2 AND !$request->manual_input)
            <a href="{{ $request->label }}" target="_blank" title="Download Label">
                <i class="fa fa-download fa-fw"></i>
            </a>
        @endif
        @if ($request->status == 0)            
            @if ($currentUser->allowedTo('edit_requests'))
                <a href="{{ route('requests.edit', ['requestId' => $request->id, 'requesterId' => $request->theRequester->requester->id]) }}"><i class="fa fa-pencil fa-fw"></i></a>
            @endif
            @if ($currentUser->allowedTo('delete_requests'))
                <a href="#" class="delete-request" data-url="{{ route('requests.delete') }}/{{ $request->id }}"><i class="fa fa-trash-o fa-fw"></i></a>
            @endif
        @endif
    </td>
    <td>{{ route('requests.details') }}/{{ $request->id }}</td>
    <td>{{ $request->status }}</td>
</tr>