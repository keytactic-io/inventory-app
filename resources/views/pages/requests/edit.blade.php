@extends('layouts.app')

@section('page-title', 'Edit Request')

@section('head-addon')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css">
<link rel="stylesheet" href="{{ asset('assets/css/bt-select-override.css') }}" />
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.errors')

    <form action="{{ route('requests.edit', ['requestId' => $requestProduct->id, 'requesterId' => $requestProduct->theRequester->requester->id]) }}" 
        method="post" autocomplete="off" id="update-request-form"
    >
        {!! csrf_field() !!}

        <div class="panel">
            <div class="panel-heading">
                @if ($currentUser->id == $requester->user->id)
                    <h4>Edit Request</h4>
                @else
                    <h4>Edit Request for <u><a href="{{ route('requests.requester.select') }}?edit={{ $requestProduct->id }}">{{ $requester->user->name() }}</a></u></h4>
                @endif
            </div>
            <div class="panel-body">

                <div class="form-horizontal">
                    <div class="form-group">
                        <label for="market-id" class="control-label col-xs-12 col-sm-3">Department</label>
                        <div class="col-xs-12 col-sm-9 col-lg-6">
                            <select name="market_id" id="market-id" class="form-control" required>
                                <option value=""></option>
                                @foreach ($markets as $market)
                                    <option value="{{ $market->id }}"
                                        @if (old('market_id', $requestProduct->market_id) == $market->id)  selected @endif
                                    >
                                        {{ $market->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

            </div>
            <div class="panel-body panel-body-invert">

                <div class="form-group">
                    <label for="products-selection">Select Asset</label>
                    <select id="products-selection" class="form-control"
                        data-live-search="true"
                    >
                        <option value=""></option>
                        @foreach ($products as $product)
                            @if ($product->remaining_stocks > 0)
                                <option value="{{ $product->id }}"
                                    data-allocation="{{ $product->allocation }}" 
                                    data-remaining="{{ $product->remaining_stocks }}"
                                >
                                    {{ $product->name }} ({{ $product->remaining_stocks }})
                                </option>
                            @endif
                        @endforeach
                    </select>
                </div>

                <div class="table-responsive">
                    <table id="products-table" class="table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Allocation</th>
                                <th style="width:124px;">Quantity</th>
                                <th style="width:60px;" class="text-right">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($requestProduct->requestedProducts as $requestedProduct)
                                <?php 
                                    $allocation = $requestedProduct->product->hasAllocation($requestProduct->theRequester->requester->id);
                                    $remaining = $requestedProduct->product->stocksRemaining();
                                    $allowed = ($remaining >= $allocation) ? $allocation : $remaining;
                                    if ($allocation < 0) {
                                        $allowed = $remaining;
                                    }
                                ?>
                                <tr id="product-tr-{{ $requestedProduct->product->id }}">
                                    <td class="table-strong">
                                        {{ $requestedProduct->product->name }} ({{ $remaining }})
                                    </td>
                                    <td>
                                        {{ $allocation < 0 ? '&#x221e;' : $allocation }}
                                    </td>
                                    <td>
                                        <input type="number" name="products[{{ $requestedProduct->product->id }}]" class="input-in-table" 
                                            placeholder="0" required min="1" max="{{ $allowed }}" value="{{ $requestedProduct->quantity }}" required
                                        />
                                    </td>
                                    <td class="text-right">
                                        <a href="#" class="remove-tr" data-target="#product-tr-{{ $requestedProduct->product->id }}">
                                            <i class="fa fa-close fa-fw"></i>
                                        </a>
                                    </td>
                                </tr>
                            @empty
                                <tr class="no-data">
                                    <td colspan="4" class="text-center active table-strong">Please select at least one (1) product</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>

            </div>
            <div class="panel-body">
                
                <div class="form-horizontal">

                    <div class="form-group">
                        <label for="address" class="control-label col-xs-12 col-sm-3">Attention</label>
                        <div class="col-xs-12 col-sm-9 col-lg-6">
                            <input type="text" class="form-control" name="attention" 
                                required value="{{ old('attention', $requestProduct->attention) }}"
                            />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="address" class="control-label col-xs-12 col-sm-3">Contact Number</label>
                        <div class="col-xs-12 col-sm-9 col-lg-6">
                            <input type="text" class="form-control" name="contact_number" 
                                required value="{{ old('contact_number', $requestProduct->contact_number) }}"
                            />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="address" class="control-label col-xs-12 col-sm-3">Company</label>
                        <div class="col-xs-12 col-sm-9 col-lg-6">
                            <input type="text" class="form-control" name="company" 
                                required value="{{ old('company', $requestProduct->company) }}"
                            />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="address" class="control-label col-xs-12 col-sm-3">Street 1</label>
                        <div class="col-xs-12 col-sm-9 col-lg-6">
                            <input type="text" class="form-control" name="street_1" 
                                required value="{{ old('street_1', $requestProduct->street_1) }}"
                            />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="address" class="control-label col-xs-12 col-sm-3">Street 2</label>
                        <div class="col-xs-12 col-sm-9 col-lg-6">
                            <input type="text" class="form-control" name="street_2" 
                                value="{{ old('street_2', $requestProduct->street_2) }}"
                            />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="address" class="control-label col-xs-12 col-sm-3">City</label>
                        <div class="col-xs-12 col-sm-9 col-lg-6">
                            <input type="text" class="form-control" name="city" 
                                required value="{{ old('city', $requestProduct->city) }}"
                            />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="address" class="control-label col-xs-12 col-sm-3">State</label>
                        <div class="col-xs-12 col-sm-9 col-lg-6">
                            <select name="state" id="state" class="form-control"
                                data-live-search="true" required placeholder="State"
                            >
                                <option value=""></option>
                                @foreach (getStates() as $state)
                                    <option value="{{ $state }}"
                                        {{ $state != $requestProduct->state ?: 'selected' }}
                                    >
                                        {{ $state }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="address" class="control-label col-xs-12 col-sm-3">Country</label>
                        <div class="col-xs-12 col-sm-9 col-lg-6">
                            <input type="text" class="form-control" name="country" 
                                required value="{{ old('country', $requestProduct->country) }}"
                            />
                        </div>
                    </div>

                    <div class="form-group fg-last">
                        <label for="address" class="control-label col-xs-12 col-sm-3">Postcode</label>
                        <div class="col-xs-12 col-sm-9 col-lg-6">
                            <input type="text" class="form-control" name="postcode" 
                                required value="{{ old('postcode', $requestProduct->postcode) }}"
                            />
                        </div>
                    </div>

                    <div class="form-group-divider"><span></span></div>

                    <div class="form-group">
                        <label for="eid" class="control-label col-xs-12 col-sm-3">Expected In-hand Date</label>
                        <div class="col-xs-12 col-sm-9 col-lg-6">
                            <input type="text" name="expected_inhand_date" id="eid" class="form-control auto" 
                            value="{{ old('expected_inhand_date', processDate($requestProduct->expected_inhand_date)) }}" required 
                            />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="justification" class="control-label col-xs-12 col-sm-3">Justification</label>
                        <div class="col-xs-12 col-sm-9 col-lg-6">
                            <textarea name="justification" id="justification" rows="2" 
                                class="form-control autosize" required>{{ old('justification', $requestProduct->justification) }}</textarea>
                        </div>
                    </div>

                    <div class="form-group fg-last">
                        <label for="notes" class="control-label col-xs-12 col-sm-3">Notes</label>
                        <div class="col-xs-12 col-sm-9 col-lg-6">
                            <textarea name="notes" id="notes" rows="2"
                                class="form-control autosize">{{ old('notes', $requestProduct->note) }}</textarea>
                        </div>
                    </div>

                    @if (count($customFields) > 0)
                        <div class="form-group-divider"><span></span></div>
                        @foreach ($customFields as $cf)
                            <div class="form-group @if ($loop->last) fg-last @endif">
                                <label for="cf-{{ $cf->slug }}" class="control-label col-xs-12 col-sm-3">{{ $cf->label }}</label>
                                <div class="col-xs-12 col-sm-9 col-lg-6">
                                    @if ($cf->type == 'input')
                                        <input type="text" class="form-control" name="custom_fields[{{ $cf->id }}]" id="cf-{{ $cf->slug }}" />
                                    @elseif ($cf->type == 'textarea')
                                        <textarea class="form-control autosize" name="custom_fields[{{ $cf->id }}]" id="cf-{{ $cf->slug }}" rows="2"></textarea>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    @endif

                    <div class="form-group-divider"><span></span></div>

                    <div class="form-group fg-last">
                        <label for="cc-email" class="control-label col-xs-12 col-sm-3">CC Notifications (optional)</label>
                        <div class="col-xs-12 col-sm-9 col-lg-6">
                            <input type="text" name="cc_emails" id="cc-email" class="form-control" 
                                length="50" value="{{ old('cc_emails', $requestProduct->cc_emails) }}" 
                            />
                        </div>
                    </div>

                </div>

            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary">Submit Changes</button>
                <a href="{{ route('requests.list') }}" class="btn btn-link">Cancel</a>
            </div>
        </div>

    </form>

</div>
</div>
</div>
@endsection

@section('footer-addon')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js"></script>
<script>
$(function() {

    $('#update-request-form').on('submit', function() {
        loader.show();
    });

    @if ($currentUser->role_id > 0)
        $('#requester-id')
            .selectpicker({
                style: 'btn-default',
                size: 6
            })
            .on('changed.bs.select', function(e) {
                loader.show();
                var url = e.target.attributes['data-url'].value;
                var id = e.target.value;
                if (id != '' && id > 0) {
                    $.get( url + '/' + id ).done(function(r) {
                        
                        var html = '<select name="address" id="address" class="form-control">';
                        $.each(r.addresses, function(key, value) {
                            var isDefault = value.default == 1 ? 'checked' : '';
                            html += '<option value="' + value.address + '" ' + isDefault + '>' + value.address + '</option>'
                        });
                        html += '</select>';
                        $('#the-addresses').html(html);
                        $('#market-id').val(r.market);

                        // refresh products
                        var products = '<option value=""></option>';
                        if (r.products.length > 0) {
                            $.each(r.products, function(key, value) {
                                products += '<option value="' + value.id + '" data-allocation="' + value.allocation + '" data-remaining="' + value.remaining + '">' + value.name + '</option>';
                            });
                        }
                        $('#products-selection').html(products);
                        $('#products-selection').selectpicker('refresh');

                        loader.hide();
                    });
                }
            });
    @endif

    $('#eid').datepicker({
        dateFormat: 'MM dd, yy'
    });    

    $('#products-selection')
        .selectpicker({
            style: 'btn-primary',
            size: 6
        })
        .on('changed.bs.select', function(e) {
            var id = e.target.value,
                allocation = $(this).find('option[value="' + id + '"]').data('allocation'),
                remaining = $(this).find('option[value="' + id + '"]').data('remaining');

            var allowed = parseInt(allocation);
            if (remaining < allocation || allowed == -1) {
                allowed = remaining;
            }
            var allocationDisplay = allocation < 0 ? '&#x221e;' : allocation;

            if (id != '' && id > 0) {
                var name = $(this).find('option[value="' + id + '"]').text(),
                    tbody = $('#products-table').find('tbody'),
                    html = '<tr id="product-tr-' + id + '">' +
                            '<td class="table-strong">' + name + '</td>' +
                            '<td>' + allocationDisplay + '</td>' +
                            '<td><input type="number" name="products[' + id + ']" class="input-in-table" placeholder="0" required min="1" max="' + allowed + '" /></td>' +
                            '<td class="text-right"><a href="#" class="remove-tr" data-target="#product-tr-' + id + '"><i class="fa fa-close fa-fw"></i></a></td>' +
                            '</tr>';
                if ($('#product-tr-' + id).length == 0) {
                    if (tbody.find('.no-data').length == 0) {
                        $(html).appendTo(tbody);    
                    } else {
                        tbody.html(html);
                    }                    
                    $('#product-tr-' + id).find('input').focus();
                }
            }

            $('.remove-tr').on('click', function(e) {
                e.preventDefault();
                $($(this).attr('data-target')).remove();
            });
        });

    $(document).on('click', '.remove-tr', function(e) {
        e.preventDefault();
        $($(this).data('target')).remove();
    });

    $(document).on('click', '.add-custom-address', function(e) {
        e.preventDefault();
        if ($('.custom-address').html() != '') {
            $('#address, #attention, #contact-number, #company').prop('disabled', false);
            $('.custom-address').html('');
        } else {
            $('#address, #attention, #contact-number, #company').prop('disabled', true);
            $('.custom-address').html('<div style="padding-top:20px;"><i class="fa fa-circle-o-notch fa-spin fa-fw text-primary"></i></div>');
            $('.custom-address').load($(this).data('url'));
        }
    });

    $('#state').selectpicker({
        style: 'btn-default',
        noneSelectedText: 'State',
        size: 6
    });

});
</script>

@endsection