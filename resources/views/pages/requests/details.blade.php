<div id="request-error" class="alert alert-danger hidden">
    <ul></ul>
</div>
<div class="purchase-order-details modal-tab-wrap">

    @if ($requestProduct->status == 4)
        <div class="alert alert-danger">
            This request has been splitted. Below are the new requests:
            <ul>
                @foreach ($requestProduct->childRq as $rQ)
                <li>
                    <strong>{{ $rQ->uid() }}</strong>
                </li>
                @endforeach
            </ul>
        </div>
    @endif

    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#details" aria-controls="details" role="tab" data-toggle="tab">Details</a></li>
        @if ($requestProduct->status == 2)
        <li role="presentation">
            <a href="#shipping-details" aria-controls="shipping-details" role="tab" data-toggle="tab">Shipping Details</a>
        </li>
        @endif
    </ul>

    <div class="tab-content">

        <div role="tabpanel" class="tab-pane active" id="details">

            <div class="clearfix">

                @if ($requestProduct->manual_input)
                    <div class="request-meta-label" style="display: block; padding-bottom: 20px;">
                        @if ($requestProduct->status == 0)
                            <span class="label label-warning">
                        @elseif ($requestProduct->status == 1)
                            <span class="label label-success">
                        @elseif ($requestProduct->status == 2)
                            <span class="label label-primary">
                        @else
                            <span class="label label-danger">
                        @endif
                            {{ requestStatuses()[$requestProduct->status] }}
                        </span>
                    </div>
                @endif

                <div class="row">
                    <div class="col-xs-12 col-sm-8 pull-left">
                        <p style="margin-bottom: -10px;">{{ processDate($requestProduct->created_at) }}</p>
                        <h1><a href="{{ route('users.profile') }}/{{ $requestProduct->theRequester->id }}" target="_blank">{{ $requestProduct->theRequester->name() }}</a></h1>
                        <h4>{{ $requestProduct->market->name }}</h4>

                        <br />

                        <h5><strong>Justification</strong></h5>
                        <p>{{ $requestProduct->justification }}</p>

                        <br />

                        <h5><strong>Notes</strong></h5>
                        <p>{{ $requestProduct->note }}</p>
                    </div>
                    <div class="col-xs-12 col-sm-4 pull-right">
                        <div class="alert alert-warning" style="margin-bottom: 0;">
                            <p>
                                <strong>Date Expected</strong><br />
                                {{ processDate($requestProduct->expected_inhand_date) }}
                            </p>
                            <br />
                            <p>
                                <strong>Ship-to Address</strong><br />
                                {{ $requestProduct->attention }}<br />
                                {{ $requestProduct->contact_number }}<br />
                                {{ $requestProduct->company }}<br />
                                {{ $requestProduct->getAddressStreet() }}<br />
                                {{ $requestProduct->getAddressCityStateZip() }}
                            </p>
                        </div>
                    </div>
                </div>

            </div>

            @if (count($requestProduct->customFields()) > 0)
                <div class="modal-separator"><span></span></div>

                <div class="request-data rd-last">
                    @foreach ($requestProduct->customFields() as $customField)
                        <strong>{{ $customField->cf->label }}</strong>: {{ $customField->value }}<br />
                    @endforeach
                </div>
            @endif

            <div class="panel-separator"><span></span></div>

            @if ($requestProduct->status == 0 AND $currentUser->allowedTo('process_requests'))
            <form method="post" autocomplete="off"
                action="{{ route('requests.process') }}/{{ $requestProduct->id }}"
                id="process-request-form"
            >
            {!! csrf_field() !!}
            @endif
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Product</th>
                                <th class="text-right">Quantity</th>
                                {{-- <th>Allowed</th> --}}
                                <th>Location</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($requestedProducts as $rP)
                                <tr>
                                    <td class="table-strong">{{ $rP->product->name }}</td>
                                    <td class="text-right">
                                        {{ $rP->quantity }}
                                        <input type="hidden" class="input-in-table" name="products[{{ $rP->id}}][allowed]" value="{{ $rP->quantity }}" />
                                    </td>
                                    {{-- <td>
                                        @if ($requestProduct->status == 0)
                                            @if ($currentUser->allowedTo('process_requests'))
                                                <input type="number" class="input-in-table" name="products[{{ $rP->id}}][allowed]"
                                                    value="{{ $rP->quantity }}"
                                                />
                                            @else
                                                <i class="fa fa-circle-o-notch fa-spin fa-fw text-primary"></i>
                                            @endif
                                        @elseif ($requestProduct->status == 3)
                                            0
                                        @elseif ($requestProduct->status == 1)
                                            {{ $rP->allowed_quantity }}
                                        @else
                                            -
                                        @endif
                                    </td> --}}
                                    <td>
                                        @if ($requestProduct->status == 0)
                                            @if ($currentUser->allowedTo('process_requests'))
                                                <select name="products[{{ $rP->id }}][location]" id="" class="input-in-table"
                                                    style="width: 100%;"
                                                >
                                                    @foreach ($locations as $location)
                                                        <option value="{{ $location->id }}">
                                                            {{ $location->name }} ({{ $location->stocksRemaining($rP->product->id)}})
                                                        </option>
                                                    @endforeach
                                                </select>
                                            @else
                                                <i class="fa fa-circle-o-notch fa-spin fa-fw text-primary"></i>
                                            @endif
                                        @elseif ($requestProduct->status == 3)
                                            NA
                                        @elseif ($requestProduct->status == 1 || $requestProduct->status == 2)
                                            {{ $rP->location->name }}
                                        @else
                                            -
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @if ($requestProduct->status == 0 AND $currentUser->allowedTo('process_requests'))
                <div style="display: block; margin-top: 24px;">
                    <button type="submit" class="btn btn-primary btn-sm" id="approve-request">Approve (and Split)</button>
                    <a href="#" class="btn btn-danger btn-sm" id="disapprove-request"
                        data-url="{{ route('requests.disapprove') }}/{{ $requestProduct->id }}"
                    >
                        Disapprove
                    </a>
                </div>
            </form>
            @endif

            @if ($requestProduct->status == 1 AND $currentUser->allowedTo('ship_requests'))
                <div style="display: block; margin-top: 24px;">
                    <a href="{{ route('requesters.ship') }}/{{ $requestProduct->id }}"
                        class="btn btn-success btn-sm"
                    >
                        Ship Using Shippo
                    </a>
                    <a href="#" class="btn btn-default btn-sm manual-shipping"
                        data-url="{{ route('shipping.manual') }}/{{ $requestProduct->id }}"
                    >
                        Input Shipping Details
                    </a>
                </div>
            @endif

            <div id="manual-shipping"></div>


        </div>

        @if ($requestProduct->status == 2)
        <div role="tabpanel" class="tab-pane" id="shipping-details">

            <div class="request-meta clearfix">

                @if ($requestProduct->manual_input)
                    <div class="request-meta-label" style="display: block; padding-bottom: 20px;">
                        <span class="label label-danger">Manual Input</span>
                    </div>
                @endif

                <h1>Tracking Number: {{ $requestProduct->tracking_number }}</h1>
                <h5><em>Rate:</em> ${{ $requestProduct->rate }}</h5>
                <h5><em>Courier:</em> {{ $requestProduct->courier }}</h5>
                @if (!empty($requestProduct->service_level_name))
                    <h5><em>Service Level Name:</em> {{ $requestProduct->service_level_name }}</h5>
                @endif
                @if (!empty($requestProduct->duration_terms))
                    <h5><em>Duration Term:</em> {{ $requestProduct->duration_terms }}</h5>
                @endif

            </div>

            @if (!$requestProduct->manual_input AND isset($trackingDetails->tracking_status))
            <div class="modal-separator"><span></span></div>

                <div class="request-data">
                    <h5>Shipment Tracking</h5>
                    <p>
                        <strong>{{ $trackingDetails->tracking_status->status }}</strong><br />
                        Details: {{ $trackingDetails->tracking_status->status_details }}<br />
                        Date: {{ Carbon\Carbon::parse($trackingDetails->tracking_status->status_date)->toFormattedDateString() }}
                        @if ($trackingDetails->tracking_status->status !== 'UNKNOWN')
                        <br />
                        Location: {{ $trackingDetails->tracking_status->location->city }},
                            {{ $trackingDetails->tracking_status->location->state }},
                            {{ $trackingDetails->tracking_status->location->country }}
                        @endif
                    </p>
                </div>

            <div class="modal-separator"><span></span></div>
            @endif

            @if (!$requestProduct->manual_input)
                <div style="display: block; margin-top: 24px;">
                    <a href="{{ $requestProduct->label_url }}" class="btn btn-primary btn-sm" target="_blank">Download Label</a>
                </div>
            @endif

        </div>
        @endif

    </div>

</div>
<script>
$(function() {

    $(document).on('click', '#approve-request', function(e) {
        e.preventDefault();
        loader.show();
        var form = $('#process-request-form'),
            url = form.attr('action'),
            data = form.serialize();

        $.post( url, data ).done(function(r) {
            if (r.errors) {
                var dom = $('#request-error'),
                    html = '';
                $.each(r.errors, function(key, value) {
                    html += '<li>' + value + '</li>';
                    dom.find('ul').html(html);
                });
                dom.removeClass('hidden');
                loader.hide();
            }
            if (r.redirect) {
                window.location = r.redirect;
            }
        });
    });

    $(document).on('click', '#disapprove-request', function(e) {
        e.preventDefault();
        loader.show();
        var url = $(this).attr('data-url');
        $.post( url ).done(function(r) {
            if (r.redirect) {
                window.location = r.redirect;
            }
            loader.hide();
        })
    });

    $(document).on('click', '.manual-shipping', function(e) {
        e.preventDefault();
        $('#manual-shipping')
            .css({ 'paddingTop': '24px' })
            .html('<i class="fa fa-circle-o-notch fa-spin fa-fw text-primary"></i>');
        $.get( $(this).data('url') ).done(function(r) {
            $('#manual-shipping')
                .html(r.view);
        });
    });

});
</script>
