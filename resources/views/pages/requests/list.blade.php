@extends('layouts.app')

@section('page-title', 'Requests')

@section('head-addon')
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.0/css/select.dataTables.min.css" />
<link rel="stylesheet" href="{{ asset('assets/css/dt-override.css') }}" />
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.status')

    <div class="panel">

        <div class="panel-heading">
            <h4>Requests</h4>
        </div>

        <div class="panel-filter">
    		@if ($currentUser->allowedTo('add_requests') OR $currentUser->is_requester)
            <div class="input-group">
                <input type="text" placeholder="Search requests" class="form-control" id="quick-search-input" autocomplete="off">
    								<span class="input-group-btn">
                          <a href="{{ route('requests.create') }}" class="btn btn-primary btn-squared">
                            <i class="fa fa-plus"></i>
                          </a>
                    </span>
            </div>
    		@else
    						<input type="text" placeholder="Search requests" class="form-control" id="quick-search-input" autocomplete="off">
        @endif
        </div>

        <div class="panel-preloader" data-target="#requests-panel">
            <i class="fa fa-circle-o-notch fa-spin fa-fw"></i> &nbsp; Preparing requests table
        </div>
        <div id="requests-panel" class="panel-body" style="display:none;">

            <ul class="nav nav-tabs" role="tablist">
                @if ($currentUser->role_id != 4)
                    <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab">All</a></li>
                @endif
                @if ($currentUser->role_id != 4)
                    <li role="presentation"><a href="#pending-approval" aria-controls="pending-approval" role="tab" data-toggle="tab">Pending Approval</a></li>
                @endif

                <li role="presentation"><a href="#pending-shipment" aria-controls="pending-shipment" role="tab" data-toggle="tab">Pending Shipment</a></li>
                <li role="presentation"><a href="#shipped" aria-controls="shipped" role="tab" data-toggle="tab">Shipped</a></li>
                <li role="presentation"><a href="#split" aria-controls="split" role="tab" data-toggle="tab">Split</a></li>

                @if ($currentUser->role_id != 4)
                    <li role="presentation"><a href="#rejected" aria-controls="rejected" role="tab" data-toggle="tab">Rejected</a></li>
                @endif
            </ul>

            <div class="tab-content">

                <!-- all -->
                <div role="tabpanel" class="tab-pane active" id="all">

                    <div class="table-responsive">
                        <table id="rq-all" class="table datatable table-hover">
                            <thead>
                                <tr>
                                    <th>UID</th>
                                    <th>Requester</th>
                                    <th>Market</th>
                                    <th>Expected In-hand Date</th>
                                    <th>Products</th>
                                    <th>Date Created</th>
                                    <th class="text-right">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($rqAll as $request)
                                    <?php
                                        if (
                                            $currentUser->role_id == 4 AND
                                            !$currentUser->managesLocation($request->locationToGet())
                                        ) {
                                            continue;
                                        }
                                    ?>

                                    @include('pages.requests.row', [
                                        'uid' => 'po-all-entry',
                                        'request' => $request
                                    ])
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
                <div role="tabpanel" class="tab-pane" id="pending-approval">

                    <div class="table-responsive">
                        <table id="rq-pending-approval" class="table datatable table-hover">
                            <thead>
                                <tr>
                                    <th>UID</th>
                                    <th>Requester</th>
                                    <th>Market</th>
                                    <th>Expected In-hand Date</th>
                                    <th>Products</th>
                                    <th>Date Created</th>
                                    <th class="text-right">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($rqPendingApproval as $request)
                                    <?php
                                        if (
                                            $currentUser->role_id == 4 AND
                                            !$currentUser->managesLocation($request->locationToGet())
                                        ) {
                                            continue;
                                        }
                                    ?>
                                    @include('pages.requests.row', [
                                        'uid' => 'po-pending-approval-entry',
                                        'request' => $request
                                    ])
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
                <div role="tabpanel" class="tab-pane" id="pending-shipment">

                    <div class="table-responsive">
                        <table id="rq-pending-shipment" class="table datatable table-hover">
                            <thead>
                                <tr>
                                    <th>UID</th>
                                    <th>Requester</th>
                                    <th>Market</th>
                                    <th>Expected In-hand Date</th>
                                    <th>Products</th>
                                    <th>Date Created</th>
                                    <th class="text-right">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($rqPendingShipment as $request)
                                    <?php
                                        if (
                                            $currentUser->role_id == 4 AND
                                            !$currentUser->managesLocation($request->locationToGet())
                                        ) {
                                            continue;
                                        }
                                    ?>
                                    @include('pages.requests.row', [
                                        'uid' => 'po-pending-shipment-entry',
                                        'request' => $request
                                    ])
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
                <div role="tabpanel" class="tab-pane" id="shipped">

                    <div class="table-responsive">
                        <table id="rq-shipped" class="table datatable table-hover">
                            <thead>
                                <tr>
                                    <th>UID</th>
                                    <th>Requester</th>
                                    <th>Market</th>
                                    <th>Expected In-hand Date</th>
                                    <th>Products</th>
                                    <th>Date Created</th>
                                    <th class="text-right">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($rqShipped as $request)
                                    <?php
                                        if (
                                            $currentUser->role_id == 4 AND
                                            !$currentUser->managesLocation($request->locationToGet())
                                        ) {
                                            continue;
                                        }
                                    ?>
                                    @include('pages.requests.row', [
                                        'uid' => 'po-shipped-entry',
                                        'request' => $request
                                    ])
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
                <div role="tabpanel" class="tab-pane" id="split">

                    <div class="table-responsive">
                        <table id="rq-split" class="table datatable table-hover">
                            <thead>
                                <tr>
                                    <th>UID</th>
                                    <th>Requester</th>
                                    <th>Market</th>
                                    <th>Expected In-hand Date</th>
                                    <th>Products</th>
                                    <th>Date Created</th>
                                    <th class="text-right">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($rqSplit as $request)
                                    <?php
                                        if (
                                            $currentUser->role_id == 4 AND
                                            !$currentUser->managesLocation($request->locationToGet())
                                        ) {
                                            continue;
                                        }
                                    ?>
                                    @include('pages.requests.row', [
                                        'uid' => 'po-split-entry',
                                        'request' => $request
                                    ])
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
                <div role="tabpanel" class="tab-pane" id="rejected">

                    <div class="table-responsive">
                        <table id="rq-rejected" class="table datatable table-hover">
                            <thead>
                                <tr>
                                    <th>UID</th>
                                    <th>Requester</th>
                                    <th>Market</th>
                                    <th>Expected In-hand Date</th>
                                    <th>Products</th>
                                    <th>Date Created</th>
                                    <th class="text-right">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($rqRejected as $request)
                                    <?php
                                        if (
                                            $currentUser->role_id == 4 AND
                                            !$currentUser->managesLocation($request->locationToGet())
                                        ) {
                                            continue;
                                        }
                                    ?>
                                    @include('pages.requests.row', [
                                        'uid' => 'po-rejected-entry',
                                        'request' => $request
                                    ])
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>

        </div>

        @if ($currentUser->allowedTo('add_requests') OR $currentUser->is_requester)
            {{-- <div class="panel-footer">
                <a href="{{ route('requests.create') }}" class="btn btn-primary">Create Request</a>
            </div> --}}
        @endif

    </div>

</div>
</div>
</div>
@endsection

@section('footer-addon')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script>
$(function() {

    function tabInfo(table) {
        var id = $(table).closest('.tab-pane').attr('id'),
            tab = $('.nav-tabs a[aria-controls='+id+']'),
            length = $(table).DataTable().page.info().recordsDisplay,
            label = tab.find('span');
        if (label.length) { label.remove(); }
        tab.append(' <span>('+length+')</span>');
    }

    var tables = $('.datatable')
        .on('draw.dt', function () {
            tabInfo(this);
        })
        .on('search.dt', function () {
            tabInfo(this);
        })
        .DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5',
                'print'
            ],
            pageLength: 10,
            ordering: true,
            lengthChange: false,
            pagingType: 'numbers',
            'order': [ 0, 'desc' ],
            'columnDefs': [
                {
                    'targets': [6],
                    'orderable': false
                },
                {
                    'targets': [7, 8],
                    'visible': false
                }
            ]
        });

    $('#quick-search-input').on('keyup', function () {
        tables.search( this.value ).draw();
    });

    $(document).on('click', '.view-details a', function(e) {
        e.stopPropagation();
    });

    $(document).on('click', '.view-details', function(e) {
        e.preventDefault();
        loader.show();
        var url = $(this).data('url'),
            rqStatus = $(this).data('status');
            modalRqStatus = 'modal-rq-danger';

        if (rqStatus == 0) {
            modalRqStatus = 'modal-rq-warning';
        } else if (rqStatus == 1) {
            modalRqStatus = 'modal-rq-success';
        } else if (rqStatus == 2) {
            modalRqStatus = 'modal-rq-info';
        }
        $.get( url )
            .done(function(r) {
                loader.hide();
                var ap_dap = r.visibility;
                bootbox.dialog({
                    title: r.title,
                    message: r.view,
                    className: 'modal-rq ' + modalRqStatus,
                    size: 'large',
                    onEscape: function () {
                        bootbox.hideAll();
                    },
                    buttons: {
                        'Close' : {
                            className: 'btn-default'
                        }
                    }
                });
            })
            .fail(function(r) {
                loader.hide();
            });
    });

    $(document).on('click', '.delete-request', function(e) {
        e.preventDefault();
        var url = $(this).data('url');
        bootbox.dialog({
            title: 'Hey!',
            message: '<div style="font-size: 16px;">Are you sure you want to delete this request?</div>',
            className: 'modal-danger',
            onEscape: function () {
                bootbox.hideAll();
            },
            buttons: {
                'confirm': {
                    label: 'Yes',
                    className: 'btn-danger',
                    callback: function() {
                        window.location = url;
                    }
                },
                'Close' : {
                    className: 'btn-default'
                }
            }
        });
    });

});
</script>
@endsection
