@extends('layouts.app')

@section('page-title', 'Select Requester')

@section('head-addon')
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    <div style="margin: 0 auto; max-width: 600px;">
        <div class="panel">
            <div class="panel-heading">
                <h4>Select Requester</h4>
            </div>
            <div class="list-group select-rqr-list">
                @foreach ($requesters as $requester)
                    <a class="list-group-item" href="{{ route('requests.create', ['requesterId' => $requester->id]) }}">
                        <strong class="text-primary">{{ $requester->user->name() }}</strong>
                    </a>
                @endforeach
            </div>
        </div>
    </div>

</div>
</div>
</div>
@endsection

@section('footer-addon')
@endsection
