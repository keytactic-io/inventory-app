<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{ $filename }}</title>
    <style>
    body {
        font-family: 'DejaVu Sans';
        line-height: 1.3;
        font-size: 8.5pt;
        font-weight: book;
    }
    .text-right { text-align: right; }
    .text-left { text-align: left; }
    .text-center { text-align: center; }
    .po-received {
        padding: 4pt 10pt 2pt;
        background: #def4da;
        color: #43653c;
        margin-bottom: 24px;
    }
    .po-header {
        margin-bottom: 20pt;
    }
        .po-header h1 {
            margin: 0;
            padding: 0;
            line-height: 1.2;
            font-size: 18pt;
            font-weight: bold;
        }
        .po-header h5 {
            margin: 0;
            padding: 0;
            line-height: 1.2;
            font-size: 9pt;
            font-weight: normal;
        }
    table {
        width: 100%;
        border-collapse: collapse;
        border-top: .5pt solid #eeeeee;
    }
    table thead tr th {
        text-align: left;
        font-size: 7pt;
        text-transform: uppercase;
        padding: 5pt 10pt;
        border-bottom: .5pt solid #eeeeee;
        background-color: #fbfbfb;
        white-space: nowrap;
    }
    table tbody tr td {
        padding: 7pt 10pt;
        vertical-align: top;
        border-bottom: .5pt solid #eeeeee;
    }
    .nothing-follows {
        padding-top: 20pt;
    }
    table.no-padding  {
        border-top: 0;
    }
    table.no-padding tbody tr td {
        padding: 0;
        background: none;
        border-bottom: 0;
    }
    table tbody tr.highlighted td {
        background: #fbfbfb;
    }

    #addresses {
        margin-top: 20pt;
    }
        #addresses h4 {
            margin: 0;
            padding: 0;
            line-height: 1.4;
            font-size: 7pt;
            text-transform: uppercase;
        }

        .address-container {
            width: 90%;
        }
            .address-container strong {
                font-size: 11pt;
            }

    .text-right {
        text-align: right;
    }

    #note {
        padding-top: 20pt;
        width: 75%;
    }
        #note h4 {
            margin: 0 0 5pt 0;
            padding: 0;
            line-height: 1.4;
        }

    #calculation {
        padding-left: 345pt;
        overflow: hidden;
        margin-top: 20pt;
    }
        #calculation table {
            width: 200pt;
        }
    </style>
</head>
<body>

    <div class="po-header">
        <table class="no-padding">
            <tr>
                <td>
                    <?php $title = app_setting('application_name') == '' ? 'Inventory App' : app_setting('application_name'); ?>
                    @if (app_setting('logo') != '')
                        <?php
                            $imageMeta = getimagesize(asset('uploads/' . app_setting('logo')));
                            $ratio = $imageMeta[1] / $imageMeta[0];
                            $width = 40 / $ratio;
                        ?>
                        <img src="{{ asset('uploads/' . app_setting('logo')) }}" alt="{{ $title }}" height="40pt" width="{{ $width }}pt" style="margin-bottom: 7pt;" />
                    @else
                        <h1>{{ $title }}</h1>
                    @endif                    
                    <h5>{{ app_setting('address') }}</h5>
                    <h5>{{ app_setting('contact_number') }} &nbsp; | &nbsp; {{ app_setting('email') }}</h5>
                </td>
                <td>
                    <table style="margin-top: 12px; border-top: 0;">
                        <tr>
                            <td>RQ #:</td>
                            <td>{{ $requestProduct->uid() }}</td>
                        </tr>
                        <tr>
                            <td>RQ Date:</td>
                            <td>{{ processDate($requestProduct->created_at) }}</td>
                        </tr>
                        @if ($requestProduct->processed_by != null)
                            <tr>
                                <td>Processed by:</td>
                                <td>{{ $requestProduct->processedBy->name() }}</td>
                            </tr>
                            <tr>
                                <td>Date Processed:</td>
                                <td>{{ processDate($requestProduct->processed_at) }}</td>
                            </tr>
                        @endif
                    </table>
                </td>
            </tr>
        </table>

        <table id="addresses" class="no-padding">
            <tr>
                <td style="width: 50%;">
                    <h4>Requester</h4>
                    <div class="address-container">
                        <strong>{{ $requestProduct->theRequester->name() }}</strong><br />
                        Market: {{ $requestProduct->theRequester->requester->market->name }}<br />
                        Attention: {{ $requestProduct->attention }}<br />
                        Contact Number: {{ $requestProduct->contact_number }}<br />
                        Company: {{ $requestProduct->company }}<br />
                        Ship-to Address: {{ $requestProduct->getAddress() }}<br />
                        Expected In-hand Date: {{ processDate($requestProduct->expected_inhand_date) }}<br />
                    </div>
                </td>
            </tr>
        </table>

    </div>

    <table style="margin-top: 20pt">
        <thead>
            <tr>
                <th>Product Name</th>
                <th>Description</th>
                <th class="text-right">Quantity</th>
                @if ($requestProduct->processed_by != null)
                    <th class="text-right">Allowed</th>
                @endif
            </tr>
        </thead>
        <tbody>
            @foreach ($requestProduct->requestedProducts as $requestedProduct)
                <tr>
                    <td><strong>{{ $requestedProduct->product->name }}</strong></td>
                    <td>{{ $requestedProduct->product->description }}</td>
                    <td class="text-right">{{ number_format($requestedProduct->quantity, '0', '.', ',') }}</td>
                    @if ($requestProduct->processed_by != null)
                        <td class="text-right">{{ number_format($requestedProduct->allowed_quantity, '0', '.', ',') }}</td>
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>

    @if ($requestProduct->justification != null AND $requestProduct->justification != '')
        <div id="note">
            <h4>Justification</h4>
            <p>{{ $requestProduct->justification }}</p>
        </div>
    @endif

    @if ($requestProduct->note != null AND $requestProduct->note != '')
        <div id="note">
            <h4>Notes/Memo</h4>
            <p>{{ $requestProduct->note }}</p>
        </div>
    @endif


</body>
</html>