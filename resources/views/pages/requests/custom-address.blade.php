<div class="form-control-group">
    <input type="text" class="form-control" name="attention" placeholder="Attention (If blank, Requester's name will be saved)" />
    <input type="text" class="form-control" name="contact_number" placeholder="Contact Number" required />
    <input type="text" class="form-control" name="company" placeholder="Company" required />
    <input type="text" class="form-control" name="street_1" placeholder="Street 1" required />
    <input type="text" class="form-control" name="street_2" placeholder="Street 2" />
    <input type="text" class="form-control" name="city" placeholder="City" required />
    <select name="state" id="state" class="form-control"
        data-live-search="true" required placeholder="State"
    >
        <option value=""></option>
        @foreach (getStates() as $state)
            <option value="{{ $state }}">
                {{ $state }}
            </option>
        @endforeach
    </select>
    <input type="text" class="form-control" name="postcode" placeholder="Postcode" required />
</div>

<input type="hidden" name="country" value="US" />

<div class="form-group fg-last">
    <input type="hidden" name="link_to_requester" value="0" />
    <div class="checkbox">
        <label>
            <input type="checkbox" name="link_to_requester" id="" value="1" checked />
            <span>Link Address to Requester</span>
        </label>
    </div>
</div>

<script>
$(function() {

    $('#state').selectpicker({
        style: 'btn-default',
        noneSelectedText: 'State',
        size: 6
    });

});
</script>