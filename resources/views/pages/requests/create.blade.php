@extends('layouts.app')

@section('page-title', 'Create Request')

@section('head-addon')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css">
<link rel="stylesheet" href="{{ asset('assets/css/bt-select-override.css') }}" />
<link href="{{ asset('assets/css/vendors/tagsinput.css') }}" rel="stylesheet" />
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.errors')

    <form action="{{ route('requests.create', ['requesterId' => $requester->id]) }}" 
        method="post" autocomplete="off" id="create-request-form"
    >

        <div class="panel">
            <div class="panel-heading">
                @if ($currentUser->id == $requester->user->id)
                    <h4>Create Request</h4>
                @else
                    <h4>Create Request for <u><a href="{{ route('requests.requester.select') }}">{{ $requester->user->name() }}</a></u></h4>
                @endif
            </div>
            <div class="panel-body">

                <div class="form-horizontal">
                    <div class="form-group">
                        <label for="market-id" class="control-label col-xs-12 col-sm-3">Department</label>
                        <div class="col-xs-12 col-sm-9 col-lg-6">
                            <select name="market_id" id="market-id" class="form-control" required>
                                <option value=""></option>
                                @foreach ($markets as $market)
                                    <option value="{{ $market->id }}"
                                        @if (old('market_id', $requester->market_id) == $market->id)  selected @endif
                                    >
                                        {{ $market->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

            </div>
            <div class="panel-body panel-body-invert">

                <div class="form-group">
                    <label for="products-selection">Select Asset</label>
                    <select id="products-selection" class="form-control"
                        data-live-search="true"
                    >
                        <option value=""></option>
                        @foreach ($products as $product)
                            @if ($product->remaining_stocks > 0)
                                <option value="{{ $product->id }}"
                                    data-allocation="{{ $product->allocation }}" 
                                    data-remaining="{{ $product->remaining_stocks }}"
                                >
                                    {{ $product->name }} ({{ $product->remaining_stocks }})
                                </option>
                            @endif
                        @endforeach
                    </select>
                </div>

                <div class="table-responsive">
                    <table id="products-table" class="table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Allocation</th>
                                <th style="width:124px;">Quantity</th>
                                <th style="width:60px;" class="text-right">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="no-data">
                                <td colspan="4" class="text-center active table-strong">Please select at least one (1) product</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                
            </div>
            <div class="panel-body">
        
                <div class="form-horizontal">

                    <div class="form-group">
                        <label for="address" class="control-label col-xs-12 col-sm-3">Address</label>
                        <div class="col-xs-12 col-sm-9 col-lg-6">
                            <select name="address" id="address" class="form-control">
                                @foreach ($requester->addresses as $address)
                                    <option value="{{ $address->id }}"
                                        <?php if ($address->default): ?>checked<?php endif; ?>1
                                    >
                                        {{ $address->getAddress() }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12 col-sm-9 col-sm-offset-3 col-lg-6">
                            <a href="#" class="btn btn-sm btn-primary add-custom-address"
                                data-url="{{ route('requesters.aca') }}"
                            >
                                Add New Address
                            </a>
                            <div class="custom-address"></div>
                        </div>
                    </div>

                    <div class="form-group-divider"><span></span></div>

                    <div class="form-group">
                        <label for="eid" class="control-label col-xs-12 col-sm-3">Expected In-hand Date</label>
                        <div class="col-xs-12 col-sm-9 col-lg-6">
                            <input type="text" name="expected_inhand_date" id="eid" class="form-control auto" value="{{ old('expected_inhand_date')}}" required />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="justification" class="control-label col-xs-12 col-sm-3">Justification</label>
                        <div class="col-xs-12 col-sm-9 col-lg-6">
                            <textarea name="justification" id="justification" rows="2" class="form-control autosize" required>{{ old('justification')}}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="notes" class="control-label col-xs-12 col-sm-3">Notes</label>
                        <div class="col-xs-12 col-sm-9 col-lg-6">
                            <textarea name="notes" id="notes" rows="2" class="form-control autosize">{{ old('notes')}}</textarea>
                        </div>
                    </div>

                    @if (count($customFields) > 0)
                        <div class="form-group-divider"><span></span></div>
                        @foreach ($customFields as $cf)
                            <div class="form-group @if ($loop->last) fg-last @endif">
                                <label for="cf-{{ $cf->slug }}" class="control-label col-xs-12 col-sm-3">{{ $cf->label }}</label>
                                <div class="col-xs-12 col-sm-9 col-lg-6">
                                    @if ($cf->type == 'input')
                                        <input type="text" class="form-control" name="custom_fields[{{ $cf->id }}]" id="cf-{{ $cf->slug }}" />
                                    @elseif ($cf->type == 'textarea')
                                        <textarea class="form-control autosize" name="custom_fields[{{ $cf->id }}]" id="cf-{{ $cf->slug }}" rows="2"></textarea>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    @endif

                    <div class="form-group-divider"><span></span></div>

                    <div class="form-group fg-last">
                        <label for="cc-email" class="control-label col-xs-12 col-sm-3">CC Notifications (optional)</label>
                        <div class="col-xs-12 col-sm-9 col-lg-6">
                            <input type="text" name="cc_emails" id="cc-email" class="form-control" length="50" 
                                value="{{ old('cc_emails') }}" 
                            />
                        </div>
                    </div>

                </div>

            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ route('requests.list') }}" class="btn btn-link">Cancel</a>
            </div>
        </div>

    {!! csrf_field() !!}
    </form>

</div>
</div>
</div>
@endsection

@section('footer-addon')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js"></script>
<script src="{{ asset('assets/js/vendors/tagsinput.js') }}"></script>
<script>
$(function() {

        $('#cc-email').tagsInput({
            height: 'auto',
            width: 'auto',
            defaultText: 'add email',
        });

        $('#create-request-form').on('submit', function() {
            loader.show();
        });

        $('#eid').datepicker({
            dateFormat: 'MM dd, yy'
        });

        $('#products-selection')
        .selectpicker({
                style: 'btn-primary',
                size: 6,
                noneSelectedText: 'Please select a product'
        })
        .on('changed.bs.select', function(e) {
            var id         = e.target.value,
                selected   = $(this).find('option[value="' + id + '"]'),
                allocation = selected.data('allocation'),
                remaining  = selected.data('remaining');

            var allowed = parseInt(allocation);
            if (remaining < allocation || allowed == -1) {
                allowed = remaining;
            }
            var allocationDisplay = allocation < 0 ? '&#x221e;' : allocation;

            if (id != '' && id > 0) {
                var name = $(this).find('option[value="' + id + '"]').text(),
                    tbody = $('#products-table').find('tbody'),
                    html = '<tr id="product-tr-' + id + '">' +
                                    '<td class="table-strong">' + name + '</td>' +
                                    '<td>' + allocationDisplay + '</td>' +
                                    '<td><input type="number" name="products[' + id + ']" class="input-in-table" placeholder="0" required min="1" max="' + allowed + '" /></td>' +
                                    '<td class="text-right"><a href="#" class="remove-tr" data-target="#product-tr-' + id + '"><i class="fa fa-close fa-fw"></i></a></td>' +
                                    '</tr>';
                if ($('#product-tr-' + id).length == 0) {
                    if (tbody.find('.no-data').length == 0) {
                        $(html).appendTo(tbody);
                    } else {
                        tbody.html(html);
                    }
                    $('#product-tr-' + id).find('input').focus();
                }
            }

            $(this).val('').selectpicker('render');

        });

        $(document).on('click', '.add-custom-address', function(e) {
            e.preventDefault();
            if ($('.custom-address').html() != '') {
                $('#address').prop('disabled', false);
                $('.custom-address').html('');
            } else {
                $('#address').prop('disabled', true);
                $('.custom-address').html('<div style="padding-top:20px;"><i class="fa fa-circle-o-notch fa-spin fa-fw text-primary"></i></div>');
                $('.custom-address').load($(this).data('url'));
            }
        });

        $(document).on('click', '.remove-tr', function(e) {
            e.preventDefault();
            $($(this).attr('data-target')).remove();
        });

        $('#send_noti').on('change', function() {
            var ccfield = $('#cc-email');
            if ($(this).prop('checked')) {
                ccfield.attr('disabled', false);
            } else {
                ccfield.attr('disabled', true);
            }
        });

});
</script>
@endsection
