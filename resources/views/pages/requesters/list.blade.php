@extends('layouts.app')

@section('page-title', 'Requesters')

@section('head-addon')
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.0/css/select.dataTables.min.css" />
<link rel="stylesheet" href="{{ asset('assets/css/dt-override.css') }}" />
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">
    @include('includes.status')
</div>
<div class="col-xs-12 col-sm-8 col-md-9">

    <div class="panel">

        <div class="panel-heading">
            <h4>Requesters</h4>
        </div>

        <div class="panel-filter">
            @if ($currentUser->allowedTo('add_requesters'))
                <div class="input-group">
                    <input type="text" placeholder="Search requesters" class="form-control" id="quick-search-input" autocomplete="off">
                    <span class="input-group-btn">
                        <a href="{{ route('requesters.add') }}" class="btn btn-primary btn-squared">
                            <i class="fa fa-plus"></i>
                        </a>
                    </span>
                </div>
            @else
              <input type="text" placeholder="Search requesters" class="form-control" id="quick-search-input" autocomplete="off">
            @endif
        </div>

        <div class="panel-preloader" data-target="#requesters-panel-body">
            <i class="fa fa-circle-o-notch fa-spin fa-fw"></i> Preparing requesters table
        </div>
        <div id="requesters-panel-body" class="panel-body hidden" style="display:none;">

            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab">All</a></li>
                <li role="presentation"><a href="#active" aria-controls="active" role="tab" data-toggle="tab">Active</a></li>
                <li role="presentation"><a href="#inactive" aria-controls="inactive" role="tab" data-toggle="tab">Inactive</a></li>
            </ul>

            <div class="tab-content">

                <!-- all -->
                <div role="tabpanel" class="tab-pane active" id="all">

                    <div class="table-responsive">
                        <table id="requesters-all" class="table datatable table-hover">
                            <thead>
                                <tr>
                                    <th class="table-photo">DP</th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Email</th>
                                    <th>Contact #</th>
                                    <th>Status</th>
                                    <th>ID</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($requesters as $requester)
                                    <tr id="rq-all-entry-{{ $requester->id }}">
                                        <td class="table-photo">
                                            <div class="table-user-dp" style="background: url({{ $requester->dp() }}) center center no-repeat; background-size: cover; "></div>
                                        </td>
                                        <td class="table-strong nowrap">{{ $requester->name() }}</td>
                                        <td>{{ $requester->defaultAddress() }}</td>
                                        <td>
                                            <a href="mailto:{{ $requester->user->email }}">
                                                {{ $requester->user->email }}
                                            </a>
                                        </td>
                                        <td>{{ $requester->contact_number }}</td>
                                        <td>
                                            @if ($requester->user->is_active)
                                                Active
                                            @else
                                                Inactive
                                            @endif
                                        </td>
                                        <td>{{ $requester->id }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>

                <!-- active -->
                <div role="tabpanel" class="tab-pane" id="active">

                    <div class="table-responsive">
                        <table id="requesters-active" class="table datatable table-hover">
                            <thead>
                                <tr>
                                    <th class="table-photo">DP</th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Email</th>
                                    <th>Contact #</th>
                                    <th>Status</th>
                                    <th>ID</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($requesters as $requester)
                                    @if ($requester->user->is_active)
                                        <tr id="rq-active-entry-{{ $requester->id }}">
                                            <td class="table-photo">
                                                <div class="table-user-dp" style="background: url({{ $requester->dp() }}) center center no-repeat; background-size: cover; "></div>
                                            </td>
                                            <td class="table-strong nowrap">{{ $requester->name() }}</td>
                                            <td>{{ $requester->defaultAddress() }}</td>
                                            <td>
                                                <a href="mailto:{{ $requester->user->email }}">
                                                    {{ $requester->user->email }}
                                                </a>
                                            </td>
                                            <td>{{ $requester->contact_number }}</td>
                                            <td>
                                                @if ($requester->user->is_active)
                                                    Active
                                                @else
                                                    Inactive
                                                @endif
                                            </td>
                                            <td>{{ $requester->id }}</td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>

                <!-- inactive -->
                <div role="tabpanel" class="tab-pane" id="inactive">

                    <div class="table-responsive">
                        <table id="requesters-inactive" class="table datatable table-hover">
                            <thead>
                                <tr>
                                    <th class="table-photo">DP</th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Email</th>
                                    <th>Contact #</th>
                                    <th>Status</th>
                                    <th>ID</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($requesters as $requester)
                                    @if (!$requester->user->is_active)
                                        <tr id="rq-inactive-entry-{{ $requester->id }}">
                                            <td class="table-photo">
                                                <div class="table-user-dp" style="background: url({{ $requester->dp() }}) center center no-repeat; background-size: cover; "></div>
                                            </td>
                                            <td class="table-strong nowrap">{{ $requester->name() }}</td>
                                            <td>{{ $requester->defaultAddress() }}</td>
                                            <td>
                                                <a href="mailto:{{ $requester->user->email }}">
                                                    {{ $requester->user->email }}
                                                </a>
                                            </td>
                                            <td>{{ $requester->contact_number }}</td>
                                            <td>
                                                @if ($requester->user->is_active)
                                                    Active
                                                @else
                                                    Inactive
                                                @endif
                                            </td>
                                            <td>{{ $requester->id }}</td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>

        </div>

    </div>

</div>
<div class="col-xs-12 col-sm-4 col-md-3">

    <div class="panel">
        <div class="panel-body">
            <div id="requester-preview" class="preview-container" data-preview-url="{{ route('requesters.preview') }}">
                <div class="qp-photo"><i class="fa fa-image"></i></div>
                <br clear="all" />
                <div class="qp-info">
                    <h3>Select a requester</h3>
                </div>
            </div>
        </div>
        @if ($currentUser->allowedTo('fdsafdsa-add_requesters'))
            <div class="panel-footer">
                <a href="{{ route('requesters.add') }}" class="btn btn-primary">Add Requester</a>
            </div>
        @endif
    </div>

</div>
</div>
@endsection

@section('footer-addon')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script>
$(function() {

    function tabInfo(table) {
        var id = $(table).closest('.tab-pane').attr('id'),
            tab = $('.nav-tabs a[aria-controls='+id+']'),
            length = $(table).DataTable().page.info().recordsDisplay,
            label = tab.find('span');
        if (label.length) { label.remove(); }
        tab.append(' <span>('+length+')</span>');
    }

    var tables = $('.datatable')
        .on('draw.dt', function () {
            tabInfo(this);
        })
        .on('search.dt', function () {
            tabInfo(this);
        })
        .DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5',
                'print'
            ],
            pageLength: 10,
            ordering: true,
            lengthChange: false,
            pagingType: 'numbers',
            select: {
                style: 'single'
            },
            'order': [ 1, 'asc' ],
            'columnDefs': [
                {
                    'targets': [0, 2, 3, 4],
                    'orderable': false
                },
                {
                    'targets': [6],
                    'visible': false
                }
            ]
        })
        .on( 'select', function ( e, dt, type, indexes ) {
            var data = $(this).DataTable().rows(indexes).data()[0],
                id = data[6],
                previewContainer = $('#requester-preview'),
                url = previewContainer.attr('data-preview-url');

            previewContainer.append('<div class="preview-preloader-overlay"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i></div>');
            $.get( url + '/' + id ).done(function(r) {
                r = $.parseJSON(r);
                previewContainer.html(r.view);
            });

        });

    $('#quick-search-input').on('keyup', function () {
        tables.search( this.value ).draw();
    });

    $(document).on('click', '#delete-a', function(e) {
        e.preventDefault();
        var url = $(this).data('url');
        bootbox.dialog({
            title: 'Hey!',
            message: '<div style="font-size: 16px;">Are you sure you want to delete this requester?</div>',
            className: 'modal-danger',
            onEscape: function () {
                bootbox.hideAll();
            },
            buttons: {
                'confirm': {
                    label: 'Yes',
                    className: 'btn-danger',
                    callback: function() {
                        window.location = url;
                    }
                },
                'Close' : {
                    className: 'btn-default'
                }
            }
        });
    });

});
</script>
@endsection
