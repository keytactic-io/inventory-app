@extends('layouts.app')

@section('page-title', 'Add Requester')

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.errors')

    <form action="{{ route('requesters.edit') }}/{{ $requester->id }}" method="post" autocomplete="off"
        id="edit-requester-form" class="with-preloader" 
    >
        {!! csrf_field() !!}
        <input type="hidden" name="requester_id" value="{{ $requester->id}}" />
        <div class="panel">
            <div class="panel-heading">
                <h4>Edit Requester</h4>
            </div>

            <div class="panel-body">

                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label for="first-name">First Name</label>
                            <input type="text" name="first_name" id="first-name" class="form-control" 
                                value="{{ old('first_name', $requester->user->first_name) }}" 
                            />
                        </div>  
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label for="last-name">Last Name</label>
                            <input type="text" name="last_name" id="last-name" class="form-control" 
                                value="{{ old('last_name', $requester->user->last_name) }}" 
                            />
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="address">Address</label>
                            <input type="text" name="address" id="address" class="form-control" 
                                value="{{ old('address', $requester->address) }}" 
                            />
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" name="email" id="email" class="form-control" 
                                value="{{ old('email', $requester->user->email) }}" 
                            />
                        </div>  
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-group">
                            <label for="contact-number">Contact Number</label>
                            <input type="text" name="contact_number" id="contact-number" class="form-control" 
                                value="{{ old('contact_number', $requester->contact_number) }}" 
                            />
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="text" name="password" id="password" class="form-control" 
                                value="{{ old('password') }}" placeholder="Leave blank to remain unchange" 
                            />
                        </div>
                    </div>
                </div>                

            </div>
            
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ route('requesters.list') }}" class="btn btn-default">Cancel</a>
            </div>

        </div>
    </form>

</div>
</div>
</div>
@endsection

@section('footer-addon')
<script>
$(function() {



});
</script>
@endsection
