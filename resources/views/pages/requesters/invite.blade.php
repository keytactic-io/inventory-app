@extends('layouts.app')

@section('page-title', 'Invite Requester')

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">
    @include('includes.errors')
    @include('includes.status')
</div>
<div class="col-xs-12">    

    <form action="{{ route('requesters.invite') }}" method="post" autocomplete="off"
        id="invite-requester-form" class="with-preloader" 
    >
        {!! csrf_field() !!}

        <div class="panel">
            <div class="panel-heading">
                <h4>Invite Requester</h4>
            </div>

            <div class="panel-body">

                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" name="email" id="email" class="form-control" value="{{ old('email') }}" placeholder="email@website.com" required />
                </div>

                <div class="form-group fg-last">
                    <label for="market">Market</label>
                    <select name="market_id" id="market" class="form-control" required>
                        <option value=""></option>
                        @foreach ($markets as $market)
                            <option value="{{ $market->id }}">{{ $market->name }}</option>
                        @endforeach
                    </select>
                </div>

            </div>
            
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary">Send Invite</button>
                <a href="{{ route('requesters.list') }}" class="btn btn-default">Cancel</a>
            </div>

        </div>
    </form>

</div>
</div>
</div>
@endsection

@section('footer-addon')
<script>
$(function() {



});
</script>
@endsection
