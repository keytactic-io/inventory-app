@extends('layouts.app')

@section('page-title', 'Requester Invites')

@section('head-addon')
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.0/css/select.dataTables.min.css" />
<link rel="stylesheet" href="{{ asset('assets/css/dt-override.css') }}" />
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.status')

    <div class="panel">

        <div class="panel-heading">
            <h4>Requester Invites</h4>
        </div>

        <div class="panel-preloader" data-target="#invites-panel-body">
            <i class="fa fa-circle-o-notch fa-spin fa-fw"></i> Preparing invites table
        </div>
        <div id="invites-panel-body" class="panel-body hidden" style="display:none;">
            <div class="table-responsive">
                <table class="table datatable">
                    <thead>
                        <tr>
                            <th>Email</th>
                            <th>Market</th>
                            <th>Date Sent</th>
                            <th class="text-right">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($requesters as $requester)
                            <tr>
                                <td>
                                    <a href="mailto:{{ $requester->user->email }}">{{ $requester->user->email }}</a>
                                </td>
                                <td>
                                    <a href="{{ route('markets.details') }}/{{ $requester->market->id }}" target="_blank">
                                        {{ $requester->market->name }}
                                    </a>
                                </td>
                                <td>{{ processDate2($requester->created_at) }}</td>
                                <td class="text-right">
                                    <a href="#" class="resend-invite" title="Resend Invite">
                                        <i class="fa fa-send fa-fw"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>

</div>
</div>
</div>
@endsection

@section('footer-addon')
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script>
$(function() {

    $('.datatable').DataTable({
        pageLength: 20,
        ordering: true,
        lengthChange: false,
        pagingType: 'numbers',
        'order': [ 2, 'asc' ],
        'columnDefs': [
            {
                'targets': [3],
                'orderable': false
            }
        ]
    });

});
</script>
@endsection
