@extends('layouts.app')

@section('page-title', 'Add Requester')

@section('head-addon')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css">
<link rel="stylesheet" href="{{ asset('assets/css/bt-select-override.css') }}" />
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.errors')

    <form action="{{ route('requesters.add') }}" method="post" autocomplete="off"
        id="add-requester-form" class="with-preloader" 
    >
        {!! csrf_field() !!}

        <div class="panel">
            <div class="panel-heading">
                <h4>Add Requester</h4>
            </div>

            <div class="panel-body">

                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label for="first-name">First Name</label>
                            <input type="text" name="first_name" id="first-name" class="form-control" value="{{ old('first_name') }}" required />
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label for="last-name">Last Name</label>
                            <input type="text" name="last_name" id="last-name" class="form-control" value="{{ old('last_name') }}" required />
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label for="market">Market</label>
                            <select name="market_id" id="market" class="form-control"
                                required
                            >
                                <option value=""></option>
                                @foreach ($markets as $market)
                                    <option value="{{ $market->id }}">{{ $market->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="panel-separator"><span></span></div>
                    </div>

                    <div class="col-xs-12">
                        <div class="form-group fg-last">
                            <label for="last-name">Address</label>
                        </div>
                        <div class="addresses clearfix">
                            <div class="addresses-entry row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="default" id="" value="1" checked />
                                                <span>Default address</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="addresses[1][street_1]" id="street-1" class="form-control"
                                            value="{{ old('addresses.1.street_1') }}" placeholder="Street 1" required
                                        />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="addresses[1][street_2]" id="street-2" class="form-control"
                                            value="{{ old('addresses.1.street_2') }}" placeholder="Street 2"
                                        />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-5">
                                    <div class="form-group">
                                        <input type="text" name="addresses[1][city]" id="city" class="form-control"
                                            value="{{ old('addresses.1.city') }}" placeholder="City" required
                                        />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-5">
                                    <div class="form-group">
                                        <select name="addresses[1][state]" id="state-1" class="form-control the-states"
                                            data-live-search="true" required
                                        >
                                            <option value=""></option>
                                            @foreach (getStates() as $code => $state)
                                            <option value="{{ $state }}">
                                                {{ $state }}
                                            </option>
                                            @endforeach
                                        </select>
                                        <input type="hidden" name="addresses[1][country]" value="US" />
                                    </div>
                                </div>
                                <?php /*
                                <div class="col-xs-12 col-sm-8 col-md-3">
                                    <div class="form-group">
                                        <select name="addresses[1][country]" id="country" class="form-control countries"
                                            data-live-search="true" required
                                        >
                                            <option value=""></option>
                                            @foreach (getCountryCodes() as $code => $name)
                                                <option value="{{ $code}}"
                                                    @if ($code == 'US') selected @endif
                                                >
                                                    {{ $name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                */ ?>
                                <div class="col-xs-12 col-sm-6 col-md-2">
                                    <div class="form-group">
                                        <input type="text" name="addresses[1][postcode]" id="postcode" class="form-control"
                                            value="{{ old('addresses.1.postcode') }}" placeholder="Zip" required
                                        />
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <div class="address-separator"><span></span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <button id="add-address" class="btn btn-xs btn-primary" data-url="{{ route('requesters.addaddressfields') }}">Add Address</button>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="panel-separator"><span></span></div>
                    </div>

                    <div class="col-xs-12 col-sm-4">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" name="email" id="email" class="form-control" value="{{ old('email') }}" required />
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-group">
                            <label for="contact-number">Contact Number</label>
                            <input type="text" name="contact_number" id="contact-number" class="form-control" value="{{ old('contact_number') }}" required />
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="text" name="password" id="password" class="form-control" value="{{ old('password') }}" required />
                        </div>
                    </div>
                </div>

            </div>

            <div class="panel-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ route('requesters.list') }}" class="btn btn-default">Cancel</a>
            </div>

        </div>
    </form>

</div>
</div>
</div>
@endsection

@section('footer-addon')
<script src="{{ asset('assets/js/vendors/inputmask.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js"></script>
<script>
$(function() {

    $('#contact-number').mask('999-999-9999');

    $(document).on('click', '#add-address', function(e) {
        e.preventDefault();
        var count = $('.addresses-entry').length + 1;

        $.get( $(this).data('url'), {c: count} ).done(function(r) {
            $('.addresses').append(r.view);
            $('.the-states').selectpicker({
                style: 'btn-default',
                size: 4
            });
        });
    });

    $(document).on('click', '.remove-address', function(e) {
        e.preventDefault();
        $($(this).attr('data-target')).remove();
    });

    $('.the-states').selectpicker({
        style: 'btn-default',
        size: 4
    });

});
</script>
@endsection
