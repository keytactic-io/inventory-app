@extends('layouts.app')

@section('page-title', 'Add Requester')

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @if (!$currentUser->is_requester)
    @include('includes.status')
    @include('includes.errors')

    <form action="{{ route('requesters.productallocation', ['requesterId' => $requester->id]) }}" method="post"
        id="product-allocation-form" class="with-preloader" 
    >
    {!! csrf_field() !!}
    @endif
    <div class="panel">

        <div class="panel-heading">
            @if (!$currentUser->is_requester)
                <h4>Product Allocations for <span class="text-primary">{{ $requester->name() }}</span></h4>
            @else
                <h4>Your Product Allocations</h4>
            @endif
        </div>

        <div class="panel-body">
            
            <div class="table-responsive">
                <table id="products-all" class="table datatable table-hover">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Allocation</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($products as $product)
                            <?php $allocation = $product->hasAllocation($requester->id); ?>
                            <tr id="products-all-entry-{{ $product->id }}">
                                <td class="table-strong">
                                    <a href="{{ route('products.details') }}/{{ $product->id }}" target="_blank">
                                        {{ $product->name }}
                                    </a>
                                </td>
                                <td>{{ $product->description }}</td>
                                <td style="width: 100px; padding-right: 0;">
                                    @if (!$currentUser->is_requester)
                                        <input type="number" name="allocations[{{ $product->id}}]" class="input-in-table"  id="allocation-{{ $product->id }}"
                                            min="0" required value="{{ $allocation < 0 ? '' : $allocation }}" 
                                            @if ($allocation < 0) disabled @else placeholder="0" @endif
                                        />
                                    @else
                                        {{ $product->hasAllocation($requester->id) < 0 ? '&#x221e;' : $product->hasAllocation($requester->id) }}
                                    @endif
                                </td>
                                <td style="width: 100px;">
                                    @if ($currentUser->is_requester)
                                        <i class="fa fa-check @if ($allocation < 0) text-primary @else text-muted2 @endif "></i> &nbsp;Infinite
                                    @else
                                        <div class="checkbox" style="margin: 0;">
                                            <label>
                                                <input type="checkbox" name="allocations[{{ $product->id}}]" value="-1"
                                                    class="infinite-allocation" 
                                                    data-target="#allocation-{{ $product->id }}" 
                                                    @if ($allocation < 0) checked @endif
                                                />
                                                <span>Infinite</span>
                                            </label>
                                        </div>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>
        @if (!$currentUser->is_requester)
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary">Save Allocation</button>
                <a href="{{ route('users.profile') }}/{{ $requester->user->id }}" class="btn btn-default">Back to Profile</a>
            </div>
        @endif

    </div>
    @if (!$currentUser->is_requester)
    </form>
    @endif

</div>
</div>
</div>
@endsection

@section('footer-addon')
@if ($currentUser->is_requester)
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script>
$(function() {

    $('.datatable').DataTable({
        pageLength: 20,
        ordering: true,
        lengthChange: false,
        pagingType: 'numbers',
        'order': [ 0, 'asc' ],
        'columnDefs': [
            {
                'targets': [2, 3],
                'orderable': false
            }
        ]
    });

})
</script>
@endif
<script>
$(function() {

    $('.infinite-allocation').each(function() {
        $(this).on('change', function() {
            if ($(this).prop('checked')) {
                $($(this).data('target')).attr('placeholder', '').prop('disabled', true);
            } else {
                $($(this).data('target')).attr('placeholder', 0).prop('disabled', false).focus();
            }
        });
    });

});
</script>
@endsection
