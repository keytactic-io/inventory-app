<div class="qp-photo" style="background: url({{ $requester->dp() }}) center center no-repeat; background-size: cover; "></div>
<br clear="all" />
<div class="qp-info">
    @if ($requester->user->is_active)
        <span class="user-status label label-primary">Active</span>
    @else
        <span class="user-status label label-warning">Inactive</span>
    @endif
    <h3>{{ $requester->name() }}</h3>
    <div class="qp-meta">
        <p>{{ $requester->market->name }}</p>
        <p>{{ $requester->contact_number }}</p>
    </div>
</div>

<div class="qp-options clearfix">
    @if ($currentUser->allowedTo('view_requesters'))
    <a id="details-a" href="{{ route('users.profile') }}/{{ $requester->user->id }}" class="btn btn-primary btn-sm">Profile</a>
    @endif
    @if ($currentUser->allowedTo('edit_requesters'))
    <a id="edit-btn" href="{{ route('users.profile.edit') }}/{{ $requester->user->id }}" class="btn btn-warning btn-sm">Edit</a>
    @endif
    @if ($currentUser->allowedTo('delete_requesters'))
    <a id="delete-a" href="#" data-url="{{ route('users.delete') }}/{{ $requester->user->id }}" class="btn btn-danger btn-sm">Delete</a>
    @endif
    @if ($currentUser->allowedTo('edit_requesters'))
    <a href="{{ route('requesters.productallocation', ['requesterId' => $requester->id]) }}" class="btn btn-success btn-sm">Product Allocation</a>
    @endif
</div>
