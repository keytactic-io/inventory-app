@extends('layouts.app')

@section('page-title', 'Requester Sign Up')
@section('body-class', 'login')

@section('head-addon')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css">
<link rel="stylesheet" href="{{ asset('assets/css/bt-select-override.css') }}" />
@endsection

@section('content')
<div class="container">
<div class="row">
<div class="col-xs-12 col-md-6 col-md-offset-3">

    @include('includes.errors')

    <form action="{{ route('requesters.signup') }}" method="post" autocomplete="off">
        {!! csrf_field() !!}

        <div id="requester-signup-panel" class="panel">
            <div class="panel-heading">
                <h4>Requester Sign Up</h4>
            </div>

            <div class="panel-body">

                <div class="form-group">
                    <span class="label label-warning">All fields are required.</span>
                </div>

                <div class="form-group">
                    <label for="first-name">First Name</label>
                    <input type="text" name="first_name" id="first-name" class="form-control" value="{{ old('first_name') }}" />
                </div>
                <div class="form-group">
                    <label for="last-name">Last Name</label>
                    <input type="text" name="last_name" id="last-name" class="form-control" value="{{ old('last_name') }}" />
                </div>

                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" name="email" id="email" class="form-control" value="{{ old('email', $requester->user->email) }}" placeholder="email@website.com" readonly />
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" id="password" class="form-control" value="{{ old('password') }}" />
                </div>
                <div class="form-group">
                    <label for="c-password">Confirm Password</label>
                    <input type="password" name="confirm_password" id="c-password" class="form-control" value="{{ old('confirm_password') }}" />
                </div>

                <div class="form-group">
                    <label for="contact-number">Contact Number</label>
                    <input type="text" name="contact_number" id="contact-number" class="form-control" value="{{ old('contact_number') }}" />
                </div>

                <div class="form-group-divider"><span></span></div>

                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group fg-last">
                            <label for="last-name">Address</label>
                        </div>
                        <div class="addresses clearfix">
                            <div id="address-entry-1" class="addresses-entry row">
                                
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="default" id="" value="1" checked />
                                                <span>Default address</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <input type="text" name="addresses[1][street_1]" id="street-1" class="form-control" 
                                            value="{{ old('addresses.1.street_1') }}" placeholder="Street 1" 
                                        />
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <input type="text" name="addresses[1][street_2]" id="street-2" class="form-control" 
                                            value="{{ old('addresses.1.street_2') }}" placeholder="Street 2" 
                                        />
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <input type="text" name="addresses[1][city]" id="city" class="form-control" 
                                            value="{{ old('addresses.1.city') }}" placeholder="City" 
                                        />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-8">
                                    <div class="form-group">
                                        <select name="addresses[1][state]" id="state" class="form-control the-states"
                                            data-live-search="true" required
                                        >
                                            <option value=""></option>
                                            @foreach (getStates() as $code => $state)
                                            <option value="{{ $state }}">
                                                {{ $state }}
                                            </option>
                                            @endforeach
                                        </select>
                                        <input type="hidden" name="addresses[1][country]" value="US" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4">
                                    <div class="form-group">
                                        <input type="text" name="addresses[1][postcode]" id="postcode" class="form-control" 
                                            value="{{ old('addresses.1.postcode') }}" placeholder="Postcode" 
                                        />
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <div class="address-separator"><span></span></div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="form-group fg-last">
                            <button id="add-address" class="btn btn-xs btn-primary"
                                data-url="{{ route('requesters.addaddressfields') }}"
                            >
                                Add Address
                            </button>
                        </div>
                    </div>
                </div>

            </div>
            
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary">Sign Up</button>
            </div>

        </div>
    </form>

</div>
</div>
</div>
@endsection

@section('footer-addon')
<script src="{{ asset('assets/js/vendors/inputmask.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js"></script>
<script>
$(function() {

    $('#contact-number').mask('999-999-9999');

    $('.the-states').selectpicker({
        style: 'btn-default',
        size: 4
    });

    $(document).on('click', '#add-address', function(e) {
        e.preventDefault();
        var count = $('.addresses-entry').length + 1;

        $.get( $(this).data('url'), {c: count, front: true} ).done(function(r) {
            $('.addresses').append(r.view);
            $('.the-states').selectpicker({
                style: 'btn-default',
                size: 4
            });
        });

    });

    $(document).on('click', '.remove-address', function(e) {
        e.preventDefault();
        $($(this).attr('data-target')).remove();
    });

});
</script>
@endsection
