<div id="address-entry-{{ $count }}" class="addresses-entry row">

    <div class="col-xs-12">
        <div class="form-group">
            <div class="radio">
                <label>
                    <input type="radio" name="default" id="" value="{{ $count }}" />
                    <span>Default address</span>
                </label>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group">
            <input type="text" name="addresses[{{ $count }}][street_1]" id="street-{{ $count }}" class="form-control" 
                value="{{ old('addresses.' . $count . '.street_1') }}" placeholder="Street 1" 
            />
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group">
            <input type="text" name="addresses[{{ $count }}][street_2]" id="street-{{ $count }}" class="form-control" 
                value="{{ old('addresses.' . $count . '.street_2') }}" placeholder="Street 2" 
            />
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group">
            <input type="text" name="addresses[{{ $count }}][city]" id="city-{{ $count }}" class="form-control" 
                value="{{ old('addresses.' . $count . '.city') }}" placeholder="City" 
            />
        </div>
    </div>
    <div class="col-xs-12 col-sm-8">
        <div class="form-group">
            <select name="addresses[{{ $count }}][state]" id="state-{{ $count }}" class="form-control the-states"
                data-live-search="true" required
            >
                <option value=""></option>
                @foreach (getStates() as $code => $state)
                <option value="{{ $state }}">
                    {{ $state }}
                </option>
                @endforeach
            </select>
            <input type="hidden" name="addresses[{{ $count }}][country]" value="US" />
        </div>
    </div>
    <div class="col-xs-12 col-sm-4">
        <div class="form-group">
            <input type="text" name="addresses[{{ $count }}][postcode]" id="postcode-{{ $count }}" class="form-control" 
                value="{{ old('addresses.' . $count . '.postcode') }}" placeholder="Postcode" 
            />
        </div>
    </div>

    <div class="col-xs-12">
        <div class="form-group"><a href="#" class="remove-address btn btn-xs btn-danger" data-target="#address-entry-{{ $count }}">Remove Address</a></div>
    </div>

    <div class="col-xs-12">
        <div class="form-group">
            <div class="address-separator"><span></span></div>
        </div>
    </div>

</div>