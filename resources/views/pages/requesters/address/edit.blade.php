<div class="form-error"></div>
<form action="{{ route('requesters.address.edit') }}" method="post" autocomplete="off" id="edit-requester-address">
{!! csrf_field() !!}
<input type="hidden" name="requester_id" value="{{ $address->requester->id }}" />
<input type="hidden" name="address_id" value="{{ $address->id }}" />

    <div class="form-horizontal">

        <div class="form-group">
            <label for="street1" class="col-xs-12 col-sm-3 control-label">Street 1</label>
            <div class="col-xs-12 col-sm-9">
                <input type="text" name="street_1" id="street1" class="form-control" value="{{ $address->street_1 }}" />
            </div>
        </div>

        <div class="form-group">
            <label for="street2" class="col-xs-12 col-sm-3 control-label">Street 2</label>
            <div class="col-xs-12 col-sm-9">
                <input type="text" name="street_2" id="street2" class="form-control" value="{{ $address->street_2 }}" />
            </div>
        </div>

        <div class="form-group">
            <label for="city" class="col-xs-12 col-sm-3 control-label">City</label>
            <div class="col-xs-12 col-sm-9">
                <input type="text" name="city" id="city" class="form-control" value="{{ $address->city }}" />
            </div>
        </div>

        <div class="form-group">
            <label for="state" class="col-xs-12 col-sm-3 control-label">State</label>
            <div class="col-xs-12 col-sm-9">
                <select name="state" id="state" class="form-control" required>
                    <option value=""></option>
                    @foreach (getStates() as $state)
                        <option value="{{ $state }}"
                            @if ($state == $address->state) selected @endif
                        >
                            {{ $state }}
                        </option>
                    @endforeach
                </select>
                <input type="hidden" name="country" value="US" />
            </div>
        </div>

        <?php /*
        <div class="form-group">
            <label for="country" class="col-xs-12 col-sm-3 control-label">Country</label>
            <div class="col-xs-12 col-sm-9">
                <select name="country" id="country" class="form-control" required>
                    <option value=""></option>
                    @foreach (getCountryCodes() as $code => $name)
                        <option value="{{ $code}}"
                            @if ($code == $address->country) selected @endif
                        >
                            {{ $name }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
        */ ?>

        <div class="form-group fg-last">
            <label for="postcode" class="col-xs-12 col-sm-3 control-label">Zip</label>
            <div class="col-xs-12 col-sm-9">
                <input type="text" name="postcode" id="postcode" class="form-control" value="{{ $address->postcode }}" />
            </div>
        </div>

    </div>

</form>
