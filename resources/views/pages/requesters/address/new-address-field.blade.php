<div id="address-entry-{{ $count }}" class="addresses-entry row">

    <div class="col-xs-12">
        <div class="form-group">
            <div class="radio">
                <label>
                    <input type="radio" name="default" id="" value="{{ $count }}" />
                    <span>Set as default address</span>
                </label>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-md-6">
        <div class="form-group">
            <input type="text" name="addresses[{{ $count }}][street_1]" id="street-1" class="form-control" placeholder="Street 1" required />
        </div>
    </div>

    <div class="col-xs-12 col-md-6">
        <div class="form-group">
            <input type="text" name="addresses[{{ $count }}][street_2]" id="street-2" class="form-control" placeholder="Street 2" />
        </div>
    </div>

    <div class="col-xs-12 col-md-5">
        <div class="form-group">
            <input type="text" name="addresses[{{ $count }}][city]" id="city" class="form-control" placeholder="City" required />
        </div>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-5">
        <div class="form-group">
            <select name="addresses[{{ $count }}][state]" id="state-{{ $count }}" class="form-control the-states"
                data-live-search="true" required
            >
                <option value=""></option>
                @foreach (getStates() as $state)
                    <option value="{{ $state }}">
                        {{ $state }}
                    </option>
                @endforeach
            </select>
            <input type="hidden" name="country" value="US" />
        </div>
    </div>

    <?php /*
    <div class="col-xs-12 col-sm-8 col-md-3">
        <div class="form-group">
            <select name="addresses[{{ $count }}][country]" id="country" class="form-control countries"
                data-live-search="true" required
            >
                <option value=""></option>
                @foreach (getCountryCodes() as $code => $name)
                    <option value="{{ $code}}"
                        @if ($code == 'US') selected @endif 
                    >
                        {{ $name }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
    */ ?>

    <div class="col-xs-12 col-sm-6 col-md-2">
        <div class="form-group">
            <input type="text" name="addresses[{{ $count }}][postcode]" id="postcode" class="form-control" placeholder="Postcode" required />
        </div>
    </div>

    <div class="col-xs-12">
        <div class="form-group"><a href="#" class="remove-address btn btn-xs btn-danger" data-target="#address-entry-{{ $count }}">Remove Address</a></div>
    </div>

    <div class="col-xs-12">
        <div class="form-group">
            <div class="address-separator"><span></span></div>
        </div>
    </div>

</div>