@extends('layouts.app')

@section('page-title', 'Markets')

@section('head-addon')
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.0/css/select.dataTables.min.css" />
<link rel="stylesheet" href="{{ asset('assets/css/dt-override.css') }}" />
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.status')

    <div class="panel">

        <div class="panel-heading">
            <h4>Markets</h4>
        </div>

        <div class="panel-filter">
    		@if ($currentUser->allowedTo('add_markets'))
            <div class="input-group">
                <input type="text" placeholder="Search markets" class="form-control" id="quick-search-input" autocomplete="off">
    								<span class="input-group-btn">
                          <a href="{{ route('markets.add') }}" class="btn btn-primary btn-squared">
                            <i class="fa fa-plus"></i>
                          </a>
                    </span>
            </div>
    		@else
    						<input type="text" placeholder="Search markets" class="form-control" id="quick-search-input" autocomplete="off">			
        @endif
        </div>

        <div class="panel-preloader" data-target="#markets-panel">
            <i class="fa fa-circle-o-notch fa-spin fa-fw"></i> &nbsp; Preparing markets table
        </div>
        <div id="markets-panel" class="panel-body" style="display:none;">

            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab">All</a></li>
                {{-- <li role="presentation"><a href="#received" aria-controls="received" role="tab" data-toggle="tab">Received</a></li> --}}
            </ul>

            <div class="tab-content">

                <!-- all -->
                <div role="tabpanel" class="tab-pane active" id="all">

                    <div class="table-responsive">
                        <table id="markets-all" class="table datatable table-hover">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Requesters Count</th>
                                    <th class="text-right">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($markets as $market)
                                    <tr id="markets-entry-{{ $market->id }}">
                                        <td class="table-strong">
                                            <a href="{{ route('markets.details') }}/{{ $market->id }}">
                                                {{ $market->name }}
                                            </a>
                                        </td>
                                        <td>
                                            <a href="#">
                                                {{ $market->requestersCount() }}
                                            </a>
                                        </td>
                                        <td class="text-right">
                                            @if ($currentUser->allowedTo('delete_markets'))
                                                <a href="{{ route('markets.edit') }}/{{ $market->id }}">
                                                    <i class="fa fa-pencil fa-fw"></i>
                                                </a>
                                            @endif
                                            @if ($currentUser->allowedTo('delete_markets'))
                                                <a href="#" class="delete-market" data-url="{{ route('markets.delete') }}/{{ $market->id }}">
                                                    <i class="fa fa-trash-o fa-fw"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>

        </div>

    </div>

</div>
</div>
</div>
@endsection

@section('footer-addon')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script>
$(function() {

    function tabInfo(table) {
        var id = $(table).closest('.tab-pane').attr('id'),
            tab = $('.nav-tabs a[aria-controls='+id+']'),
            length = $(table).DataTable().page.info().recordsDisplay,
            label = tab.find('span');
        if (label.length) { label.remove(); }
        tab.append(' <span>('+length+')</span>');
    }

    var tables = $('.datatable')
        .on('draw.dt', function () {
            tabInfo(this);
        })
        .on('search.dt', function () {
            tabInfo(this);
        })
        .DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5',
                'print'
            ],
            pageLength: 10,
            ordering: true,
            lengthChange: false,
            pagingType: 'numbers',
            'order': [ 0, 'asc' ],
            'columnDefs': [
                {
                    'targets': [2],
                    'orderable': false
                }
            ]
        });

    $('#quick-search-input').on('keyup', function () {
        tables.search( this.value ).draw();
    });

    $(document).on('click', '.delete-market', function(e) {
        e.preventDefault();
        var url = $(this).data('url');
        bootbox.dialog({
            title: 'Hey!',
            message: '<div style="font-size: 16px;">Are you sure you want to delete this market?</div>',
            className: 'modal-danger',
            onEscape: function () {
                bootbox.hideAll();
            },
            buttons: {
                'confirm': {
                    label: 'Yes',
                    className: 'btn-danger',
                    callback: function() {

                        loader.show();
                        window.location = url;
                        return false;

                    }
                },
                'Close' : {
                    className: 'btn-default'
                }
            }
        });
    });

});
</script>
@endsection
