@extends('layouts.app')

@section('page-title', 'Edit Market')

@section('head-addon')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css">
<link rel="stylesheet" href="{{ asset('assets/css/bt-select-override.css') }}" />
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.status')

    <form action="{{ route('markets.edit') }}/{{ $market->id }}" method="post" autocomplete="off">
    {!! csrf_field() !!}
    <div class="panel">

        <div class="panel-heading">
            <h4>Edit Market</h4>
        </div>

        <div class="panel-body">
            
            <div class="form-group fg-last">
                <label for="location">Name of Market</label>
                <input type="text" name="name" id="location" class="form-control" value="{{ old('name', $market->name)}}" required />
            </div>

        </div>
    
        <div class="panel-footer">
            <button type="submit" class="btn btn-primary">Submit Changes</button>
            <a href="{{ route('markets.list') }}" class="btn btn-default">Cancel</a>
        </div>

    </div>
    </form>

</div>
</div>
</div>
@endsection