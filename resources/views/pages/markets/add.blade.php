@extends('layouts.app')

@section('page-title', 'Add Market')

@section('head-addon')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css">
<link rel="stylesheet" href="{{ asset('assets/css/bt-select-override.css') }}" />
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">
    @include('includes.errors')
</div>
<div class="col-xs-12">    

    <form action="{{ route('markets.add') }}" method="post" autocomplete="off">
    {!! csrf_field() !!}
    <div class="panel">

        <div class="panel-heading">
            <h4>Add Market</h4>
        </div>

        <div class="panel-body">
            
            <div class="form-group">
                <label for="location">Name of Market</label>
                <input type="text" name="name" id="location" class="form-control" value="{{ old('name')}}" required />
            </div>

            <div class="form-group">
                <label for="requesters-selection">Requester</label>
                <select id="requesters-selection" class="form-control" 
                    data-live-search="true" 
                >
                    <option value=""></option>
                    @foreach ($requesters as $requester)
                        <option value="{{ $requester->id }}">{{ $requester->name() }}</option>
                    @endforeach
                </select>
            </div>

            <div class="table-responsive">
                <table class="table table-hover" id="requesters-table">
                    <thead>
                        <th>Name</th>
                        <th class="text-right">&nbsp;</th>
                    </thead>
                    <tbody>
                        <tr class="no-data">
                            <td colspan="2" class="text-center active">Please select at least one (1) requester</td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
    
        <div class="panel-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a href="{{ route('markets.list') }}" class="btn btn-default">Cancel</a>
        </div>

    </div>
    </form>

</div>
</div>
</div>
@endsection

@section('footer-addon')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js"></script>
<script>
function addProduct(e)
{
    var id = e.target.value;
    if (id != '' && id > 0) {
        var name = $('#' + e.target.attributes['id'].value).find('option[value="' + id + '"]').text(),
            tbody = $('#requesters-table').find('tbody'),
            html = '<tr id="rq-tr-' + id + '">' +
                    '<td class="table-strong">' + name + '<input type="hidden" name="requesters[]" value="' + id + '" /></td>' +
                    '<td class="text-right"><a href="#" class="remove-tr" data-target="#rq-tr-' + id + '"><i class="fa fa-close fa-fw"></i></a></td>' +
                    '</tr>';

        if (tbody.find('tr.no-data').length == 1) {
            $(tbody).html(html);
        } else {

            if ($('#rq-tr-' + id).length == 0) {
                $(html).appendTo(tbody);
            }

        }
    }
}
$(function() {

    $('#requesters-selection')
        .selectpicker({
            style: 'btn-primary',
            size: 6
        })
        .on('changed.bs.select', function(e) {
            addProduct(e);
        })
        .on('refreshed.bs.select', function(e) {
            addProduct(e);
        });

    $(document).on('click', '.remove-tr', function(e) {
        e.preventDefault();
        $($(this).attr('data-target')).remove();
        var tbody = $('#requesters-table').find('tbody');
        if ($(tbody).find('tr').length == 0) {
            $(tbody).html('<tr class="no-data"><td colspan="2" class="text-center active">Please select at least one (1) requester</td></tr>');
        }
    });

});
</script>
@endsection