@extends('layouts.app')

@section('page-title', $market->name)

@section('head-addon')
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.0/css/select.dataTables.min.css" />
<link rel="stylesheet" href="{{ asset('assets/css/dt-override.css') }}" />
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.status')

</div>
<div class="col-xs-12">


    <div class="panel">
        
        <div class="panel-body">
            
            <div class="details-content">

                <h1 style="margin-bottom: 0;">{{ $market->name }}</h1>

            </div>

        </div>
        @if ($currentUser->allowedTo('edit_markets'))
            <div class="panel-footer">
                @if (!$currentUser->allowedTo('delete_markets') AND $currentUser->allowedTo('edit_markets'))
                    <a href="{{ route('markets.edit') }}/{{ $market->id }}" class="btn btn-primary btn-sm">Edit</a>
                @else
                    <div class="btn-group">
                        <a href="{{ route('markets.edit') }}/{{ $market->id }}" class="btn btn-primary btn-sm">Edit</a>
                        <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="{{ route('markets.delete') }}/{{ $market->id }}">Delete</a></li>
                        </ul>
                    </div>
                @endif
            </div>
        @endif

    </div>

</div>
<div class="col-xs-12">
    
    <div class="panel">

        <div class="panel-heading">
            <h4>Requesters</h4>
        </div>

        <div class="panel-preloader" data-target="#requesters-panel-body">
            <i class="fa fa-circle-o-notch fa-spin fa-fw"></i> Preparing requesters table
        </div>
        <div id="requesters-panel-body" class="panel-body hidden" style="display:none;">

            <div class="table-responsive">
                <table class="table datatable">
                    <thead>
                        <tr>
                            <th class="table-photo">DP</th>
                            <th>Requester</th>
                            <th>Requests</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($market->requesters as $requester)
                            @if (!$requester->registered)
                                <?php continue; ?>
                            @endif
                            <tr>
                                <td class="table-photo">
                                    <div class="table-user-dp" style="background: url({{ $requester->user->dp() }}) center center no-repeat; background-size: cover; "></div>
                                </td>
                                <td class="table-strong">
                                    <a href="{{ route('users.profile') }}/{{ $requester->user->id }}">
                                        {{ $requester->user->name() }}
                                    </a>
                                </td>
                                <td>
                                    {{ $requester->user->requestsCount() }}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>

</div>
</div>
</div>
@endsection

@section('footer-addon')
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script>
$(function() {

    $('.datatable').DataTable({
        pageLength: 20,
        ordering: true,
        lengthChange: false,
        pagingType: 'numbers',
        'order': [ 1, 'asc' ],
        'columnDefs': [
            {
                'targets': [0],
                'orderable': false
            }
        ]
    });

});
</script>
@endsection