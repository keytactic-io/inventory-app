<div class="form-error"></div>
<form action="{{ route('pos.emailtovendor') }}/{{ $purchaseOrder->id }}" method="post" 
    autocomplete="off" id="email-pos-to-vendor-form"
>
{!! csrf_field() !!}

    <div class="form-group">
        <div class="input-group">
            <span class="input-group-addon" id="basic-addon1">
                <i class="fa fa-envelope"></i>
            </span>
            <input type="email" class="form-control" placeholder="email" name="mail_recipient" aria-describedby="basic-addon1" 
                id="recipient" required value="{{ $email }}"
            />
        </div>
    </div>

    <div class="form-group fg-last">
        <label for="cc-emails">CC</label>
        <input type="text" name="cc_emails" id="cc-emails" class="form-control" value="{{ app_setting('accounting_email') }}" required />
    </div>

</form>
<script>
$(function() {

    $('#cc-emails').tagsInput({
        height: 'auto',
        width: 'auto',
        defaultText: 'add email',
    });

});
</script>