@extends('layouts.app')

@section('page-title', 'Purchase Orders')

@section('head-addon')
<link href="{{ asset('assets/css/vendors/tagsinput.css') }}" rel="stylesheet" />
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.0/css/select.dataTables.min.css" />
<link rel="stylesheet" href="{{ asset('assets/css/dt-override.css') }}" />
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.status')

    <div class="panel">

        <div class="panel-heading">
            <h4>Purchase Orders</h4>
        </div>

        <div class="panel-filter">
    		@if ($currentUser->allowedTo('add_purchase_orders'))
            <div class="input-group">
                <input type="text" placeholder="Search purchase orders" class="form-control" id="quick-search-input" autocomplete="off">
    								<span class="input-group-btn">
                          <a href="{{ route('pos.create') }}" class="btn btn-primary btn-squared">
                            <i class="fa fa-plus"></i>
                          </a>
                    </span>
            </div>
    		@else
    						<input type="text" placeholder="Search purchase orders" class="form-control" id="quick-search-input" autocomplete="off">
        @endif
        </div>

        <div class="panel-preloader" data-target="#purchase-orders-panel">
            <i class="fa fa-circle-o-notch fa-spin fa-fw"></i> &nbsp; Preparing purchase orders table
        </div>
        <div id="purchase-orders-panel" class="panel-body" style="display:none;">

            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab">All</a></li>
                <li role="presentation"><a href="#waiting" aria-controls="waiting" role="tab" data-toggle="tab">Waiting for Arrival</a></li>
                <li role="presentation"><a href="#received" aria-controls="received" role="tab" data-toggle="tab">Received</a></li>
            </ul>

            <div class="tab-content">

                <!-- all -->
                <div role="tabpanel" class="tab-pane active" id="all">

                    <div class="table-responsive">
                        <table id="po-all" class="table datatable table-hover">
                            <thead>
                                <tr>
                                    <th>UID</th>
                                    <th>Vendor</th>
                                    <th>Products Count</th>
                                    <th>Created by</th>
                                    <th>Created at</th>
                                    <th>Status</th>
                                    <th class="text-right">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($poAll as $pO)
                                    @include('pages.purchase-orders.row', [
                                        'uid' => 'po-all-entry',
                                        'pO' => $pO
                                    ])
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>

                <div role="tabpanel" class="tab-pane" id="waiting">

                    <div class="table-responsive">
                        <table id="po-waiting" class="table datatable table-hover">
                            <thead>
                                <tr>
                                    <th>UID</th>
                                    <th>Vendor</th>
                                    <th>Products Count</th>
                                    <th>Created by</th>
                                    <th>Created at</th>
                                    <th>Status</th>
                                    <th class="text-right">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($poWaiting as $pO)
                                    @include('pages.purchase-orders.row', [
                                        'uid' => 'po-waiting-entry',
                                        'pO' => $pO
                                    ])
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>

                <div role="tabpanel" class="tab-pane" id="received">

                    <div class="table-responsive">
                        <table id="po-received" class="table datatable table-hover">
                            <thead>
                                <tr>
                                    <th>UID</th>
                                    <th>Vendor</th>
                                    <th>Products Count</th>
                                    <th>Created by</th>
                                    <th>Created at</th>
                                    <th>Status</th>
                                    <th class="text-right">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($poReceived as $pO)
                                    @include('pages.purchase-orders.row', [
                                        'uid' => 'po-received-entry',
                                        'pO' => $pO
                                    ])
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>

        </div>

        @if ($currentUser->allowedTo('add_purchase_orders'))
            {{-- <div class="panel-footer">
                <a href="{{ route('pos.create') }}" class="btn btn-primary">Create Purchase Order</a>
            </div> --}}
        @endif

    </div>

</div>
</div>
</div>
@endsection

@section('footer-addon')
<script src="{{ asset('assets/js/vendors/tagsinput.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script>
$(function() {

    $(document).on('click', '.mail-to-vendor', function(e) {
        e.preventDefault();
        var email = $(this).data('email');

        $.get( $(this).data('url'), {email: email} ).done(function(r) {
            bootbox.dialog({
                title: r.title,
                message: r.view,
                onEscape: function () {
                    bootbox.hideAll();
                },
                buttons: {
                    cancel: {
                        label: 'Close',
                        className: 'btn-default'
                    },
                    confirm: {
                        label: 'Send',
                        className: 'btn-primary',
                        callback: function() {

                            loader.show();
                            var form = $('#email-pos-to-vendor-form');
                            $.post( form.attr('action'), form.serialize() ).done(function(r) {
                                if (r.error) {
                                    $('.form-error').html(r.error);
                                    loader.hide();
                                }
                                if (r.success) {
                                    window.location = r.redirect;
                                }
                            });
                            return false;

                        }
                    }
                }
            });
        });
    });

    $(document).on('click', '.preview-po', function(e) {
        e.preventDefault();
        loader.show();
        $.get( $(this).attr('data-url') ).done(function(r) {
            loader.hide();
            r = $.parseJSON(r);
            bootbox.dialog({
                title: r.title,
                message: r.view,
                size: 'large',
                onEscape: function () {
                    bootbox.hideAll();
                },
                buttons: {
                    cancel: {
                        label: 'Close',
                        className: 'btn-default'
                    }
                }
            });
        })
    });

    function tabInfo(table) {
        var id = $(table).closest('.tab-pane').attr('id'),
            tab = $('.nav-tabs a[aria-controls='+id+']'),
            length = $(table).DataTable().page.info().recordsDisplay,
            label = tab.find('span');
        if (label.length) { label.remove(); }
        tab.append(' <span>('+length+')</span>');
    }

    var tables = $('.datatable')
        .on('draw.dt', function () {
            tabInfo(this);
        })
        .on('search.dt', function () {
            tabInfo(this);
        })
        .DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5',
                'print'
            ],
            pageLength: 10,
            ordering: true,
            lengthChange: false,
            pagingType: 'numbers',
            'order': [ 0, 'desc' ],
            'columnDefs': [
                {
                    'targets': [6],
                    'orderable': false
                }
            ]
        });

    $('#quick-search-input').on('keyup', function () {
        tables.search( this.value ).draw();
    });

    $(document).on('click', '.delete-po', function(e) {
        e.preventDefault();
        var url = $(this).data('url');
        bootbox.dialog({
            title: 'Hey!',
            message: '<div style="font-size: 16px;">Are you sure you want to delete this purchase order?</div>',
            className: 'modal-danger',
            onEscape: function () {
                bootbox.hideAll();
            },
            buttons: {
                'confirm': {
                    label: 'Yes',
                    className: 'btn-danger',
                    callback: function() {
                        window.location = url;
                    }
                },
                'Close' : {
                    className: 'btn-default'
                }
            }
        });
    });

});
</script>
@endsection
