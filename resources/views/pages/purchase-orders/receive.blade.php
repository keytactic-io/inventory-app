@extends('layouts.app')

@section('page-title', 'Create Purchase Order')

@section('head-addon')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css">
<link rel="stylesheet" href="{{ asset('assets/css/bt-select-override.css') }}" />
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.errors')

    <form action="{{ route('pos.receive') }}/{{ $purchaseOrder->id }}" method="post" autocomplete="off" enctype="multipart/form-data" id="receive-po-form">
        {!! csrf_field() !!}
        <div class="panel">

            <div class="panel-heading">
                <h4>Receive Purchase Order</h4>
            </div>

            <div class="panel-body">


                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="po-meta">
                            <span class="label label-primary">Vendor</span>
                            <h2>{{ $purchaseOrder->vendor->name }}</h2>
                            <p>
                                {{ $purchaseOrder->vendor->getAddress() }}<br />
                                <a href="mailto:{{ $purchaseOrder->vendor->email }}">{{ $purchaseOrder->vendor->email }}</a>
                                @if ($purchaseOrder->vendor->mobile_number)
                                    <br />{{ $purchaseOrder->vendor->mobile_number }}
                                @endif
                                @if ($purchaseOrder->vendor->tel_number)
                                    <br />{{ $purchaseOrder->vendor->tel_number }}
                                @endif
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="po-meta">
                            <span class="label label-primary">Ship To</span>
                            <h2>{{ $purchaseOrder->shipTo->name }}</h2>
                            <p>
                                {{ $purchaseOrder->shipTo->exact_location }}
                            </p>
                        </div>
                    </div>
                </div>
                
                <table class="table">
                    <thead>
                        <tr>
                            <th>Ordered Product</th>
                            <th>Ordered Quantity</th>
                            <th>Received Quantity</th>
                            <th>Price</th>
                            <th>Line Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($orderedProducts as $oP)
                            <tr id="ordered-product-{{ $oP->id }}">
                                <td class="table-strong">
                                    <a href="{{ route('products.details') }}/{{ $oP->product_id }}" target="_blank">
                                        {{ $oP->product->name }}
                                    </a>
                                </td>
                                <td>{{ number_format($oP->quantity, 0, '.', ',') }}</td>
                                <td>
                                    <input type="number" name="ordered_products[{{ $oP->id }}][received_quantity]" id="" class="input-in-table product-rq" 
                                        value="{{ $oP->quantity }}" data-pid="{{ $oP->product_id }}" required step="0.0001"
                                    />
                                </td>
                                <td>
                                    <input type="number" name="ordered_products[{{ $oP->id }}][price]" id="" class="input-in-table product-p" 
                                        value="{{ $oP->price }}" data-pid="{{ $oP->product_id }}" required step="0.0001"
                                    />
                                </td>
                                <td>
                                    <span class="product-lt" data-pid="{{ $oP->product_id }}"></span>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <div class="panel-separator"><span></span></div>

                <div class="form-group">
                    <label>Attach Files</label>
                    <div class="form-control-group" style="margin-top: 0;">
                        <div class="input-group" id="file-1">
                            <input type="file" name="attachments[]" id="attachment-1" class="form-control" />
                            <span class="input-group-btn">
                                <button class="btn btn-primary btn-squared remove-file" type="button" data-target="#file-1">&times;</button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group fg-last">
                    <button type="button" class="btn btn-primary btn-sm add-file">Add File</button>
                </div>

                <div class="panel-separator"><span></span></div>

                <div class="form-group fg-last">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="notify_accounting" id="" value="1" checked />
                            <span>Notify Accounting</span>
                        </label>
                    </div>
                </div>

            </div>
            
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary">Receive Products</button>
                <a href="{{ route('pos.list') }}" class="btn btn-default">Cancel</a>
            </div>

        </div>
    </form>

</div>
</div>
</div>
@endsection

@section('footer-addon')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js"></script>
<script>
function addFileField(id)
{
    var html = '<div class="input-group" id="file-' + id + '">' +
        '<input type="file" name="attachments[]" id="attachment-' + id + '" class="form-control" />' +
        '<span class="input-group-btn">' +
        '<button class="btn btn-primary btn-squared remove-file" type="button" data-target="#file-' + id + '">&times;</button>' +
        '</span>' +
        '</div>';

    $('.form-control-group').append(html);
    $('#attachment-' + id).click();
}
$(function() {

    $('#receive-po-form').on('submit', function() {
        loader.show();
    });

    $('.add-file').on('click', function(e) {
        e.preventDefault();
        var currentCount = $('.form-control-group .input-group').length;
        addFileField((currentCount + 1));
    });

    $(document).on('click', '.remove-file', function(e) {
        e.preventDefault();
        $($(this).data('target')).remove();
    })

    $('.location-selector')
        .selectpicker({
            style: 'btn-default',
            size: 6
        })
        .on('changed.bs.select', function(e) {
            var target = e.target.attributes['data-target'].value,
                locationGroup = e.target.attributes['data-location-group'].value,
                opId = e.target.attributes['data-opid'].value,
                id = e.target.value;

            if (id != '' && id > 0) {
                var name = $(this).find('option[value="' + id + '"]').text(),
                    tbody = $(target).find('tbody'),
                    html = '<tr id="location-' + locationGroup + '-' + id + '">' +
                        '<td class="table-strong">' + name + '</td>' +
                        '<td style="width: 128px;"><input type="number" name="ordered_products[' + opId + '][' + id + ']" class="input-in-table" placeholder="0" /></td>' +
                        '<td class="text-right"><a href="#" class="remove-tr" data-target="#location-' + locationGroup + '-' + id + '"><i class="fa fa-close fa-fw"></i></a></td>' + 
                        '</tr>';

                if ($('#location-' + locationGroup + '-' + id).length == 0) {
                    if (tbody.find('.no-tr').length == 1) {
                        $(tbody).html(html);
                    } else {
                        $(html).appendTo(tbody);
                    }                    
                }
            }
        });

    $(document).on('click', '.remove-tr', function(e) {
        e.preventDefault();
        $($(this).attr('data-target')).remove();
    });

    $('.product-p').each(function() {
        var rq = $('.product-rq[data-pid="' + $(this).data('pid') + '"]'),
            lt = $('.product-lt[data-pid="' + $(this).data('pid') + '"]');

        lt.html(parseFloat(rq.val()) * parseFloat($(this).val()));
    });

    $('.product-rq, .product-p').each(function() {
        $(this).on('keyup', function() {
            var rq = $('.product-rq[data-pid="' + $(this).data('pid') + '"]'),
                pr = $('.product-p[data-pid="' + $(this).data('pid') + '"]'),
                lt = $('.product-lt[data-pid="' + $(this).data('pid') + '"]');

            if (parseFloat(rq.val()) != 0 && parseFloat(pr.val()) != 0) {
                lt.html(parseFloat(rq.val()) * parseFloat(pr.val()));
            } else {
                lt.html(0);
            }
        });
    });

});
</script>
@endsection
