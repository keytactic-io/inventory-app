<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{ $filename }}</title>
    <style>
    body {
        font-family: 'DejaVu Sans';
        line-height: 1.3;
        font-size: 8.5pt;
        font-weight: book;
    }
    .text-right { text-align: right; }
    .text-left { text-align: left; }
    .text-center { text-align: center; }
    .po-received {
        padding: 4pt 10pt 2pt;
        background: #def4da;
        color: #43653c;
        margin-bottom: 24px;
    }
    .po-header {
        margin-bottom: 20pt;
    }
        .po-header h1 {
            margin: 0;
            padding: 0;
            line-height: 1.2;
            font-size: 18pt;
            font-weight: bold;
        }
        .po-header h5 {
            margin: 0;
            padding: 0;
            line-height: 1.2;
            font-size: 9pt;
            font-weight: normal;
        }
    table {
        width: 100%;
        border-collapse: collapse;
        border-top: .5pt solid #eeeeee;
    }
    table thead tr th {
        text-align: left;
        font-size: 7pt;
        text-transform: uppercase;
        padding: 5pt 10pt;
        border-bottom: .5pt solid #eeeeee;
        background-color: #fbfbfb;
        white-space: nowrap;
    }
    table tbody tr td {
        padding: 7pt 10pt;
        vertical-align: top;
        border-bottom: .5pt solid #eeeeee;
    }
    .nothing-follows {
        padding-top: 20pt;
    }
    table.no-padding  {
        border-top: 0;
    }
    table.no-padding tbody tr td {
        padding: 0;
        background: none;
        border-bottom: 0;
    }
    table tbody tr.highlighted td {
        background: #fbfbfb;
    }

    #addresses {
        margin-top: 20pt;
    }
        #addresses h4 {
            margin: 0;
            padding: 0;
            line-height: 1.4;
            font-size: 7pt;
            text-transform: uppercase;
        }

        .address-container {
            width: 90%;
        }
            .address-container strong {
                font-size: 11pt;
            }

    .text-right {
        text-align: right;
    }

    .notes-cf {
        float: left;
    }

    #note {
        padding-top: 20pt;
        width: 60%;
    }
        #note h4 {
            margin: 0 0 5pt 0;
            padding: 0;
            line-height: 1.4;
        }

    #custom-fields {
        padding-top: 20pt;
        width: 60%;
    }

    #calculation {
        padding-left: 345pt;
        overflow: hidden;
        float: right;
    }
        #calculation table {
            width: 200pt;
            border-top: 0;
        }
    </style>
</head>
<body>

    <div class="po-header">
        <table class="no-padding">
            <tr>
                <td>
                    <?php $title = app_setting('application_name') == '' ? 'Inventory App' : app_setting('application_name'); ?>
                    @if (app_setting('logo') != '')
                        <?php
                            $imageMeta = getimagesize(asset('uploads/' . app_setting('logo')));
                            $ratio = $imageMeta[1] / $imageMeta[0];
                            $width = 40 / $ratio;
                        ?>
                        <img src="{{ asset('uploads/' . app_setting('logo')) }}" alt="{{ $title }}" height="40pt" width="{{ $width }}pt" style="margin-bottom: 7pt;" />
                    @else
                        <h1>{{ $title }}</h1>
                    @endif                    
                    <h5>{{ app_setting('address') }}</h5>
                    <h5>{{ app_setting('contact_number') }} &nbsp; | &nbsp; {{ app_setting('email')}}</h5>
                </td>
                <td>
                    <table style="margin-top: 12px; border-top: 0;">
                        <tr>
                            <td>PO #:</td>
                            <td>{{ $purchaseOrder->uid() }}</td>
                        </tr>
                        <tr>
                            <td>PO Date:</td>
                            <td>{{ processDate($purchaseOrder->created_at) }}</td>
                        </tr>
                        @if ($version == 'updated' AND $purchaseOrder->received)
                        <tr>
                            <td>Date Received:</td>
                            <td>{{ processDate($purchaseOrder->received_at) }}</td>
                        </tr>
                        <tr>
                            <td>Received by:</td>
                            <td>{{ $purchaseOrder->processedBy->name() }}</td>
                        </tr>
                        @endif
                    </table>
                </td>
            </tr>
        </table>

        <table id="addresses" class="no-padding">
            <tr>
                <td style="width: 50%;">
                    <h4>Vendor</h4>
                    <div class="address-container">
                        <strong>{{ $purchaseOrder->vendor->name }}</strong><br />
                        {{ $purchaseOrder->vendor->getAddress() }}<br />
                        M: {{ $purchaseOrder->vendor->mobile_number }}<br />
                        @if ($purchaseOrder->vendor->landline_number)
                            L: {{ $purchaseOrder->vendor->landline_number }}<br />
                        @endif
                        @if ($purchaseOrder->vendor->fax_number)
                            F: {{ $purchaseOrder->vendor->fax_number }}<br />
                        @endif
                        E: {{ $purchaseOrder->vendor->email }}<br />
                    </div>
                </td>
                <td style="width: 50%;">
                    <h4>Ship To</h4>
                    <div class="address-container">
                        <strong>{{ $purchaseOrder->shipTo->name }}</strong><br />
                        {{ $purchaseOrder->shipTo->getAddress() }}<br />
                        P: {{ $purchaseOrder->shipTo->staffIncharge->name() }}
                        @if ($purchaseOrder->shipTo->contact_number != null)
                        <br />M: {{ $purchaseOrder->shipTo->contact_number }}
                        @endif
                    </div>
                </td>
            </tr>
        </table>

    </div>

    @if ($version == 'updated' AND $purchaseOrder->received)
        {{-- <div class="po-received">
            <strong>RECEIVED</strong>
            <p>
                Received by: {{ $purchaseOrder->processedBy->name() }}<br />
                Received at: {{ processDate($purchaseOrder->received_at, true) }}
            </p>
        </div> --}}
    @endif

    <table style="margin-top: 20pt">
        <thead>
            <tr>
                <th>Delivery Date</th>
                <th>Shipping Method</th>
                <th class="text-right">Shipping Terms</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{ processDate($purchaseOrder->delivery_date )}}</td>
                <td>{{ $purchaseOrder->shipping_method }}</td>
                <td class="text-right">{{ $purchaseOrder->shipping_terms }}</td>
            </tr>
        </tbody>
    </table>

    <?php $subtotal = 0; ?>

    <table style="margin-top: 40pt">
        <thead>
            <tr>
                <th>Product Name</th>
                <th>Description</th>
                <th class="text-right">Quantity</th>
                <th class="text-right">Unit Cost</th>
                <th class="text-right">Line Total</th>
                @if ($version == 'updated' AND $purchaseOrder->received)
                    {{-- <th>Location &amp; Quantity Received</th> --}}
                @endif
            </tr>
        </thead>
        <tbody>
            @foreach ($purchaseOrder->orderedProducts as $orderedProduct)
                <tr>
                    <td><strong>{{ $orderedProduct->product->name }}</strong></td>
                    <td>{{ $orderedProduct->product->description }}</td>
                    <td class="text-right">{{ number_format($orderedProduct->quantity, '0', '.', ',') }}</td>
                    <td class="text-right">{{ number_format($orderedProduct->price, '2', '.', ',') }}</td>
                    <td class="text-right">
                        {{ number_format($orderedProduct->quantity * $orderedProduct->price, '2', '.', ',') }}
                        <?php $subtotal += ($orderedProduct->quantity * $orderedProduct->price); ?>
                    </td>
                    @if ($version == 'updated' AND $purchaseOrder->received)
                        {{-- <td>
                            @foreach ($orderedProduct->productsLocations as $productsLocation)
                                @if ($loop->iteration > 1)<br />@endif
                                {{ $productsLocation->location->name }} ({{ $productsLocation->received_quantity }})
                            @endforeach
                        </td> --}}
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>

    {{-- <p class="nothing-follows text-center">&mdash; Nothing Follows &mdash;</p> --}}

    <div class="notes-cf">
        @if ($purchaseOrder->notes != null AND $purchaseOrder->notes != '')
            <div id="note">
                <h4>Notes/Memo</h4>
                <p>{{ $purchaseOrder->notes }}</p>
            </div>
        @endif

        @if (count($purchaseOrder->customFields()) > 0)
            <div id="custom-fields">
                @foreach ($purchaseOrder->customFields() as $customField)
                    <div class="custom-field-entry">
                        <b>{{ $customField->cf->label }}:</b> {{ $customField->value }}
                    </div>
                @endforeach
            </div>
        @endif
    </div>


    <div id="calculation">
        <table>
            <tr>
                <td>Sub-total</td>
                <td class="text-right">{{ number_format($subtotal, '2', '.', ',') }}</td>
            </tr>
            <tr>
                <?php $tax = 0.00; ?>
                <td>Tax ({{ number_format($tax, '2', '.', ',') }}%)</td>
                <td class="text-right">
                    {{ number_format(($subtotal * ($tax / 100)), '2', '.', ',') }}
                </td>
            </tr>
            <tr class="highlighted">
                <td><strong>Total</strong></td>
                <td class="text-right">
                    <strong>${{ number_format($subtotal + ($subtotal * ($tax / 100)), '2', '.', ',') }}</strong>
                </td>
            </tr>
        </table>
    </div>


</body>
</html>