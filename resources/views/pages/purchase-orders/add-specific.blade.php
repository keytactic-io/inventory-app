@extends('layouts.app')

@section('page-title', 'Create Purchase Order')

@section('head-addon')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css">
<link rel="stylesheet" href="{{ asset('assets/css/bt-select-override.css') }}" />
@endsection

@section('content')
{!! csrf_field() !!}
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.errors')

</div>
<div class="col-xs-12">
    
    <form action="{{ route('pos.create.specific') }}/{{ $product->id }}" method="post" autocomplete="off">
    {!! csrf_field() !!}
    <div class="panel">
        
        <div class="panel-heading">
            <h4>Create Purchase Order</h4>
        </div>
        
        <div class="panel-body">
            
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="product-id">Product</label>
                        <input type="text" class="form-control" value="{{ $product->name }}" readonly />
                        <input type="hidden" min="0" class="form-control" id="product-id" name="product_id" value="{{ $product->id }}" />
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="vendor-id">Vendor</label>
                        <input type="text" class="form-control" value="{{ $vendor->name }}" readonly />
                        <input type="hidden" class="form-control" id="vendor-id" name="vendor_id" value="{{ $vendor->id }}" />
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="location-id">Ship-to Location</label>
                        <select name="location_id" id="location-id" class="form-control" data-live-search="true" required>
                            <option value=""></option>
                            @foreach ($locations as $location)
                                <option value="{{ $location->id }}">{{ $location->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="the-quantity">Quantity</label>
                        <input type="number" min="0" class="form-control" id="the-quantity" name="quantity" value="{{ $product->reorder_quantity }}" required />
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="the-price">Price</label>
                        <input type="number" min="0" class="form-control" id="the-price" name="price" value="{{ $product->latestPrice() }}" step="0.001" required />
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="the-total">Total Price</label>
                        <input type="number" class="form-control" id="the-total" name="total" readonly step="0.001" />
                    </div>
                </div>

                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="delivery-date">Delivery Date</label>
                        <input type="text" name="delivery_date" id="delivery-date" class="form-control datepicker" value="{{ old('delivery_date') }}" />
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="shipping-method">Shipping Method</label>
                        <input type="text" name="shipping_method" id="shipping-method" class="form-control" value="{{ old('shipping_method') }}" />
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="form-group">
                        <label for="shipping-terms">Shipping Terms</label>
                        <input type="text" name="shipping_terms" id="shipping-terms" class="form-control" value="{{ old('shipping_terms') }}" />
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="form-group fg-last">
                        <label for="notes">Note/Memo</label>
                        <textarea name="notes" id="notes" rows="2" class="form-control autosize">{{ old('notes')}}</textarea>
                    </div>
                </div>
            </div>

        </div>
        <div class="panel-footer">
            <button type="submit" class="btn btn-primary">Create</button>
            <a href="{{ route('products.details') }}/{{ $product->id }}" class="btn btn-default">Cancel</a>
        </div>

    </div>
    </form>

</div>
</div>
</div>
@endsection

@section('footer-addon')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js"></script>
<script>
$(function() {

    $('#delivery-date').datepicker({
        dateFormat: 'MM dd, yy'
    });

    $('#location-id').selectpicker({
        style: 'btn-default',
        size: 5,
        noneSelectedText: 'Please select location'
    });

    var price = $('#the-price'),
        quantity = $('#the-quantity'),
        total = $('#the-total');
    if (parseFloat(price.val()) > 0) {
        total.val(parseFloat(quantity.val()) * parseFloat(price.val()));
    }

    price.on('keyup', function() {
        if ($(this).val() > 0) {
            total.val(parseFloat(quantity.val()) * parseFloat(price.val()));
        } else {
            total.val(0);
        }
    });

    quantity.on('keyup', function() {
        if ($(this).val() > 0) {
            total.val(parseFloat(quantity.val()) * parseFloat(price.val()));
        } else {
            total.val(0);
        }
    });

})
</script>
@endsection
