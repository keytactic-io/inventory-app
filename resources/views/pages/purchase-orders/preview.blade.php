<div class="purchase-order-details modal-tab-wrap">
    
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#details" aria-controls="details" role="tab" data-toggle="tab">Details</a></li>
        <li role="presentation">
            <a href="#products" aria-controls="products" role="tab" data-toggle="tab">
                Products &nbsp; <span class="badge">{{ count($orderedProducts) }}</span>
            </a>
        </li>
        @if (count($purchaseOrder->attachments) > 0)
            <li role="presentation">
                <a href="#attachments" aria-controls="attachments" role="tab" data-toggle="tab">
                    Attachments &nbsp; <span class="badge">{{ count($purchaseOrder->attachments) }}</span>
                </a>
            </li>
        @endif
    </ul>

    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="details">

            <div class="modal-tab-details-container clearfix">

                <div class="po-label" style="display: block; padding-bottom: 20px;">
                    @if ($purchaseOrder->received)
                        <span class="label label-success">Received</span>
                    @else
                        <span class="label label-warning">Waiting for Arrival</span>
                    @endif
                </div>

                <h1>
                    <a href="#">
                        {{ $vendor->name }}
                    </a>
                </h1>
                <h5><em>Address:</em> {{ $vendor->getAddress() }}</h5>
                <h5><em>Email:</em> <a href="mailto:{{ $vendor->email }}">{{ $vendor->email }}</a></h5>
                @if ($vendor->mobile_number)
                    <h5><em>Mobile Number:</em> {{ $vendor->mobile_number }}</h5>
                @endif
                @if ($vendor->landline_number)
                    <h5><em>Landline Number:</em> {{ $vendor->landline_number }}</h5>
                @endif
                @if ($vendor->fax_number)
                    <h5><em>Fax Number:</em> {{ $vendor->fax_number }}</h5>
                @endif

            </div>

            <div class="modal-tab-details-container clearfix">
                <h5><em>Created by:</em> <a href="{{ route('users.profile') }}/{{ $purchaseOrder->processedBy->id }}">{{ $purchaseOrder->processedBy->name() }}</a></h5>
                <h5><em>Date created:</em> {{ processDate($purchaseOrder->created_at, true) }}</h5>
            </div>

            @if (count($purchaseOrder->customFields()) > 0)
                <div class="modal-tab-details-container">
                    @foreach ($purchaseOrder->customFields() as $customField)
                        <strong>{{ $customField->cf->label }}</strong>: {{ $customField->value }}<br />
                    @endforeach
                </div>
            @endif

        </div>
        <div role="tabpanel" class="tab-pane" id="products">

            <h5>Ordered Products</h5>
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Product</th>
                            <th class="text-right">Quantity</th>
                            <th class="text-right">Unit Cost</th>
                            <th class="text-right">Line Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($orderedProducts as $orderedProduct)
                            <tr>
                                <td class="table-strong">
                                    <a href="{{ route('products.details') }}/{{ $orderedProduct->product_id }}">
                                        {{ $orderedProduct->product->name }}
                                    </a>
                                </td>
                                <td class="text-right">{{ number_format($orderedProduct->quantity, '0', '.', ',') }}</td>
                                <td class="text-right">{{ number_format($orderedProduct->price, '2', '.', ',') }}</td>
                                <td class="text-right">{{ number_format($orderedProduct->quantity * $orderedProduct->price, '2', '.', ',') }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div style="display: block; margin-top: 24px;">
                @if (!$purchaseOrder->received)
                <a href="{{ route('pos.pdf') }}/{{ $purchaseOrder->id }}" title="PDF" target="_blank" class="btn btn-sm btn-danger">
                @else
                <a href="{{ route('pos.pdf') }}/{{ $purchaseOrder->id }}/updated" title="PDF" target="_blank" class="btn btn-sm btn-danger">
                @endif
                    <i class="fa fa-file-pdf-o fa-fw"></i> View PDF
                </a>
                @if (!$purchaseOrder->received AND $currentUser->allowedTo('receive_purchase_orders'))
                    <a href="{{ route('pos.receive') }}/{{ $purchaseOrder->id }}" title="Receive" class="btn btn-sm btn-primary">
                        <i class="fa fa-mail-forward fa-fw"></i> Receive Purchase Order
                    </a>
                @endif
            </div>

        </div>
        <div role="tabpanel" class="tab-pane" id="attachments">

            <div class="upload-error"></div>

            <h5>Attachments</h5>
            <div class="table-responsive">
                <table id="attachment-table" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Filename</th>
                            <th>Date Uploaded</th>
                            <th class="text-right">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($purchaseOrder->attachments as $attachment)
                            <tr id="attachment-{{ $attachment->id }}">
                                <td>
                                    <a href="{{ $attachment->getUrl() }}" target="_blank">
                                        {{ $attachment->filename }}
                                    </a>
                                </td>
                                <td>{{ processDate($attachment->created_at) }}</td>
                                <td class="text-right">
                                    <a href="{{ route('pos.deletefile') }}/{{ $attachment->id }}" class="remove-file">
                                        <i class="fa fa-trash-o fa-fw"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="panel-separator"><span></span></div>

            <div style="display: block;">
                <form action="{{ route('pos.addfile') }}/{{ $purchaseOrder->id }}" method="post" autocomplete="off" enctype="multipart/form-data" id="add-file-form">
                    {!! csrf_field() !!}
                    <div class="input-group">
                        <input type="file" name="attachment" id="attachment" class="form-control" />
                        <span class="input-group-btn">
                            <button class="btn btn-primary" type="button" id="add-file">Add File</button>
                        </span>
                    </div>
                </form>
            </div>

        </div>
    </div>

</div>
<script>
$(function() {

    $('#add-file').on('click', function(e) {
        loader.show();
        e.preventDefault();
        var form = $('#add-file-form'),
            formData = new FormData(form[0]);

        formData.append('image', form.find('input[type=file]')[0].files[0]);
        $.ajax({
            url: form.attr('action'),
            data: formData,
            type: 'post',
            contentType: false,
            processData: false,
        }).done(function(r) {

            if (r.error) {
                $('.upload-error').html(r.error);
            }

            if (r.success) {
                var newRow = '<tr id="attachment-' + r.file.id + '"><td><a href="' + r.file.fileUrl + '" target="_blank">' + r.file.filename + '</a></td><td>' + r.file.date + '</td><td class="text-right"><a href="' + r.file.deleteUrl + '" class="remove-file"><i class="fa fa-trash-o fa-fw"></i></a></td></tr>';
                $('#attachment-table tbody').append(newRow);
                $('#attachment').val('');
            }

            loader.hide();
        });
    });

    $(document).on('click', '.remove-file', function(e) {
        e.preventDefault();
        var url = $(this).attr('href');
        $.get( url ).done(function(r) {
            if (r.success) {
                $('#attachment-' + r.id).remove();
            }
        });
    });

});
</script>