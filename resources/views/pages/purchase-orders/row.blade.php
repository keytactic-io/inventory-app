<tr id="{{ $uid }}-{{ $pO->id }}">
    <td class="table-strong">
        <a href="#" class="preview-po" data-url="{{ route('pos.preview') }}/{{ $pO->id }}">
            {{ $pO->uid() }}
        </a>
    </td>
    <td>
        <a href="{{ route('vendors.details') }}/{{ $pO->vendor_id }}">
            {{ $pO->vendor->name }}
        </a>
    </td>
    <td>
        {{ $pO->productCount() }}
    </td>
    <td>
        <a href="{{ route('users.profile') }}/{{ $pO->processedBy->id }}">
            {{ $pO->processedBy->name() }}
        </a>
    </td>
    <td>
        {{ processDate($pO->created_at, true) }}
    </td>
    <td>
        @if ($pO->received)
            <span class="label label-success">Received</span>
        @else
            <span class="label label-warning">Waiting for Arrival</span>
        @endif
    </td>
    <td class="text-right">
        @if (!$pO->received AND $currentUser->allowedTo('add_purchase_orders'))
            <a href="#" data-url="{{ route('pos.emailtovendor') }}/{{ $pO->id }}" title="Mail to" 
                class="mail-to-vendor" data-email="{{ $pO->vendor->email }}"
            >
                <i class="fa fa-send fa-fw"></i>
            </a>
        @endif
        @if (!$pO->received)
        <a href="{{ route('pos.pdf') }}/{{ $pO->id }}" title="PDF" target="_blank">
        @else
        <a href="{{ route('pos.pdf') }}/{{ $pO->id }}/updated" title="PDF" target="_blank">
        @endif
            <i class="fa fa-file-pdf-o fa-fw"></i>
        </a>

        @if (!$pO->received AND $currentUser->allowedTo('receive_purchase_orders'))
            <a href="{{ route('pos.receive') }}/{{ $pO->id }}" title="Receive">
                <i class="fa fa-mail-forward fa-fw"></i>
            </a>
        @endif
        @if (app_setting('allow_update_po') == 1)
            @if (!$pO->received AND $currentUser->allowedTo('edit_purchase_orders'))
                <a href="{{ route('pos.edit') }}/{{ $pO->id }}" titke="Edit">
                    <i class="fa fa-pencil fa-fw"></i>
                </a>
            @endif
        @endif
        @if (!$pO->received AND $currentUser->allowedTo('delete_purchase_orders'))
            <a href="#" data-url="{{ route('pos.delete') }}/{{ $pO->id }}" 
                title="Delete" class="delete-po" 
            >
                <i class="fa fa-trash-o fa-fw"></i>
            </a>
        @endif
    </td>
</tr>