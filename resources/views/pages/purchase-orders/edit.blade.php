@extends('layouts.app')

@section('page-title', 'Edit Purchase Order')

@section('head-addon')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css">
<link rel="stylesheet" href="{{ asset('assets/css/bt-select-override.css') }}" />
@endsection

@section('content')
<form action="{{ route('pos.edit') }}/{{ $purchaseOrder->id }}" method="post" autocomplete="off" class="with-preloader">
{!! csrf_field() !!}
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.errors')

</div>
@if (count($vendors) > 0)
<div class="col-xs-12 col-md-4">

    <div class="panel">
        <div class="panel-heading">
            <h4>Edit Purchase Order</h4>
        </div>

        <div class="panel-body">
            
            <?php $oldVendor = old('vendor_id') ? old('vendor_id') : ''; ?>
            <div class="form-group">
                <label for="vendor-id">Vendor</label>
                <div class="row">
                    <div class="col-xs-12">
                        <select name="vendor_id" id="vendor-id" class="form-control"
                            data-live-search="true" required
                        >
                            <option value=""></option>
                            @foreach ($vendors as $vendor)
                                <option value="{{ $vendor->id }}"
                                    @if ($purchaseOrder->vendor_id == $vendor->id) selected @endif
                                    @if ($oldVendor == $vendor->id) selected @endif
                                >
                                    {{ $vendor->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            @if ($currentUser->allowedTo('add_vendors'))
                <div class="form-group">
                    <a href="#" id="add-vendor" class="btn btn-sm btn-primary" data-url="{{ route('vendors.add.modal') }}">
                        Add Vendor
                    </a>
                </div>
            @endif

            <?php $oldLocation = old('location_id') ? old('location_id') : ''; ?>
            <div class="form-group">
                <label for="location-id">Ship-to Address</label>
                <div class="row">
                    <div class="col-xs-12">
                        <select name="location_id" id="location-id" class="form-control"
                            data-live-search="true" required
                        >
                            <option value=""></option>
                            @foreach ($locations as $location)
                                <option value="{{ $location->id }}"
                                    @if ($purchaseOrder->ship_to_location == $location->id) selected @endif
                                    @if ($oldVendor == $location->id) selected @endif
                                >
                                    {{ $location->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group-divider"><span></span></div>

            <div class="form-group">
                <label for="delivery-date">Delivery Date</label>
                <input type="text" name="delivery_date" id="delivery-date" class="form-control datepicker" 
                    value="{{ old('delivery_date', processDate($purchaseOrder->delivery_date)) }}" required
                />
            </div>

            <div class="form-group">
                <label for="shipping-method">Shipping Method</label>
                <input type="text" name="shipping_method" id="shipping-method" class="form-control" 
                    value="{{ old('shipping_method', $purchaseOrder->shipping_method) }}" required
                />
            </div>

            <div class="form-group">
                <label for="shipping-terms">Shipping Terms</label>
                <input type="text" name="shipping_terms" id="shipping-terms" class="form-control" 
                    value="{{ old('shipping_terms', $purchaseOrder->shipping_terms) }}" required
                />
            </div>

            <div class="form-group fg-last">
                <label for="notes">Notes</label>
                <textarea name="notes" id="notes" rows="2" class="form-control autosize">{{ old('notes', $purchaseOrder->notes)}}</textarea>
            </div>

            @if (count($customFields) > 0)
            <div class="form-group-divider"><span></span></div>

                @foreach ($customFields as $cf)
                    <div class="form-group @if ($loop->last) fg-last @endif">
                        <label for="cf-{{ $cf->slug }}">{{ $cf->label }}</label>
                        @if ($cf->type == 'input')
                            <input type="text" class="form-control" name="custom_fields[{{ $cf->id }}]" id="cf-{{ $cf->slug }}" 
                                value="{{ $purchaseOrder->customFieldValue($cf->id) }}"
                            />
                        @elseif ($cf->type == 'textarea')
                            <textarea class="form-control autosize" name="custom_fields[{{ $cf->id }}]" id="cf-{{ $cf->slug }}" rows="2">{{ $purchaseOrder->customFieldValue($cf->id) }}</textarea>
                        @endif
                    </div>
                @endforeach

            @endif

        </div>

    </div>

</div>
<div class="col-xs-12 col-md-8">
    
    <div class="panel">

        <div class="panel-heading">
            <h4>Products</h4>
        </div>

        <div class="panel-preloader" data-target="#products-panel-body">
            <i class="fa fa-circle-o-notch fa-spin fa-fw"></i>
        </div>
        <div id="products-panel-body" class="panel-body hidden" style="display:none;">

            <div class="form-group">
                <label for="products-selection">Select Product</label>
                <select id="products-selection" class="form-control"
                    data-live-search="true" @if (count($purchaseOrder->orderedProducts) == 0) required @endif
                >
                    <option value=""></option>
                    @foreach ($products as $product)
                        <option value="{{ $product->id }}"
                            data-rq="{{ $product->reorder_quantity }}"
                            data-price="{{ $product->latestPrice() }}"
                        >
                            {{ $product->name }}
                        </option>
                    @endforeach
                </select>
            </div>

            @if ($currentUser->allowedTo('add_products'))
                <div class="form-group">
                    <a href="#" id="add-product" class="btn btn-sm btn-primary" data-url="{{ route('products.add.modal') }}">
                        Add New Product
                    </a>
                </div>
            @endif

            <div class="table-responsive">
                <table id="products-table" class="table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th style="width:124px;">Quantity</th>
                            <th>Unit Cost</th>
                            <th>Line Total</th>
                            <th style="width:60px;" class="text-right">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($purchaseOrder->orderedProducts as $orderedProduct)
                            <tr id="product-tr-{{ $orderedProduct->product->id }}">
                                <td class="table-strong">
                                    {{ $orderedProduct->product->name }}
                                </td>
                                <td>
                                    <input type="number" name="products[{{ $orderedProduct->product->id }}][quantity]" class="input-in-table the-quantity" 
                                        data-id="{{ $orderedProduct->product->id }}" id="the-quantity-{{ $orderedProduct->product->id }}" placeholder="0" min="0" required value="{{ $orderedProduct->quantity }}"
                                    />
                                </td>
                                <td>
                                    <input type="number" name="products[{{ $orderedProduct->product->id }}][price]" class="input-in-table the-price" 
                                        data-id="{{ $orderedProduct->product->id }}" id="the-price-{{ $orderedProduct->product->id }}" placeholder="0" step="0.0001" min="0" required value="{{ $orderedProduct->price }}"
                                    />
                                </td>
                                <td><span class="the-total" id="the-total-{{ $orderedProduct->product->id }}">0</span></td>
                                <td class="text-right">
                                    <a href="#" class="remove-tr" data-target="#product-tr-{{ $orderedProduct->product->id }}">
                                        <i class="fa fa-close fa-fw"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr class="no-tr">
                                <td colspan="5" class="text-center">Please select at least one (1) product</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>

        </div>
        <div class="panel-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a href="{{ route('pos.list') }}" class="btn btn-default">Cancel</a>
        </div>

    </div>

</div>
@else
    <div class="alert alert-warning text-center">
        You dont have a vendors yet.
        <a href="{{ route('vendors.add') }}">
            Click here to add your first vendor.
        </a>
    </div>
@endif
</div>
</div>
</form>
@endsection

@section('footer-addon')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js"></script>
<script>
function addProduct(e)
{
    var id = e.target.value,
        rq = 0,
        price = 0;

    $.each(e.target, function(key, value) {
        if (value.attributes['value'].value == id) {
            rq = value.attributes['data-rq'].value;
            price = value.attributes['data-price'].value;
        }
    });
    if (id != '' && id > 0) {
        var name = $('#' + e.target.attributes['id'].value).find('option[value="' + id + '"]').text(),
            tbody = $('#products-table').find('tbody'),
            html = '<tr id="product-tr-' + id + '">' +
                    '<td class="table-strong">' + name + '</td>' +
                    '<td><input type="number" name="products[' + id + '][quantity]" class="input-in-table the-quantity" data-id="' + id + '" id="the-quantity-' + id + '" placeholder="0" min="0" required /></td>' +
                    '<td><input type="number" name="products[' + id + '][price]" class="input-in-table the-price" data-id="' + id + '" id="the-price-' + id + '" placeholder="0" step="0.0001" min="0" required /></td>' +
                    '<td><span class="the-total" id="the-total-' + id + '">0</span></td>' +
                    '<td class="text-right"><a href="#" class="remove-tr" data-target="#product-tr-' + id + '"><i class="fa fa-close fa-fw"></i></a></td>' +
                    '</tr>';
        if ($('#product-tr-' + id).length == 0) {
            if (tbody.find('.no-tr').length == 1) {
                $(tbody).html(html);
            } else {
                $(html).appendTo(tbody);                
            }
            $('#product-tr-' + id).find('input.the-quantity').val(rq);
            $('#product-tr-' + id).find('input.the-price').val(price);
            $('#product-tr-' + id).find('span.the-total').html(parseFloat(price) * parseFloat(rq));
        }

        $('.the-price, .the-quantity').each(function() {
            $(this).on('keyup', function() {
                var id = $(this).data('id'),
                    thePrice = $('#the-price-' + id),
                    theQuantity = $('#the-quantity-' + id),
                    theTotal = $('#the-total-' + id);

                if (parseFloat(thePrice.val()) != 0 && parseFloat(theQuantity.val()) != 0) {
                    theTotal.html(parseFloat(thePrice.val()) * parseFloat(theQuantity.val()));
                } else {
                    theTotal.html('0');
                }
            });
        });
    }
}
$(function() {

    $('.the-quantity').each(function() {
        var id = $(this).data('id'),
            thePrice = $('#the-price-' + id),
            theQuantity = $('#the-quantity-' + id),
            theTotal = $('#the-total-' + id);

        if (parseFloat(thePrice.val()) != 0 && parseFloat(theQuantity.val()) != 0) {
            theTotal.html(parseFloat(thePrice.val()) * parseFloat(theQuantity.val()));
        } else {
            theTotal.html('0');
        }
    });

    $('#create-po-form').on('submit', function() {
            loader.show();
        });

        $('#delivery-date').datepicker({
            dateFormat: 'MM dd, yy'
        });

        var tokenData = {
            autosize: true, 
            displayDropdownOnFocus: false, 
            newElements: false,
            nbDropdownElements: 5,
            maxElements: 1
        };

        $('#vendor-id').selectpicker({
            style: 'btn-default',
            size: 5
        });

        $('#location-id').selectpicker({
            style: 'btn-default',
            size: 5
        });

        $('#products-selection')
            .selectpicker({
                style: 'btn-primary',
                size: 6
            })
            .on('changed.bs.select', function(e) {
                addProduct(e);
            })
            .on('refreshed.bs.select', function(e) {
                addProduct(e);
            });

        $(document).on('click', '.remove-tr', function(e) {
            e.preventDefault();
            if ($('#products-table tbody tr').length == 1) {
                $('#products-table tbody').html('<tr class="no-tr"><td colspan="5" class="text-center">Please select at least one (1) product</td></tr>');
            } else {
                $($(this).attr('data-target')).remove();
            }        
        });

        $('#add-vendor').on('click', function(e) {
            e.preventDefault();
            $.get( $(this).attr('data-url') ).done(function(r) {
                r = $.parseJSON(r);
                bootbox.dialog({
                    title: r.title,
                    message: r.view,
                    size: 'large',
                    onEscape: function () {
                        bootbox.hideAll();
                    },
                    buttons: {
                        confirm: {
                            label: 'Add',
                            className: 'btn-primary',
                            callback: function() {

                                loader.show();
                                var form = $('#add-vendor-form');
                                $.post( form.attr('action'), form.serialize() ).done(function(r) {
                                    if (r.error) {
                                        $('.form-error').html(r.error);
                                    }
                                    if (r.done) {
                                        var html = '<option value="' + r.vendor.id + '" selected>' + r.vendor.name + '</option>';
                                        $('#vendor-id').append(html);
                                        $('#vendor-id').selectpicker('refresh');
                                        bootbox.hideAll();
                                        loader.hide();
                                    }
                                });

                                return false;

                            }
                        },
                        cancel: {
                            label: 'Close',
                            className: 'btn-default'
                        }
                    }
                });
            });
        });

        $('#add-product').on('click', function(e) {
            e.preventDefault();
            $.get( $(this).attr('data-url') ).done(function(r) {
                r = $.parseJSON(r);
                bootbox.dialog({
                    title: r.title,
                    message: r.view,
                    size: 'large',
                    onEscape: function () {
                        bootbox.hideAll();
                    },
                    buttons: {
                        confirm: {
                            label: 'Add',
                            className: 'btn-primary',
                            callback: function() {

                                loader.show();
                                var form = $('#add-product-form');
                                $.post( form.attr('action'), form.serialize() ).done(function(r) {
                                    if (r.error) {
                                        $('.form-error').html(r.error);
                                    }
                                    if (r.done) {
                                        var html = '<option value="' + r.product.id + '" data-rq="' + r.product.rq + '" data-price="' + r.product.price + '" selected>' + r.product.name + '</option>';
                                        $('#products-selection').append(html);
                                        $('#products-selection').selectpicker('refresh');
                                        bootbox.hideAll();
                                        loader.hide();
                                    }
                                });

                                return false;

                            }
                        },
                        cancel: {
                            label: 'Close',
                            className: 'btn-default'
                        }
                    }
                });
            });
        });

});
</script>
@endsection
