@extends('layouts.app')

@section('page-title', 'App Settings')

@section('head-addon')
<link href="{{ asset('assets/css/vendors/tagsinput.css') }}" rel="stylesheet" />
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.status')
    @include('includes.errors')

    <form action="{{ route('settings.save') }}" method="post" autocomplete="off" enctype="multipart/form-data">
    {!! csrf_field() !!}
    <div class="panel">

        <div class="panel-heading">
            <h4>App Settings</h4>
        </div>

        <div class="panel-body">
            
            <div class="form-horizontal">

                <div class="form-group">
                    <label for="application-name" class="col-xs-12 col-sm-3 control-label">Application Name</label>
                    <div class="col-xs-12 col-sm-6">
                        <input type="text" name="application_name" id="application-name" class="form-control" value="{{ app_setting('application_name') }}" required />
                    </div>
                </div>

                <div class="form-group">
                    <label for="application-logo" class="col-xs-12 col-sm-3 control-label">Logo</label>
                    <div class="col-xs-12 col-sm-6">
                        @if (app_setting('logo') != '')
                            <div class="logo-preview">
                                <img src="{{ asset('uploads/' . app_setting('logo')) }}" alt="" />
                                <br clear="all" />
                                <a href="{{ route('settings.removelogo') }}" class="btn btn-xs btn-danger">Remove</a>
                            </div>
                        @endif
                        <input type="file" name="logo" id="application-logo" class="form-control" />
                        <span class="text-muted" style="display:block; padding-top: 8px;">
                            <small>* jpeg, jpg, png &mdash; max of 1,000kb</small>
                        </span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="application-address" class="col-xs-12 col-sm-3 control-label">Address</label>
                    <div class="col-xs-12 col-sm-6">
                        <input type="text" name="address" id="application-address" class="form-control" value="{{ app_setting('address') }}" required />
                    </div>
                </div>

                <div class="form-group">
                    <label for="application-email" class="col-xs-12 col-sm-3 control-label">Email</label>
                    <div class="col-xs-12 col-sm-6">
                        <input type="email" name="email" id="application-email" class="form-control" value="{{ app_setting('email') }}" required />
                    </div>
                </div>

                <div class="form-group">
                    <label for="application-contact-number" class="col-xs-12 col-sm-3 control-label">Contact Number</label>
                    <div class="col-xs-12 col-sm-6">
                        <input type="text" name="contact_number" id="application-contact-number" class="form-control" value="{{ app_setting('contact_number') }}" required />
                    </div>
                </div>

                <div class="form-group fg-last">
                    <label for="accounting-email" class="col-xs-12 col-sm-3 control-label">Accounting Email</label>
                    <div class="col-xs-12 col-sm-6">
                        <input type="text" name="accounting_email" id="accounting-email" class="form-control" value="{{ app_setting('accounting_email') }}" />
                    </div>
                </div>

                <div class="panel-separator"><span></span></div>

                <div class="form-group fg-last">
                    <label for="items-per-page" class="col-xs-12 col-sm-3 control-label">Items Per Page</label>
                    <div class="col-xs-12 col-sm-6">
                        <input type="text" name="items_per_page" id="items-per-page" class="form-control" value="{{ app_setting('items_per_page') }}" />
                    </div>
                </div>

                <div class="panel-separator"><span></span></div>

                <div class="form-group">
                    <label for="" class="col-xs-12 col-sm-3 control-label">Allow Update PO</label>
                    <div class="col-xs-12 col-sm-6">
                        <?php $allowUpdate = app_setting('allow_update_po'); ?>
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-primary  @if ($allowUpdate == 1) active @endif">
                                <input type="radio" name="allow_update_po" id="yes" autocomplete="off" value="1" @if ($allowUpdate == 1) checked @endif />Yes
                            </label>
                            <label class="btn btn-primary @if ($allowUpdate == 0) active @endif ">
                                <input type="radio" name="allow_update_po" id="no" autocomplete="off" value="0" @if ($allowUpdate == 0) checked @endif />No
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group fg-last">
                    <label for="" class="col-xs-12 col-sm-3 control-label">Show locations with 0 quantity</label>
                    <div class="col-xs-12 col-sm-6">
                        <?php $showZeros = app_setting('show_zero_locations'); ?>
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-primary @if ($showZeros == 1) active @endif ">
                                <input type="radio" name="show_zero_locations" id="yes" autocomplete="off" value="1" @if ($showZeros == 1) checked @endif />Yes
                            </label>
                            <label class="btn btn-primary @if ($showZeros == 0) active @endif">
                                <input type="radio" name="show_zero_locations" id="no" autocomplete="off" value="0" @if ($showZeros == 0) checked @endif />No
                            </label>
                        </div>
                    </div>
                </div>

                <div class="panel-separator"><span></span></div>

                <div class="form-group fg-last">
                    <label for="shippo-api-key" class="col-xs-12 col-sm-3 control-label">Shippo API Key</label>
                    <div class="col-xs-12 col-sm-6">
                        <input type="text" name="shippo_private" id="shippo-api-key" class="form-control" value="{{ env('SHIPPO_PRIVATE') }}" />
                    </div>
                </div>
                
            </div>

        </div>
        <div class="panel-footer">
            <button type="submit" class="btn btn-primary">Save Changes</button>
        </div>
        
    </div>
    </form>

</div>
</div>
</div>
@endsection

@section('footer-addon')
<script src="{{ asset('assets/js/vendors/tagsinput.js') }}"></script>
<script>
$(function() {

    $('#accounting-email').tagsInput({
        height: 'auto',
        width: 'auto',
        defaultText: 'add email',
    });

});
</script>
@endsection
