@extends('layouts.app')

@section('page-title', 'Env Values')

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.status')
    @include('includes.errors')

    <form action="{{ route('settings.env') }}" method="post" autocomplete="off">
    {!! csrf_field() !!}
    <div class="panel">

        <div class="panel-heading">
            <h4>Env Values</h4>
        </div>

        <div class="panel-body">
            <div class="table-responsive">
            
                <table class="table table-bordered table-env">
                    <thead>
                        <tr>
                            <th width="200px">Key</th>
                            <th>Value</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($envValues as $key => $value)
                            <tr id="env-{{ strtolower($key) }}">
                                <td><label for="{{ strtolower($key) }}">{{ $key }}</label></td>
                                <td>
                                    <input type="text" class="input-in-table iit-100" 
                                        name="{{ strtolower($key) }}" 
                                        id="{{ strtolower($key) }}" 
                                        value="{{ $value }}" 
                                        required 
                                    />
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            <button type="submit" class="btn btn-primary">Save Changes</button>
        </div>
        
    </div>
    </form>

</div>
</div>
</div>
@endsection

@section('footer-addon')
<script>
$(function() {

    

});
</script>
@endsection
