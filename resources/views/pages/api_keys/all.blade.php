@extends('layouts.app')

@section('page-title', 'API Keys')

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.status')

    <div class="panel">

        <div class="panel-heading">
            <h4>API Keys</h4>
        </div>

        <div class="panel-body" style="padding-bottom: 0;">
            @if ($currentUser->role->name == 'Super Admin' || $currentUser->role->name == 'Admin')
                <form action="{{ route('api.keys.generate') }}" autocomplete="off" method="post">
                    {!! csrf_field() !!}
                    <button type="submit" class="btn btn-primary btn-sm">Generate Key</button>
                </form>
            @endif
        </div>

        <div class="panel-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>API Key</th>
                            <th class="text-right">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($apiKeys as $apiKey)
                            <tr>
                                <td><code>{{ $apiKey->api_key }}</code></td>
                                <td class="text-right">
                                    <a href="#">
                                        <i class="fa fa-trash-o fa-fw"></i>
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="2" class="text-center active">No API Keys yet</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>

    </div>

</div>
</div>
</div>
@endsection
