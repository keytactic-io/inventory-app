<div class="manual-shipping-form-error"></div>
<form method="post" autocomplete="off"
    action="{{ route('shipping.manual') }}/{{ $requestProduct->id }}"
    id="manual-shipping-form"
>
{{ csrf_field() }}

    <div class="row">

        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
                <label for="courier">Courier</label>
                <input type="text" name="courier" id="courier" class="form-control" required />
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
                <label for="rate">Rate</label>
                <input type="number" step="0.0001" min="0" name="rate" id="rate" class="form-control" required />
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-group">
                <label for="tracking-number">Tracking Number</label>
                <input type="text" name="tracking_number" id="tracking-number" class="form-control" required />
            </div>
        </div>

        <div class="col-xs-12">
            <div class="form-group fg-last">
                <button type="submit" class="btn btn-primary btn-sm ship-now">Submit</button>
                <a href="#" class="btn btn-default btn-sm ship-cancel">Cancel</a>
            </div>
        </div>

    </div>

</form>
<script>
$(function() {

    $(document).on('click', '.ship-now', function(e) {
        e.preventDefault();
        loader.show();
        var form = $('#manual-shipping-form');
        $.post( form.attr('action'), form.serialize() ).done(function(r) {
            if (r.error) {
                $('.manual-shipping-form-error').html(r.error);
                loader.hide();
            }
            if (r.success) {
                window.location = r.redirect;
            }
        });
    });

    $(document).on('click', '.ship-cancel', function(e) {
        e.preventDefault();
        // clear content
        $('#manual-shipping')
            .css({ 'paddingTop': '0' })
            .html('');
    });

});
</script>