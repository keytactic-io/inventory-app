@extends('layouts.app')

@section('page-title', 'Shipping Rates Test')

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.status')
    @include('includes.errors')

    <div class="panel">

        <div class="panel-heading">
            <h4>Shipping Rates List</h4>
        </div>

        <div id="panel-rates" class="panel-body">
            
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Courier</th>
                            <th>Terms</th>
                            <th>Service Level Name</th>
                            <th>Duration Terms</th>
                            <th>Amount</th>
                            <th class="text-right">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($rates != null AND count($rates) > 0)
                            @foreach ($rates as $rate)
                                <tr>
                                    <td>
                                        <img src="{{ $rate->provider_image_200 }}" alt="" style="height: 18px;" />
                                    </td>
                                    <td>{{ $rate->provider }}</td>
                                    <td>{{ $rate->servicelevel->name }}</td>
                                    <td>
                                        {{ $rate->duration_terms }}&nbsp;
                                        @if (empty($rate->days))
                                            (Days not indicated)
                                        @else
                                            ({{ $rate->days }} {{ $rate->days == 1 ? 'day' : 'days' }})
                                        @endif
                                    </td>
                                    <td>${{ $rate->amount }}</td>
                                    <td class="text-right">
                                        <a href="#" class="btn btn-primary btn-xs create-label"
                                            @if ($requestProduct)
                                                data-url="{{ route('shipping.create-label', ['objectId' => $rate->object_id]) }}/{{ $requestProduct->id }}"
                                            @else
                                                data-url="{{ route('shipping.create-label', ['objectId' => $rate->object_id]) }}"
                                            @endif
                                            data-service-level-name="{{ $rate->servicelevel->name }}"
                                            @if (empty($rate->days))
                                                data-duration-terms="{{ $rate->duration_terms }} (Days not indicated)"
                                            @else
                                                data-duration-terms="{{ $rate->duration_terms }} ({{ $rate->days }} {{ $rate->days == 1 ? 'day' : 'days' }})"
                                            @endif                                            
                                            data-amount="{{ $rate->amount }}"
                                            data-courier="{{ $rate->provider }}"
                                        >
                                            Create Label
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="4" class="text-center">Sorry, no rates found.</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>

        </div>
        
    </div>

</div>
</div>
</div>
@endsection

@section('footer-addon')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script>
$(function() {

    var redirectFromLabel = '';

    $(document).on('click', '.create-label', function(e) {
        e.preventDefault();
        loader.show();
        var data = {
            rate: $(this).data('amount'),
            courier: $(this).data('courier'),
            service_level_name: $(this).data('service-level-name'),
            duration_terms: $(this).data('duration-terms')
        };
        $.post( $(this).data('url'), data ).done(function(r) {
            if (r.error) {
                if ($('#panel-rates .alert').length == 0)  {
                    $('#panel-rates').prepend('<div class="alert alert-danger">' + r.error + '</div>');
                } else {
                    $('#panel-rates .alert').html(r.error);
                }
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                loader.hide();
            }
            if (r.success) {
                loader.hide();
                bootbox.dialog({
                    title: 'Successfully created a label!',
                    message: '<div class="text-center" style="padding: 24px 0;"><a href="' + r.labelUrl + '" class="btn btn-primary btn-lg dl-label" target="_blank">Download Label</a></div>',
                    onEscape: function () {
                        bootbox.hideAll();
                    },
                    buttons: {
                        'Close' : {
                            className: 'btn-default'
                        }
                    }
                });
                redirectFromLabel = r.redirect;
            }
        });
    });

    $(document).on('click', '.dl-label', function(e) {
        window.location = redirectFromLabel;
    });

});
</script>
@endsection
