@extends('layouts.app')

@section('page-title', 'Ship Request')

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.status')
    @include('includes.errors')

</div>
<form action="{{ route('requesters.shipping-rates') }}" autocomplete="off" method="post" id="find-rates-form">
{!! csrf_field() !!}
<?php
    if ($requestProduct) {
        $rP = $requestProduct->requestedProducts()->first();
    }
?>
<input type="hidden" name="request_id" value="@if ($requestProduct) {{ $requestProduct->id }} @endif">

<div class="col-xs-12">

    <div class="panel">

        <div class="panel-heading">
            <h4>Ship-from Address</h4>
        </div>

        <div class="panel-body">

            <div class="row">

                <input type="hidden" name="address[from][object_purpose]" value="PURCHASE" />

                <div class="col-xs-12">
                    <div class="form-group">
                        <label for="ship-from-name">Name</label>
                        <input type="text" name="address[from][name]" id="ship-from-name" class="form-control" placeholder="Attention:" required 
                            @if ($requestProduct)
                                value="{{ $rP->location->staffIncharge->name() }}"
                            @endif
                        />
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="form-group">
                        <label for="ship-from-company">Company</label>
                        <input type="text" name="address[from][company]" id="ship-from-company" class="form-control" placeholder="Company" />
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="form-group">
                        <label for="ship-from-phone">Phone Number</label>
                        <input type="text" name="address[from][phone]" id="ship-from-phone" class="form-control" placeholder="Phone" 
                            @if ($requestProduct)
                                value="{{ $rP->location->contact_number }}"
                            @endif
                        />
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="form-group">
                        <label for="ship-from-email">Email</label>
                        <input type="text" name="address[from][email]" id="ship-from-email" class="form-control" placeholder="Email" required 
                            @if ($requestProduct)
                                value="{{ $rP->location->staffIncharge->email }}"
                            @endif
                        />
                    </div>
                </div>

                <div class="col-xs-12 col-md-6">
                    <div class="form-group">
                        <label for="ship-from-street-1">Street 1</label>
                        <input type="text" name="address[from][street1]" id="ship-from-street-1" class="form-control" placeholder="Street 1" required 
                            @if ($requestProduct)
                                value="{{ $rP->location->street_1 }}"
                            @endif
                        />
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="form-group">
                        <label for="ship-from-street-2">Street 2</label>
                        <input type="text" name="address[from][street2]" id="ship-from-street-2" class="form-control" placeholder="Street 2" 
                            @if ($requestProduct)
                                value="{{ $rP->location->street_2 }}"
                            @endif
                        />
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="form-group">
                        <label for="ship-from-city">City</label>
                        <input type="text" name="address[from][city]" id="ship-from-city" class="form-control" placeholder="City" required 
                            @if ($requestProduct)
                                value="{{ $rP->location->city }}"
                            @endif
                        />
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="form-group">
                        <label for="ship-from-state">State</label>
                        <input type="text" name="address[from][state]" id="ship-from-state" class="form-control" placeholder="State" required 
                            @if ($requestProduct)
                                value="{{ $rP->location->state }}"
                            @endif
                        />
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="form-group">
                        <label for="ship-from-country">Country</label>
                        <input type="text" name="address[from][country]" id="ship-from-country" class="form-control" placeholder="Country" required 
                            @if ($requestProduct)
                                value="{{ $rP->location->country }}"
                            @endif
                        />
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-2">
                    <div class="form-group">
                        <label for="ship-from-postcode">Postcode</label>
                        <input type="text" name="address[from][zip]" id="ship-from-postcode" class="form-control" placeholder="Postcode" required 
                            @if ($requestProduct)
                                value="{{ $rP->location->postcode }}"
                            @endif
                        />
                    </div>
                </div>

            </div>

        </div>

    </div>

    <div class="panel">

        <div class="panel-heading">
            <h4>Ship-to Address</h4>
        </div>

        <div class="panel-body">

            <div class="row">

                <input type="hidden" name="address[to][object_purpose]" value="PURCHASE" />

                <div class="col-xs-12">
                    <div class="form-group">
                        <label for="ship-to-name">Name</label>
                        <input type="text" name="address[to][name]" id="ship-to-name" class="form-control" placeholder="Attention:" required 
                            @if ($requestProduct)
                                value="{{ $requestProduct->attention }}"
                            @endif
                        />
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="form-group">
                        <label for="ship-to-company">Company</label>
                        <input type="text" name="address[to][company]" id="ship-to-company" class="form-control" placeholder="Company" 
                            @if ($requestProduct)
                                value="{{ $requestProduct->company }}"
                            @endif
                        />
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="form-group">
                        <label for="ship-to-phone">Phone Number</label>
                        <input type="text" name="address[to][phone]" id="ship-to-phone" class="form-control" placeholder="Phone" 
                            @if ($requestProduct)
                                value="{{ $requestProduct->contact_number }}"
                            @endif
                        />
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="form-group">
                        <label for="ship-to-email">Email</label>
                        <input type="text" name="address[to][email]" id="ship-to-email" class="form-control" placeholder="Email" required 
                            @if ($requestProduct)
                                value="{{ $requestProduct->theRequester->email }}"
                            @endif
                        />
                    </div>
                </div>

                <div class="col-xs-12 col-md-6">
                    <div class="form-group">
                        <label for="ship-to-street-1">Street 1</label>
                        <input type="text" name="address[to][street1]" id="ship-to-street-1" class="form-control" placeholder="Street 1" required 
                            @if ($requestProduct)
                                value="{{ $requestProduct->street_1 }}"
                            @endif
                        />
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="form-group">
                        <label for="ship-to-street-2">Street 2</label>
                        <input type="text" name="address[to][street2]" id="ship-to-street-2" class="form-control" placeholder="Street 2"  
                            @if ($requestProduct)
                                value="{{ $requestProduct->street_2 }}"
                            @endif
                        />
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="form-group">
                        <label for="ship-to-city">City</label>
                        <input type="text" name="address[to][city]" id="ship-to-city" class="form-control" placeholder="City" required 
                            @if ($requestProduct)
                                value="{{ $requestProduct->city }}"
                            @endif
                        />
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="form-group">
                        <label for="ship-to-state">State</label>
                        <input type="text" name="address[to][state]" id="ship-to-state" class="form-control" placeholder="State" required 
                            @if ($requestProduct)
                                value="{{ $requestProduct->state }}"
                            @endif
                        />
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="form-group">
                        <label for="ship-to-country">Country</label>
                        <input type="text" name="address[to][country]" id="ship-to-country" class="form-control" placeholder="Country" required 
                            @if ($requestProduct)
                                value="{{ $requestProduct->country }}"
                            @endif
                        />
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-2">
                    <div class="form-group">
                        <label for="ship-to-postcode">Postcode</label>
                        <input type="text" name="address[to][zip]" id="ship-to-postcode" class="form-control" placeholder="Postcode" required 
                            @if ($requestProduct)
                                value="{{ $requestProduct->postcode }}"
                            @endif
                        />
                    </div>
                </div>

            </div>

        </div>

    </div>

    <div class="panel">

        <div class="panel-heading">
            <h4>Parcel/Package Details</h4>
        </div>

        <div class="panel-body">

            <div class="parcels">
                <div class="the-parcel parcel-entry row">

                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="form-group">
                            <label for="package-length">Length</label>
                            <input type="number" min="0" step="0.001" name="parcels[1][length]" class="form-control" id="package-length-1" required />
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="form-group">
                            <label for="package-width">Width</label>
                            <input type="number" min="0" step="0.001" name="parcels[1][width]" class="form-control" id="package-width-1" required />
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="form-group">
                            <label for="package-height">Height</label>
                            <input type="number" min="0" step="0.001" name="parcels[1][height]" class="form-control" id="package-height-1" required />
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="form-group">
                            <label for="package-size-unit">Size Unit</label>
                            <select name="parcels[1][distance_unit]" id="package-size-unit-1" class="form-control" required>
                                <option value=""></option>
                                <option value="cm">centimeter</option>
                                <option value="in">inch</option>
                                <option value="ft">feet</option>
                                <option value="mm">millimeter</option>
                                <option value="m">meter</option>
                                <option value="yd">yard</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="form-group">
                            <label for="package-weight">Weight</label>
                            <input type="number" min="0" step="0.001" name="parcels[1][weight]" class="form-control" id="package-weight-1" required />
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="form-group">
                            <label for="package-mass-unit">Mass Unit</label>
                            <select name="parcels[1][mass_unit]" id="package-mass-unit-1" class="form-control" required>
                                <option value=""></option>
                                <option value="g">gram</option>
                                <option value="oz">ounce</option>
                                <option value="lb">pound</option>
                                <option value="kg">kilogram</option>
                            </select>
                        </div>
                    </div>

                </div>
            </div>

            <div style="display: block;">
                <a href="#" class="btn btn-sm btn-primary add-parcel">Add Parcel</a>
            </div>

        </div>

        <div class="panel-footer">
            <button type="submit" class="btn btn-primary">Find Rates</button>
            <a href="{{ route('requesters.ship') }}" class="btn btn-default">Clear</a>
        </div>
        
    </div>

</div>
</form>
</div>
</div>
@endsection

@section('footer-addon')
<script src="{{ asset('assets/js/vendors/inputmask.js') }}"></script>
<script>
var count = 1;
$(function() {

    $('#ship-to-phone, #ship-from-phone').mask('999-999-9999');
    

    $(document).on('click', '.add-parcel', function(e) {
        e.preventDefault();
        count++;
        if ($('.parcel-entry').length < 10) {
            $('.parcels').append(newParcel(count));
        }
    });

    $(document).on('click', '.remove-parcel', function(e) {
        e.preventDefault();
        var container = $(this).closest('.parcel-entry');
        container.remove();
    });

    $('#find-rates-form').on('submit', function() {
        loader.show();
    });

});
function newParcel(id)
{
    var html = 
        '<div class="parcel-entry">' +
        '<div class="panel-separator"><span></span></div>' + 
        '<div style="padding-bottom: 12px;"><a href="#" class="btn btn-default btn-sm remove-parcel">Remove Parcel</a></div>' +
        '<div class="row">' +
        '<div class="col-xs-12 col-sm-6 col-md-3">' +
        '<div class="form-group">' +
        '<label for="package-length">Length</label>' +
        '<input type="number" min="0" step="0.001" name="parcels[' + id + '][length]" class="form-control" id="package-length-' + id + '" required />' +
        '</div>' +
        '</div>' +
        '<div class="col-xs-12 col-sm-6 col-md-3">' +
        '<div class="form-group">' +
        '<label for="package-width">Width</label>' +
        '<input type="number" min="0" step="0.001" name="parcels[' + id + '][width]" class="form-control" id="package-width-' + id + '" required />' +
        '</div>' +
        '</div>' +
        '<div class="col-xs-12 col-sm-6 col-md-3">' +
        '<div class="form-group">' +
        '<label for="package-height">Height</label>' +
        '<input type="number" min="0" step="0.001" name="parcels[' + id + '][height]" class="form-control" id="package-height-' + id + '" required />' +
        '</div>' +
        '</div>' +
        '<div class="col-xs-12 col-sm-6 col-md-3">' +
        '<div class="form-group">' +
        '<label for="package-size-unit">Size Unit</label>' +
        '<select name="parcels[' + id + '][distance_unit]" id="package-size-unit-' + id + '" class="form-control" required>' +
        '<option value=""></option>' +
        '<option value="cm">centimeter</option>' +
        '<option value="in">inch</option>' +
        '<option value="ft">feet</option>' +
        '<option value="mm">millimeter</option>' +
        '<option value="m">meter</option>' +
        '<option value="yd">yard</option>' +
        '</select>' +
        '</div>' +
        '</div>' +
        '<div class="col-xs-12 col-sm-6 col-md-3">' +
        '<div class="form-group">' +
        '<label for="package-weight">Weight</label>' +
        '<input type="number" min="0" step="0.001" name="parcels[' + id + '][weight]" class="form-control" id="package-weight-' + id + '" required />' +
        '</div>' +
        '</div>' +
        '<div class="col-xs-12 col-sm-6 col-md-3">' +
        '<div class="form-group">' +
        '<label for="package-mass-unit">Mass Unit</label>' +
        '<select name="parcels[' + id + '][mass_unit]" id="package-mass-unit-' + id + '" class="form-control" required>' +
        '<option value=""></option>' +
        '<option value="g">gram</option>' +
        '<option value="oz">ounce</option>' +
        '<option value="lb">pound</option>' +
        '<option value="kg">kilogram</option>' +
        '</select>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';

    return html;
}
</script>
@endsection