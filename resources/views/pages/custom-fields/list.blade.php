@extends('layouts.app')

@section('page-title', 'Custom Fields')

@section('head-addon')
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.0/css/select.dataTables.min.css" />
<link rel="stylesheet" href="{{ asset('assets/css/dt-override.css') }}" />
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.status')

    <div class="panel">

        <div class="panel-heading">
            <h4>Custom Fields</h4>
        </div>

        <div class="panel-filter">
            @if ($currentUser->allowedTo('add_custom_fields'))
                <div class="input-group">
                    <input type="text" placeholder="Search custom field" class="form-control" id="quick-search-input" autocomplete="off">
                    <span class="input-group-btn">
                        <a href="#" class="btn btn-primary add-cf btn-squared" data-url="{{ route('cf.add') }}">
                            <i class="fa fa-plus"></i>
                        </a>
                    </span>
                </div>
            @else
                <input type="text" placeholder="Search custom field" class="form-control" id="quick-search-input" autocomplete="off">
            @endif            
        </div>

        <div class="panel-preloader" data-target="#requesters-panel-body">
            <i class="fa fa-circle-o-notch fa-spin fa-fw"></i> Preparing requesters table
        </div>
        <div id="requesters-panel-body" class="panel-body hidden" style="display:none;">

            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#pr-tab" aria-controls="pr-tab" role="tab" data-toggle="tab">Products</a></li>
                <li role="presentation"><a href="#rq-tab" aria-controls="rq-tab" role="tab" data-toggle="tab">Requests</a></li>
                <li role="presentation"><a href="#po-tab" aria-controls="po-tab" role="tab" data-toggle="tab">Purchase Orders</a></li>
            </ul>

            <div class="tab-content">

                <!-- products -->
                <div role="tabpanel" class="tab-pane active" id="pr-tab">

                    <div class="table-responsive">
                        <table id="cf-pr" class="table datatable table-hover">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Slug</th>
                                    <th>Type</th>
                                    <th class="text-right">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($prFields as $cf)
                                    @include('pages.custom-fields.row', [
                                        'uid' => 'cf-pr',
                                        'cf' => $cf
                                    ])
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>

                <!-- requests -->
                <div role="tabpanel" class="tab-pane" id="rq-tab">

                    <div class="table-responsive">
                        <table id="cf-rq" class="table datatable table-hover">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Slug</th>
                                    <th>Type</th>
                                    <th class="text-right">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($rqFields as $cf)
                                    @include('pages.custom-fields.row', [
                                        'uid' => 'cf-rq',
                                        'cf' => $cf
                                    ])
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    
                </div>

                <!-- purchase oders -->
                <div role="tabpanel" class="tab-pane" id="po-tab">

                    <div class="table-responsive">
                        <table id="cf-po" class="table datatable table-hover">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Slug</th>
                                    <th>Type</th>
                                    <th class="text-right">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($poFields as $cf)
                                    @include('pages.custom-fields.row', [
                                        'uid' => 'cf-po',
                                        'cf' => $cf
                                    ])
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    
                </div>

            </div>

        </div>

    </div>

</div>
</div>
</div>
@endsection

@section('footer-addon')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script>
$(function() {

    function tabInfo(table) {
        var id = $(table).closest('.tab-pane').attr('id'),
            tab = $('.nav-tabs a[aria-controls='+id+']'),
            length = $(table).DataTable().page.info().recordsDisplay,
            label = tab.find('span');
        if (label.length) { label.remove(); }
        tab.append(' <span>('+length+')</span>');
    }

    var tables = $('.datatable')
        .on('draw.dt', function () {
            tabInfo(this);
        })
        .on('search.dt', function () {
            tabInfo(this);
        })
        .DataTable({
            pageLength: 10,
            ordering: true,
            lengthChange: false,
            pagingType: 'numbers',
            'order': [ 0, 'asc' ],
            'columnDefs': [
                {
                    'targets': [1, 3],
                    'orderable': false
                }
            ]
        });

    $('#quick-search-input').on('keyup', function () {
        tables.search( this.value ).draw();
    });

    $(document).on('click', '.add-cf', function(e) {
        e.preventDefault();
        $.get( $(this).data('url') ).done(function(r) {
            bootbox.dialog({
                title: r.title,
                message: r.view,
                onEscape: function () {
                    bootbox.hideAll();
                },
                buttons: {
                    confirm: {
                        label: 'Add',
                        className: 'btn-primary',
                        callback: function() {

                            loader.show();

                            var form = $('#add-cf-form');
                            $.post( form.attr('action'), form.serialize() ).done(function(r) {
                                if (r.error) {
                                    $('.form-error').html(r.error);
                                    loader.hide();
                                }
                                if (r.done) {
                                    window.location = r.redirect;
                                }                                
                            });

                            return false;

                        }
                    },
                    cancel: {
                        label: 'Cancel',
                        className: 'btn-default'
                    }
                }
            });
        });
    });

    $(document).on('click', '.edit-cf', function(e) {
        e.preventDefault();
        loader.show();
        $.get( $(this).data('url') ).done(function(r) {
            loader.hide();
            bootbox.dialog({
                title: r.title,
                message: r.view,
                onEscape: function () {
                    bootbox.hideAll();
                },
                buttons: {
                    confirm: {
                        label: 'Submit Changes',
                        className: 'btn-primary',
                        callback: function() {

                            var form = $('#edit-cf-form');
                            $.post( form.attr('action'), form.serialize() ).done(function(r) {
                                if (r.error) {
                                    $('.form-error').html(r.error);
                                    loader.hide();
                                }
                                if (r.done) {
                                    window.location = r.redirect;
                                }                                
                            });

                            return false;

                        }
                    },
                    cancel: {
                        label: 'Cancel',
                        className: 'btn-default'
                    }
                }
            });
        });
    });

    $(document).on('click', '.delete-cf', function(e) {
        e.preventDefault();
        var url = $(this).data('url');
        bootbox.dialog({
            title: 'Hey!',
            message: '<div style="font-size: 16px;">Are you sure you want to delete this custom fields?<br />All values saved referencing on this custom field will also be deleted.</div>',
            className: 'modal-danger',
            onEscape: function () {
                bootbox.hideAll();
            },
            buttons: {
                'confirm': {
                    label: 'Yes',
                    className: 'btn-danger',
                    callback: function() {

                        loader.show();
                        window.location = url;
                        return false;

                    }
                },
                'Close' : {
                    className: 'btn-default'
                }
            }
        });
    });

});
</script>
@endsection