<tr id="{{ $uid }}-{{ $cf->id }}">
    <td class="table-strong">{{ $cf->label }}</td>
    <td>{{ $cf->slug }}</td>
    <td>{{ $cf->type }}</td>
    <td class="text-right">

        @if ($currentUser->allowedTo('edit_custom_fields'))
            <a href="#" class="edit-cf" data-url="{{ route('cf.edit') }}/{{ $cf->id }}">
                <i class="fa fa-pencil fa-fw"></i>
            </a>
        @endif
        
        @if ($currentUser->allowedTo('delete_custom_fields'))
            <a href="#" class="delete-cf" data-url="{{ route('cf.delete') }}/{{ $cf->id }}">
                <i class="fa fa-trash-o fa-fw"></i>
            </a>
        @endif

    </td>
</tr>