<div class="form-error"></div>
<form action="{{ route('cf.edit') }}/{{ $cf->id }}" method="post" autocomplete="off" id="edit-cf-form">
{!! csrf_field() !!}

    <div class="form-group">
        <label for="cf-model">Model</label>
        <select name="model" id="cf-model" class="form-control">
            <option value=""></option>
            @foreach (getCustomFieldModels() as $key => $value)
                <option value="{{ $key }}"
                    @if ($cf->model == $key) selected @endif 
                >
                    {{ $value }}
                </option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label for="cf-label">Label</label>
        <input type="text" name="label" id="cf-label" class="form-control" required placeholder="eg. Dimension, Weight" value="{{ $cf->label }}" />
    </div>

    <div class="form-group">
        <label for="cf-slug">Slug</label>
        <input type="text" name="slug" id="cf-slug" class="form-control" required placeholder="eg. destination-from-warehouse" value="{{ $cf->slug }}" />
    </div>

    <div class="form-group fg-last">
        <label for="cf-type">Type</label>
        <select name="type" id="cf-type" class="form-control">
            <option value=""></option>
            @foreach (getCustomFieldTypes() as $key => $value)
                <option value="{{ $key }}"
                    @if ($cf->type == $key) selected @endif 
                >
                    {{ $value }}
                </option>
            @endforeach
        </select>
    </div>

</form>