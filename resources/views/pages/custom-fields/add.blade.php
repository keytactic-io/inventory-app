<div class="form-error"></div>
<form action="{{ route('cf.add') }}" method="post" autocomplete="off" id="add-cf-form">
{!! csrf_field() !!}

    <div class="form-group">
        <label for="cf-model">Model</label>
        <select name="model" id="cf-model" class="form-control">
            <option value=""></option>
            <option value="pr">Product</option>
            <option value="rq">Request</option>
            <option value="po">Purchase Order</option>
        </select>
    </div>

    <div class="form-group">
        <label for="cf-label">Label</label>
        <input type="text" name="label" id="cf-label" class="form-control" required placeholder="eg. Dimension, Weight" />
    </div>

    <div class="form-group">
        <label for="cf-slug">Slug</label>
        <input type="text" name="slug" id="cf-slug" class="form-control" required placeholder="eg. destination-from-warehouse" />
    </div>

    <div class="form-group fg-last">
        <label for="cf-type">Type</label>
        <select name="type" id="cf-type" class="form-control">
            <option value=""></option>
            <option value="input">Input Text</option>
            <option value="textarea">Textarea</option>
        </select>
    </div>

</form>