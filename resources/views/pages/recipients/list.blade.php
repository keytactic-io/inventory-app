@extends('layouts.app')

@section('page-title', 'Recipients')

@section('head-addon')
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.0/css/select.dataTables.min.css" />
<link rel="stylesheet" href="{{ asset('assets/css/dt-override.css') }}" />
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.status')

    <div class="panel">

        <div class="panel-heading">
            <h4>Recipients</h4>
        </div>

        <div class="panel-filter">
            <div class="input-group">
                <input type="text" placeholder="Find recipient" class="form-control" id="quick-search-input" autocomplete="off">
                <span class="input-group-btn">
                    <button class="btn btn-primary btn-squared add-recipient" type="button"
                        data-url="{{ route('recipients.add') }}" data-target="#add-recipient-form"
                    >
                        <i class="fa fa-plus fa-fw"></i>
                    </button>
                </span>
            </div>
        </div>

        <div class="panel-preloader" data-target="#recipients-panel">
            <i class="fa fa-circle-o-notch fa-spin fa-fw"></i> &nbsp; Preparing recipients table
        </div>
        <div id="recipients-panel" class="panel-body" style="display:none;">

            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#all" aria-controls="all" role="tab" data-toggle="tab">All</a>
                </li>
                <li role="presentation">
                    <a href="#cc-always" aria-controls="cc-always" role="tab" data-toggle="tab">CC Always</a>
                </li>
                <li role="presentation">
                    <a href="#non-cc" aria-controls="non-cc" role="tab" data-toggle="tab">Non-CC</a>
                </li>
            </ul>

            <div class="tab-content">

                <!-- all -->
                <div role="tabpanel" class="tab-pane active" id="all">

                    <div class="table-responsive">
                        <table id="recipients-all" class="table datatable table-hover">
                            <thead>
                                <tr>
                                    <th>Label</th>
                                    <th>Email</th>
                                    <th>CC Always</th>
                                    <th class="text-right">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($recipients as $recipient)
                                    @include('pages.recipients.row', ['recipient' => $recipient])
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>

                <div role="tabpanel" class="tab-pane" id="cc-always">

                    <div class="table-responsive">
                        <table id="recipients-cc-always" class="table datatable table-hover">
                            <thead>
                                <tr>
                                    <th>Label</th>
                                    <th>Email</th>
                                    <th>CC Always</th>
                                    <th class="text-right">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($recipients as $recipient)
                                    @if ($recipient->cc_always)
                                        @include('pages.recipients.row', ['recipient' => $recipient])
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>

                <div role="tabpanel" class="tab-pane" id="non-cc">

                    <div class="table-responsive">
                        <table id="recipients-non-cc" class="table datatable table-hover">
                            <thead>
                                <tr>
                                    <th>Label</th>
                                    <th>Email</th>
                                    <th>CC Always</th>
                                    <th class="text-right">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($recipients as $recipient)
                                    @if (!$recipient->cc_always)
                                        @include('pages.recipients.row', ['recipient' => $recipient])
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>

        </div>

    </div>

</div>
</div>
</div>
@endsection

@section('footer-addon')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script>
$(function() {

    function tabInfo(table) {
        var id = $(table).closest('.tab-pane').attr('id'),
            tab = $('.nav-tabs a[aria-controls='+id+']'),
            length = $(table).DataTable().page.info().recordsDisplay,
            label = tab.find('span');
        if (label.length) { label.remove(); }
        tab.append(' <span>('+length+')</span>');
    }

    var tables = $('.datatable')
        .on('draw.dt', function () {
            tabInfo(this);
        })
        .on('search.dt', function () {
            tabInfo(this);
        })
        .DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5',
                'print'
            ],
            pageLength: 10,
            ordering: true,
            lengthChange: false,
            pagingType: 'numbers',
            'order': [ 0, 'asc' ],
            'columnDefs': [
                {
                    'targets': [2, 3],
                    'orderable': false
                }
            ]
        });

    $('#quick-search-input').on('keyup', function () {
        tables.search( this.value ).draw();
    });

    $(document).on('click', '.add-recipient, .edit-recipient', function(e) {
        e.preventDefault();
        loader.show();
        var url = $(this).data('url'),
            targetForm = $(this).data('target');
        $.get( url ).done(function(r) {
            loader.hide();
            bootbox.dialog({
                title: r.title,
                message: r.view,
                onEscape: function () {
                    bootbox.hideAll();
                },
                buttons: {
                    confirm: {
                        label: r.btnlabel,
                        className: 'btn-primary',
                        callback: function() {

                            loader.show();
                            var form = $(targetForm);
                            $.post( form.attr('action'), form.serialize() ).done(function(r) {
                                if (r.error) {
                                    $('.form-error').html(r.error);
                                    loader.hide();
                                }
                                if (r.success) {
                                    window.location = r.redirect;
                                }
                            });

                            return false;

                        }
                    },
                    'Close' : {
                        className: 'btn-default'
                    }
                }
            });
        });
    });

    $(document).on('click', '.remove-recipient', function(e) {
        e.preventDefault();
        loader.show();
        $.get( $(this).data('url') ).done(function(r) {
            if (r.success) {
                tables.row( $('#recipient-entry-' + r.id)[0] ).remove().draw();
                loader.hide();
            }
        });
    });

});
</script>

@endsection