<div class="form-error"></div>
<form action="{{ route('recipients.add') }}" method="post" autocomplete="off" id="add-recipient-form">
{!! csrf_field() !!}

    <div class="form-group">
        <label for="label">Label</label>
        <input type="text" name="label" id="label" class="form-control" placeholder="Accounting Department" />
    </div>

    <div class="form-group">
        <label for="email">Email</label>
        <input type="email" name="email" id="email" class="form-control" placeholder="accounting@email.com" />
    </div>

    <div class="form-group fg-last">
        <div class="checkbox">
            <label>
                <input type="checkbox" name="cc_always" id="" value="1" />
                <span>CC Always</span>
            </label>
        </div>
    </div>

</form>