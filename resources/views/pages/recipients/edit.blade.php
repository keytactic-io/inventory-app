<div class="form-error"></div>
<form action="{{ route('recipients.edit') }}/{{ $recipient->id }}" method="post" autocomplete="off" id="edit-recipient-form">
{!! csrf_field() !!}

    <div class="form-group">
        <label for="label">Label</label>
        <input type="text" name="label" id="label" class="form-control" placeholder="Accounting Department" 
            value="{{ $recipient->label }}"
        />
    </div>

    <div class="form-group">
        <label for="email">Email</label>
        <input type="email" name="email" id="email" class="form-control" placeholder="accounting@email.com" 
            value="{{ $recipient->email }}"
        />
    </div>

    <div class="form-group fg-last">
        <div class="checkbox">
            <label>
                <input type="checkbox" name="cc_always" id="" value="1" 
                    @if ($recipient->cc_always) checked @endif
                />
                <span>CC Always</span>
            </label>
        </div>
    </div>

</form>