<tr id="recipient-entry-{{ $recipient->id }}">
    <td class="table-strong">
        {{ $recipient->label }}
        </td>
    <td>
        <a href="mailto:{{ $recipient->email }}">
            {{ $recipient->email }}
        </a>
    </td>
    <td>
        @if ($recipient->cc_always)
            <i class="fa fa-check text-primary"></i>
        @else
            <i class="fa fa-check text-muted2"></i>
        @endif
    </td>
    <td class="text-right">
        <a href="#" class="edit-recipient" data-url="{{ route('recipients.edit') }}/{{ $recipient->id }}" data-target="#edit-recipient-form">
            <i class="fa fa-pencil fa-fw"></i>
        </a>
        <a href="#" class="remove-recipient" data-url="{{ route('recipients.remove') }}/{{ $recipient->id }}">
            <i class="fa fa-trash-o fa-fw"></i>
        </a>
    </td>
</tr>