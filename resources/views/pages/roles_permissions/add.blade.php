@extends('layouts.app')

@section('page-title', 'Add Role')

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.errors')

</div>
<div class="col-xs-12">

    <form action="{{ route('roles.add') }}" method="post" autocomplete="off">
    {!! csrf_field() !!}
    <div class="panel">

        <div class="panel-heading">
            <h4>Add Role</h4>
        </div>
        
        <div class="panel-body">
            
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" name="name" required />
            </div>

            <div class="form-group">
                <label for="description">Description</label>
                <textarea name="description" id="description" rows="2" class="form-control"></textarea>
            </div>

            <div class="form-group" style="margin-bottom:0;">
                <label>Permissions</label>
            </div>
            <div class="table-responsive">
                <table class="table">
                    <tbody>
                        @foreach ($permissionCategories as $cat)
                            <tr>
                                <td class="table-strong">{{ $cat->name }}</td>
                                <td>
                                    @foreach ($cat->permissions as $permission)
                                    <div class="checkbox" style="float:left;margin: 0 24px 0 0;">
                                        <label>
                                            <input type="checkbox" name="permissions[{{ $cat->id }}][{{ $permission->id }}]" value="1" />
                                            <span style="font-weight: normal;">{{ $permission->name }}</span>
                                        </label>
                                    </div>
                                    @endforeach
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="panel-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a href="{{ route('roles.list') }}" class="btn btn-default">Cancel</a>
        </div>

    </div>
    </form>
</div>
</div>
</div>
@endsection
