@extends('layouts.app')

@section('page-title', 'Roles & Permissions')

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.status')

</div>
<div class="col-xs-12">

    <div class="panel">

        <div class="panel-heading">
            <h4>Roles &amp; Permissions</h4>
        </div>

        <div class="panel-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th class="text-right">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($roles as $role)
                            <tr>
                                <td class="table-strong">
                                    @if ($currentUser->allowedTo('edit_roles'))
                                        <a href="{{ route('roles.edit') }}/{{ $role->id }}">
                                            {{ $role->name }}
                                        </a>
                                    @else
                                        {{ $role->name }}
                                    @endif
                                </td>
                                <td>
                                    {{ $role->description }}
                                </td>
                                <td class="text-right">
                                    @if ($currentUser->allowedTo('edit_roles'))
                                        <a href="{{ route('roles.edit') }}/{{ $role->id }}"><i class="fa fa-pencil fa-fw"></i></a>
                                    @endif
                                    @if ($currentUser->allowedTo('delete_roles'))
                                        <a href="{{ route('roles.delete') }}/{{ $role->id }}"><i class="fa fa-trash-o fa-fw"></i></a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        @if ($currentUser->allowedTo('add_roles'))
        <div class="panel-footer">
            <a href="{{ route('roles.add') }}" class="btn btn-primary">Add Role</a>
        </div>
        @endif

    </div>
</div>
</div>
</div>
@endsection
