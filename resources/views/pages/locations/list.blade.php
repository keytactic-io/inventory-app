@extends('layouts.app')

@section('page-title', 'Locations')

@section('head-addon')
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.0/css/select.dataTables.min.css" />
<link rel="stylesheet" href="{{ asset('assets/css/dt-override.css') }}" />
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.status')

    <div class="panel">

        <div class="panel-heading">
            <h4>Locations</h4>
        </div>

        <div class="panel-filter">
            @if ($currentUser->allowedTo('add_locations'))
                <div class="input-group">
                    <input type="text" placeholder="Search locations" class="form-control" id="quick-search-input" autocomplete="off">
                    <span class="input-group-btn">
                        <a href="{{ route('locations.add') }}" class="btn btn-primary btn-squared">
                            <i class="fa fa-plus"></i>
                        </a>
                    </span>
                </div>
            @else
                <input type="text" placeholder="Search locations" class="form-control" id="quick-search-input" autocomplete="off">          
            @endif
        </div>

        <div class="panel-preloader" data-target="#locations-panel">
            <i class="fa fa-circle-o-notch fa-spin fa-fw"></i> &nbsp; Preparing locations table
        </div>
        <div id="locations-panel" class="panel-body" style="display:none;">

            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab">All</a></li>
            </ul>

            <div class="tab-content">

                <!-- all -->
                <div role="tabpanel" class="tab-pane active" id="all">

                    <div class="table-responsive">
                        <table id="locations-all" class="table datatable table-hover">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Staff In-charge</th>
                                    <th>Contact Number</th>
                                    <th>Address</th>
                                    <th>Staff Count</th>
                                    <th>Products Count</th>
                                    <th class="text-right">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($locations as $location)
                                    <?php if (!$showLocationWithZero AND $location->productsCount() == 0) { continue; } ?>
                                    <tr id="locations-all-entry-{{ $location->id }}">
                                        <td class="table-strong nowrap">
                                            <a href="{{ route('locations.details') }}/{{ $location->id }}">
                                                {{ $location->name }}
                                            </a>
                                        </td>
                                        <td class="nowrap">
                                            <strong>{{ $location->staffIncharge->name() }}</strong><br />
                                            {{ $location->staffIncharge->email }}
                                        </td>
                                        <td>{{ $location->contact_number }}</td>
                                        <td>{{ $location->getAddress() }}</td>
                                        <td>{{ $location->staff()->count() }}</td>
                                        <td>
                                            {{ $location->productsCount() }}
                                        </td>
                                        <td class="text-right" style="width: 40px;">
                                            @if ($currentUser->allowedTo('edit_locations'))
                                                <a href="{{ route('locations.edit') }}/{{ $location->id }}">
                                                    <i class="fa fa-pencil fa-fw"></i>
                                                </a>
                                            @endif
                                            @if ($currentUser->allowedTo('delete_locations'))
                                                <a href="#" class="delete-location" data-url="{{ route('locations.delete') }}/{{ $location->id }}">
                                                    <i class="fa fa-trash-o fa-fw"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>

        </div>

        @if ($currentUser->allowedTo('add_locations'))
            {{-- <div class="panel-footer">
                <a href="{{ route('locations.add') }}" class="btn btn-primary">Add Location</a>
            </div> --}}
        @endif

    </div>

</div>
</div>
</div>
@endsection

@section('footer-addon')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script>
$(function() {

    function tabInfo(table) {
        var id = $(table).closest('.tab-pane').attr('id'),
            tab = $('.nav-tabs a[aria-controls='+id+']'),
            length = $(table).DataTable().page.info().recordsDisplay,
            label = tab.find('span');
        if (label.length) { label.remove(); }
        tab.append(' <span>('+length+')</span>');
    }

    var tables = $('.datatable')
        .on('draw.dt', function () {
            tabInfo(this);
        })
        .on('search.dt', function () {
            tabInfo(this);
        })
        .DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5',
                'print'
            ],
            pageLength: 10,
            ordering: true,
            lengthChange: false,
            pagingType: 'numbers',
            'order': [ 0, 'asc' ],
            'columnDefs': [
                {
                    'targets': [6],
                    'orderable': false
                }
            ]
        });

    $('#quick-search-input').on('keyup', function () {
        tables.search( this.value ).draw();
    });

    $(document).on('click', '.delete-location', function(e) {
        e.preventDefault();
        var url = $(this).data('url');
        bootbox.dialog({
            title: 'Hey!',
            message: '<div style="font-size: 16px;">Are you sure you want to delete this location?</div>',
            className: 'modal-danger',
            onEscape: function () {
                bootbox.hideAll();
            },
            buttons: {
                'confirm': {
                    label: 'Yes',
                    className: 'btn-danger',
                    callback: function() {

                        loader.show();
                        window.location = url;
                        return false;

                    }
                },
                'Close' : {
                    className: 'btn-default'
                }
            }
        });
    });

});
</script>
@endsection
