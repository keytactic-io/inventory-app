<div class="form-error"></div>
<form action="{{ route('locations.staff.add') }}" method="post" autocomplete="off" id="add-location-staff">
{!! csrf_field() !!}
<input type="hidden" name="location_id" value="{{ $location->id }}" />

    <div class="form-group fg-last">
        <label for="street1">Staff</label>
        <select name="user_id" id="staff-guide" class="form-control">
            <option value=""></option>
            @foreach ($users as $user)
                @if ($user->id != $location->staff_incharge AND !in_array($user->id, $locationStaff))
                    <option value="{{ $user->id }}">
                        {{ $user->name() }}
                    </option>
                @endif
            @endforeach
        </select>
    </div>

</form>
<script>