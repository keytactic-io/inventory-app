@extends('layouts.app')

@section('page-title', $location->name)

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.status')

</div>
<div class="col-xs-12">


    <div class="panel">
        
        <div class="panel-body">
            
            <div class="details-content">

                <h1>{{ $location->name }}</h1>
                <h4 title="Exact Location"><i class="fa fa-map-marker fa-fw"></i> &nbsp; {{ $location->getAddress() }}</h4>

            </div>

        </div>
        @if ($currentUser->allowedTo('edit_locations'))
            <div class="panel-footer">
                <div class="btn-group">
                    <a href="{{ route('locations.edit') }}/{{ $location->id }}" class="btn btn-primary btn-sm">Edit</a>
                    <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a href="{{ route('locations.delete') }}/{{ $location->id }}">Delete</a></li>
                        {{-- <li role="separator" class="divider"></li> --}}
                    </ul>
                </div>
                <a href="#" class="btn btn-primary btn-sm add-location-staff"
                    data-url="{{ route('locations.staff.add') }}/{{ $location->id }}"
                >
                    Add Staff
                </a>
            </div>
        @endif

    </div>

</div>
<div class="col-xs-12 col-sm-8">
    
    <div class="panel">

        <div class="panel-heading">
            <h4>Products</h4>
        </div>

        <div class="panel-body">

            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Products</th>
                            <th>Description</th>
                            <th class="text-right">Remaining Stocks</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($products as $product)
                            <tr>
                                <td class="table-strong">
                                    <a href="{{ route('products.details') }}/{{ $product->id }}">
                                        {{ $product->name }}
                                    </a>
                                </td>
                                <td>{{ $product->description }}</td>
                                <td class="text-right">
                                    {{ number_format($product->stocksInLocation($location->id), '0', '.', ',') }}
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4" class="text-center active table-strong">No products yet</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>

    </div>

</div>
<div class="col-xs-12 col-sm-4">
    
    <div class="panel">
        <div class="panel-heading">
            <h4>Location Staff</h4>
        </div>
        <div class="panel-body">
            
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>DP</th>
                            <th>Name</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($location->staff as $staff)
                            <tr id="staff-entry-{{ $staff->id }}">
                                <td class="table-photo">
                                    <div class="table-user-dp" 
                                        style="background: url({{ $staff->user->dp() }}) center center no-repeat; background-size: cover;"
                                    ></div>
                                </td>
                                <td class="table-strong">
                                    <a href="{{ route('users.profile') }}/{{ $staff->user->id }}">
                                        {{ $staff->user->name() }}
                                    </a>
                                    @if ($staff->user_id == $location->staff_incharge)
                                        &nbsp;&nbsp; <span class="label label-primary">In-charge</span>
                                    @endif
                                </td>
                                <td class="text-right">
                                    @if ($currentUser->allowedTo('edit_locations'))
                                        <a href="#" class="remove-staff" data-url="{{ route('locations.staff.delete') }}/{{ $staff->id }}">
                                            <i class="fa fa-trash-o fa-fw"></i>
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>

</div>
</div>
</div>
@endsection

@section('footer-addon')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script>
$(function() {

    $(document).on('click', '.add-location-staff', function(e) {
        e.preventDefault();
        loader.show();
        var url = $(this).data('url');
        $.get( url ).done(function(r) {
            loader.hide();
            bootbox.dialog({
                title: r.title,
                message: r.view,
                onEscape: function () {
                    bootbox.hideAll();
                },
                buttons: {
                    confirm: {
                        label: 'Add',
                        className: 'btn-primary',
                        callback: function() {

                            loader.show();
                            var form = $('#add-location-staff');
                            $.post( form.attr('action'), form.serialize() ).done(function(r) {
                                if (r.error) {
                                    $('.form-error').html(r.error);
                                    loader.hide();
                                }
                                if (r.done) {
                                    window.location = r.redirect;
                                }
                            });                            

                            return false;

                        }
                    },
                    'Close' : {
                        className: 'btn-default'
                    }
                }
            });

        });
    });

    $(document).on('click', '.remove-staff', function(e) {
        e.preventDefault();
        loader.show();
        $.get( $(this).data('url') ).done(function(r) {
            if (r.done) {
                $('#staff-entry-' + r.toRemove).remove();
                loader.hide();
            }
        });
    });

});
</script>
@endsection
