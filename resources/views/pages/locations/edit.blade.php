@extends('layouts.app')

@section('page-title', 'Edit Location')

@section('head-addon')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css">
<link rel="stylesheet" href="{{ asset('assets/css/bt-select-override.css') }}" />
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.status')

    <form action="{{ route('locations.edit') }}/{{ $location->id }}" method="post" autocomplete="off">
    {!! csrf_field() !!}
    <div class="panel">

        <div class="panel-heading">
            <h4>Edit Location</h4>
        </div>

        <div class="panel-body">

            <div class="form-horizontal">
                <div class="form-group">
                    <label for="location" class="col-xs-12 col-sm-3 control-label">Name of Location</label>
                    <div class="col-xs-12 col-sm-6">
                        <input type="text" name="name" id="location" class="form-control" value="{{ old('name', $location->name)}}" required />
                    </div>
                </div>

                <div class="form-group">
                    <label for="staff-incharge" class="col-xs-12 col-sm-3 control-label">Staff In-charge</label>
                    <div class="col-xs-12 col-sm-6">
                        <select name="staff_incharge" id="staff-incharge" class="form-control"
                            data-live-search="true" required
                        >
                            <option value=""></option>
                            @foreach ($users as $user)
                                <option value="{{ $user->id }}"
                                    @if ($location->staff_incharge == $user->id) selected @endif
                                >
                                    {{ $user->name() }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="contact-number" class="col-xs-12 col-sm-3 control-label">Location/Staff In-charge Contact Number</label>
                    <div class="col-xs-12 col-sm-6">
                        <input type="text" name="contact_number" id="contact-number" class="form-control"
                            value="{{ old('contact_number', $location->contact_number)}}"
                        />
                    </div>
                </div>

                <div class="form-group">
                    <label for="street1" class="col-xs-12 col-sm-3 control-label">Street 1</label>
                    <div class="col-xs-12 col-sm-6">
                        <input type="text" name="street_1" id="street1" class="form-control" value="{{ old('street_1', $location->street_1)}}" required />
                    </div>
                </div>

                <div class="form-group">
                    <label for="street2" class="col-xs-12 col-sm-3 control-label">Street 2</label>
                    <div class="col-xs-12 col-sm-6">
                        <input type="text" name="street_2" id="street2" class="form-control" value="{{ old('street_2', $location->street_2)}}" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="city" class="col-xs-12 col-sm-3 control-label">City</label>
                    <div class="col-xs-12 col-sm-6">
                        <input type="text" name="city" id="city" class="form-control" value="{{ old('city', $location->city)}}" required />
                    </div>
                </div>

                <div class="form-group">
                    <label for="state" class="col-xs-12 col-sm-3 control-label">State</label>
                    <div class="col-xs-12 col-sm-6">
                        <select name="state" id="state" class="form-control"
                            data-live-search="true" required
                        >
                            <option value=""></option>
                            @foreach (getStates() as $state)
                                <option value="{{ $state }}"
                                    @if ($state == $location->state) selected @endif
                                >
                                    {{ $state }}
                                </option>
                            @endforeach
                        </select>
                        <input type="hidden" name="country" value="US" />
                    </div>
                </div>

                <?php /*
                <div class="form-group">
                    <label for="country" class="col-xs-12 col-sm-3 control-label">Country</label>
                    <div class="col-xs-12 col-sm-6">
                        <select name="country" id="country" class="form-control"
                            data-live-search="true" required
                        >
                            <option value=""></option>
                            @foreach (getCountryCodes() as $code => $name)
                                <option value="{{ $code}}"
                                    @if ($code == $location->country) selected @endif
                                >
                                    {{ $name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                */ ?>

                <div class="form-group fg-last">
                    <label for="postcode" class="col-xs-12 col-sm-3 control-label">Zip</label>
                    <div class="col-xs-12 col-sm-6">
                        <input type="text" name="postcode" id="postcode" class="form-control" value="{{ old('postcode', $location->postcode)}}" required />
                    </div>
                </div>
            </div>

        </div>

        <div class="panel-footer">
            <button type="submit" class="btn btn-primary">Submit Changes</button>
            <a href="{{ route('locations.list') }}" class="btn btn-default">Cancel</a>
        </div>

    </div>
    </form>

</div>
</div>
</div>
@endsection

@section('footer-addon')
<script src="{{ asset('assets/js/vendors/inputmask.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js"></script>
<script>
$(function() {

    $('#contact-number').mask('999-999-9999');

    $('#country').selectpicker({
        style: 'btn-default',
        size: 6
    });

    $('#staff-incharge').selectpicker({
        style: 'btn-default',
        size: 6
    });

});
</script>
@endsection
