@extends('layouts.app')

@section('page-title', 'Product Categories')

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.status')

    <div class="panel">

        <div class="panel-heading">
            <h4>Categories</h4>
        </div>

        <div class="panel-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Category Name</th>
                            <th>Description</th>
                            <th class="text-right">Product Count</th>
                            <th class="text-right">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($parentCategories as $parentCat)
                            <tr>
                                <td class="table-strong"><a href="{{ route('products.list') }}/all/{{ $parentCat->id }}">{{ $parentCat->name }}</a></td>
                                <td>{{ $parentCat->description }}</td>
                                <td class="text-right">{{ $parentCat->productsCount() }}</td>
                                <td class="text-right">
                                    @if ($currentUser->allowedTo('edit_product_categories'))
                                        <a href="{{ route('categories.edit') }}/{{ $parentCat->id }}"><i class="fa fa-pencil fa-fw"></i></a>
                                    @endif
                                    @if ($currentUser->allowedTo('delete_product_categories'))
                                        <a href="{{ route('categories.delete') }}/{{ $parentCat->id }}"><i class="fa fa-trash-o fa-fw"></i></a>
                                    @endif
                                </td>
                            </tr>
                            @if (count($parentCat->childCategories) > 0)
                                <?php $childCategories = $parentCat->childCategories; ?>
                                @foreach ($childCategories as $childCat)
                                    <tr>
                                        <td>&mdash; <a href="{{ route('products.list') }}/all/{{ $childCat->id }}">{{ $childCat->name }}</a></td>
                                        <td>{{ $childCat->description }}</td>
                                        <td class="text-right">{{ $childCat->productsCount() }}</td>
                                        <td class="text-right">
                                            @if ($currentUser->allowedTo('edit_product_categories'))
                                                <a href="{{ route('categories.edit') }}/{{ $childCat->id }}"><i class="fa fa-pencil fa-fw"></i></a>
                                            @endif
                                            @if ($currentUser->allowedTo('delete_product_categories'))
                                                <a href="{{ route('categories.delete') }}/{{ $childCat->id }}"><i class="fa fa-trash-o fa-fw"></i></a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        @if ($currentUser->allowedTo('add_product_categories'))
            {{-- <div class="panel-footer">
                <a href="{{ route('categories.add') }}" class="btn btn-primary">Add Category</a>
            </div> --}}
        @endif

    </div>

</div>
</div>
</div>
@endsection
