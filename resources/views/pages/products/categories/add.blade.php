@extends('layouts.app')

@section('page-title', 'Add Product Category')

@section('head-addon')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css">
<link rel="stylesheet" href="{{ asset('assets/css/bt-select-override.css') }}" />
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.errors')

    <form action="{{ route('categories.add') }}" method="post" autocomplete="off"
        id="add-category-form" class="with-preloader" 
    >
        {!! csrf_field() !!}

        <div class="panel">
            <div class="panel-heading">
                <h4>Add Category</h4>
            </div>

            <div class="panel-body">
                
                <div class="form-group">
                    <label for="name">Category Name</label>
                    <input type="text" class="form-control" id="name" name="name" required />
                </div>

                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea name="description" id="description" rows="2" class="form-control"></textarea>
                </div>

                <div class="form-group fg-last">
                    <label for="parent-category">Parent Category</label>
                    <select name="parent_category" id="parent-category" class="form-control" 
                        data-live-search="true" required 
                    >
                        <option value="0" selected>This is a Parent Category</option>
                        @foreach ($parentCategories as $parentCat)
                            <option value="{{ $parentCat->id }}">{{ $parentCat->name }}</option>
                        @endforeach
                    </select>
                </div>

            </div>
            
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ route('categories.list') }}" class="btn btn-default">Cancel</a>
            </div>

        </div>
    </form>

</div>
</div>
</div>
@endsection

@section('footer-addon')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js"></script>
<script>
$(function() {

    $('#parent-category').selectpicker({
        style: 'btn-default',
        size: 6
    });

});
</script>
@endsection
