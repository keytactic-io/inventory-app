<div class="table-responsive">
    <table class="nice-table">
        <thead>
            <tr>
                <th>&nbsp;</th>
                <th>Name</th>
                <th>Vendor</th>
                <th>Remaining</th>
                <th>Latest Unit Price</th>
                <th class="text-right">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($products as $product)
                @include('pages.products.row', [
                    'uid' => 'products-all-entry',
                    'product' => $product
                ])
            @empty
                <tr>
                    <td colspan="6" class="text-center no-entries noselect">
                        <strong>No entries</strong>
                    </td>
                </tr>
            @endforelse
        </tbody>
    </table>
</div>

@if ($products->links() != '')
    @include('includes.pagination', [
        'results' => $products
    ])
@endif
