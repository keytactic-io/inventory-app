@extends('layouts.app')

@section('page-title', 'Add Product')

@section('head-addon')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css">
<link rel="stylesheet" href="{{ asset('assets/css/bt-select-override.css') }}" />
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.errors')

    <form action="{{ route('products.transfer') }}" method="post" autocomplete="off"
        id="transfer-product-form" class="with-preloader" 
    >
        {!! csrf_field() !!}
        <div class="panel">

            <div class="panel-heading">
                <h4>Transfer Product</h4>
            </div>

            <div class="panel-body">
                
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label for="location-from">From</label>
                            <select name="from" id="location-from" class="form-control bt-select"
                                data-live-search="true" data-url="{{ route('locations.products') }}" required 
                            >
                                <option value=""></option>
                                @foreach ($locations as $location)
                                    @if ($location->productsCount() > 0)
                                        <option value="{{ $location->id }}"
                                        >
                                            {{ $location->name }} &nbsp; ({{ $location->productsCount() }})
                                        </option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label for="location-to">To</label>
                            <select name="to" id="location-to" class="form-control bt-select"
                                data-live-search="true" required
                            >
                                <option value=""></option>
                                @foreach ($locations as $location)
                                    <option value="{{ $location->id }}"
                                    >
                                        {{ $location->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-8">
                        <div class="form-group">
                            <label for="products-selection">Product to Transfer</label>
                            <div id="products-selection-wrap">
                                <div style="padding: 8px 0;">
                                    <i class="fa fa-circle-o-notch fa-spin fa-fw text-primary"></i> &nbsp; Select a location
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="form-group fg-last">
                            <label for="quantity">Quantity</label>
                            <input type="number" name="quantity" id="quantity" class="form-control" min="1" required />
                        </div>
                    </div>
                </div>                

            </div>
            
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary">Transfer</button>
                <a href="{{ route('products.transferred') }}" class="btn btn-default">Cancel</a>
            </div>

        </div>
    </form>

</div>
</div>
</div>
@endsection

@section('footer-addon')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js"></script>
<script>
$(function() {

    $('.bt-select')
        .selectpicker({
            style: 'btn-default',
            size: 6
        })
        .on('changed.bs.select', function(e) {            
            if (e.target.id == 'location-from') {
                loader.show();
                var target = $('#' + e.target.id);
                var url = target.attr('data-url') + '/' + target.val();
                $.get( url ).done(function(r) {
                    r = $.parseJSON(r);
                    var html = '<select id="products-selection" name="product_id" class="form-control bt-select" data-live-search="true"><option value=""></option>';
                    $.each(r.products, function(key, product) {
                        html += '<option value="' + product.id + '" data-remaining="' + product.remaining + '">' + product.name + '</option>';
                    });
                    html += '</select>';
                    $('#products-selection-wrap').html(html);

                    $('#products-selection')
                        .selectpicker({
                            style: 'btn-default',
                            size: 6
                        })
                        .on('changed.bs.select', function(e) {
                            var id = e.target.value,
                                max = 0;

                            $.each(e.target, function(key, value) {
                                if (value.attributes['value'].value == id) {
                                    max = value.attributes['data-remaining'].value;
                                }
                            });

                            $('#quantity')
                                .attr('max', max)
                                .attr('placeholder', 'Maximum quantity: ' + max);
                        });

                    loader.hide();
                });
            }
        });

});
</script>
@endsection