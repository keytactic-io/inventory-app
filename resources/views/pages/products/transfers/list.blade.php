@extends('layouts.app')

@section('page-title', 'Transferred Products')

@section('head-addon')
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.0/css/select.dataTables.min.css" />
<link rel="stylesheet" href="{{ asset('assets/css/dt-override.css') }}" />
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.status')

    <div class="panel">

        <div class="panel-heading">
            <h4>Transferred Products</h4>
        </div>

    <div class="panel-filter">
		@if ($currentUser->allowedTo('add_transferred_products'))
        <div class="input-group">
            <input type="text" placeholder="Search product transfers" class="form-control" id="quick-search-input" autocomplete="off">
								<span class="input-group-btn">
                      <a href="{{ route('products.transfer') }}" class="btn btn-primary btn-squared">
                        <i class="fa fa-plus"></i>
                      </a>
                </span>
        </div>
		@else
						<input type="text" placeholder="Search product transfers" class="form-control" id="quick-search-input" autocomplete="off">
    @endif
    </div>

        <div class="panel-preloader" data-target="#transferred-products-panel">
            <i class="fa fa-circle-o-notch fa-spin fa-fw"></i> &nbsp; Preparing transferred products table
        </div>
        <div id="transferred-products-panel" class="panel-body" style="display:none;">

            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab">All</a></li>
                {{-- <li role="presentation"><a href="#received" aria-controls="received" role="tab" data-toggle="tab">Received</a></li> --}}
            </ul>

            <div class="tab-content">

                <!-- all -->
                <div role="tabpanel" class="tab-pane active" id="all">

                    <div class="table-responsive">
                        <table id="vendors-all" class="table datatable table-hover">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>From</th>
                                    <th>To</th>
                                    <th>Quantity</th>
                                    <th>Date Transferred</th>
                                    <th class="text-right"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($transferredProducts as $tP)
                                    <tr>
                                        <td class="table-strong">
                                            <a href="{{ route('products.details') }}/{{ $tP->product_id }}">
                                                {{ $tP->product->name }}
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{ route('locations.details') }}/{{ $tP->from }}">
                                                {{ $tP->locationFrom->name }}
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{ route('locations.details') }}/{{ $tP->to }}">
                                                {{ $tP->locationTo->name }}
                                            </a>
                                        </td>
                                        <td>{{ $tP->quantity }}</td>
                                        <td>{{ processDate($tP->created_at, true) }}</td>
                                        <td class="text-right">
                                            @if ($currentUser->can('edit_transferred_products'))
                                                <a href="{{ route('products.reverttransfer') }}/{{ $tP->id }}" title="Revert">
                                                    <i class="fa fa-rotate-left fa-fw"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @empty
                                    {{-- <tr>
                                        <td colspan="6" class="text-center active table-strong">No product transfers yet</td>
                                    </tr> --}}
                                @endforelse
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>

        </div>

    </div>

</div>
</div>
</div>
@endsection

@section('footer-addon')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script>
$(function() {

    function tabInfo(table) {
        var id = $(table).closest('.tab-pane').attr('id'),
            tab = $('.nav-tabs a[aria-controls='+id+']'),
            length = $(table).DataTable().page.info().recordsDisplay,
            label = tab.find('span');
        if (label.length) { label.remove(); }
        tab.append(' <span>('+length+')</span>');
    }

    var tables = $('.datatable')
        .on('draw.dt', function () {
            tabInfo(this);
        })
        .on('search.dt', function () {
            tabInfo(this);
        })
        .DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5',
                'print'
            ],
            pageLength: 10,
            ordering: true,
            lengthChange: false,
            pagingType: 'numbers',
            select: {
                style: 'single'
            },
            'order': [ 4, 'desc' ],
            'columnDefs': [
                {
                    'targets': [5],
                    'orderable': false
                }
            ]
        });

    $('#quick-search-input').on('keyup', function () {
        tables.search( this.value ).draw();
    });

});
</script>
@endsection
