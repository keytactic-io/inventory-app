<div class="form-error"></div>
<form action="{{ route('products.add.modal') }}" method="post" autocomplete="off" id="add-product-form">
{!! csrf_field() !!}
        
    <div class="form-group">
        <label for="photo">Photo</label>
        <input type="file" class="form-control" id="photo" name="photo" />
    </div>
    
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" />
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="form-group">
                <label for="default-vendor">Default Vendor</label>
                <select name="default_vendor" id="default-vendor" class="form-control"
                    data-live-search="true"
                >
                    <option value=""></option>
                    @foreach ($vendors as $vendor)
                        <option value="{{ $vendor->id }}"
                        >
                            {{ $vendor->name }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="form-group">
                <label for="reoder-point">Reorder Point</label>
                <input type="number" class="form-control" id="reoder-point" name="reorder_point" value="{{ old('reorder_point') }}" />
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="form-group">
                <label for="reoder-quantity">Reorder Quantity</label>
                <input type="number" class="form-control" id="reoder-quantity" name="reorder_quantity" value="{{ old('reorder_quantity') }}" />
            </div>
        </div>
    </div>

    <div class="form-group">
        <label for="description">Description</label>
        <textarea name="description" id="description" rows="2" class="form-control">{{ old('description') }}</textarea>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label for="opening-balance">Opening Balance</label>
                <input type="number" class="form-control" id="opening-balance" name="opening_balance" value="{{ old('opening_balance') }}" />
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label for="location-id">Location</label>
                <select name="location_id" id="location-id" class="form-control"
                    data-live-search="true"
                >
                    <option value=""></option>
                    @foreach ($locations as $location)
                        <option value="{{ $location->id }}"
                        >
                            {{ $location->name }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group">
                <label for="price">Price</label>
                <input type="number" step="0.01" class="form-control" id="price" name="price" value="{{ old('price') }}" />
            </div>        
        </div>
    </div>

    <div class="form-group fg-last">
        <label for="category">Category</label>
        @forelse ($parentCategories as $parentCat)
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="categories[{{ $parentCat->id }}][0]" value="1" class="parent-checkbox" 
                        id="parent-{{ $parentCat->id }}" data-id="{{ $parentCat->id }}"
                    />
                    <span><strong>{{ $parentCat->name }}</strong></span>
                </label>
            </div>
            @if (count($parentCat->childCategories) > 0)
                <?php $childCategories = $parentCat->childCategories; ?>
                <div  style="padding-left: 24px;">
                    @foreach ($childCategories as $childCat)
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="categories[{{ $parentCat->id}}][{{ $childCat->id }}]" value="1" 
                                    class="child-checkbox" data-parent="{{ $parentCat->id }}"
                                />
                                <span>{{ $childCat->name }}</span>
                            </label>
                        </div>
                    @endforeach
                </div>
            @endif
        @empty
            <p>No categories yet. <a href="{{ route('categories.add') }}">Click here to add</a></p>
        @endforelse
    </div>

</form>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js"></script>
<script>
$(function() {

    // $('#location-id, #default-vendor').selectpicker({
    //     style: 'btn-default',
    //     size: 3
    // });

    $(document).on('change', '.child-checkbox', function() {
        var parent = $(this).attr('data-parent'),
            count = 0;
        $('.child-checkbox[data-parent="' + parent + '"]').each(function() {
            if ($(this).prop('checked') == true) {
                count++;
            }
        });
        if (count > 0) {
            $('#parent-' + parent).prop('checked', true);
        }
    });

    $(document).on('change', '.parent-checkbox', function() {
        var children = $('.child-checkbox[data-parent="' + $(this).attr('data-id') + '"]'),
            count = 0;
        children.each(function() {
            if ($(this).prop('checked') == true) {
                count++;
            }
        });
        if (count > 0) {            
            $(this).prop('checked', true);
        }
    });

});
</script>