<tr id="{{ $uid }}-{{ $product->id }}">
    <td class="table-photo">
        <a id="details-a" href="{{ route('products.details') }}/{{ $product->id }}">
            <div class="table-user-dp" style="background: url({{ $product->photo() }}) center center no-repeat #ffffff; background-size: contain;"></div>
        </a>
    </td>
    <td>
        <a href="{{ route('products.details') }}/{{ $product->id }}" class="text-nowrap">
            <strong>{{ $product->name }}</strong>
        </a>
        @if (!empty($product->description) AND $product->description != null)
            <br /><small><em>{{ $product->description }}</em></small>
        @else
            <br /><small><em>-</em></small>
        @endif
        <br />
        <small>
            <i class="fa fa-tags"></i> &nbsp; 
            @foreach ($product->categories as $cat)
                @if ($loop->iteration > 1), @endif
                <a href="{{ route('products.list') }}/all/{{ $cat->category->id }}">
                    {{ $cat->category->name }}
                </a>
            @endforeach
        </small>
    </td>
    <td>
        <a href="{{ route('vendors.details') }}/{{ $product->default_vendor }}">
            {{ $product->defaultVendor->name }}
        </a>
    </td>
    <td>
        {{ number_format($product->remaining_stocks, '0', '.', ',') }}
    </td>
    <td>
        {{ number_format($product->latestPrice(), '2', '.', ',') }}
    </td>
    <td class="nice-table-actions text-nowrap">
        @if (!$currentUser->is_requester)

            @if ($currentUser->allowedTo('delete_products'))
                <a href="#" title="Delete" onclick="promptDelete(
                        'Are you sure you want to delete this product?',
                        '{{ route('products.delete') }}/{{ $product->id }}'
                    );"
                >
                    <i class="fa fa-trash-o fa-fw"></i>
                </a>
                &nbsp;&nbsp;
            @endif

            @if ($currentUser->allowedTo('edit_products'))
                <a href="{{ route('products.edit') }}/{{ $product->id }}" title="Edit">
                    <i class="fa fa-pencil fa-fw"></i>
                </a>
            @endif

            @if (($product->isOutOfStock() OR $product->isLowStock()) AND $currentUser->allowedTo('add_purchase_orders'))
                <a href="{{ route('pos.create.specific') }}/{{ $product->id }}" title="Create Purchase Order">
                    <i class="fa fa-file-text-o fa-fw"></i>
                </a>
            @endif

        @endif
    </td>
</tr>
