@extends('layouts.app')

@section('page-title', 'Edit Product')

@section('head-addon')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css">
<link rel="stylesheet" href="{{ asset('assets/css/bt-select-override.css') }}" />
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.errors')

    <form action="{{ route('products.edit') }}/{{ $product->id }}" method="post" 
        autocomplete="off" enctype="multipart/form-data"
        class="with-preloader" id="edit-product-form"
    >

        <div class="row">
            <div class="col-xs-12 col-lg-6">
                
                <div class="panel">
                    <div class="panel-heading">
                        <h4>Product Details</h4>
                    </div>
                    <div class="panel-body">
                        
                        <div class="form-horizontal">
                            
                            <div class="form-group">
                                <label for="photo" class="control-label col-xs-12 col-sm-3">Photo</label>
                                <div class="col-xs-12 col-sm-9">
                                    <img src="{{ $product->photo() }}" alt="" height="80px" style="margin-bottom: 12px;" />
                                    <input type="file" class="form-control" id="photo" name="photo" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="name" class="control-label col-xs-12 col-sm-3">Name</label>
                                <div class="col-xs-12 col-sm-9">
                                    <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $product->name) }}" required />
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="name" class="control-label col-xs-12 col-sm-3">Default Vendor</label>
                                <div class="col-xs-12 col-sm-9">
                                    <select name="vendor_id" id="vendor-id" class="form-control"
                                        data-live-search="true" required
                                    >
                                        <option value=""></option>
                                        @foreach ($vendors as $vendor)
                                            <option value="{{ $vendor->id }}"
                                                @if (old('vendor_id', $product->default_vendor) == $vendor->id) selected @endif
                                            >
                                                {{ $vendor->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="reoder-point" class="control-label col-xs-12 col-sm-3">Reorder Point</label>
                                <div class="col-xs-12 col-sm-9">
                                    <input type="number" class="form-control auto" id="reoder-point" name="reorder_point" 
                                        value="{{ old('reorder_point', $product->reorder_point) }}" required 
                                    />
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="reoder-quantity" class="control-label col-xs-12 col-sm-3">Reorder Quantity</label>
                                <div class="col-xs-12 col-sm-9">
                                    <input type="number" class="form-control auto" id="reoder-quantity" name="reorder_quantity" 
                                        value="{{ old('reorder_quantity', $product->reorder_quantity) }}" required 
                                    />
                                </div>
                            </div>

                            <div class="form-group fg-last">
                                <label for="description" class="control-label col-xs-12 col-sm-3">Description</label>
                                <div class="col-xs-12 col-sm-9">
                                    <textarea name="description" id="description" rows="2" class="form-control">{{ old('description', $product->description) }}</textarea>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

            </div>
            <div class="col-xs-12 col-lg-6">

                <div class="panel">
                    <div class="panel-heading">
                        <h4>Category</h4>
                    </div>
                    <div class="panel-body">
                        
                        <div class="form-horizontal">
                            
                            <div class="form-group last">
                                <div class="col-xs-12">
                                    @forelse ($parentCategories as $parentCat)
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="categories[{{ $parentCat->id }}][0]" value="1" class="parent-checkbox" 
                                                    id="parent-{{ $parentCat->id }}" data-id="{{ $parentCat->id }}"
                                                />
                                                <span><strong>{{ $parentCat->name }}</strong></span>
                                            </label>
                                        </div>
                                        @if (count($parentCat->childCategories) > 0)
                                            <?php $childCategories = $parentCat->childCategories; ?>
                                            <div  style="padding-left: 24px;">
                                                @foreach ($childCategories as $childCat)
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" name="categories[{{ $parentCat->id}}][{{ $childCat->id }}]" value="1" 
                                                                class="child-checkbox" data-parent="{{ $parentCat->id }}"
                                                            />
                                                            <span>{{ $childCat->name }}</span>
                                                        </label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        @endif
                                    @empty
                                        <p>No categories yet. <a href="{{ route('categories.add') }}">Click here to add</a></p>
                                    @endforelse
                                </div>
                            </div>

                        </div>

                    </div>
                    @if (count($customFields) == 0)
                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary">Submit Changes</button>
                            <a href="{{ route('products.list') }}" class="btn btn-link">Cancel</a>
                        </div>
                    @endif
                </div>
                
                @if (count($customFields) > 0)
                    <div class="panel">
                        <div class="panel-heading">
                            <h4>Custom Fields</h4>
                        </div>
                        <div class="panel-body">

                            <div class="form-horizontal">

                                @foreach ($customFields as $cf)
                                    <div class="form-group @if ($loop->last) fg-last @endif">
                                        <label for="cf-{{ $cf->slug }}" class="control-label col-xs-12 col-sm-3">{{ $cf->label }}</label>
                                        <div class="col-xs-12 col-sm-9">
                                            @if ($cf->type == 'input')
                                                <input type="text" class="form-control" name="custom_fields[{{ $cf->id }}]" id="cf-{{ $cf->slug }}" value="{{ $product->customFieldValue($cf->id) }}" />
                                            @elseif ($cf->type == 'textarea')
                                                <textarea class="form-control autosize" name="custom_fields[{{ $cf->id }}]" id="cf-{{ $cf->slug }}" rows="2">{{ $product->customFieldValue($cf->id) }}</textarea>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                            
                        </div>
                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary">Submit Changes</button>
                            <a href="{{ route('products.list') }}" class="btn btn-link">Cancel</a>
                        </div>
                    </div>

                @endif

            </div>
        </div>

        {!! csrf_field() !!}
        <input type="hidden" name="product_id" value="{{ $product->id }}" />
    </form>

</div>
</div>
</div>
@endsection

@section('footer-addon')
<script src="{{ asset('assets/js/vendors/autosize.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js"></script>
<script>
$(function() {

    autosize($('#description'));

    $('#opening-balance').on('keyup', function() {
        if (parseFloat($(this).val()) > 0) {
            $('#price').prop('required', true);
            $('#location-id').prop('required', true);
        } else {
            $('#price').prop('required', false);
            $('#location-id').prop('required', false);
        }
    });

    $('#location-id, #vendor-id').selectpicker({
        style: 'btn-default',
        size: 6
    });

    $(document).on('change', '.child-checkbox', function() {
        var parent = $(this).attr('data-parent'),
            count = 0;
        $('.child-checkbox[data-parent="' + parent + '"]').each(function() {
            if ($(this).prop('checked') == true) {
                count++;
            }
        });
        if (count > 0) {
            $('#parent-' + parent).prop('checked', true);
        }
    });

    $(document).on('change', '.parent-checkbox', function() {
        var children = $('.child-checkbox[data-parent="' + $(this).attr('data-id') + '"]'),
            count = 0;
        children.each(function() {
            if ($(this).prop('checked') == true) {
                count++;
            }
        });
        if (count > 0) {            
            $(this).prop('checked', true);
        }
    });

});
</script>
@endsection