@extends('layouts.app')

@section('page-title', 'Products')

@section('head-addon')
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.0/css/select.dataTables.min.css" />
<link rel="stylesheet" href="{{ asset('assets/css/dt-override.css') }}" />
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.status')
    @include('includes.errors')
    
</div>
<div class="col-xs-12">

    <div class="panel">
        <div class="panel-heading">
            <h4>
                Products
                @if ($category)
                    ({{ $category->name }})
                @endif
            </h4>
        </div>

        <div class="panel-filter">
    		@if ($currentUser->allowedTo('add_products'))
            <div class="input-group">
                <input type="text" placeholder="Search products" class="form-control" id="quick-search-input" autocomplete="off">
    								<span class="input-group-btn">
                          <a href="{{ route('products.add') }}" class="btn btn-primary btn-squared">
                            <i class="fa fa-plus"></i>
                          </a>
                    </span>
            </div>
    		@else
    						<input type="text" placeholder="Search products" class="form-control" id="quick-search-input" autocomplete="off">
        @endif
        </div>

        <div class="panel-preloader" data-target="#products-panel-body">
            <i class="fa fa-circle-o-notch fa-spin fa-fw"></i> Preparing products table
        </div>
        <div id="products-panel-body" class="panel-body hidden" style="display:none;">

            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab">All</a></li>
                <li role="presentation"><a href="#in-stock" aria-controls="in-stock" role="tab" data-toggle="tab">In-Stock</a></li>
                <li role="presentation"><a href="#low-stock" aria-controls="low-stock" role="tab" data-toggle="tab">Low Stock</a></li>
                <li role="presentation"><a href="#out-stock" aria-controls="out-stock" role="tab" data-toggle="tab">Out of Stock</a></li>
            </ul>

            <div class="tab-content">

                <!-- all -->
                <div role="tabpanel" class="tab-pane active" id="all">

                    <div class="table-responsive">
                        <table id="products-all" class="table datatable table-hover">
                            <thead>
                                <tr>
                                    <th>Photo</th>
                                    <th>Name</th>
                                    <th>Categories</th>
                                    <th>Remaining</th>
                                    <th>Latest Unit Price</th>
                                    <th>ID</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($products as $product)
                                    @include('pages.products.row', [
                                        'uid' => 'products-all-entry',
                                        'product' => $product
                                    ])
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>

                <!-- in-stock -->
                <div role="tabpanel" class="tab-pane" id="in-stock">

                    <div class="table-responsive">
                        <table id="products-in-stock" class="table datatable table-hover">
                            <thead>
                                <tr>
                                    <th>Photo</th>
                                    <th>Name</th>
                                    <th>Categories</th>
                                    <th>Remaining</th>
                                    <th>Latest Unit Price</th>
                                    <th>ID</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($inStockProducts as $product)
                                    @include('pages.products.row', [
                                        'uid' => 'products-in-stock-entry',
                                        'product' => $product
                                    ])
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>

                <!-- low inventory -->
                <div role="tabpanel" class="tab-pane" id="low-stock">

                    <div class="table-responsive">
                        <table id="products-low-stock" class="table datatable table-hover">
                            <thead>
                                <tr>
                                    <th>Photo</th>
                                    <th>Name</th>
                                    <th>Categories</th>
                                    <th>Remaining</th>
                                    <th>Latest Unit Price</th>
                                    <th>ID</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($lowStockProducts as $product)
                                    @include('pages.products.row', [
                                        'uid' => 'products-low-stock-entry',
                                        'product' => $product
                                    ])
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>

                <!-- out of stock -->
                <div role="tabpanel" class="tab-pane" id="out-stock">

                    <div class="table-responsive">
                        <table id="products-out-stock" class="table datatable table-hover">
                            <thead>
                                <tr>
                                    <th>Photo</th>
                                    <th>Name</th>
                                    <th>Categories</th>
                                    <th>Remaining</th>
                                    <th>Latest Unit Price</th>
                                    <th>ID</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($outStockProducts as $product)
                                    @include('pages.products.row', [
                                        'uid' => 'products-out-stock-entry',
                                        'product' => $product
                                    ])
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>

        </div>

    </div>

</div>
</div>
@endsection

@section('footer-addon')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script>
$(function() {

    function tabInfo(table) {
        var id = $(table).closest('.tab-pane').attr('id'),
            tab = $('.nav-tabs a[aria-controls='+id+']'),
            length = $(table).DataTable().page.info().recordsDisplay,
            label = tab.find('span');
        if (label.length) { label.remove(); }
        tab.append(' <span>('+length+')</span>');
    }

    var tables = $('.datatable')
        .on('draw.dt', function () {
            tabInfo(this);
        })
        .on('search.dt', function () {
            tabInfo(this);
        })
        .DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5',
                'print'
            ],
            pageLength: 10,
            ordering: true,
            lengthChange: false,
            pagingType: 'numbers',
            // select: {
            //     style: 'single'
            // },
            'order': [ 1, 'asc' ],
            'columnDefs': [
                {
                    'targets': [5],
                    'visible': false
                }
            ]
        })
        .on( 'select', function ( e, dt, type, indexes ) {
            // var data = $(this).DataTable().rows(indexes).data()[0],
            //     id = data[5],
            //     previewContainer = $('#product-preview');
            //     url = previewContainer.attr('data-preview-url');

            // previewContainer.append('<div class="preview-preloader-overlay"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i></div>');
            // $.get( url + '/' + id ).done(function(r) {
            //     previewContainer.html(r.view);
            // });
        });

    $('#quick-search-input').on('keyup', function () {
        tables.search( this.value ).draw();
    });

    $(document).on('click', '#delete-a', function(e) {
        e.preventDefault();
        var url = $(this).data('url');
        bootbox.dialog({
            title: 'Hey!',
            message: '<div style="font-size: 16px;">Are you sure you want to delete this product?</div>',
            className: 'modal-danger',
            onEscape: function () {
                bootbox.hideAll();
            },
            buttons: {
                'confirm': {
                    label: 'Yes',
                    className: 'btn-danger',
                    callback: function() {
                        window.location = url;
                    }
                },
                'Close' : {
                    className: 'btn-default'
                }
            }
        });
    });    

});
</script>
@endsection
