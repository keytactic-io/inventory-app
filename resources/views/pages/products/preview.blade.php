<div class="qp-photo" style="background: url({{ $product->photo() }}) center center no-repeat; background-size: contain; "></div>
<br clear="all" />
<div class="qp-info">

    <h3><a id="details-a" href="{{ route('products.details') }}/{{ $product->id }}">{{ $product->name }}</a></h3>
    <div class="qp-meta">

        @if ($product->description)
            <p class="description">{{ $product->description }}</p>
        @endif
        <small>
            @foreach ($product->categories as $cat)
                @if ($loop->iteration > 1), @endif
                {{ $cat->category->name }}
            @endforeach
        </small>
        <p class="remaining" style="margin-top: 20px;">Remaining: <strong>{{ number_format($product->stocksRemaining(), 0, '.', ',') }}</strong></p>
        <p class="reorder-point">Re-order Point: <strong>{{ number_format($product->reorder_point, 0, '.', ',') }}</strong></p>
        <p class="default-vendor">Vendor: <strong><a href="{{ route('vendors.details') }}/{{ $product->default_vendor }}">{{ $product->defaultVendor->name }}</a></strong></p>
        @if (count($product->customFields()) > 0)
            <p>
                @foreach ($product->customFields() as $customField)
                    {{ $customField->cf->label }}: <strong>{{ $customField->value }}</strong><br />
                @endforeach
            </p>
        @endif

    </div>

</div>
<div class="qp-options clearfix">

    <a id="details-a" href="{{ route('products.details') }}/{{ $product->id }}" class="btn btn-primary btn-sm">Details</a>
    @if (!$currentUser->is_requester)

        @if ($currentUser->allowedTo('edit_products'))
            <a id="edit-btn" href="{{ route('products.edit') }}/{{ $product->id }}" class="btn btn-warning btn-sm">Edit</a>
        @endif

        @if ($currentUser->allowedTo('delete_products'))
            <a class="btn btn-danger btn-sm" id="delete-a" href="#" data-url="{{ route('products.delete') }}/{{ $product->id }}">Delete</a>
        @endif

        @if (($product->isOutOfStock() OR $product->isLowStock()) AND $currentUser->allowedTo('add_purchase_orders'))
            <a class="btn btn-success btn-sm" id="create-po" href="{{ route('pos.create.specific') }}/{{ $product->id }}">Create PO</a>
        @endif

    @endif

</div>
