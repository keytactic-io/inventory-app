@extends('layouts.app')

@section('page-title', $product->name)

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.status')

</div>
<div class="col-xs-12">

    <div class="panel">

        <div class="panel-body">

            <div class="details-content">

                <div class="details-main-info clearfix">

                    <div class="details-photo" style="background:url({{ $product->photo() }}) center center no-repeat;background-size:contain;"></div>

                    <h1>{{ $product->name }}</h1>
                    <h4 title="Categories"><i class="fa fa-tags fa-fw"></i> &nbsp;
                        @foreach ($product->categories as $cat)
                            <a href="#">{{ $cat->category->name }}</a><?php if ($loop->iteration != $loop->count): ?>, <?php endif; ?>
                        @endforeach
                    </h4>
                    <h4 title="Re-order Point"><i class="fa fa-refresh fa-fw"></i> &nbsp; {{ number_format($product->reorder_point, 0, '.', ',') }}</h4>
                    <h4 title="Latest Price"><i class="fa fa-money fa-fw"></i> &nbsp; {{ number_format($product->latestPrice(), 2, '.', ',') }}</h4>
                    <br />
                    @if (count($product->customFields()) > 0)
                        <h4>
                            @foreach ($product->customFields() as $customField)
                            <div class="col-xs-12 col-sm-6" style="margin-left: -12px;">
                                <strong>{{ $customField->cf->label }}</strong>: {{ $customField->value }}
                            </div>
                            @endforeach
                        </h4>
                    @endif

                </div>

                @if ($product->description != '' OR $product->description != null)
                    <div class="form-group-divider"><span></span></div>

                    <div class="details-more-info">
                        <p>{{ $product->description }}</p>
                    </div>
                @endif

            </div>

        </div>
        @if ($currentUser->allowedTo('edit_products'))
            <div class="panel-footer">
				<a href="{{ route('products.edit') }}/{{ $product->id }}" class="btn btn-warning btn-lg">Edit</a>
                <a id="remove-photo-a" href="{{ route('products.remove-photo') }}/{{ $product->id }}" class="btn btn-danger btn-lg">Remove Photo</a>
            @if ($currentUser->allowedTo('delete_products'))
                <a id="delete-a" href="{{ route('products.delete') }}/{{ $product->id }}" class="btn btn-danger btn-lg">Delete</a>
            @endif
            @if (($product->isOutOfStock() OR $product->isLowStock()) AND $currentUser->allowedTo('add_purchase_orders'))
                <a id="create-po" href="{{ route('pos.create.specific') }}/{{ $product->id }}" class="btn btn-success btn-lg pull-right">Create PO</a>
            @endif
				<div class="clearfix"></div>
            </div>
        @endif

    </div>

</div>
<div class="col-xs-12 col-lg-8">

    <div class="panel">

        <div class="panel-heading">
            <h4>Product Locations</h4>
        </div>

        <div class="panel-body">

            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Location</th>
                            <th>Exact Address</th>
                            <th class="text-right">Remaining</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $count = 0;
                            $showZero = app_setting('show_zero_locations') == 1 ? true : false;
                        ?>
                        @foreach ($locations as $location)
                            @if ($showZero)
                                <tr>
                                    <td class="table-strong">
                                        <a href="{{ route('locations.details') }}/{{ $location->id }}">
                                            {{ $location->name }}
                                        </a>
                                    </td>
                                    <td>{{ $location->exact_location }}</td>
                                    <td class="text-right">{{ number_format($location->stocksRemaining($product->id), '0', '.', ',') }}</td>
                                </tr>
                            @else
                                @if ($location->stocksRemaining($product->id) > 0)
                                    <tr>
                                        <td class="table-strong">
                                            <a href="{{ route('locations.details') }}/{{ $location->id }}">
                                                {{ $location->name }}
                                            </a>
                                        </td>
                                        <td>{{ $location->exact_location }}</td>
                                        <td class="text-right">{{ number_format($location->stocksRemaining($product->id), '0', '.', ',') }}</td>
                                    </tr>
                                    <?php $count++; ?>
                                @endif
                            @endif
                        @endforeach

                        @if (!$showZero AND $count == 0)
                            <tr>
                                <td colspan="3" class="text-center active table-strong">
                                    No products found on all locations
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>

    </div>

</div>
<div class="col-xs-12 col-lg-4">

    <div class="panel">

        <div class="panel-heading">
            <h4>Price Logs</h4>
        </div>

        <div class="panel-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Date Logged</th>
                            <th class="text-right">Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($priceLogs as $priceLog)
                            <tr>
                                <td>{{ processDate($priceLog->created_at, true) }}</td>
                                <td class="text-right table-strong">{{ number_format($priceLog->price, '2', '.', ',') }}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="2" class="text-center active table-strong">No price logs yet</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>

    </div>

</div>
</div>
</div>
@endsection
