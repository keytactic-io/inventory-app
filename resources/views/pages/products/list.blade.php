@extends('layouts.app')

@section('page-title', 'Products')

@section('head-addon')
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.0/css/select.dataTables.min.css" />
<link rel="stylesheet" href="{{ asset('assets/css/dt-override.css') }}" />
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.status')
    @include('includes.errors')

    <div class="page-header">
        <h2>Products</h2>
        @if ($currentUser->allowedTo('add_products'))
            <div class="page-head-actions">
                <a href="{{ route('products.add') }}" class="btn btn-primary btn-sm">
                    Add Product
                </a>
            </div>
        @endif
    </div>

    <div class="table-filter">

        <div class="row">

            <div class="col-xs-12 col-md-6">
                <div class="form-group">
                    <form action="{{ Request::url() }}" autocomplete="off" method="get" id="search-form">
                        <div class="input-group">
                            <input type="text" id="s" name="s" class="form-control" value="{{ $keyword }}" placeholder="Keyword" />
                            <span class="input-group-btn">
                                <button class="btn btn-primary btn-squared" type="submit"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="form-group">
                    <select id="stock-filter" class="form-control" data-url="{{ route('products.list') }}" data-category="{{ $currentCategory }}">
                        <option value="all" @if ($filter == 'all') selected @endif>All</option>
                        <option value="in-stock" @if ($filter == 'in-stock') selected @endif>In-Stock</option>
                        <option value="low-stock" @if ($filter == 'low-stock') selected @endif>Low Stock</option>
                        <option value="out-of-stock" @if ($filter == 'out-of-stock') selected @endif>Out of Stock</option>
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="form-group fg-last">
                    <select id="category-filter" class="form-control" data-url="{{ route('products.list') }}" data-filter="{{ $filter }}">
                        <option value="">All Categories</option>
                        @foreach ($categories as $category)
                            <option value="{{ $category->id }}"
                                @if ($currentCategory == $category->id) selected @endif
                            >
                                {{ $category->name }}
                            </option>
                            @if (count($category->childCategories) > 0)
                                @foreach ($category->childCategories as $childCategory)
                                    <option value="{{ $childCategory->id }}"
                                        @if ($currentCategory == $childCategory->id) selected @endif
                                    >
                                        &mdash; {{ $childCategory->name }}
                                    </option>
                                @endforeach
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

    </div>

    <div id="the-list">
        @include('pages.products.nice-list')
    </div>

</div>
</div>
@endsection

@section('footer-addon')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script>
$(function () {

    $(document)
        .on('change', '#stock-filter', function (e) {
            e.preventDefault();
            var categoryFilter = $('#category-filter'),
                value = $(this).val(),
                url = $(this).data('url');
            categoryFilter.data('filter', value);

            var newUrl = $(this).data('category') == 0 ? url + '/' + value : 
                url + '/' + value + '/' + $(this).data('category');

            $('#s').val('');

            updateAndGet(newUrl);
        })
        .on('change', '#category-filter', function (e) {
            e.preventDefault();
            var stockFilter = $('#stock-filter'),
                value = $(this).val(),
                url = $(this).data('url');
            stockFilter.data('category', value == '' ? 0 : value);

            var newUrl = value == '' ? url + '/' + $(this).data('filter') :
                url + '/' + $(this).data('filter') + '/' + value;

            $('#s').val('');

            updateAndGet(newUrl);
        })
        .on('submit', '#search-form', function (e) {
            e.preventDefault();
            var form = $('#search-form'),
                field = $('#search-form #s');
            if (field.val() != '' && field.val() != null) {
                var input = form.serialize();
                var newUrl = form.attr('action') + '?' + input;

                updateAndGet(newUrl);
            } else {
                updateAndGet(form.attr('action'));
            }
        });

    function updateAndGet(url)
    {
        loader.show();
        history.pushState(null, null, url);

        $('#search-form')
            .attr('action', 
                (url.indexOf('?s=') > 0) ? 
                    url.substr(0, url.indexOf('?s=')) : 
                    url
            );
        $('#stock-filter, #category-filter, #s').blur();

        $.get(url.replace('list/', 'list/json-'))
            .done(function (r) {
                $('#the-list').html(r.newList);
                loader.hide();
            })
            .fail(function (r) {
                location.reload();
            });
    }

});
</script>
@endsection
