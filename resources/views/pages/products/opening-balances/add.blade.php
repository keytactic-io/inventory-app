@extends('layouts.app')

@section('page-title', 'Add Opening Balance')

@section('head-addon')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css">
<link rel="stylesheet" href="{{ asset('assets/css/bt-select-override.css') }}" />
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.errors')

    <form action="{{ route('products.ob.add') }}" method="post" autocomplete="off"
        id="add-opening-balance-form" class="with-preloader"
    >
        {!! csrf_field() !!}

        <div class="panel">
            <div class="panel-heading">
                <h4>Add Opening Balance</h4>
            </div>

            <div class="panel-body">
                
                <div class="opening-balances">

                    <div class="ob-entry row" data-count="1">
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="form-group clearfix">
                                <label for="product-id-1">Product</label>
                                <select id="product-id-1" name="products[1][product_id]" class="form-control products-list" 
                                    data-live-search="true" required 
                                >
                                    <option value=""></option>
                                    @foreach ($products as $product)
                                        <option value="{{ $product->id }}"
                                        >
                                            {{ $product->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="form-group">
                                <label for="location-id-1">Location</label>
                                <select id="location-id-1" class="form-control locations-list" name="products[1][location_id]" 
                                    data-live-search="true" required
                                >
                                    <option value=""></option>
                                    @foreach ($locations as $location)
                                        <option value="{{ $location->id }}">{{ $location->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-2">
                            <div class="form-group">
                                <label for="price-1">Price</label>
                                <input type="number" name="products[1][price]" id="price-1" step="0.001" min="0" class="form-control" required />
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-2">
                            <div class="form-group">
                                <label for="lquantity-1">Quantity</label>
                                <input type="number" min="0" name="products[1][quantity]" id="quantity-1" class="form-control" required />
                            </div>
                        </div>
                    </div>

                </div>
                <div style="padding-top: 24px;">
                    <a href="#" class="btn btn-primary btn-sm add-ob-item" data-url="{{ route('products.ob.newfield') }}">Add Entry</a>
                </div>

            </div>
            
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ route('products.ob.list') }}" class="btn btn-default">Cancel</a>
            </div>

        </div>
    </form>

</div>
</div>
</div>
@endsection

@section('footer-addon')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js"></script>
<script>
$(function() {

    $('.locations-list, .products-list').selectpicker({
        style: 'btn-default',
        size: 4
    });

    $(document).on('click', '.remove-tr', function(e) {
        e.preventDefault();
        $($(this).attr('data-target')).remove();
        if ($('#locations-table tbody tr').length == 0) {
            var html = '<tr class="no-data"><td colspan="3" class="text-center active text-center table-strong">Please select at least one (1) location</td></tr>';
            $('#locations-table tbody').html(html);
        }
    });

    $(document).on('click', '.add-ob-item', function(e) {
        e.preventDefault();
        loader.show();
        var indexes = [],
            count = 0;
        $('.ob-entry').each(function() {
            indexes[count] = $(this).data('count');
            count++;
        });
        var newCount = Math.max.apply(Math, indexes) + 1;
        $.get( $(this).data('url'), {c: newCount } )
            .done(function(r) {
                $('.opening-balances').append(r.view);
                $('.locations-list, .products-list').selectpicker({
                    style: 'btn-default',
                    size: 5
                });
                loader.hide();
            })
            .fail(function(r) {
                loader.hide();
            });
    });

    $(document).on('click', '.remove-ob-entry', function(e) {
        e.preventDefault();
        $(this).closest('.ob-entry').remove();
    });

});
</script>
@endsection
