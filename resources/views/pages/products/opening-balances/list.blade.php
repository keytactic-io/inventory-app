@extends('layouts.app')

@section('page-title', 'Opening Balances')

@section('head-addon')
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.0/css/select.dataTables.min.css" />
<link rel="stylesheet" href="{{ asset('assets/css/dt-override.css') }}" />
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.status')

    <div class="panel">

        <div class="panel-heading">
            <h4>Opening Balances</h4>
        </div>

        <div class="panel-preloader" data-target="#opening-balances-panel-body">
            <i class="fa fa-circle-o-notch fa-spin fa-fw"></i> Preparing opening balances table
        </div>
        <div id="opening-balances-panel-body" class="panel-body hidden" style="display:none;">
            <div class="table-responsive">
                <table class="table datatable">
                    <thead>
                        <tr>
                            <th>Photo</th>
                            <th>Name</th>
                            <th class="text-right">Quantity</th>
                            <th>Location</th>
                            <th>Created By</th>
                            <th>Created At</th>
                            <th class="text-right">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($openingBalances as $oB)
                            <tr>
                                <td>
                                    <div class="table-user-dp" style="background:url({{ $oB->product->photo() }}) center center no-repeat;background-size:cover;"></div>
                                </td>
                                <td class="table-strong">
                                    <a href="{{ route('products.details') }}/{{ $oB->product_id }}">
                                        {{ $oB->product->name }}
                                    </a>
                                </td>
                                <td class="text-right">{{ $oB->quantity }}</td>
                                <td>
                                    <a href="{{ route('locations.details') }}/{{ $oB->location_id }}">
                                        {{ $oB->location->name }}
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ route('users.profile') }}/{{ $oB->created_by }}">
                                        {{ $oB->createdBy->name() }}
                                    </a>
                                </td>
                                <td>{{ processDate($oB->created_at, true) }}</td>
                                <td class="text-right">
                                    @if ($currentUser->allowedTo('edit_opening_balances'))
                                        <a href="{{ route('products.ob.edit') }}/{{ $oB->id }}">
                                            <i class="fa fa-pencil fa-fw"></i>
                                        </a>
                                    @endif
                                    @if ($currentUser->allowedTo('delete_opening_balances'))
                                        <a href="#" class="delete-ob" data-url="{{ route('products.ob.delete') }}/{{ $oB->id }}">
                                            <i class="fa fa-trash-o fa-fw"></i>
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="7" class="text-center active table-strong">No opening balances yet</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        
        @if ($currentUser->allowedTo('add_opening_balances'))
            {{-- <div class="panel-footer">
                <a href="{{ route('products.ob.add') }}" class="btn btn-primary">Add Opening Balance</a>
            </div> --}}
        @endif

    </div>

</div>
</div>
</div>
@endsection

@section('footer-addon')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script>
$(function() {

    $(document).on('click', '.delete-ob', function(e) {
        e.preventDefault();
        var url = $(this).data('url');
        bootbox.dialog({
            title: 'Hey!',
            message: '<div style="font-size: 16px;">Are you sure you want to delete this opening balance?</div>',
            className: 'modal-danger',
            onEscape: function () {
                bootbox.hideAll();
            },
            buttons: {
                'confirm': {
                    label: 'Yes',
                    className: 'btn-danger',
                    callback: function() {
                        window.location = url;
                    }
                },
                'Close' : {
                    className: 'btn-default'
                }
            }
        });
    });

    $('.datatable').DataTable({
        pageLength: 20,
        ordering: true,
        lengthChange: false,
        pagingType: 'numbers',
        'order': [ 5, 'asc' ],
        'columnDefs': [
            {
                'targets': [0, 6],
                'orderable': false
            }
        ]
    });

});
</script>
@endsection
