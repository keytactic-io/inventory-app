@extends('layouts.app')

@section('page-title', 'Add Opening Balance')

@section('head-addon')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css">
<link rel="stylesheet" href="{{ asset('assets/css/bt-select-override.css') }}" />
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.errors')

    <form action="{{ route('products.ob.edit') }}/{{ $openingBalance->id }}" method="post" autocomplete="off"
        id="edit-opening-balance-form" class="with-preloader"
    >
        {!! csrf_field() !!}

        <div class="panel">
            <div class="panel-heading">
                <h4>Edit Opening Balance</h4>
            </div>

            <div class="panel-body">
                
                <div class="opening-balances">

                    <div class="ob-entry row">
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="form-group clearfix">
                                <label for="product-id">Product</label>
                                <select id="product-id" name="product_id" class="form-control products-list" 
                                    data-live-search="true" required 
                                >
                                    <option value=""></option>
                                    @foreach ($products as $product)
                                        <option value="{{ $product->id }}"
                                            @if ($openingBalance->product_id == $product->id) selected @endif
                                        >
                                            {{ $product->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="form-group">
                                <label for="location-id">Location</label>
                                <select id="location-id" class="form-control locations-list" name="location_id" 
                                    data-live-search="true" required
                                >
                                    <option value=""></option>
                                    @foreach ($locations as $location)
                                        <option value="{{ $location->id }}"
                                            @if ($openingBalance->location_id == $location->id) selected @endif
                                        >
                                            {{ $location->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-2">
                            <div class="form-group">
                                <label for="price-1">Price</label>
                                <input type="number" name="price" id="price" step="0.001" min="0" class="form-control" required 
                                    value="{{ $openingBalance->product->latestPrice() }}"
                                />
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-2">
                            <div class="form-group">
                                <label for="lquantity-1">Quantity</label>
                                <input type="number" min="0" name="quantity" id="quantity" class="form-control" required 
                                    value="{{ $openingBalance->quantity }}"
                                />
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ route('products.ob.list') }}" class="btn btn-default">Cancel</a>
            </div>

        </div>
    </form>

</div>
</div>
</div>
@endsection

@section('footer-addon')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js"></script>
<script>
$(function() {

    $('.locations-list, .products-list').selectpicker({
        style: 'btn-default',
        size: 4
    });

});
</script>
@endsection
