<div class="ob-entry" data-count="{{ $count }}">
    <div class="panel-separator"><span></span></div>
    <div style="padding-bottom: {{ $count }}2px">
        <a href="#" class="btn btn-default btn-sm remove-ob-entry">Remove Entry</a>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="form-group clearfix">
                <label for="product-id-{{ $count }}">Product</label>
                <select id="product-id-{{ $count }}" name="products[{{ $count }}][product_id]" class="form-control products-list" 
                    data-live-search="true" required 
                >
                    <option value=""></option>
                    @foreach ($products as $product)
                        <option value="{{ $product->id }}"
                        >
                            {{ $product->name }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="form-group">
                <label for="location-id-{{ $count }}">Location</label>
                <select id="location-id-{{ $count }}" class="form-control locations-list" name="products[{{ $count }}][location_id]" 
                    data-live-search="true" required
                >
                    <option value=""></option>
                    @foreach ($locations as $location)
                        <option value="{{ $location->id }}">{{ $location->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-2">
            <div class="form-group">
                <label for="price-{{ $count }}">Price</label>
                <input type="number" name="products[{{ $count }}][price]" id="price-{{ $count }}" step="0.001" min="0" class="form-control" required />
            </div>
        </div>
        <div class="col-xs-12 col-sm-2">
            <div class="form-group">
                <label for="lquantity-{{ $count }}">Quantity</label>
                <input type="number" min="0" name="products[{{ $count }}][quantity]" id="quantity-{{ $count }}" class="form-control" required />
            </div>
        </div>
    </div>
</div>