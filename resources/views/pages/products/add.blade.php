@extends('layouts.app')

@section('page-title', 'Add Product')

@section('head-addon')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css">
<link rel="stylesheet" href="{{ asset('assets/css/bt-select-override.css') }}" />
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.errors')

    <form action="{{ route('products.add') }}" method="post" autocomplete="off" 
        enctype="multipart/form-data" id="add-product-form" class="with-preloader" 
    >
        <div class="row">
            <div class="col-xs-12 col-lg-6">

                <div class="panel">
                    <div class="panel-heading">
                        <h4>Product Details</h4>
                    </div>
                    <div class="panel-body">

                        <div class="form-horizontal">
                            
                            <div class="form-group">
                                <label for="p-photo" class="control-label col-xs-12 col-sm-3">Photo</label>
                                <div class="col-xs-12 col-sm-9">
                                    <input type="file" class="form-control" id="p-photo" name="photo" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="p-name" class="control-label col-xs-12 col-sm-3">Name</label>
                                <div class="col-xs-12 col-sm-9">
                                    <input type="text" class="form-control" id="p-name" name="name" 
                                        value="{{ old('name') }}" required 
                                    />
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="p-default-vendor" class="control-label col-xs-12 col-sm-3">Default Vendor</label>
                                <div class="col-xs-12 col-sm-9">
                                    <select name="vendor_id" id="p-default-vendor" class="form-control"
                                        data-live-search="true" required 
                                    >
                                        <option value=""></option>
                                        @foreach ($vendors as $vendor)
                                            <option value="{{ $vendor->id }}"
                                                @if (old('vendor_id') == $vendor->id) selected @endif 
                                            >
                                                {{ $vendor->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="p-reoder-point" class="control-label col-xs-12 col-sm-3">Reorder Point</label>
                                <div class="col-xs-12 col-sm-9">
                                    <input type="number" class="form-control auto" id="p-reoder-point" name="reorder_point" 
                                        value="{{ old('reorder_point') }}" required 
                                    />
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="p-reoder-quantity" class="control-label col-xs-12 col-sm-3">Reorder Quantity</label>
                                <div class="col-xs-12 col-sm-9">
                                    <input type="number" class="form-control auto" id="p-reoder-quantity" name="reorder_quantity" 
                                        value="{{ old('reorder_quantity') }}" required 
                                    />
                                </div>
                            </div>

                            <div class="form-group fg-last">
                                <label for="p-description" class="control-label col-xs-12 col-sm-3">Description</label>
                                <div class="col-xs-12 col-sm-9">
                                    <textarea name="description" id="p-description" rows="2" class="form-control">{{ old('description') }}</textarea>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

                <div class="panel">
                    <div class="panel-heading">
                        <h4>Category</h4>
                    </div>
                    <div class="panel-body">
                        
                        <div class="form-horizontal">
                            
                            <div class="form-group last">
                                <div class="col-xs-12">
                                    @forelse ($parentCategories as $parentCat)
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="categories[{{ $parentCat->id }}][0]" value="1" class="parent-checkbox" 
                                                    id="parent-{{ $parentCat->id }}" data-id="{{ $parentCat->id }}"
                                                />
                                                <span><strong>{{ $parentCat->name }}</strong></span>
                                            </label>
                                        </div>
                                        @if (count($parentCat->childCategories) > 0)
                                            <?php $childCategories = $parentCat->childCategories; ?>
                                            <div  style="padding-left: 24px;">
                                                @foreach ($childCategories as $childCat)
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" name="categories[{{ $parentCat->id}}][{{ $childCat->id }}]" value="1" 
                                                                class="child-checkbox" data-parent="{{ $parentCat->id }}"
                                                            />
                                                            <span>{{ $childCat->name }}</span>
                                                        </label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        @endif
                                    @empty
                                        <p>No categories yet. <a href="{{ route('categories.add') }}">Click here to add</a></p>
                                    @endforelse
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
                
            </div>
            <div class="col-xs-12 col-lg-6">
                
                <div class="panel">
                    <div class="panel-heading">
                        <h4>Opening Balance</h4>
                    </div>
                    <div class="panel-body">
                        
                        <div class="form-horizontal">

                            <div class="form-group">
                                <label for="p-opening-balance" class="control-label col-xs-12 col-sm-3">Opening Balance</label>
                                <div class="col-xs-12 col-sm-9">
                                    <input type="number" class="form-control" id="p-opening-balance" name="opening_balance" 
                                        value="{{ old('opening_balance') }}" 
                                    />
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="p-location-id" class="control-label col-xs-12 col-sm-3">Location</label>
                                <div class="col-xs-12 col-sm-9">
                                    <select name="location_id" id="p-location-id" class="form-control"
                                        data-live-search="true"
                                    >
                                        <option value=""></option>
                                        @foreach ($locations as $location)
                                            <option value="{{ $location->id }}"
                                                @if (old('location_id') == $location->id) selected @endif 
                                            >
                                                {{ $location->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group fg-last">
                                <label for="p-price" class="control-label col-xs-12 col-sm-3">Price</label>
                                <div class="col-xs-12 col-sm-9">
                                    <input type="number" step="0.01" class="form-control auto" id="p-price" name="price" 
                                        value="{{ old('price') }}" 
                                    />
                                </div>
                            </div>
                            
                        </div>

                    </div>
                </div>

                @if (count($customFields) > 0)
                    <div class="panel">
                        <div class="panel-heading">
                            <h4>Custom Fields</h4>
                        </div>
                        <div class="panel-body">
                            
                            <div class="form-horizontal">

                                @foreach ($customFields as $cf)
                                    <div class="form-group @if ($loop->last) fg-last @endif">
                                        <label for="cf-{{ $cf->slug }}" class="control-label col-xs-12 col-sm-3">{{ $cf->label }}</label>
                                        <div class="col-xs-12 col-sm-9">
                                            @if ($cf->type == 'input')
                                                <input type="text" class="form-control" name="custom_fields[{{ $cf->id }}]" id="cf-{{ $cf->slug }}" />
                                            @elseif ($cf->type == 'textarea')
                                                <textarea class="form-control autosize" name="custom_fields[{{ $cf->id }}]" id="cf-{{ $cf->slug }}" rows="2"></textarea>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach

                            </div>

                        </div>
                    </div>
                @endif

                @if (count($requesters) > 0)
                    <div class="panel">
                        <div class="panel-heading">
                            <h4>Product Allocation</h4>
                        </div>
                        <div class="panel-body">
                            
                            <div class="form-horizontal">
                                
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-9 col-md-6 col-sm-offset-3">
                                        <button class="btn btn-primary btn-sm zero-all">
                                            Set to Zero
                                        </button>
                                        <button class="btn btn-primary btn-sm infinite-all">
                                            Set Infinite
                                        </button>
                                    </div>
                                </div>
                                @foreach ($requesters as $requester)
                                    <div class="form-group @if ($loop->last) fg-last @endif">
                                        <label for="p-allocation-{{ $requester->id }}" class="control-label col-xs-12 col-sm-3">{{ $requester->user->name() }}</label>
                                        <div class="col-xs-12 col-sm-9">
                                            <input type="number" class="form-control auto allocations-field" id="p-allocation-{{ $requester->id }}" min="-1"
                                                name="allocations[{{ $requester->id }}]" value="{{ old('allocations.' . $requester->id, -1) }}" 
                                            />
                                        </div>
                                    </div>
                                @endforeach

                            </div>

                        </div>
                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <a href="{{ route('products.list') }}" class="btn btn-link">Cancel</a>
                        </div>
                    </div>
                @endif


            </div>
        </div>
        {!! csrf_field() !!}
    </form>

</div>
</div>
</div>
@endsection

@section('footer-addon')
<script src="{{ asset('assets/js/vendors/autosize.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.min.js"></script>
<script>
$(function() {

    autosize($('#p-description'));

    $('#opening-balance').on('keyup', function() {
        if (parseFloat($(this).val()) > 0) {
            $('#price').prop('required', true);
            $('#location-id').prop('required', true);
        } else {
            $('#price').prop('required', false);
            $('#location-id').prop('required', false);
        }
    });

    $('#p-location-id, #p-default-vendor').selectpicker({
        style: '',
        size: 6
    });

    $(document)
        .on('change', '.child-checkbox', function() {
            var parent = $(this).attr('data-parent'),
                count = 0;
            $('.child-checkbox[data-parent="' + parent + '"]').each(function() {
                if ($(this).prop('checked') == true) {
                    count++;
                }
            });
            if (count > 0) {
                $('#parent-' + parent).prop('checked', true);
            }
        })
        .on('change', '.parent-checkbox', function() {
            var children = $('.child-checkbox[data-parent="' + $(this).attr('data-id') + '"]'),
                count = 0;
            children.each(function() {
                if ($(this).prop('checked') == true) {
                    count++;
                }
            });
            if (count > 0) {            
                $(this).prop('checked', true);
            }
        })
        .on('click', '.infinite-all', function (e) {
            e.preventDefault();
            $('.allocations-field').each(function () {
                $(this).val(-1);
            });
        })
        .on('click', '.zero-all', function (e) {
            e.preventDefault();
            $('.allocations-field').each(function () {
                $(this).val(0);
            });
        });

});
</script>
@endsection