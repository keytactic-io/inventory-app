@extends('layouts.app-front')

@section('page-title', 'Walk-in Request')
@section('body-class', 'front with-fixed-button')

@section('content')
<div class="container">
<div class="row">
<div class="col-xs-12">

    @include('includes.status')
    @include('includes.errors')

    <form action="{{ route('request.walkin', ['locationId' => $location->id]) }}" 
        method="post" autocomplete="off"
        id="wrq-form"
    >
    {!! csrf_field() !!}
    <div class="wrq-header">
        <h1>Walk-in Request : <a href="{{ route('request.walkin.switch') }}">{{ $location->name }}</a></h1>
    </div>
    <div class="panel">
        <div class="panel-body">
            
            <div class="form-horizontal">
                
                <div class="form-group fg-last">
                    <label for="wrq-fullname" class="control-label col-xs-12 col-sm-3">Full Name</label>
                    <div class="col-xs-12 col-sm-9 col-md-6">
                        <input name="full_name" id="wrq-fullname" type="text" class="form-control" 
                            placeholder="John Doe" required
                            value="{{ old('full_name') }}"
                        />
                    </div>
                </div>

                <div class="form-group-divider"><span></span></div>

                <div class="wrq-products">
                    <div class="row">
                    @foreach ($products as $product)

                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="wrq-product-entry">
                                <input type="checkbox" class="products-cb" value="1" 
                                    data-qty="#wrq-quantity-{{ $product->id }}" name="products[{{ $product->id }}][checked]"
                                />
                                <div class="wrq-product-entry-caption">
                                    <span id="wrq-product-{{ $product->id }}">{{ $product->name }}</span>
                                    <div class="wrq-product-field-container">
                                        <input type="number" class="wrq-product-field" placeholder="Max: {{ $location->stocksRemaining($product->id) }}" 
                                            id="wrq-quantity-{{ $product->id }}" name="products[{{ $product->id }}][quantity]" 
                                            max="{{ $location->stocksRemaining($product->id) }}" data-label="#wrq-product-{{ $product->id }}"
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- <div class="form-group wrq-product-entry @if ($loop->last AND $loop->count <= 10) fg-last @endif">
                            <div class="col-xs-12 col-sm-9 col-md-6 col-sm-offset-3">
                                <div class="row">
                                    <div class="col-xs-8">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" class="products-cb" value="1" 
                                                    data-qty="#wrq-quantity-{{ $product->id }}"
                                                />
                                                <span>{{ $product->name }}</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <input type="number" class="form-control" placeholder="Max: {{ $location->stocksRemaining($product->id) }}" 
                                            id="wrq-quantity-{{ $product->id }}" name="products[{{ $product->id }}]" 
                                            max="{{ $location->stocksRemaining($product->id) }}"
                                        />
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                    @endforeach
                    </div>
                </div>

                <div class="form-group-divider"><span></span></div>

                <div class="form-group fg-last">
                    <label for="wrq-notes" class="control-label col-xs-12 col-sm-3">Notes/Justifications</label>
                    <div class="col-xs-12 col-sm-9 col-md-6">
                        <textarea name="notes" id="wrq-notes" rows="2" class="form-control autosize">{{ old('notes') }}</textarea>
                    </div>
                </div>

            </div>

        </div>
    </div>

    <div class="fixed-button">
        <button type="submit" class="btn btn-primary btn-lg btn-block btn-submit-wrq" disabled>Submit</button>
    </div>
    </form>
    
</div>
</div>
</div>
@endsection

@section('footer-addon')
<script>
$(function() {

    $(document).on('click', '.wrq-product-entry', function(e) {
        e.stopPropagation();
        $(this).find('.products-cb').click();
    });

    $(document).on('change', '.products-cb', function() {
        if ($(this).prop('checked')) {
            // set as required then focus
            $($(this).data('qty'))
                .prop('required', true)
                .focus();
        } else {
            // remove required attribute then clear the value
            $($(this).data('qty'))
                .prop('required', false)
                .val('');
        }
        var count = 0;
        $('.products-cb').each(function() {
            if ($(this).prop('checked')) {
                count++;
            }
        });
        if (count > 0) {
            $('.btn-submit-wrq').prop('disabled', false);
        } else {
            $('.btn-submit-wrq').prop('disabled', true);
        }
    });

});
</script>
@endsection
