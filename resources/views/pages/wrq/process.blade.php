<div class="form-errors"></div>
<form action="{{ route('wrq.process', ['wrqId' => $wrq->id]) }}" method="post" autocomplete="off"
    id="process-rwq-form"
>

    <div class="wrq-details clearfix">
        @if ($wrq->is_released)
            <span class="label label-success">{{ $wrq->uid }}</span>
        @else
            <span class="label label-warning">{{ $wrq->uid }}</span>
        @endif
        <h1>{{ $wrq->requester_name }}</h1>
        <h4>{{ $wrq->location->name }}</h4>
        @if ($wrq->notes != null)
            <p>{{ $wrq->notes }}</p>
        @endif
    </div>

    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th>Product</th>
                    <th class="text-right">Quantity</th>
                    <th @if ($wrq->is_released) class="text-right" @endif>Allowed</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($wrq->requestedProducts as $requestedProduct)
                    <tr>
                        <tr>
                            <td class="table-photo">
                                <div class="table-user-dp" style="background: url({{ $requestedProduct->product->photo() }}) center center no-repeat; background-size: contain; ">
                                    <a href="{{ route('products.details', ['productId' => $requestedProduct->product_id]) }}" target="_blank"></a>
                                </div>
                            </td>
                            <td>
                                <a href="{{ route('products.details', ['productId' => $requestedProduct->product_id]) }}" target="_blank">
                                    <strong>{{ $requestedProduct->product->name }}</strong>
                                </a>
                            </td>
                            <td class="text-right">{{ $requestedProduct->quantity }}</td>
                            @if ($wrq->is_released)
                                <td class="text-right">{{ $requestedProduct->allowed_quantity }}</td>
                            @else
                                <td>
                                    <input type="hidden" name="products[{{ $requestedProduct->id}}][product_id]" value="{{ $requestedProduct->product_id }}" />
                                    <input type="number" class="input-in-table" name="products[{{ $requestedProduct->id}}][allowed_quantity]" 
                                        @if ($requestedProduct->quantity > $wrq->location->stocksRemaining($requestedProduct->product_id))
                                            value="{{ $wrq->location->stocksRemaining($requestedProduct->product_id) }}" 
                                        @else
                                            value="{{ $requestedProduct->quantity }}" 
                                        @endif
                                        max="{{ $wrq->location->stocksRemaining($requestedProduct->product_id) }}" min="0" 
                                    />
                                </td>
                            @endif
                        </tr>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</form>