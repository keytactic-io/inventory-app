@extends('layouts.app-front')

@section('page-title', 'Walk-in Request')
@section('body-class', 'front with-fixed-button')

@section('content')
<div class="container">
<div class="row">
<div class="col-xs-12">

    @include('includes.status')

    <div class="wrq-header">
        <h1>Walk-in Request</h1>
    </div>

    <div class="panel">
        <div class="panel-body" style="padding-bottom: 0;">
            
            <div class="details-content">

                <h1>{{ $walkInRequest->requester_name }}</h1>
                @if ($walkInRequest->notes != null)
                    <p>{{ $walkInRequest->notes }}</p>
                @endif

            </div>

        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th>Product</th>
                            <th class="text-right">Quantity</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($walkInRequest->requestedProducts as $requestedProduct)
                            <tr>
                                <td class="table-photo">
                                    <div class="table-user-dp" style="background: url({{ $requestedProduct->product->photo() }}) center center no-repeat; background-size: contain; "></div>
                                </td>
                                <td><strong>{{ $requestedProduct->product->name }}</strong></td>
                                <td class="text-right">{{ $requestedProduct->quantity }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
</div>
</div>
</div>
@endsection

@section('footer-addon')
<script>
$(function() {

    setTimeout(function() {
        window.location = '{{ route('request.walkin', ['locationId' => $walkInRequest->location_id]) }}';
    }, 4000);

});
</script>
@endsection
