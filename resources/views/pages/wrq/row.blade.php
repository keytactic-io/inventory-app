<tr>
    <td>
        <a href="#" class="process-wrq"
            data-url="{{ route('wrq.process', ['wrqId' => $wrq->id]) }}"
        >
            @if ($wrq->is_released)
                <span class="label label-success">{{ $wrq->uid }}</span>
            @else
                <span class="label label-warning">{{ $wrq->uid }}</span>
            @endif
        </a>
    </td>
    <td><strong>{{ $wrq->requester_name }}</strong></td>
    <td>{{ $wrq->location->name }}</td>
    <td>
        <a href="#" class="process-wrq"
            data-url="{{ route('wrq.process', ['wrqId' => $wrq->id]) }}"
        >
            {{ count($wrq->products) }}
        </a>
    </td>
    <td class="text-right">
        @if (!$wrq->is_released)
            <a href="#" class="process-wrq"
                data-url="{{ route('wrq.process', ['wrqId' => $wrq->id]) }}"
            >
                <i class="fa fa-shopping-basket fa-fw"></i>
            </a>
        @endif
        <a href="#">
            <i class="fa fa-close fa-fw"></i>
        </a>
    </td>
</tr>