@extends('layouts.app-front')

@section('page-title', 'Switch Location')
@section('body-class', 'front with-fixed-button')

@section('content')
<div class="container">
<div class="row">
<div class="col-xs-12">

    <div class="wrq-header">
        <h1>Switch Location</h1>
    </div>
    
    <div class="row wrq-switch-loc">
        @foreach ($locations as $location)
            @if ($location->productsCount() > 0)
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <a href="{{ route('request.walkin', ['locationId' => $location->id]) }}" class="btn btn-xxlg btn-block btn-primary">
                        {{ $location->name }}
                        <small>{{ $location->getAddress() }}</small>
                    </a>
                </div>
            @endif
        @endforeach
    </div>
    
</div>
</div>
</div>
@endsection

@section('footer-addon')
@endsection
