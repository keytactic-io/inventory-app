@extends('layouts.app')

@section('page-title', 'Walk-in Requests')

@section('head-addon')
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/select/1.2.0/css/select.dataTables.min.css" />
<link rel="stylesheet" href="{{ asset('assets/css/dt-override.css') }}" />
@endsection

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    @include('includes.status')
    @include('includes.errors')
    
</div>
<div class="col-xs-12">

    <div class="panel">
        <div class="panel-heading">
            <h4>Walk-in Requests</h4>
        </div>

        <div class="panel-filter">
            <input type="text" placeholder="Search walk-in request" class="form-control" id="quick-search-input" autocomplete="off">
        </div>

        <div class="panel-preloader" data-target="#wrq-panel-body">
            <i class="fa fa-circle-o-notch fa-spin fa-fw"></i> Preparing walk-in requests table
        </div>
        <div id="wrq-panel-body" class="panel-body hidden" style="display:none;">

            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab">All</a></li>
                <li role="presentation"><a href="#wrq-pending" aria-controls="wrq-pending" role="tab" data-toggle="tab">Pending</a></li>
                <li role="presentation"><a href="#wrq-released" aria-controls="wrq-released" role="tab" data-toggle="tab">Released</a></li>
            </ul>

            <div class="tab-content">

                <!-- all -->
                <div role="tabpanel" class="tab-pane active" id="all">

                    <div class="table-responsive">
                        <table id="wrq-all" class="table datatable table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Requester</th>
                                    <th>Location</th>
                                    <th>Products Count</th>
                                    <th class="text-right">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($allWrq as $wrq)
                                    @include('pages.wrq.row', ['wrq', $wrq])
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>

                <!-- wrq-pending -->
                <div role="tabpanel" class="tab-pane" id="wrq-pending">

                    <div class="table-responsive">
                        <table id="wrq-pending-table" class="table datatable table-hover">
                            <thead>
                                <th>ID</th>
                                <th>Requester</th>
                                <th>Location</th>
                                <th>Products Count</th>
                                <th class="text-right">Actions</th>
                            </thead>
                            <tbody>
                                @foreach ($pendingWrq as $wrq)
                                    @include('pages.wrq.row', ['wrq', $wrq])
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>

                <!-- wrq-released -->
                <div role="tabpanel" class="tab-pane" id="wrq-released">

                    <div class="table-responsive">
                        <table id="wrq-released-table" class="table datatable table-hover">
                            <thead>
                                <th>ID</th>
                                <th>Requester</th>
                                <th>Location</th>
                                <th>Products Count</th>
                                <th class="text-right">Actions</th>
                            </thead>
                            <tbody>
                                @foreach ($releasedWrq as $wrq)
                                    @include('pages.wrq.row', ['wrq', $wrq])
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>

        </div>

    </div>

</div>
</div>
@endsection

@section('footer-addon')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script>
$(function() {

    function tabInfo(table) {
        var id = $(table).closest('.tab-pane').attr('id'),
            tab = $('.nav-tabs a[aria-controls='+id+']'),
            length = $(table).DataTable().page.info().recordsDisplay,
            label = tab.find('span');
        if (label.length) { label.remove(); }
        tab.append(' <span>('+length+')</span>');
    }

    var tables = $('.datatable')
        .on('draw.dt', function () {
            tabInfo(this);
        })
        .on('search.dt', function () {
            tabInfo(this);
        })
        .DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5',
                'print'
            ],
            pageLength: 10,
            ordering: true,
            lengthChange: false,
            pagingType: 'numbers',
            // select: {
            //     style: 'single'
            // },
            // 'order': [ 1, 'asc' ],
            // 'columnDefs': [
            //     {
            //         'targets': [5],
            //         'visible': false
            //     }
            // ]
        });

    $('#quick-search-input').on('keyup', function () {
        tables.search( this.value ).draw();
    });

    $(document).on('click', '.process-wrq', function(e) {
        e.preventDefault();
        loader.show();
        $.get( $(this).data('url') )
            .done(function(r) {
                var btnClass = r.released ? 'hidden' : '';
                loader.hide();
                bootbox.dialog({
                    title: r.title,
                    message: r.view,
                    size: 'large',
                    onEscape: function () {
                        bootbox.hideAll();
                    },
                    buttons: {
                        'confirm': {
                            label: 'Approve',
                            className: 'btn-primary ' + btnClass,
                            callback: function() {
                                loader.show();
                                var form = $('#process-rwq-form');

                                $.post( form.attr('action'), form.serialize() )
                                    .done(function(r) {
                                        if (r.error) {
                                            $('.form-errors').html(r.error);
                                            loader.hide();
                                        }
                                        if (r.done) {
                                            window.location = r.redirect;
                                        }
                                    })
                                    .fail(function(r) {
                                        loader.hide();
                                    });
                                
                                return false;
                            }
                        },
                        'Close' : {
                            className: 'btn-default'
                        }
                    }
                });
            })
            .fail(function(r) {
                loader.hide();
            });
    })

    $(document).on('click', '#delete-a', function(e) {
        e.preventDefault();
        var url = $(this).data('url');
        bootbox.dialog({
            title: 'Hey!',
            message: '<div style="font-size: 16px;">Are you sure you want to delete this product?</div>',
            className: 'modal-danger',
            onEscape: function () {
                bootbox.hideAll();
            },
            buttons: {
                'confirm': {
                    label: 'Yes',
                    className: 'btn-danger',
                    callback: function() {
                        window.location = url;
                    }
                },
                'Close' : {
                    className: 'btn-default'
                }
            }
        });
    });

});
</script>
@endsection
