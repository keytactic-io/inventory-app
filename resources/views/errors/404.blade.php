@extends('layouts.app')

@section('page-title', 'Dashboard')

@section('content')
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    <div class="error-page">
        <h1>404</h1>
        <h3>Page Not Found</h3>
    </div>

</div>
</div>
</div>
@endsection
