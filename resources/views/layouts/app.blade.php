<!DOCTYPE html>
<html lang="en">
<head>
    
    @include('layouts.fragments.head')

</head>
<body class="@yield('body-class')">

    @include('layouts.fragments.header')

    @yield('content')

    @include('layouts.fragments.scripts')

    @yield('footer-addon')

</body>
</html>
