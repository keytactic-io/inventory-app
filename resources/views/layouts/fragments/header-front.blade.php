<header>
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">

    <div class="navbar-header">

        <?php $title = app_setting('application_name') == '' ? 'Inventory App' : app_setting('application_name'); ?>
        <a class="navbar-brand" href="{{ url('/') }}" title="{{ $title }}" @if (app_setting('logo') != '') style="padding-left: 0; padding-right: 0;" @endif>
            @if (app_setting('logo') != '')
                <img src="{{ asset('uploads/' . app_setting('logo')) }}" alt="{{ $title }}" />
            @else
                {{ $title }}
            @endif
        </a>

        <div id="quick-add" class="dropdown">
            <a id="quick-add-trigger" href="{{ url('/') }}"><i class="fa fa-sign-in"></i></a>
        </div>

    </div>

</div>
</div>
</div>
</header>
