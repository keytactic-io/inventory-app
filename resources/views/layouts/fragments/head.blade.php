<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">

<?php
    $appName = app_setting('application_name') != '' ? app_setting('application_name') : 'Inventory App';
    if (app_setting('logo') != '') {
        $appName = app_setting('application_name');
    }
?>
<title>@yield('page-title') - {{ $appName }}</title>

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
<link href="{{ asset('assets/css/global.css') }}" rel="stylesheet" />

@if (Auth::check())
    <link href="{{ asset('assets/css/themes/' . $currentUser->theme .  '.css') }}" rel="stylesheet" id="theme-css" />
    <meta name="theme-color" content="{{ getThemePrimaryColor()[$currentUser->theme] }}" />
    <meta name="msapplication-navbutton-color" content="{{ getThemePrimaryColor()[$currentUser->theme] }}" />
    <meta name="apple-mobile-web-app-status-bar-style" content="{{ getThemePrimaryColor()[$currentUser->theme] }}" />
@else
    <link href="{{ asset('assets/css/themes/ice.css') }}" rel="stylesheet" />
    <meta name="theme-color" content="#3bafda" />
    <meta name="msapplication-navbutton-color" content="#3bafda" />
    <meta name="apple-mobile-web-app-status-bar-style" content="#3bafda" />
@endif

<link href="{{ asset('assets/css/vendors/dataTables.bootstrap.min.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/css/vendors/ion.rangeSlider.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/css/vendors/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/css/vendors/tokenize.css') }}" rel="stylesheet" />

<link href="https://rsms.me/inter/inter-ui.css" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
{{-- <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" /> --}}

<script>
    window.Laravel = <?php echo json_encode([
        'csrfToken' => csrf_token(),
    ]); ?>
</script>

@yield('head-addon')

<link href="{{ asset('assets/css/rwd.css') }}" rel="stylesheet" />
