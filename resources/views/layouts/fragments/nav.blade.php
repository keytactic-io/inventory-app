<aside>
<div class="aside-content">

    <div class="aside-menu">
        <div id="collapsible-menu-wrap">

            <div class="panel-group" id="main-nav" role="tablist" aria-multiselectable="true">

                <div class="panel">
                    <div class="panel-heading" id="menu-dashboard">
                        <a href="{{ url('/') }}" class="@if (inUrl('dashboard')) active @else collapsed @endif">
                            <i class="fa fa-dashboard fa-fw"></i>Dashboard
                        </a>
                    </div>
                </div>

                @if (
                    $currentUser->is_requester OR
                    $currentUser->allowedTo('view_products') OR
                    $currentUser->allowedTo('view_vendors') OR
                    $currentUser->allowedTo('view_purchase_orders')
                )
                    <div class="panel">
                        <div class="panel-heading" role="tab" id="menu-products">
                            <a role="button" data-toggle="collapse" data-parent="#main-nav" href="#menu-products-collapse" aria-controls="menu-products-collapse"
                                class="@if (inUrl('products.*', 'purchase-orders', 'purchase-orders.*', 'vendors', 'vendors.*', 'locations', 'locations.*', 'transferred-products', 'transferred-products.*', 'categories', 'categories.*')) active @else collapsed @endif"
                                aria-expanded="@if (inUrl('products.*', 'purchase-orders', 'purchase-orders.*', 'vendors', 'vendors.*', 'locations', 'locations.*', 'transferred-products', 'transferred-products.*', 'categories', 'categories.*')) true @else false @endif"
                            >
                                <i class="fa fa-cubes fa-fw"></i>Products
                            </a>
                        </div>
                        <div id="menu-products-collapse" role="tabpanel" aria-labelledby="menu-products"
                            class="panel-collapse collapse @if (inUrl('products.*', 'purchase-orders', 'purchase-orders.*', 'vendors', 'vendors.*', 'locations', 'locations.*', 'transferred-products', 'transferred-products.*', 'categories', 'categories.*')) in @endif"
                        >

                            <ul class="main-nav-ul">
                                @if ($currentUser->allowedTo('view_products') OR $currentUser->is_requester)
                                    <li class="@if (inUrl('products.list.*', 'products.add', 'products.edit.*', 'products.details.*')) active @endif">
                                        <a href="{{ route('products.list') }}/all">Products</a>
                                    </li>
                                @endif

                                @if ($currentUser->allowedTo('view_product_categories') OR $currentUser->is_requester)
                                    <li class="@if (inUrl('categories')) active @endif">
                                        <a href="{{ route('categories.list') }}">Categories</a>
                                    </li>
                                @endif

                                @if ($currentUser->allowedTo('view_locations'))
                                    <li class="@if (inUrl('locations')) active @endif">
                                        <a href="{{ route('locations.list') }}">Locations</a>
                                    </li>
                                @endif

                                @if (!$currentUser->is_requester AND $currentUser->allowedTo('view_vendors'))
                                    <li class="@if (inUrl('vendors')) active @endif">
                                        <a href="{{ route('vendors.list') }}">Vendors</a>
                                    </li>
                                @endif

                                @if (!$currentUser->is_requester AND $currentUser->allowedTo('view_purchase_orders'))
                                    <li class="@if (inUrl('purchase-orders', 'purchase-orders.*')) active @endif">
                                        <a href="{{ route('pos.list') }}">Purchase Orders</a>
                                    </li>
                                @endif

                                @if ($currentUser->allowedTo('view_opening_balances'))
                                    <li class="@if (inUrl('products.opening-balances', 'products.opening-balances.*')) active @endif">
                                        <a href="{{ route('products.ob.list') }}">Opening Balances</a>
                                    </li>
                                @endif

                                @if ($currentUser->allowedTo('view_transferred_products'))
                                    <li class="@if (inUrl('transferred-products')) active @endif">
                                        <a href="{{ route('products.transferred') }}">Product Transfers</a>
                                    </li>
                                @endif

                            </ul>
                        </div>
                    </div>
                @endif

                @if (!$currentUser->is_requester AND $currentUser->allowedTo('view_requests'))
                    <div class="panel">
                        <div class="panel-heading" id="menu-dashboard">
                            <a href="{{ route('requests.list') }}" class="@if (inUrl('requests')) active @else collapsed @endif">
                                <i class="fa fa-clipboard fa-fw"></i>Requests
                            </a>
                        </div>
                    </div>
                @endif

                @if (!$currentUser->is_requester AND $currentUser->allowedTo('view_users') OR $currentUser->allowedTo('view_requesters'))
                    <div class="panel">
                        <div class="panel-heading" id="menu-dashboard">
                            <a role="button" data-toggle="collapse" data-parent="#main-nav" href="#menu-users-collapse" aria-controls="menu-users-collapse"
                                class="@if (inUrl('users', 'users.*', 'requesters', 'requesters.add', 'profile.edit.*', 'roles', 'roles.*', 'markets', 'markets.*')) active @else collapsed @endif"
                                aria-expanded="@if (inUrl('users', 'users.*', 'requesters', 'requesters.add', 'profile.edit.*', 'roles', 'roles.*', 'markets', 'markets.*')) true @else false @endif"
                        >
                                <i class="fa fa-users fa-fw"></i>Users
                            </a>
                        </div>
                    <div id="menu-users-collapse" role="tabpanel" aria-labelledby="menu-users"
                        class="panel-collapse collapse @if (inUrl('users', 'users.*', 'requesters', 'requesters.add', 'profile.edit.*', 'roles', 'roles.*', 'markets', 'markets.*')) in @endif"
                    >
                        <ul class="main-nav-ul">
                            @if ($currentUser->allowedTo('view_users'))
                                <li class="@if (inUrl('users', 'users.*')) active @endif">
                                    <a href="{{ route('users.list') }}">Users</a>
                                </li>
                            @endif

                            @if ($currentUser->allowedTo('view_requesters'))
                                <li class="@if (inUrl('requesters', 'requesters.add')) active @endif">
                                    <a href="{{ route('requesters.list') }}">Requesters</a>
                                </li>
                            @endif

                            @if ($currentUser->allowedTo('view_roles'))
                                <li class="@if (inUrl('roles', 'roles.*')) active @endif">
                                    <a href="{{ route('roles.list') }}">Roles</a>
                                </li>
                            @endif

                            @if ($currentUser->allowedTo('view_markets'))
                                <li class="@if (inUrl('markets', 'markets.*')) active @endif">
                                    <a href="{{ route('markets.list') }}">Markets</a>
                                </li>
                            @endif

                        </ul>
					          </div>
                    </div>
                @endif

                @if ($currentUser->is_requester)
                    <div class="panel">
                        <div class="panel-heading" id="menu-dashboard">
                            <a href="{{ route('requests.list') }}" class="@if (inUrl('requests', 'requests.create')) active @else collapsed @endif">
                                <i class="fa fa-clipboard fa-fw"></i>Requests
                            </a>
                        </div>
                    </div>

                    <div class="panel">
                        <div class="panel-heading" id="menu-dashboard">
                            <a href="{{ route('requesters.productallocation', ['requesterId' => $currentUser->requester->id]) }}"
                                class="@if (inUrl('requesters.*.product-allocation')) active @else collapsed @endif"
                            >
                                <i class="fa fa-database fa-fw"></i>Allocations
                            </a>
                        </div>
                    </div>
                @endif

                @if (
                    $currentUser->allowedTo('view_settings') OR
                    $currentUser->allowedTo('view_custom_fields') OR
                    $currentUser->allowedTo('add_requesters')
                )
                    <div class="panel">
                        <div class="panel-heading" role="tab" id="menu-settings">
                            <a role="button" data-toggle="collapse" data-parent="#main-nav" href="#menu-settings-collapse" aria-controls="menu-settings-collapse"
                                class="@if (inUrl('settings', 'custom-fields', 'requesters.invites-sent')) active @else collapsed @endif"
                                aria-expanded="@if (inUrl('settings', 'custom-fields', 'requesters.invites-sent')) true @else false @endif"
                            >
                                <i class="fa fa-cogs fa-fw"></i>Settings
                            </a>
                        </div>
                        <div id="menu-settings-collapse" role="tabpanel" aria-labelledby="menu-settings"
                            class="panel-collapse collapse @if (inUrl('settings', 'custom-fields', 'requesters.invites-sent')) in @endif"
                        >
                            <ul class="main-nav-ul">

                                @if ($currentUser->allowedTo('view_settings'))
                                    <li class="@if (inUrl('settings')) active @endif">
                                        <a href="{{ route('settings.home') }}">App Settings</a>
                                    </li>
                                @endif

                                @if ($currentUser->allowedTo('view_custom_fields'))
                                    <li class="@if (inUrl('custom-fields')) active @endif">
                                        <a href="{{ route('cf.home') }}">Custom Fields</a>
                                    </li>
                                @endif

                                @if ($currentUser->allowedTo('add_requesters'))
                                    <li class="@if (inUrl('requesters.invites-sent')) active @endif">
                                        <a href="{{ route('requesters.invites') }}">Sent Invites</a>
                                    </li>
                                @endif

                            </ul>
                        </div>
                    </div>
                @endif

            </div>

        </div>
    </div>
    <div class="aside-upanel dropup">

        <div class="upanel clearfix dropdown-toggle" id="upanel-dropup"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
            title="{{ $currentUser->name() }}"
        >
            <div class="upanel-dp" style="background: url({{ $currentUser->dp() }}) center center no-repeat;background-size: cover;"></div>
            <div class="upanel-info">
                <h4>{{ $currentUser->name() }}</h4>
                <h5>
                    @if ($currentUser->is_requester)
                        Requester
                    @else
                        {{ $currentUser->role->name }}
                    @endif
                </h5>
            </div>
        </div>

        <ul class="upanel-dd dropdown-menu" aria-labelledby="upanel-dropup">
            <li><a href="{{ route('users.profile') }}/{{ $currentUser->id }}">Profile</a></li>
            <li><a href="{{ route('users.profile.edit') }}/{{ $currentUser->id }}">Edit Profile</a></li>
            <li><a href="#" id="change-theme" data-toggle="modal" data-target="#theme-modal">Change Theme</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="{{ url('/logout') }}">Log Out</a></li>
        </ul>

    </div>

</div>
</aside>

<div class="modal fade bs-example-modal-sm" id="theme-modal" tabindex="-1" role="dialog" aria-labelledby="theme-modal-title">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="theme-modal-title">Change Theme</h4>
        </div>
        <div class="modal-body">

            <form action="{{ route('users.changetheme') }}" method="post" id="change-theme-form">
            {!! csrf_field() !!}
            <div class="the-themes">
                @foreach (getThemes() as $theme)
                    <div class="a-theme theme-{{ strtolower($theme) }}">
                        <label>
                            <input type="radio" name="theme" id="" value="{{ strtolower($theme) }}" class="theme-option" />
                            <div class="theme-data clearfix">
                                {{ $theme }}
                                <div class="theme-accent">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </div>
                        </label>
                    </div>
                @endforeach
            </div>
            </form>

        </div>
    </div>
    </div>
</div>
