<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="{{ asset('assets/js/scripts.js') }}"></script>
<script src="{{ asset('assets/js/vendors/js-webshim/polyfiller.js') }}"></script>
<script>
    webshim.activeLang('en');
    webshims.polyfill('forms');
    webshims.cfg.no$Switch = true;
</script>
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
var loader = {
    html: '<div class="loader-wrap"><i class="fa fa-loader fa-circle-o-notch fa-spin"></i></div>',
    loadinghtml: '<div class="loader-wrap"><div class="loading-wrap"><div class="loading-bar"></div></div><div class="loading-caption"></div></div>',
    show: function() {
        $('body').append(this.html);
        setTimeout(
            function() {
                $('.loader-wrap').addClass('in');
            },
            100
        );
    },
    hide: function() {
        $('.loader-wrap').removeClass('in');
        setTimeout(
            function() {
                $('.loader-wrap').remove();
            },
            600
        );
    }
};
@if (Auth::check())
document.addEventListener('DOMContentLoaded', function () {
    if (!Notification) {
        alert('Desktop notifications not available in your browser.');
        return;
    }
    if (Notification.permission !== "granted") {
        Notification.requestPermission();
    }
});

function notifyUser(title, body, url = '') {
    if (Notification.permission !== "granted") {
        Notification.requestPermission();
    } else {
        var notification = new Notification(title, {
            // link to app icon
            // icon: "{{ asset('/notification-icon.png') }}",
            body: body,
        });

        notification.onclick = function () {
            if (url != '') {
                window.open(url);
            }
        };

    }
}
@endif
</script>

<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('assets/js/vendors/jquery.slimscroll.min.js') }}"></script>
<script>
$(function() {

    $('#collapsible-menu-wrap').slimScroll({
        height: '100%',
        size: '5px',
        position: 'right',
        color: '#666666',
        alwaysVisible: false,
        distance: '0',
        railVisible: false,
        railOpacity: 0.3,
        wheelStep: 10,
        allowPageScroll: false,
        disableFadeOut: false
    });

    $('.theme-option').each(function() {
        $(this).on('change', function() {
            var form = $('#change-theme-form');
            $.post( form.attr('action'), form.serialize() ).done(function(r) {
                if (r.success) {
                    $('#theme-css').attr('href', r.newTheme);
                    $('meta[name=theme-color]').attr('content', r.hex);
                    $('meta[name=msapplication-navbutton-color]').attr('content', r.hex);
                    $('meta[name=apple-mobile-web-app-status-bar-style]').attr('content', r.hex);
                }
            });
        });
    });

});
</script>

<script src="{{ asset('assets/js/vendors/jquery.smartWizard.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>
<script src="{{ asset('assets/js/vendors/tokenize.js') }}"></script>
<script src="{{ asset('assets/js/vendors/autosize.js') }}"></script>
