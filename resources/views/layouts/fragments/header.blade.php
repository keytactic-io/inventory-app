@if (Auth::check())
<header>
<div class="container-fluid">
<div class="row">
<div class="col-xs-12">
    
    <div class="navbar-header">

        <button id="push-menu-trigger">
            <span></span>
            <span></span>
            <span></span>
        </button>

        @php
            $title = app_setting('application_name') != '' ? app_setting('application_name') : 'Inventory App';
        @endphp
        <a class="navbar-brand" href="{{ url('/') }}" title="{{ $title }}" @if (app_setting('logo') != '') style="padding-left: 0; padding-right: 0;" @endif>
            @if (app_setting('logo') != '')
                <img src="{{ asset('uploads/' . app_setting('logo')) }}" alt="{{ $title }}" />
            @else
                {{ $title }}
            @endif
        </a>

        <?php // if not 4 / Shipping Staff ?>
        @if ($currentUser->role_id != 4 OR $currentUser->is_requester)
            <div id="quick-add" class="dropdown">
                <button id="quick-add-trigger" class="dropdown-toggle" type="button" id="quick-add-menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <span>
                        <span></span>
                        <span></span>
                    </span>            
                </button>
                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="quick-add-menu">

                    @if ($currentUser->is_requester)

                        <li><a href="{{ route('requests.create') }}">Create Request</a></li>

                    @else

                        @if ($currentUser->role->name == 'Purchasing Staff')

                            <li><a href="{{ route('products.add') }}">Add Product</a></li>
                            <li><a href="{{ route('categories.add') }}">Add Category</a></li>

                            <li role="separator" class="divider"></li>

                            <li><a href="{{ route('vendors.add') }}">Add Vendor</a></li>
                            <li><a href="{{ route('pos.create') }}">Create Purchase Order</a></li>

                        @else

                            @if ($currentUser->allowedTo('add_products'))
                                <li><a href="{{ route('products.add') }}">Add Product</a></li>
                            @endif

                            @if ($currentUser->allowedTo('add_opening_balances'))
                                <li><a href="{{ route('products.ob.add') }}">Add Opening Balance</a></li>
                            @endif

                            @if ($currentUser->allowedTo('add_product_categories'))
                                <li><a href="{{ route('categories.add') }}">Add Category</a></li>
                            @endif

                            @if ($currentUser->allowedTo('add_transferred_products'))
                                <li><a href="{{ route('products.transfer') }}">Transfer Product</a></li>
                            @endif

                            <li role="separator" class="divider"></li>

                            @if ($currentUser->allowedTo('add_requesters'))
                                <li><a href="{{ route('requesters.add') }}">Add Requester</a></li>
                            @endif

                            @if ($currentUser->allowedTo('add_requesters'))
                                <li><a href="{{ route('requesters.invite') }}">Invite Requester</a></li>
                            @endif

                            @if ($currentUser->allowedTo('add_requests'))
                                <li><a href="{{ route('requests.create') }}">Create Request</a></li>
                            @endif

                            <li role="separator" class="divider"></li>

                            @if ($currentUser->allowedTo('add_vendors'))
                                <li><a href="{{ route('vendors.add') }}">Add Vendor</a></li>
                            @endif

                            @if ($currentUser->allowedTo('add_purchase_orders'))
                                <li><a href="{{ route('pos.create') }}">Create Purchase Order</a></li>
                            @endif                

                            <li role="separator" class="divider"></li>

                            @if ($currentUser->allowedTo('add_locations'))
                                <li><a href="{{ route('locations.add') }}">Add Location</a></li>
                            @endif

                            @if ($currentUser->allowedTo('add_markets'))
                                <li><a href="{{ route('markets.add') }}">Add Market</a></li>
                            @endif

                            <li role="separator" class="divider"></li>

                            @if ($currentUser->allowedTo('add_users'))
                                <li><a href="{{ route('users.add') }}">Add User</a></li>
                            @endif

                            @if ($currentUser->allowedTo('add_roles'))
                                <li><a href="{{ route('roles.add') }}">Add Role</a></li>
                            @endif

                        @endif

                    @endif

                </ul>
            </div>
        @endif

    </div>

</div>
</div>
</div>
</header>

@include('layouts.fragments.nav')

<nav class="navbar navbar-default navbar-static-top" style="display: none;">
    <div class="container-fluid">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}">
                Inventory App
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                @include('layouts.fragments.nav')
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">Login</a></li>
                    <li><a href="{{ url('/register') }}">Register</a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            Hi, {{ Auth::user()->first_name }}! <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="{{ url('/logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
@endif