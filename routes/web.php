<?php

/*
|--------------------------------------------------------------------------
| Unsorted Routes
|--------------------------------------------------------------------------
*/
Route::get('/', function () {
    if (Auth::check()) {
        return redirect(url('/dashboard'));
    }
    return redirect(url('/login'));
});

Route::get('/dashboard', 'HomeController@index');
Route::get('/shipping', 'HomeController@shippingTest');

/*
|--------------------------------------------------------------------------
| Auth Routes
|--------------------------------------------------------------------------
*/
Auth::routes();
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

/*
|--------------------------------------------------------------------------
| Setup Routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'setup'], function () {
    Route::get('/app-settings', 'SetupController@appSettings')->name('setup.app.settings');
    Route::post('/app-settings', 'SetupController@saveAppSettings')->name('setup.app.settings');
});

/*
|--------------------------------------------------------------------------
| Profile Routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'profile'], function () {

    Route::get('/{userId?}', 'UserController@profile')->name('users.profile');
    Route::get('/edit/{userId?}', 'UserController@editProfile')->name('users.profile.edit');
    Route::post('/edit/{userId?}', 'UserController@editProfileSave')->name('users.profile.edit');
    Route::get('/details/{userId?}', 'UserController@getUserDetails')->name('users.details');
    Route::get('/delete/{userId?}', 'UserController@deleteUser')->name('users.delete');
    Route::post('/change-theme', 'UserController@changeTheme')->name('users.changetheme');

});

/*
|--------------------------------------------------------------------------
| Users Routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'users'], function () {

    Route::get('/', 'UserController@getUsers')->name('users.list');
    Route::get('/json', 'UserController@usersJson')->name('users.json');
    Route::get('/add', 'UserController@addUser')->name('users.add');
    Route::post('/add', 'UserController@addUserSave')->name('users.add');
    Route::get('/edit/{userId?}', 'UserController@editUser')->name('users.edit');
    Route::post('/edit/{userId?}', 'UserController@editUserSave')->name('users.edit');
    Route::get('/delete/{userId?}', 'UserController@deleteUser')->name('users.delete');

});

/*
|--------------------------------------------------------------------------
| Roles Routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'roles'], function () {

    Route::get('/', 'RoleController@getRoles')->name('roles.list');
    Route::get('/add', 'RoleController@addRole')->name('roles.add');
    Route::post('/add', 'RoleController@addRoleSave')->name('roles.add');
    Route::get('/edit/{roleId?}', 'RoleController@editRole')->name('roles.edit');
    Route::post('/edit/{roleId?}', 'RoleController@editRoleSave')->name('roles.edit');
    Route::get('/delete/{roleId?}', 'RoleController@deleteRole')->name('roles.delete');

});

/*
|--------------------------------------------------------------------------
| Products Routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'products'], function () {

    Route::get('/list/{filter?}/{category?}', 'ProductController@productsList')->name('products.list');
    Route::get('/', 'ProductController@getProducts')->name('products.list.old');
    Route::get('/preview/{productId?}', 'ProductController@previewProduct')->name('products.preview');
    Route::get('/details/{productId?}', 'ProductController@viewProduct')->name('products.details');
    Route::get('/add', 'ProductController@addProduct')->name('products.add');
    Route::post('/add', 'ProductController@addProductSave')->name('products.add');
    Route::get('/edit/{productId?}', 'ProductController@editProduct')->name('products.edit');
    Route::post('/edit/{productId?}', 'ProductController@editProductSave')->name('products.edit');
    Route::get('/delete/{productId?}', 'ProductController@deleteProduct')->name('products.delete');
    Route::get('/remove-photo/{productId?}', 'ProductController@removeProductPhoto')->name('products.remove-photo');
    Route::get('/opening-balances', 'ProductController@viewOpeningBalances')->name('products.ob.list');
    Route::get('/opening-balances/add', 'ProductController@addOpeningBalance')->name('products.ob.add');
    Route::post('/opening-balances/add', 'ProductController@addOpeningBalanceSave')->name('products.ob.add');
    Route::get('/opening-balances/edit/{obId?}', 'ProductController@editOpeningBalance')->name('products.ob.edit');
    Route::post('/opening-balances/edit/{obId?}', 'ProductController@editOpeningBalanceSave')->name('products.ob.edit');
    Route::get('/opening-balances/delete/{obId?}', 'ProductController@deleteOpeningBalance')->name('products.ob.delete');
    Route::get('/ob-new-field', 'ProductController@openingBalanceNewField')->name('products.ob.newfield');
    Route::get('/add/modal', 'ProductController@addProductModal')->name('products.add.modal');
    Route::post('/add/modal', 'ProductController@addProductModalSave')->name('products.add.modal');

});

/*
|--------------------------------------------------------------------------
| Transferred Products Routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'transferred-products'], function () {

    Route::get('/', 'ProductController@transferredProduct')->name('products.transferred');
    Route::get('/transfer', 'ProductController@transferProduct')->name('products.transfer');
    Route::post('/transfer', 'ProductController@transferProductSave')->name('products.transfer');
    Route::get('/revert/{transferId?}', 'ProductController@revertTransfer')->name('products.reverttransfer');

});

/*
|--------------------------------------------------------------------------
| Categories Routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'categories'], function () {

    Route::get('/', 'ProductController@getCategories')->name('categories.list');
    Route::get('/add', 'ProductController@addCategory')->name('categories.add');
    Route::post('/add', 'ProductController@addCategorySave')->name('categories.add');
    Route::get('/edit/{categoryId?}', 'ProductController@editCategory')->name('categories.edit');
    Route::post('/edit/{categoryId?}', 'ProductController@editCategorySave')->name('categories.edit');
    Route::get('/delete/{categoryId?}', 'ProductController@deleteCategory')->name('categories.delete');

});

/*
|--------------------------------------------------------------------------
| Locations Routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'locations'], function () {

    Route::get('/', 'LocationController@getLocations')->name('locations.list');
    Route::get('/products/{locationId?}', 'ProductController@fetchProductsList')->name('locations.products');
    Route::get('/details/{locationId?}', 'LocationController@viewLocation')->name('locations.details');
    Route::get('/add', 'LocationController@addLocation')->name('locations.add');
    Route::post('/add', 'LocationController@addLocationSave')->name('locations.add');
    Route::get('/edit/{locationId?}', 'LocationController@editLocation')->name('locations.edit');
    Route::post('/edit/{locationId?}', 'LocationController@editLocationSave')->name('locations.edit');
    Route::get('/delete/{locationId?}', 'LocationController@deleteLocation')->name('locations.delete');
    Route::get('/staff/add/{locationId?}', 'LocationController@addStaff')->name('locations.staff.add');
    Route::post('/staff/add/', 'LocationController@addStaffSave')->name('locations.staff.add');
    Route::get('/staff/remove/{staffId?}', 'LocationController@deleteStaff')->name('locations.staff.delete');

});

/*
|--------------------------------------------------------------------------
| Vendors Routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'vendors'], function () {

    Route::get('/', 'VendorController@getVendors')->name('vendors.list');
    Route::get('/details/{vendorId?}', 'VendorController@viewVendor')->name('vendors.details');
    Route::get('/add', 'VendorController@addVendor')->name('vendors.add');
    Route::post('/add', 'VendorController@addVendorSave')->name('vendors.add');
    Route::get('/edit/{vendorId?}', 'VendorController@editVendor')->name('vendors.edit');
    Route::post('/edit/{vendorId?}', 'VendorController@editVendorSave')->name('vendors.edit');
    Route::get('/delete/{vendorId?}', 'VendorController@deleteVendor')->name('vendors.delete');
    Route::get('/add/modal', 'VendorController@addVendorModal')->name('vendors.add.modal');
    Route::post('/add/modal', 'VendorController@addVendorModalSave')->name('vendors.add.modal');

});


/*
|--------------------------------------------------------------------------
| Purchase Orders Routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'purchase-orders'], function () {

    Route::get('/', 'PurchaseOrderController@getPurchaseOrders')->name('pos.list');
    Route::get('/preview/{purchaseOrderId?}', 'PurchaseOrderController@previewPurchaseOrder')->name('pos.preview');
    Route::get('/create', 'PurchaseOrderController@addPurchaseOrder')->name('pos.create');
    Route::post('/create', 'PurchaseOrderController@addPurchaseOrderSave')->name('pos.create');
    Route::get('/edit/{purchaseOrderId?}', 'PurchaseOrderController@editPurchaseOrder')->name('pos.edit');
    Route::post('/edit/{purchaseOrderId?}', 'PurchaseOrderController@editPurchaseOrderSave')->name('pos.edit');
    Route::get('/delete/{purchaseOrderId?}', 'PurchaseOrderController@deletePurchaseOrder')->name('pos.delete');
    Route::get('/receive/{purchaseOrderId?}', 'PurchaseOrderController@receivePurchaseOrder')->name('pos.receive');
    Route::post('/receive/{purchaseOrderId?}', 'PurchaseOrderController@receivePurchaseOrderSave')->name('pos.receive');
    Route::get('/pdf/{purchaseOrderId?}/{version?}', 'PurchaseOrderController@pdfView')->name('pos.pdf');
    Route::get('/email-to-vendor/{purchaseOrderId?}', 'PurchaseOrderController@emailToVendor')->name('pos.emailtovendor');
    Route::post('/email-to-vendor/{purchaseOrderId?}', 'PurchaseOrderController@emailToVendorSend')->name('pos.emailtovendor');
    Route::get('/create/product/{productId?}', 'PurchaseOrderController@createForSpecificProduct')->name('pos.create.specific');
    Route::post('/create/product/{productId?}', 'PurchaseOrderController@createForSpecificProductSave')->name('pos.create.specific');
    Route::post('/addfile/{purchaseOrderId?}', 'PurchaseOrderController@addFile')->name('pos.addfile');
    Route::get('/delete-file/{fileId?}', 'PurchaseOrderController@deleteFile')->name('pos.deletefile');

});

/*
|--------------------------------------------------------------------------
| Requesters Routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'requesters'], function () {

    Route::get('/', 'RequesterController@getRequesters')->name('requesters.list');
    Route::get('/preview/{requesterId?}', 'RequesterController@previewRequester')->name('requesters.preview');
    Route::get('/addresses-market/{requesterId?}', 'RequesterController@getAddressesAndMarket')->name('requesters.am');
    Route::get('/add', 'RequesterController@addRequester')->name('requesters.add');
    Route::post('/add', 'RequesterController@addRequesterSave')->name('requesters.add');
    Route::get('/edit/{requesterId?}', 'RequesterController@editRequester')->name('requesters.edit');
    Route::post('/edit/{requesterId?}', 'RequesterController@editRequesterSave')->name('requesters.edit');
    Route::get('/delete/{requesterId?}', 'RequesterController@deleteRequester')->name('requesters.delete');
    Route::get('/invite', 'RequesterController@inviteRequester')->name('requesters.invite');
    Route::post('/invite', 'RequesterController@inviteRequesterSave')->name('requesters.invite');
    Route::get('/signup', 'RequesterSignUpController@signupRequester')->name('requesters.signup');
    Route::post('/signup', 'RequesterSignUpController@signupRequesterSave')->name('requesters.signup');
    Route::get('/address/add/{requesterId?}', 'RequesterController@addAddress')->name('requesters.address.add');
    Route::post('/address/add', 'RequesterController@addAddressSave')->name('requesters.address.add');
    Route::get('/address/edit/{requesterId?}', 'RequesterController@editAddress')->name('requesters.address.edit');
    Route::post('/address/edit', 'RequesterController@editAddressSave')->name('requesters.address.edit');
    Route::get('/address/remove/{addressId?}', 'RequesterController@removeAddress')->name('requesters.address.remove');
    Route::post('/address/setdefault', 'RequesterController@setDefaultAddress')->name('requesters.address.setdefault');
    Route::get('/{requesterId}/product-allocation', 'RequesterController@productAllocation')->name('requesters.productallocation');
    Route::post('/{requesterId}/product-allocation', 'RequesterController@productAllocationSave')->name('requesters.productallocation');
    Route::get('/add-address-fields', 'RequesterSignUpController@addAddressFields')->name('requesters.addaddressfields');
    Route::get('/invites-sent', 'RequesterController@sentInvites')->name('requesters.invites');

});

/*
|--------------------------------------------------------------------------
| Requests Routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'requests'], function () {

    Route::get('/', 'RequestController@getRequests')->name('requests.list');
    Route::get('/details/{requestId?}', 'RequestController@viewRequest')->name('requests.details');
    Route::get('/create/{requesterId?}', 'RequestController@createRequest')->name('requests.create');
    Route::post('/create/{requesterId?}', 'RequestController@createRequestSave')->name('requests.create');

    Route::get('/select-requester', 'RequestController@selectRequester')->name('requests.requester.select');
    Route::get('/edit/{requestId}/{requesterId}', 'RequestController@editRequest')->name('requests.edit');
    Route::post('/edit/{requestId}/{requesterId}', 'RequestController@editRequestSave')->name('requests.edit');
    Route::get('/delete/{requestId?}', 'RequestController@deleteRequest')->name('requests.delete');

    Route::post('/process/{requestId?}', 'RequestController@processRequest')->name('requests.process');
    Route::post('/disapprove/{requestId?}', 'RequestController@disapproveRequest')->name('requests.disapprove');
    // Route::get('/quick-approve/{requestId?}', 'RequestController@quickApprove')->name('requests.quickapprove');

    // pdf view
    Route::get('/pdf/{requestId?}', 'RequestController@pdfView')->name('requests.pdf');
    Route::get('/add-custom-address', 'RequestController@addCustomAddress')->name('requesters.aca');
    Route::get('/ship/{requestId?}', 'RequestController@shipRequest')->name('requesters.ship');
    Route::post('/shipping-rates', 'RequestController@shippingRates')->name('requesters.shipping-rates');
    Route::post('/create-label/{objectId}/{requestId?}', 'RequestController@createLabel')->name('shipping.create-label');
    Route::get('/shipping-manual/{requestId?}', 'RequestController@shipRequestManual')->name('shipping.manual');
    Route::post('/shipping-manual/{requestId?}', 'RequestController@shipRequestManualSave')->name('shipping.manual');

});

/*
|--------------------------------------------------------------------------
| Walk-in Requests Routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'walkin-requests'], function () {

    Route::get('/', 'RequestController@allWrqs')->name('wrq.all');
    Route::get('/process/{wrqId}', 'RequestController@processWrq')->name('wrq.process');
    Route::post('/process/{wrqId}', 'RequestController@processWrqSave')->name('wrq.process');
    Route::get('/switch-loc', 'RequestController@switchLocation')->name('request.walkin.switch');
    Route::get('/c/{locationId}', 'RequestController@walkInRequest')->name('request.walkin');
    Route::post('/c/{locationId}', 'RequestController@walkInRequestSave')->name('request.walkin');
    Route::get('/{locationId}/thanks', 'RequestController@walkInRequestThanks')->name('request.walkin.thanks');

});

/*
|--------------------------------------------------------------------------
| Markets Routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'markets'], function () {

    Route::get('/', 'MarketController@getMarkets')->name('markets.list');
    Route::get('/details/{marketId?}', 'MarketController@getMarketDetails')->name('markets.details');
    Route::get('/add', 'MarketController@addMarket')->name('markets.add');
    Route::post('/add', 'MarketController@addMarketSave')->name('markets.add');
    Route::get('/edit/{marketId?}', 'MarketController@editMarket')->name('markets.edit');
    Route::post('/edit/{marketId?}', 'MarketController@editMarketSave')->name('markets.edit');
    Route::get('/delete/{marketId?}', 'MarketController@deleteMarket')->name('markets.delete');

});


/*
|--------------------------------------------------------------------------
| Settings Routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'settings'], function () {

    Route::get('/', 'SettingController@settingsHome')->name('settings.home');
    Route::post('/save', 'SettingController@settingsSave')->name('settings.save');
    Route::get('/remove-logo', 'SettingController@removeLogo')->name('settings.removelogo');
    Route::get('/env', 'SettingController@envValues')->name('settings.env');
    Route::post('/env', 'SettingController@envValuesSave')->name('settings.env');

});

/*
|--------------------------------------------------------------------------
| Custom Fields Routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'custom-fields'], function () {

    Route::get('/', 'CustomFieldController@customFields')->name('cf.home');
    Route::get('/add', 'CustomFieldController@addCustomField')->name('cf.add');
    Route::post('/add', 'CustomFieldController@addCustomFieldSave')->name('cf.add');
    Route::get('/edit/{customFieldId?}', 'CustomFieldController@editCustomField')->name('cf.edit');
    Route::post('/edit/{customFieldId?}', 'CustomFieldController@editCustomFieldSave')->name('cf.edit');
    Route::get('/delete/{customFieldId?}', 'CustomFieldController@deleteCustomField')->name('cf.delete');

});


/*
|--------------------------------------------------------------------------
| Recipients Routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'recipients'], function () {

    Route::get('/', 'RecipientController@getRecipients')->name('recipients.list');
    Route::get('/add', 'RecipientController@addRecipient')->name('recipients.add');
    Route::post('/add', 'RecipientController@addRecipientSave')->name('recipients.add');
    Route::get('/edit/{recipientId?}', 'RecipientController@editRecipient')->name('recipients.edit');
    Route::post('/edit/{recipientId?}', 'RecipientController@editRecipientSave')->name('recipients.edit');
    Route::get('/remove/{recipientId?}', 'RecipientController@removeRecipient')->name('recipients.remove');

});