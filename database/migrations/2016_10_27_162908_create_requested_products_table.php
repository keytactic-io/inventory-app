<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestedProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requested_products', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('request_id')->unsigned();
            $table->foreign('request_id')
                ->references('id')->on('requests');

            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')
                ->references('id')->on('products');

            $table->integer('quantity')->unsigned();
            $table->integer('allowed_quantity')->unsigned()->nullable();

            $table->double('price', 10, 4);

            $table->integer('get_from')->unsigned()->nullable();            
            $table->foreign('get_from')
                ->references('id')->on('locations');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requested_products');
    }
}
