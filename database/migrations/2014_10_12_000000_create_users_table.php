<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('description', 150)->nullable();
            $table->timestamps();
        });

        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();

            $table->integer('role_id')->unsigned();
            // $table->foreign('role_id')
            //     ->references('id')->on('roles');

            $table->string('display_photo')->default('no-dp.png');
            $table->smallInteger('is_active')->default(1);
            $table->smallInteger('is_requester')->default(0);
            $table->string('email')->unique();
            $table->string('password');
            $table->string('theme')->default('ice');
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {        
        Schema::dropIfExists('roles');
        Schema::dropIfExists('users');
    }
}
