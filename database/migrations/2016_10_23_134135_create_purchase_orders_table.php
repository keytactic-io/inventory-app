<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_orders', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('vendor_id')->unsigned();
            $table->foreign('vendor_id')
                ->references('id')->on('vendors');

            $table->integer('processed_by')->unsigned();
            $table->foreign('processed_by')
                ->references('id')->on('users');
                
            $table->smallInteger('received')->nullable();
            $table->dateTime('received_at')->nullable();

            $table->integer('received_by')->unsigned()->nullable();
            $table->foreign('received_by')
                ->references('id')->on('users');

            $table->integer('ship_to_location');
            // $table->foreign('ship_to_location')
            //     ->references('id')->on('locations');

            $table->date('delivery_date')->nullable();
            $table->string('shipping_method')->nullable();
            $table->string('shipping_terms')->nullable();
            $table->text('notes')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_orders');
    }
}
