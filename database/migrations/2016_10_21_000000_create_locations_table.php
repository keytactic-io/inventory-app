<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);            
            $table->integer('staff_incharge')->unsigned();
            $table->string('contact_number', 100)->nullable();
            $table->string('street_1', 150);
            $table->string('street_2', 150)->nullable();
            $table->string('city', 100);
            $table->string('state', 100);
            $table->string('country', 100)->default('US');
            $table->string('postcode', 20);
            
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
