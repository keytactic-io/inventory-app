<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductRemainingStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('archive_product_remaining_stocks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->integer('opening_balances')->default(0);
            $table->integer('orders')->default(0);
            $table->integer('requests')->default(0);
            $table->integer('walkin_requests')->default(0);
            $table->integer('status')
                ->unsigned('1:in-stock 2:low-stock 3:out-of-stock');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('archive_product_remaining_stocks');
    }
}
