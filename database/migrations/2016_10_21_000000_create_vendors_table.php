<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 150);
            $table->string('street_1', 100);
            $table->string('street_2', 100)->nullable();
            $table->string('city', 50);
            $table->string('state', 30);
            $table->string('country', 100);
            $table->string('postcode', 20);
            $table->string('email', 50)->nullable();
            $table->string('mobile_number', 50)->nullable();
            $table->string('landline_number', 50)->nullable();
            $table->string('fax_number', 50)->nullable();
            $table->string('contact_person', 100)->nullable();
            $table->string('photo')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors');
    }
}
