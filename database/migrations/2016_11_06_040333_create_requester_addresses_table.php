<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequesterAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requester_addresses', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('requester_id')->unsigned();
            $table->foreign('requester_id')
                ->references('id')->on('requesters');

            $table->string('street_1', 150);
            $table->string('street_2', 150)->nullable();
            $table->string('city', 100);
            $table->string('state', 100);
            $table->string('country', 100)->default('US');
            $table->string('postcode', 20);
            
            $table->smallInteger('default')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requester_addresses');
    }
}
