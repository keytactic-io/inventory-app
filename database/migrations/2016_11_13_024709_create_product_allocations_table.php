<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductAllocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_allocations', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('requester_id')->unsigned();
            $table->foreign('requester_id')
                ->references('id')->on('requesters');

            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')
                ->references('id')->on('products');

            $table->integer('allocation');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_allocations');
    }
}
