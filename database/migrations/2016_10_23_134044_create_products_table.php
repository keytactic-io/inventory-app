<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('photo', 50)->default('no-photo.png');
            $table->string('name', 50);
            $table->string('description', 200)->nullable();
            $table->integer('reorder_point')->unsigned();
            $table->integer('reorder_quantity')->unsigned()->default(0);

            $table->integer('default_vendor')->unsigned();
            $table->foreign('default_vendor')
                ->references('id')->on('vendors');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
