<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderedProductsLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordered_products_locations', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('ordered_product_id')->unsigned();
            $table->foreign('ordered_product_id')
                ->references('id')->on('ordered_products');

            $table->integer('received_quantity')->nullable();

            $table->integer('location_id')->unsigned()->nullable();
            $table->foreign('location_id')
                ->references('id')->on('locations');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ordered_products_locations');
    }
}
