<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('requester')->unsigned();
            $table->foreign('requester')
                ->references('id')->on('users');

            $table->integer('market_id')->unsigned();
            $table->foreign('market_id')
                ->references('id')->on('markets');

            $table->date('expected_inhand_date')->nullable();
            $table->integer('status');

            $table->string('attention', 150);
            $table->string('contact_number', 150)->nullable();
            $table->string('company', 200);
            $table->string('street_1', 150);
            $table->string('street_2', 150)->nullable();
            $table->string('city', 100);
            $table->string('state', 100);
            $table->string('country', 100)->default('US');
            $table->string('postcode', 20);

            $table->text('justification')->nullable();
            $table->text('note')->nullable();

            $table->integer('processed_by')->unsigned()->nullable();
            $table->foreign('processed_by')
                ->references('id')->on('users');

            $table->dateTime('processed_at')->nullable();

            $table->integer('parent_id')->unsigned()->nullable();
            $table->foreign('parent_id')
                ->references('id')->on('requests');

            $table->smallInteger('manual_input')->nullable();
            $table->string('courier')->nullable();
            $table->double('rate', 10, 4)->nullable();
            $table->string('tracking_number')->nullable();
            $table->text('label_url')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
