<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $categories = [
            [
                'name' => 'Uncategorized',
                'parent_category' => 0
            ],
            [
                'name' => 'Parent Category',
                'parent_category' => 0
            ],
            [
                'name' => 'Child Category',
                'parent_category' => 2
            ],
        ];

        foreach ($categories as $category) {
            Category::create($category);
        }

    }
}
