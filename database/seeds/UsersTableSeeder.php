<?php

use App\Models\User;
use App\Models\Market;
use App\Models\Requester;
use Illuminate\Database\Seeder;
use App\Models\RequesterAddress;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker\Factory::create();

        $users = [
            [
                'first_name' => 'Mo',
                'last_name' => 'Hassan',
                'role_id' => 1,
                'email' => 'mo@i.app',
                'password' => bcrypt('testing')
            ],
            [
                'first_name' => 'Generic',
                'last_name' => 'Friend',
                'role_id' => 2,
                'email' => 'eric@i.app',
                'password' => bcrypt('testing')
            ],
            [
                'first_name' => 'John',
                'last_name' => 'Purchasor',
                'role_id' => 3,
                'email' => 'johnp@i.app',
                'password' => bcrypt('testing')
            ],
            [
                'first_name' => 'Julia',
                'last_name' => 'Shifoo',
                'role_id' => 4,
                'email' => 'juliashipme@i.app',
                'password' => bcrypt('testing')
            ]
        ];

        foreach ($users as $user) {
            User::create($user);
        }

        $marketsCount = Market::count();

        for ($i=1; $i <= 8; $i++) {

            $requesterUser = User::create([
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'role_id' => 0,
                'email' => 'rq' . $i . '@i.app',
                'password' => bcrypt('testing'),
                'is_requester' => 1
            ]);

            $requester = Requester::create([
                'user_id' => $requesterUser->id,
                'market_id' => rand(1, $marketsCount),
                'contact_number' => $faker->tollFreePhoneNumber
            ]);

            $address = [
                'requester_id' => $requester->id,
                'street_1' => $faker->streetAddress,
                'city' => $faker->city,
                'state' => $faker->state,
                'country' => $faker->country,
                'postcode' => $faker->postcode,
                'default' => 1
            ];

            RequesterAddress::create($address);
        }

    }
}
