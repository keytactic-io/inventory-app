<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    
        $roles = [
            ['name' => 'Super Admin'],
            ['name' => 'Admin'],
            ['name' => 'Purchasing Staff'],
            ['name' => 'Shipping Staff']
        ];

        foreach ($roles as $role) {
            if (!Role::where('name', $role)->first()) {
                Role::create($role);
            }
        }

    }
}
