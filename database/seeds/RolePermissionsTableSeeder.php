<?php

use App\Models\Permission;
use App\Models\RolePermission;
use Illuminate\Database\Seeder;
use App\Models\PermissionCategory;

class RolePermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        if (Permission::count() > 0) {
            return;
        }

        $permissionCategories = [
            [
                'key' => 'users',
                'name' => 'Users'
            ],
            [
                'key' => 'products',
                'name' => 'Products'
            ],
            [
                'key' => 'transferred_products',
                'name' => 'Transferred Products'
            ],
            [
                'key' => 'requesters',
                'name' => 'Requesters'
            ],
            [
                'key' => 'vendors',
                'name' => 'Vendors'
            ],
            [
                'key' => 'purchase_orders',
                'name' => 'Purchase Orders'
            ],
            [
                'key' => 'product_categories',
                'name' => 'Product Categories'
            ],
            [
                'key' => 'roles',
                'name' => 'Roles'
            ],
            [
                'key' => 'settings',
                'name' => 'Settings'
            ],
            [
                'key' => 'opening_balances',
                'name' => 'Opening Balances'
            ],
            [
                'key' => 'requests',
                'name' => 'Requests'
            ],
            [
                'key' => 'locations',
                'name' => 'Locations'
            ],
            [
                'key' => 'markets',
                'name' => 'Markets'
            ],
            [
                'key' => 'custom_fields',
                'name' => 'Custom Fields'
            ],
            [
                'key' => 'recipients',
                'name' => 'Mail Recipients'
            ]
        ];

        foreach ($permissionCategories as $category) {
            $permissionCategory = PermissionCategory::create($category);

            $crudCan = ['view', 'add', 'edit', 'delete'];
            if ($permissionCategory->key == 'purchase_orders') {
                $specialCan = ['receive'];
                $crudCan = array_merge($crudCan, $specialCan);
            }
            if ($permissionCategory->key == 'requests') {
                $specialCan = ['process', 'ship'];
                $crudCan = array_merge($crudCan, $specialCan);
            }

            foreach ($crudCan as $can) {

                $permission = Permission::create([
                    'key' => $can . '_' . $permissionCategory->key,
                    'name' => ucfirst($can) . ' ' . $permissionCategory->name,
                    'permission_category_id' => $permissionCategory->id
                ]);

                // user admin
                RolePermission::create([
                    'role_id' => 1,
                    'permission_id' => $permission->id
                ]);

                // admin
                if ($can != 'delete') {
                    RolePermission::create([
                        'role_id' => 2,
                        'permission_id' => $permission->id
                    ]);
                }

                // purchasing staff
                $purchasingPermissions = [
                    'view_products', 'add_products', 'edit_products', 'view_requesters', 
                    'view_vendors', 'add_vendors', 'edit_vendors', 'view_purchase_orders', 'add_purchase_orders',
                    'edit_purchase_orders', 'delete_purchase_orders', 'receive_purchase_orders',
                    'view_product_categories', 'add_product_categories', 'edit_product_categories', 
                    'view_opening_balances', 'view_locations', 'view_markets'
                ];
                if (in_array($permission->key, $purchasingPermissions)) {
                    RolePermission::create([
                        'role_id' => 3,
                        'permission_id' => $permission->id
                    ]);
                }

                // shipping staff
                $shippingStaffPermissions = [
                    'view_products', 'view_requesters', 'view_product_categories',
                    'view_requests', 'ship_requests', 'view_locations', 'view_markets'
                ];
                if (in_array($permission->key, $shippingStaffPermissions)) {
                    RolePermission::create([
                        'role_id' => 4,
                        'permission_id' => $permission->id
                    ]);
                }

            }

        }

    }
}
