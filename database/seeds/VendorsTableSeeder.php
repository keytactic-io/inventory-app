<?php

use App\Models\Vendor;
use Illuminate\Database\Seeder;

class VendorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $faker = Faker\Factory::create();

        for ($i=0; $i < 12; $i++) { 
            $vendorData = [
                'name' => $faker->company,
                'street_1' => $faker->streetAddress,
                'city' => $faker->city,
                'state' => $faker->state,
                'country' => $faker->country,
                'postcode' => $faker->postcode,
                'contact_person' => $faker->name,
                'mobile_number' => $faker->tollFreePhoneNumber,
                'email' => $faker->companyEmail
            ];
            Vendor::create($vendorData);
        }

    }
}
