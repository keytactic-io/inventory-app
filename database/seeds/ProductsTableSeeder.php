<?php

use App\Models\Vendor;
use App\Models\Product;
use App\Models\Location;
use App\Models\Requester;
use App\Models\OpeningBalance;
use App\Models\ProductCategory;
use App\Models\ProductPriceLog;
use Illuminate\Database\Seeder;
use App\Models\ProductAllocation;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker\Factory::create();

        $reorderPoint = [300, 600, 800, 1000, 1500];
        $vendorCount = Vendor::count();
        $products = [
            ['name' => 'Konhotlab', 'reorder_point' => $reorderPoint[rand(0, 4)], 'default_vendor' => rand(1, $vendorCount), 
                'reorder_quantity' => $reorderPoint[rand(0, 4)], 'categories' => [1]],
            ['name' => 'Dento In', 'reorder_point' => $reorderPoint[rand(0, 4)], 'default_vendor' => rand(1, $vendorCount), 
                'reorder_quantity' => $reorderPoint[rand(0, 4)], 'categories' => [1]],
            ['name' => 'Dento Apfresh', 'reorder_point' => $reorderPoint[rand(0, 4)], 'default_vendor' => rand(1, $vendorCount), 
                'reorder_quantity' => $reorderPoint[rand(0, 4)], 'categories' => [1]],
            ['name' => 'La Rondax', 'reorder_point' => $reorderPoint[rand(0, 4)], 'default_vendor' => rand(1, $vendorCount), 
                'reorder_quantity' => $reorderPoint[rand(0, 4)], 'categories' => [2, 3]],
            ['name' => 'Singleron', 'reorder_point' => $reorderPoint[rand(0, 4)], 'default_vendor' => rand(1, $vendorCount), 
                'reorder_quantity' => $reorderPoint[rand(0, 4)], 'categories' => [1]],
            ['name' => 'Ice-Air', 'reorder_point' => $reorderPoint[rand(0, 4)], 'default_vendor' => rand(1, $vendorCount), 
                'reorder_quantity' => $reorderPoint[rand(0, 4)], 'categories' => [3]],
            ['name' => 'Nim-Air', 'reorder_point' => $reorderPoint[rand(0, 4)], 'default_vendor' => rand(1, $vendorCount), 
                'reorder_quantity' => $reorderPoint[rand(0, 4)], 'categories' => [1]],
            ['name' => 'Hoteco', 'reorder_point' => $reorderPoint[rand(0, 4)], 'default_vendor' => rand(1, $vendorCount), 
                'reorder_quantity' => $reorderPoint[rand(0, 4)], 'categories' => [2, 3]],
            ['name' => 'Beta Ronlam', 'reorder_point' => $reorderPoint[rand(0, 4)], 'default_vendor' => rand(1, $vendorCount), 
                'reorder_quantity' => $reorderPoint[rand(0, 4)], 'categories' => [1]],
            ['name' => 'Goodtip', 'reorder_point' => $reorderPoint[rand(0, 4)], 'default_vendor' => rand(1, $vendorCount), 
                'reorder_quantity' => $reorderPoint[rand(0, 4)], 'categories' => [2]],
            ['name' => 'Don-Cof', 'reorder_point' => $reorderPoint[rand(0, 4)], 'default_vendor' => rand(1, $vendorCount), 
                'reorder_quantity' => $reorderPoint[rand(0, 4)], 'categories' => [1]],
            ['name' => 'Santough', 'reorder_point' => $reorderPoint[rand(0, 4)], 'default_vendor' => rand(1, $vendorCount), 
                'reorder_quantity' => $reorderPoint[rand(0, 4)], 'categories' => [3]],
            ['name' => 'Opelux', 'reorder_point' => $reorderPoint[rand(0, 4)], 'default_vendor' => rand(1, $vendorCount), 
                'reorder_quantity' => $reorderPoint[rand(0, 4)], 'categories' => [1]],
        ];

        $requesters = Requester::all();
        $locationsCount = Location::count();

        foreach ($products as $product):

            $p = Product::create([   
                'name' => $product['name'],
                'description' => $faker->text(90),
                'reorder_point' => $product['reorder_point'],
                'reorder_quantity' => $product['reorder_quantity'],
                'default_vendor' => $product['default_vendor']
            ]);

            foreach ($product['categories'] as $category) {
                ProductCategory::create([
                    'product_id' => $p->id,
                    'category_id' => $category
                ]);
            }

            foreach ($requesters as $requester) {
                ProductAllocation::create([
                    'requester_id' => $requester->id,
                    'product_id' => $p->id,
                    'allocation' => -1
                ]);
            }

            OpeningBalance::create([
                'product_id' => $p->id,
                'location_id' => rand(1, $locationsCount),
                'created_by' => 1,
                'quantity' => $p->reorder_quantity
            ]);

            ProductPriceLog::create([
                'product_id' => $p->id,
                'price' => rand(1, 9)
            ]);

        endforeach;
        
    }
}
