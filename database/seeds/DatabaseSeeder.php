<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(RolePermissionsTableSeeder::class);
        
        $this->call(MarketsTableSeeder::class);
        $this->call(VendorsTableSeeder::class);

        $this->call(UsersTableSeeder::class);
        $this->call(LocationsTableSeeder::class);
        
        $this->call(CategoriesTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        
        $this->call(SettingsTableSeeder::class);
        
    }
}
