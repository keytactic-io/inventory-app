<?php

use App\Models\Market;
use Illuminate\Database\Seeder;

class MarketsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $markets = [
            ['name' => 'Pacific Northwest'],
            ['name' => 'Northern California'],
            ['name' => 'Southern Pacific'],
            ['name' => 'Midwest'],
            ['name' => 'Southwest'],
            ['name' => 'North Atlantic'],
            ['name' => 'South'],
            ['name' => 'Florida'],
            ['name' => 'Mid-Atlantic'],
            ['name' => 'Northeast']
        ];

        foreach ($markets as $market) {
            Market::create($market);
        }
    }
}
