<?php

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = [
            [
                'key' => 'application_name',
                'name' => 'Application Name',
                'value' => 'Inventory App'
            ],
            [
                'key' => 'logo',
                'name' => 'Logo',
                'value' => ''
            ],
            [
                'key' => 'allow_update_po',
                'name' => 'Allow Update Purchase Order',
                'value' => '1'
            ],
            [
                'key' => 'show_zero_locations',
                'name' => 'Show Locations With 0 Quantity',
                'value' => '1'
            ],
            [
                'key' => 'address',
                'name' => 'Address',
                'value' => '00 Awesome Avenue, New Your, NY, US'
            ],
            [
                'key' => 'email',
                'name' => 'Email Address',
                'value' => 'email@website.com'
            ],
            [
                'key' => 'contact_number',
                'name' => 'Contact Number',
                'value' => '+00 000 0000'
            ]
        ];

        foreach ($settings as $setting) {
            Setting::create($setting);
        }
    }
}
