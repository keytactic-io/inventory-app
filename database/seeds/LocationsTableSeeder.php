<?php

use App\Models\Location;
use App\Models\LocationStaff;
use Illuminate\Database\Seeder;

class LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $faker = Faker\Factory::create();

        for ($i=1; $i < 8; $i++) { 
            $locationData = [
                'name' => 'Location ' . $i,
                'staff_incharge' => 1,
                'street_1' => $faker->streetAddress,
                'city' => $faker->city,
                'state' => $faker->state,
                'country' => 'US',
                'postcode' => $faker->postcode
            ];
            $location = Location::create($locationData);

            if ($i < 3) {
                LocationStaff::create([
                    'user_id' => 4,
                    'location_id' => $location->id
                ]);
            }            
        }

    }
}
